﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ITWebNet.UMS.PluginCore.API;

namespace StringHelperPlugin
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl, IWPFPlugin
    {
        public MainViewModel ViewModel { get { return new MainViewModel(); } }

        public bool StartUIInOneProcess
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public MainView()
        {
            InitializeComponent();
        }

        public FrameworkElement GetMainPluginUI()
        {
            return this;
        }

        public ICollection<FrameworkElement> GetPlguinUIElements()
        {
            throw new NotImplementedException();
        }
    }
}
