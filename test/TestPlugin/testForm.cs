﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using ITWebNet.UMS.PluginCore.API;
//using ums.SystemApi;

namespace TestPlugin
{

    public partial class testForm : Form, IWinFormsPlugin
    {

        public List<Data> data { get; set; }

        public testForm()
        {
            InitializeComponent();

            data = new List<Data>();
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                int val = rnd.Next();
                data.Add(new Data { Name = "Test Name " + val, ID = val, Value = "Test Val " + val });
            }
        }


        private void btnException_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(Thread.CurrentThread.ManagedThreadId.ToString());
            throw new Exception("Тестовое исключение");
        }

        private void testForm_Shown(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
        }

        private void testForm_Load(object sender, EventArgs e)
        {
            dataBindingSource.DataSource = data;
        }

        private void btnSleep_Click(object sender, EventArgs e)
        {
            Thread.Sleep(20000);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Up || e.KeyData == Keys.Down)
            {
                if (dataBindingSource.Current != null)
                {
                    label.Text = dataBindingSource.Current.ToString();
                }
            }
        }

        #region IPlugin<Control> Members

        public Control GetMainPluginUI()
        {
            return this;
        }

        public ICollection<Control> GetPlguinUIElements()
        {
            throw new NotImplementedException();
        }

        public bool StartUIInOneProcess
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }

    public class Data
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return string.Format("ID \"{0}\", Name \"{1}\", Value \"{2}\"", ID, Name, Value);
        }
    }
}
