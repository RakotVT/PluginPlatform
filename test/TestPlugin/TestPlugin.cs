﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ums.PluginCore;
using ums.SystemApi;
using System.Windows.Forms;

namespace TestPlugin
{
    /// <summary>
    /// Тестовый плагин использующий первую версию TestApi
    /// </summary>
    public class TestPlugin : IPlugin
    {
        public string PluginName
        {
            get { return "TestPlugin"; }
        }

        public string PluginVersion
        {
            get { return "v1.00"; }
        }

        public string PluginDeveloper
        {
            get { return "Рога и копыта"; }
        }

        public string PluginDescription
        {
            get { return "Тестим"; }
        }

        IApiController api = null;

        public int Initialize(IApiController apiController)
        {
            api = apiController;
            return 0;
        }

        public void Start()
        {
            // тут мы имеем доступ только к TestApi первой версии, так как плагин старый, и пользуется старой версией SystemApi.dll
            // здесь SystemApi.dll и SystemApi2.dll это одна и та же dll разных версий, просто для удобства в демонстрации их 2.
            //TestApi ta = (TestApi)api.GetApi("TestApi", 1);
            //ta.Test();
            //int k = int.Parse("q");
            MessageBox.Show("Версия 1.0");
        }
    }
}
