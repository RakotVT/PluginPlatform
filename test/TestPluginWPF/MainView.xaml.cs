﻿using System;
using System.Windows;
using System.Windows.Controls;
using ITWebNet.UMS.PluginCore.API;

namespace TestPluginWPF
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl, IWPFPlugin
    {
        public MainView()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = new View();
        }
        private void btnException_Click(object sender, RoutedEventArgs e)
        {
            throw new Exception("Test Exception");
        }

        private void btnWait_Click(object sender, RoutedEventArgs e)
        {
            long n = 100000;
            System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    (i + j).GetHashCode();
                    //Console.WriteLine((i + j).GetHashCode());

                }
            }

            watch.Stop();

            MessageBox.Show(watch.ElapsedMilliseconds.ToString());
            //Thread.Sleep(5000);
        }

        #region IPlugin<FrameworkElement> Members

        public FrameworkElement GetMainPluginUI()
        {
            return this;
        }

        public System.Collections.Generic.ICollection<FrameworkElement> GetPlguinUIElements()
        {
            throw new NotImplementedException();
        }

        public bool StartUIInOneProcess
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
