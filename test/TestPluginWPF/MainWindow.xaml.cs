﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;

namespace TestPluginWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Data> data { get; set; }

        public MainWindow()
        {
            InitializeComponent();

        }

        //#region Члены IPlugin

        public MainWindow(bool activator) { }

        //public void CloseForm()
        //{
        //	Dispatcher.Invoke(new Action(() => { Close(); }));
        //	AppDomain.Unload(AppDomain.CurrentDomain);
        //}

        //#endregion

        private void btnException_Click(object sender, RoutedEventArgs e)
        {
            throw new Exception("Test Exception"); 
        }

        private void btnWait_Click(object sender, RoutedEventArgs e)
        {
            long n = 100000;
            System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    (i + j).GetHashCode();
                    //Console.WriteLine((i + j).GetHashCode());

                }
            }

            watch.Stop();

            MessageBox.Show(watch.ElapsedMilliseconds.ToString());
            //Thread.Sleep(5000);
        }


        public static System.Drawing.Image Convert(BitmapSource bitmapImage)
        {
            System.Drawing.Image bmp;
            using (MemoryStream ms = new MemoryStream())
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                encoder.Save(ms);
                bmp = new System.Drawing.Bitmap(ms, true);
            }

            return bmp;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            MessageBox.Show("Rendered");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = new View();
        }

        #region IPlugin<FrameworkElement> Members

        public FrameworkElement GetMainPluginUI()
        {
            return this;
        }

        #endregion
    }

    public class View
    {
        public List<Data> data { get; set; }

        public View()
        {
            data = new List<Data>();
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                int val = rnd.Next();
                data.Add(new Data { Name = "Test Name " + val, ID = val, Value = "Test Val " + val });
            }
        }
    }

    public class Data
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
