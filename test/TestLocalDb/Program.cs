﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ums.Core.LocalSettings.UserStorage;

namespace TestLocalDb
{
    class Program
    {
        static void Main(string[] args)
        {
            TestMethod();
            //TestMethod();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        static void TestMethod()
        {
            var performance = Stopwatch.StartNew();
            SqlCeConnectionStringBuilder sqlCeConn = new SqlCeConnectionStringBuilder();
            sqlCeConn.DataSource = @"D:\UserSettings.sdf";

            using (UserSettingsContext context = new UserSettingsContext(sqlCeConn.ConnectionString))
            {
                Console.WriteLine("Init Time: {0}", performance.ElapsedMilliseconds);
                performance.Restart();
                Console.WriteLine("Elements count: " + context.UserSettings.Count());
                foreach (var item in context.UserSettings)
                {
                    Console.WriteLine("Key = \"{0}\", Value = \"{1}\"", item.Key, item.Value);
                }

                Console.WriteLine("Elements count: " + context.Plugins.Count());
                foreach (var item in context.Plugins)
                {
                    Console.WriteLine("Name = \"{0}\", Version = \"{1}\"", item.Name, item.Version);
                }

                Console.WriteLine("Get Data Time: {0}", performance.ElapsedMilliseconds);

                context.UserSettings.FirstOrDefault(item => item.Key == "ServerIpAddress").Value = "Localhost";
                context.SaveChanges();
            }
        }
    }
}
