﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace DateCalculationPlugin
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private DateTime? _selectedDate;

        public DateTime? SelectedDate
        {
            get { return _selectedDate; }
            set { _selectedDate = value; RaisePropertyChanged("SelectedDate"); }
        }

        private DateTime? _resultDate;

        public DateTime? ResultDate
        {
            get { return _resultDate; }
            set { _resultDate = value; RaisePropertyChanged("ResultDate"); }
        }

        public bool UseDays { get; set; }

        public bool UseMonth { get; set; }

        public int? Difference { get; set; }

        public ICommand IncreaseCommand { get; set; }
        public ICommand DecreaseCommand { get; set; }

        public MainViewModel()
        {
            SelectedDate = DateTime.Now;
            IncreaseCommand = new DelegateCommand(Increase, CanIncrease);
            DecreaseCommand = new DelegateCommand(Decrease, CanDecrease);
        }

        private bool CanDecrease(object obj)
        {
            return SelectedDate.HasValue && Difference.HasValue;
        }

        private void Decrease(object obj)
        {
            if (UseDays)
                ResultDate = SelectedDate.Value.AddDays(-Difference.Value);
            else if (UseMonth)
                ResultDate = SelectedDate.Value.AddMonths(-Difference.Value);
            else
                ResultDate = SelectedDate;
        }

        private bool CanIncrease(object obj)
        {
            return SelectedDate.HasValue && Difference.HasValue;
        }

        private void Increase(object obj)
        {
            if (UseDays)
                ResultDate = SelectedDate.Value.AddDays(Difference.Value);
            else if (UseMonth)
                ResultDate = SelectedDate.Value.AddMonths(Difference.Value);
            else
                ResultDate = SelectedDate;
        }

        public virtual void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
