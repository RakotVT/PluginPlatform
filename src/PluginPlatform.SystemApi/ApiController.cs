﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ums.PluginCore;
using System.Collections;

namespace ums.SystemApi
{
    /// <summary>
    /// Класс реализующий функционал доступа к списку API оболочки.
    /// </summary>
    public class ApiController : IApiController, IEnumerable
    {

        #region Variable

        List<IApiInterface> interfaces = new List<IApiInterface>();
        private int currentIndex;

        #endregion

        public ApiController()
        {

        }

        /// <summary>
        /// Выполняет добавление нового API в ApiController
        /// </summary>
        /// <param name="api"></param>
        public void AddApi(IApiInterface api)
        {
            interfaces.Add(api);
        }

        /// <summary>
        /// Возвращает интерфейс по имени и версии
        /// </summary>
        /// <param name="name">имя интерфейса</param>
        /// <param name="version">версия</param>
        /// <returns></returns>
        public IApiInterface GetApi(string name, int version)
        {
            var res = interfaces.FirstOrDefault(e => e.ApiName == name && e.ApiVersion == version);
            if (res == null)
                throw new Exception(string.Format("Интерфейс {0} v{1} не найден в списке системных интерфейсов", name, version));
            return res;
        }

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            currentIndex = -1;
            return interfaces.GetEnumerator();
        }

        public IApiInterface Current
        {
            get
            {
                if (currentIndex == -1 || currentIndex == interfaces.Count)
                {
                    throw new InvalidOperationException();
                }
                return interfaces[currentIndex];
            }
        }

        public bool MoveNext()
        {
            if (currentIndex != interfaces.Count)
            {
                currentIndex++;
            }
            return currentIndex < interfaces.Count;
        }

        #endregion
    }
    
}
