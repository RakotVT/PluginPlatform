﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ums.PluginCore;

namespace ums.SystemApi
{
    public class CommunicationPluginAPI: IApiInterface
    {

        #region IApiInterface

        public string ApiName
        {
            get { return "CommunicationPluginAPI"; }
        }

        public int ApiVersion
        {
            get { return 1; }
        }

        public string ApiDeveloper
        {
            get { return "ApiDeveloper"; }
        }

        public string ApiDescription
        {
            get { return "ApiDescription"; }
        }

        public IPlugin Plugin
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        /// <summary>
        /// Выполняет добавление Api в пул
        /// </summary>
        public void Register()
        {
            ApiPool.AddApi(null);
        }

        public IApiInterface GetApi()
        {
            return null;
        }
    }
}
