﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ums.PluginCore;
using ums.SystemApi;
using ums.SystemApi.ApiService;

namespace ums.SystemApi
{
    public class SettingsApi : IApiInterface
    {

        #region Fields

        string _sessionKey;
        IPlugin _plugin;

        #endregion

        public SettingsApi(string sessionKey, IPlugin plugin)
        {
            _sessionKey = sessionKey;
            _plugin = plugin;
        }

        #region IApiInterface

        public string ApiName
        {
            get { return "Settings API"; }
        }

        public int ApiVersion
        {
            get { return 1; }
        }

        public string ApiDeveloper
        {
            get { return ""; }
        }

        public string ApiDescription
        {
            get { return ""; }
        }

        #endregion

        #region Properties

        public IPlugin Plugin
        {
            get { return _plugin; }
        }

        #endregion

        #region Methods

        /// <summary>  
        /// Сохранение значения параметра плагина
        /// </summary>  
        /// <param name="setting"></param> 
        /// <param name="value"></param>
        /// <returns></returns> 
        public void SetValue(string setting, string value, bool settingForUser)
        {
            try
            {
                string pluginName = _plugin.PluginName;
                ApiServiceClient client = new ApiServiceClient();
                client.InnerChannel.OperationTimeout = TimeSpan.FromSeconds(10);
                if (settingForUser)
                    client.SetValue(setting, value, pluginName, _sessionKey);
                else
                    client.SetValue(setting, value, pluginName, "");
                client.Close();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>  
        /// Получение значения параметра плагина
        /// </summary>  
        /// <param name="setting"></param> 
        /// <param name="value"></param>
        /// <returns></returns> 
        public string GetValue(string setting, bool settingForUser)
        {
            try
            {
                string value;
                string pluginName = _plugin.PluginName;
                ApiServiceClient client = new ApiServiceClient();
                client.InnerChannel.OperationTimeout = TimeSpan.FromSeconds(10);
                if (settingForUser)
                    value = client.GetValue(setting, pluginName, _sessionKey);
                else
                    value = client.GetValue(setting, pluginName, "");

                client.Close();
                return value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion
    }
}
