﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ums.PluginCore;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraBars;
using System.Reflection;

namespace ums.SystemApi
{
    public class GuiApi: IApiInterface
    {

        #region Fields

        Form _mainForm;
        BarManager _barManager;
        IPlugin _plugin = null;

        #endregion

        public GuiApi(Form mainForm, BarManager barManager)
        {
            _mainForm = mainForm;
            _barManager = barManager;
        }

        #region IApiInterface

        public string ApiName
        {
            get { return "GUI API"; }
        }

        public int ApiVersion
        {
            get { return 1; }
        }

        public string ApiDeveloper
        {
            get { return ""; }
        }

        public string ApiDescription
        {
            get { return ""; }
        }

        #endregion

        #region Properties

        public IPlugin Plugin
        {
            get { return _plugin; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Добавляет на форму панель Bar
        /// </summary>
        /// <param name="menuStrip"></param>
        public void SetPluginPanel(Bar bar)
        {
            Bar barNew = new Bar(_barManager);
            barNew.Merge(bar);
            barNew.DockStyle = BarDockStyle.Top;
            _barManager.ForceInitialize();
        }

        #endregion

    }
}
