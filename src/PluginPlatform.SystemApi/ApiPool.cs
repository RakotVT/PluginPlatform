﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ums.PluginCore;

namespace ums.SystemApi
{
    public static class ApiPool
    {

        static List<IApiInterface> interfacePool;

        static ApiPool() { interfacePool = new List<IApiInterface>(); }

        public static List<IApiInterface> InterfacePool 
        { 
            get 
            {
                return interfacePool; 
            } 
        }

        /// <summary>
        /// Добавляет Api в пул
        /// </summary>
        /// <param name="api"></param>
        public static void AddApi(IApiInterface api)
        {
            InterfacePool.Add(api);
        }
    }
}
