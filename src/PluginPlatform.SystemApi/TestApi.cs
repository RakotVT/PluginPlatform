﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ums.PluginCore;
using System.Windows.Forms;

namespace ums.SystemApi
{
    public class TestApi : IApiInterface
    {
        #region Fields

        IPlugin _plugin = null;

        #endregion

        #region IApiInterface

        public string ApiName
        {
            get { return "TestApi"; }
        }

        public int ApiVersion
        {
            get { return 1; }
        }

        public string ApiDeveloper
        {
            get { return "Azaliya, Ларюшин Дмитрий"; }
        }

        public string ApiDescription
        {
            get { return "Первая версия тестового API"; }
        }

        #endregion

        public void Test()
        {
            MessageBox.Show("Был вызван метод Test из TestApi v1");
        }

        #region Properties

        public IPlugin Plugin
        {
            get
            {
                return _plugin;
            }
        }

        #endregion
    }
}
