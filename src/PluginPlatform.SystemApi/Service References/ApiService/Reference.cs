﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.18033
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ums.SystemApi.ApiService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ApiService.IApiService")]
    public interface IApiService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IApiService/SetValue", ReplyAction="http://tempuri.org/IApiService/SetValueResponse")]
        void SetValue(string setting, string value, string pluginName, string sessionKey);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IApiService/GetValue", ReplyAction="http://tempuri.org/IApiService/GetValueResponse")]
        string GetValue(string setting, string pluginName, string sessionKey);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IApiServiceChannel : ums.SystemApi.ApiService.IApiService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ApiServiceClient : System.ServiceModel.ClientBase<ums.SystemApi.ApiService.IApiService>, ums.SystemApi.ApiService.IApiService {
        
        public ApiServiceClient() {
        }
        
        public ApiServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ApiServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ApiServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ApiServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void SetValue(string setting, string value, string pluginName, string sessionKey) {
            base.Channel.SetValue(setting, value, pluginName, sessionKey);
        }
        
        public string GetValue(string setting, string pluginName, string sessionKey) {
            return base.Channel.GetValue(setting, pluginName, sessionKey);
        }
    }
}
