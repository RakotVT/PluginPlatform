﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ITWebNet.UMS.Server.Controllers
{
    [Authorize, RoutePrefix("api/services")]
    public class ServicesController : ApiController
    {
        [Route("GetServiceAddress"), HttpGet, AllowAnonymous]
        public async Task<string> GetServiceAddress(string service)
        {
            string value = System.Configuration.ConfigurationManager.AppSettings[service];
            return value;
        }
    }
}
