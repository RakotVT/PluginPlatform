﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using PluginPlatform.ServicesCore.Models;
using Microsoft.AspNet.Identity;
using RefactorThis.GraphDiff;

namespace PluginPlatform.UserService.Controllers
{
    // api/Roles 
    //[Authorize(Roles = "Admin")]
    public class RolesController : BaseController
    {
        // api/Roles/Delete/id 
        [HttpPost]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
                return NotFound();

            var result = await RoleManager.DeleteAsync(role);

            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok();
        }

        // api/Roles/GetAll 
        public async Task<IHttpActionResult> GetAll()
        {
            var roles = await RoleManager.Roles
                .Include(r => r.Plugins)
                .Include(r => r.Users)
                .ToListAsync();
            return Ok(roles);
        }

        // api/Roles/Create 
        public async Task<IHttpActionResult> Create(Role model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Role role = new Role
            {
                Description = model.Description,
                Name = model.Name
            };

            IdentityResult result = await RoleManager.CreateAsync(role);

            //if (result.Succeeded)
            //{
            //    foreach (var item in model.Plugins)
            //    {
            //        var plugin = Context.Plugins.Find(item.Id);
            //        if (plugin != null)
            //            role.Plugins.Add(plugin);
            //    }

            //    result = await RoleManager.UpdateAsync(role);
            //}

            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok(role);
            //Context.Roles.Add(model);

            //try
            //{
            //    await Context.SaveChangesAsync();

            //    var role = await Context.Roles
            //        .Include("Plugins")
            //        .FirstOrDefaultAsync(r => r.Id == model.Id);

            //    return Ok(role);
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
        }

        // api/Roles/Update/id 
        public async Task<IHttpActionResult> Update(int id, Role model)
        {
            if (!await Context.Roles.AnyAsync(r => r.Id == id))
                return NotFound();

            Context.UpdateGraph(model, map => map
                .AssociatedCollection(role => role.Plugins));


            //Role origin = Context.Roles.Find(id);
            //Context.Entry(origin).CurrentValues.SetValues(model);
            //Context.Entry(origin).Collection(r => r.Plugins).Load();

            //var originPlugins = origin.Plugins.Select(p => p.Id);
            //var modelPlugins = model.Plugins.Select(p => p.Id);

            //if (model.Plugins != null)
            //{
            //    foreach (var item in model.Plugins)
            //        if (!originPlugins.Contains(item.Id))
            //            origin.Plugins.Add(item);

            //    foreach (var item in origin.Plugins)
            //        if (!modelPlugins.Contains(item.Id))
            //            origin.Plugins.Remove(item);
            //}

            try
            {
                await Context.SaveChangesAsync();

                var role = await Context.Roles
                    .Include("Plugins")
                    .FirstOrDefaultAsync(r => r.Id == id);

                return Ok(role);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // api/Roles/SetPlugins/id
        public async Task<IHttpActionResult> SetPlugins(int id, List<int> plugins)
        {
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
                return NotFound();

            try
            {
                var addedPlugins = Context.Plugins.Where(p => plugins.Contains(p.Id)).ToList();

                role.Plugins = addedPlugins;

                var result = await RoleManager.UpdateAsync(role);

                IHttpActionResult errorResult = GetErrorResult(result);
                if (errorResult != null)
                    return errorResult;

                return Ok(role);
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}