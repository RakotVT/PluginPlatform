﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PluginPlatform.UserService.Managers;
using Microsoft.Owin.Security;
using PluginPlatform.ServicesCore.Models;

namespace PluginPlatform.UserService.Controllers
{
    public class BaseController : ApiController
    {
        public AppUserManager UserManager { get { return Request.GetOwinContext().GetUserManager<AppUserManager>(); } }

        public AppRoleManager RoleManager { get { return Request.GetOwinContext().Get<AppRoleManager>(); } }

        public UmsDbContext Context { get { return Request.GetOwinContext().Get<UmsDbContext>(); } }

        public IAuthenticationManager Authentication { get { return Request.GetOwinContext().Authentication; } }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null && RoleManager != null)
            {
                UserManager.Dispose();
                RoleManager.Dispose();
            }

            base.Dispose(disposing);
        }

    }
}