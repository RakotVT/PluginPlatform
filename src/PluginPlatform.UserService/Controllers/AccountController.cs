﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using PluginPlatform.Common.DataModels.UserService;
using PluginPlatform.ServicesCore.Models;
using PluginPlatform.UserService.Helpers;
using PluginPlatform.UserService.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace PluginPlatform.UserService.Controllers
{
    [Authorize, RoutePrefix("api/account")]
    public class AccountController : BaseController
    {
        private const string LocalLoginProvider = "Local";

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // POST api/Account/Login
        [Route("Login"), HttpPost, AllowAnonymous]
        public async Task<IHttpActionResult> Login(LoginModel model)
        {
            var user = await UserManager.FindByNameAsync(model.UserName);

            bool isValid = false;
            if (user == null)
                return NotFound();

            if (user != null && user.IsADUser)
                using (PrincipalContext context = new PrincipalContext(ContextType.Domain, user.UserDomain))
                    isValid = context.ValidateCredentials(model.UserName, model.Password, ContextOptions.Negotiate);
            else
                isValid = await UserManager.CheckPasswordAsync(user, model.Password);


            if (isValid)
            {
                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);

                AuthenticationProperties properties = AppOAuthProvider.CreateProperties(user.UserName);
                DateTime issuedUtc = DateTime.UtcNow;
                DateTime expiresUtc = issuedUtc.AddDays(Startup.OAuthOptions.AccessTokenExpireTimeSpan.TotalDays);
                properties.IssuedUtc = issuedUtc;
                properties.ExpiresUtc = expiresUtc;
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);

                string accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);

                Token token = new Token
                {
                    AccessToken = accessToken,
                    Expires = expiresUtc,
                    ExpiresIn = (uint)Startup.OAuthOptions.AccessTokenExpireTimeSpan.TotalSeconds,
                    Issued = issuedUtc,
                    TokenType = OAuthDefaults.AuthenticationType,
                    UserName = user.UserName
                };

                return Ok(token);
            }
            else
                return BadRequest("Неверный логин или пароль");
        }

        // POST api/Account/Regiser
        [Route("Register"), HttpPost, AllowAnonymous]
        public async Task<IHttpActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            User user = new User
            {
                UserName = model.UserName,
                Email = model.Email
            };

            var result = await UserManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                if (!await RoleManager.RoleExistsAsync("User"))
                    result = await RoleManager.CreateAsync(new Role()
                    {
                        Name = "User",
                        Description = "Пoльзователь"
                    });

                if (result.Succeeded)
                    result = await UserManager.AddToRoleAsync(user.Id, "User");
            }

            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok(string.Format("Пользователь {0} успешно создан", user.UserName));
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword"), HttpPost]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            IdentityResult result = await UserManager.ChangePasswordAsync(
                User.Identity.GetUserId<int>(),
                model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
                return GetErrorResult(result);

            return Ok("Пароль успешно изменен.");
        }

        // POST api/Account/SetPassword
        [Route("SetPassword"), HttpPost]
        public async Task<IHttpActionResult> SetPassword(SetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId<int>(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/UserInfo
        [Route("UserInfo"), HttpGet]
        public async Task<IHttpActionResult> GetUserInfo()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());

            var userInfo = new UserInfo()
            {
                Id = user.Id,
                FullName = String.Join(" ", user.LastName, user.FirstName, user.Patronymic),
                Email = user.Email
            };
            return Ok(userInfo);
        }

        // GET api/Account/AvailablePlugins
        [Route("AvailablePlugins"), HttpGet]
        public async Task<IHttpActionResult> GetAvailablePlugins()
        {
            int userId = User.Identity.GetUserId<int>();
            var user = await UserManager.FindByIdAsync(userId);

            var result = await UserManager.GetRolesAsync(userId);

            List<Role> roles = new List<Role>();
            foreach (var name in result)
            {
                var role = await RoleManager.FindByNameAsync(name);
                await Context.Entry(role).Collection(r => r.Plugins).LoadAsync();
                roles.Add(role);
            }

            var plugins = roles.SelectMany(item => item.Plugins.Select(p => p.Id)).Distinct().ToList();

            return Ok(plugins);
        }
    }
}
