﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using PluginPlatform.ServicesCore.Models;
using PluginPlatform.UserService.Models;
using Microsoft.AspNet.Identity;

namespace PluginPlatform.UserService.Controllers
{
    // api/Users
    //[Authorize(Roles = "Admin")]
    public class UsersController : BaseController
    {
        // api/Users/Lock/id 
        public async Task<IHttpActionResult> Lock(int id, int? forDays)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
                return NotFound();

            if (await UserManager.IsLockedOutAsync(id))
                return Ok();

            var result = await UserManager.SetLockoutEnabledAsync(id, true);

            if (result.Succeeded)
            {
                DateTimeOffset duration = forDays.HasValue ?
                    DateTimeOffset.UtcNow.AddDays(forDays.Value) :
                    DateTimeOffset.MaxValue;

                result = await UserManager.SetLockoutEndDateAsync(id, duration);
            }

            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;


            return Ok("Пользователь заблокирован");
        }

        // api/Users/Unlock/id 
        public async Task<IHttpActionResult> Unlock(int id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
                return NotFound();

            if (!await UserManager.IsLockedOutAsync(id))
                return Ok("Пользователь разблокирован");

            var result = await UserManager.ResetAccessFailedCountAsync(id);
            if (result.Succeeded)
                result = await UserManager.SetLockoutEndDateAsync(id, DateTimeOffset.MinValue);

            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok("Пользователь разблокирован");
        }

        // api/Users/IsLocked/id 
        public async Task<IHttpActionResult> IsLocked(int id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
                return NotFound();

            if (!await UserManager.IsLockedOutAsync(id))
                return Ok("Пользователь разблокирован");
            else
                return Ok(string.Format("Пользователь заблокирован до {0}", UserManager.GetLockoutEndDate(id)));
        }


        // api/Users/Create 
        public async Task<IHttpActionResult> Create(User model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await UserManager.CreateAsync(model);

            if (result.Succeeded)
            {
                if (model.Roles == null)
                    return Ok(model);

                var roleIds = model.Roles.Select(r => r.RoleId);
                var modelRoles = Context.Roles.Where(r => roleIds.Contains(r.Id)).Select(r => r.Name).ToList();
                var existedRoles = await UserManager.GetRolesAsync(model.Id);

                string[] toAdd = modelRoles.Except(existedRoles).ToArray();

                result = await UserManager.AddToRolesAsync(model.Id, toAdd);
            }


            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok(model);
        }

        // api/Users/Delete/id 
        public async Task<IHttpActionResult> Delete(int id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
                return NotFound();

            var result = await UserManager.DeleteAsync(user);
            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok();
        }



        // api/Users/GetAll 
        public IHttpActionResult GetAll()
        {
            //throw new InvalidOperationException("Test", new ArgumentException("Inner"));

            var users = Context.Users
                .Include(u => u.Roles);

            return Ok(users);
        }

        // api/Users/GetFromAD
        public IHttpActionResult GetFromAD(string domain = null)
        {
            if (string.IsNullOrWhiteSpace(domain))
                domain = Domain.GetComputerDomain().Name;

            List<User> users = new List<User>();

            using (var context = new PrincipalContext(ContextType.Domain, domain))
            {
                using (var search = new PrincipalSearcher(new UserPrincipal(context) { Enabled = true }))
                {
                    foreach (UserPrincipal item in search.FindAll())
                    {
                        users.Add(new User
                        {
                            IsADUser = true,
                            UserDomain = domain,
                            Email = item.EmailAddress,
                            UserName = item.SamAccountName,
                            FirstName = item.GivenName,
                            LastName = item.Surname,
                            Patronymic = item.MiddleName,

                        });
                    }
                }
            }

            return Ok(users);
        }

        // POST api/Users/SetPassword 
        [HttpPost]
        public async Task<IHttpActionResult> SetPassword(SetUserPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = await UserManager.FindByNameAsync(model.UserName);

            IdentityResult result = await UserManager.AddPasswordAsync(user.Id, model.NewPassword);

            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok("Пароль успешно установлен");
        }

        //api/Users/Update/id
        public async Task<IHttpActionResult> Update(int id, User model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //var user = await UserManager.FindByIdAsync(id);
            //if (user == null)
            //    return NotFound();
            var user = await UserManager.FindByIdAsync(id);
            Context.Entry(user).CurrentValues.SetValues(model);

            var result = await UserManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                if (model.Roles == null)
                    return Ok(model);

                var roleIds = model.Roles.Select(r => r.RoleId);
                var modelRoles = Context.Roles.Where(r => roleIds.Contains(r.Id)).Select(r => r.Name).ToList();
                var existedRoles = await UserManager.GetRolesAsync(id);

                string[] toAdd = modelRoles.Except(existedRoles).ToArray();
                string[] toRemove = existedRoles.Except(modelRoles).ToArray();

                result = await UserManager.AddToRolesAsync(id, toAdd);
                if (result.Succeeded)
                    result = await UserManager.RemoveFromRolesAsync(id, toRemove);
            }

            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok(user);
        }
    }
}