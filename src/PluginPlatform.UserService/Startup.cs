﻿using System;
using System.Web.Http;
using PluginPlatform.ServicesCore.Models;
using PluginPlatform.UserService.Managers;
using PluginPlatform.UserService.Models;
using PluginPlatform.UserService.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(PluginPlatform.UserService.Startup))]
namespace PluginPlatform.UserService
{
    public class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.UseWebApi(config);
            app.UseCors(CorsOptions.AllowAll);
        }

        private void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(UmsDbContext.Create);
            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.CreatePerOwinContext<AppRoleManager>(AppRoleManager.Create);

            OAuthOptions = new OAuthAuthorizationServerOptions()
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new AppOAuthProvider(),
#if DEBUG
                AllowInsecureHttp = true,
#else
                AllowInsecureHttp = true,
#endif
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
            };

            app.UseOAuthBearerTokens(OAuthOptions);
        }
    }
}