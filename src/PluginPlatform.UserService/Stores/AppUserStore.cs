﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PluginPlatform.ServicesCore.Models;
using PluginPlatform.UserService.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PluginPlatform.UserService.Stores
{
    public class AppUserStore : UserStore<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public AppUserStore(UmsDbContext context)
            : base(context)
        {

        }
    }
}