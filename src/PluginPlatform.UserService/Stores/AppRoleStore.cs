﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PluginPlatform.ServicesCore.Models;
using PluginPlatform.UserService.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PluginPlatform.UserService.Managers
{
    public class AppRoleStore : RoleStore<Role, int, UserRole>
    {
        public AppRoleStore(UmsDbContext context)
            : base(context)
        { }
    }
}