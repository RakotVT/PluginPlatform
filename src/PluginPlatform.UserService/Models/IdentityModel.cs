﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using ITWebNet.UMS.UserService.Managers;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ITWebNet.UMS.UserService.Models
{
    public class AppDbContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public AppDbContext()
            : base("AppDbContext")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static AppDbContext Create()
        {
            return new AppDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims");
        }
    }

    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public bool IsADUser { get; set; }
        public string UserDomain { get; internal set; }
    }

    public class Role : IdentityRole<int, UserRole>
    {
        public string Description { get; set; }

        public List<int> Plugins { get; set; }
    }

    public class UserLogin : IdentityUserLogin<int>
    {

    }

    public class UserRole : IdentityUserRole<int>
    {

    }

    public class UserClaim : IdentityUserClaim<int>
    {

    }

    public class AppClaimsPrincipal : ClaimsPrincipal
    {
        public AppClaimsPrincipal(ClaimsPrincipal principal) : base(principal)
        { }

        public int UserId { get { return int.Parse(this.FindFirst(ClaimTypes.Sid).Value); } }
    }
}