﻿using PluginPlatform.ServicesCore.Models;
using PluginPlatform.UserService.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace PluginPlatform.UserService.Managers
{
    public class AppRoleManager : RoleManager<Role, int>
    {
        public AppRoleManager(AppRoleStore store)
            : base(store)
        { }

        public static AppRoleManager Create(IdentityFactoryOptions<AppRoleManager> options, IOwinContext context)
        {
            AppRoleStore roleStore = new AppRoleStore(context.Get<UmsDbContext>());
            return new AppRoleManager(roleStore);
        }
    }
}