﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using PluginPlatform.Common.DataModels.UserService;
using PluginPlatform.ServicesCore.Models;
using PluginPlatform.UserService.Managers;
using PluginPlatform.UserService.Models;

namespace PluginPlatform.UserService.Helpers
{
    public static class RoleToRoleModel
    {
        public static Role ToEntity(this RoleModel model)
        {
            return new Role
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
            };
        }

        public static Role UpdateEntity(this RoleModel model, Role role)
        {
            if (role == null)
                return null;
            role.Name = model.Name;
            role.Description = model.Description;
            return role;
        }

        public static RoleModel ToModel(this Role role)
        {
            return new RoleModel
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description,
                Plugins = role.Plugins.Select(item => item.Id).ToList(),
            };
        }
    }

    public static class UserToUserModel
    {
        public static User ToEntity(this UserModel model)
        {
            return new User
            {
                AccessFailedCount = model.AccessFailedCount,
                Email = model.Email,
                EmailConfirmed = model.EmailConfirmed,
                FirstName = model.FirstName,
                Id = model.Id,
                LastName = model.LastName,
                LockoutEnabled = model.LockoutEnabled,
                LockoutEndDateUtc = model.LockoutEndDateUtc,
                Patronymic = model.Patronymic,
                PhoneNumber = model.PhoneNumber,
                PhoneNumberConfirmed = model.PhoneNumberConfirmed,
                TwoFactorEnabled = model.TwoFactorEnabled,
                UserName = model.UserName,
                IsADUser = model.IsADUser,
                UserDomain = model.UserDomain
            };
        }

        public static User UpdateEntity(this UserModel model, User user)
        {
            user.AccessFailedCount = model.AccessFailedCount;
            user.Email = model.Email;
            user.EmailConfirmed = model.EmailConfirmed;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.LockoutEnabled = model.LockoutEnabled;
            user.LockoutEndDateUtc = model.LockoutEndDateUtc;
            user.Patronymic = model.Patronymic;
            user.PhoneNumber = model.PhoneNumber;
            user.PhoneNumberConfirmed = model.PhoneNumberConfirmed;
            user.TwoFactorEnabled = model.TwoFactorEnabled;
            user.UserName = model.UserName;
            user.IsADUser = model.IsADUser;
            user.UserDomain = model.UserDomain;

            return user;
        }

        public static UserModel ToModel(this User user)
        {
            return new UserModel
            {
                AccessFailedCount = user.AccessFailedCount,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed,
                FirstName = user.FirstName,
                Id = user.Id,
                LastName = user.LastName,
                LockoutEnabled = user.LockoutEnabled,
                LockoutEndDateUtc = user.LockoutEndDateUtc,
                Patronymic = user.Patronymic,
                PhoneNumber = user.PhoneNumber,
                PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                TwoFactorEnabled = user.TwoFactorEnabled,
                UserName = user.UserName,
                IsADUser = user.IsADUser,
                UserDomain = user.UserDomain
            };
        }
    }

    public static class UserExtensions
    {
        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this User user, AppUserManager userManager, string authenticationType)
        {
            var userIdentity = await userManager.CreateIdentityAsync(user, authenticationType);
            return userIdentity;
        }
    }
}