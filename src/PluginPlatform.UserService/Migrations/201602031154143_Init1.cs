namespace ITWebNet.UMS.UserService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsADUser", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "UserDomain", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "UserDomain");
            DropColumn("dbo.Users", "IsADUser");
        }
    }
}
