namespace ITWebNet.UMS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeLicenseInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LicenseInfoes", "OwnerId", c => c.Int(nullable: false));
            DropColumn("dbo.LicenseInfoes", "Owner");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LicenseInfoes", "Owner", c => c.String(maxLength: 4000));
            DropColumn("dbo.LicenseInfoes", "OwnerId");
        }
    }
}
