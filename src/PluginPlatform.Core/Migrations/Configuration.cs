namespace ITWebNet.UMS.Core.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ITWebNet.UMS.Core.LocalSettings.UserStorage;

    internal sealed class Configuration : DbMigrationsConfiguration<UserSettingsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(UserSettingsContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.UserSettings.AddOrUpdate(
                p => p.Key,
                new UserSetting
                {
                    Key = "ServerIpAddress",
                    Value = "10.166.249.11"
                },
                new UserSetting
                {
                    Key = "UpdateServicePort",
                    Value = "8092"
                },
                new UserSetting
                {
                    Key = "AuthorizationServicePort",
                    Value = "8090"
                },
                new UserSetting
                {
                    Key = "LogServicePort",
                    Value = "8089"
                },
                new UserSetting
                {
                    Key = "LicensingServicePort",
                    Value = "8091"
                });
        }
    }
}
