namespace ITWebNet.UMS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendPluginTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Plugins", "Caption", c => c.String(maxLength: 4000));
            AddColumn("dbo.Plugins", "Description", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Plugins", "Description");
            DropColumn("dbo.Plugins", "Caption");
        }
    }
}
