namespace ITWebNet.UMS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserSettings",
                c => new
                {
                    Key = c.String(nullable: false, maxLength: 4000),
                    Value = c.String(maxLength: 4000),
                })
                .PrimaryKey(t=>t.Key);

            CreateTable(
                "dbo.Plugins",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 4000),
                    MainFile = c.String(nullable: false, maxLength: 4000),
                    PluginId = c.Int(nullable: false),
                    Version = c.String(nullable: false, maxLength: 4000)
                })
                .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
            DropTable("dbo.UserSettings");
            DropTable("dbo.Plugins");
        }
    }
}
