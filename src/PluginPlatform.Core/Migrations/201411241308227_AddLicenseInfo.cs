namespace ITWebNet.UMS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLicenseInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LicenseInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Owner = c.String(maxLength: 4000),
                        Data = c.Binary(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LicenseInfoes");
        }
    }
}
