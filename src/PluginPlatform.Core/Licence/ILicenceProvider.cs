﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ums.Core.Licence
{
    public interface ILicenceProvider
    {
        /// <summary>
        /// Получает лицензионный ключ
        /// </summary>
        /// <returns>Лицензионный ключ в формате guid</returns>
        Guid GetLicenceKey();
    }
}
