﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PluginPlatform.Core.Extentions
{
    public static class PropertyByPropertyConverter
    {
        /// <summary>
        /// Копирует свойства объекта TSource в новый обхект TResult по имени свойств.
        /// </summary>
        /// <typeparam name="TSource"> Тип исходного объекта. </typeparam>
        /// <typeparam name="TResult"> Тип объекта, возвращаемого методом. </typeparam>
        /// <param name="source"> Объект, который следует преобразовать. </param>
        /// <returns> Новый объект заданного типа, содержащий значения открытых полей с тем же именем, что и исходный объект. </returns>
        public static TResult Convert<TSource, TResult>(this TSource source) 
            where TSource : class, new()
            where TResult : class, new()
        {
            if (source == default(TSource))
                return default(TResult);

            PropertyInfo[] sourceProperties = source.GetType().GetProperties();
            PropertyInfo[] resultProperties = typeof(TResult).GetProperties();

            TResult result = new TResult();
            foreach (PropertyInfo item in resultProperties)
            {
                PropertyInfo sourceProp = sourceProperties.FirstOrDefault(prop => prop.Name == item.Name);
                if (sourceProp != null)
                    item.SetValue(result, sourceProp.GetValue(source, null), null);
            }

            return result;
        }
    }
}
