﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Windows.Threading;

namespace PluginPlatform.Core.Extentions
{
    public static class WPF
    {
        private delegate T GetPropertyThreadSafeDelegateWPF<T>(System.Windows.FrameworkElement control, Expression<Func<T>> property);

        private delegate void SetPropertyThreadSafeDelegateWPF<T>(System.Windows.FrameworkElement control, Expression<Func<T>> property, T value);

        #region WPF

        /// <summary>
        /// Обрабатывает все сообщения Windows, которые в данный момент находятся в очереди сообщений. 
        /// </summary>
        /// <param name="app"> Экземпляр <see cref="System.Windows.Application" />. </param>
        public static void DoEvents(this System.Windows.Application application)
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new ExitFrameHandler(frm => frm.Continue = false), frame);
            Dispatcher.PushFrame(frame);
        }

        /// <summary>
        /// Потокобезопасное получение значения свойства элемента <see cref="System.Windows.FrameworkElement" /> . 
        /// </summary>
        /// <typeparam name="T"> Тит задаваемого значения </typeparam>
        /// <param name="control"> Целевой элемент <see cref="System.Windows.FrameworkElement" /> . </param>
        /// <param name="property"> Целевое свойство элемента <see cref="System.Windows.FrameworkElement" /> . </param>
        /// <returns> Значение свойства элемента <see cref="System.Windows.FrameworkElement" /> . </returns>
        public static T GetPropertyThreadSafe<T>(this System.Windows.FrameworkElement control, Expression<Func<T>> property)
        {
            var propertyInfo = (property.Body as MemberExpression).Member as PropertyInfo;

            if (propertyInfo == null ||
                !control.GetType().IsSubclassOf(propertyInfo.ReflectedType) ||
                control.GetType().GetProperty(propertyInfo.Name, propertyInfo.PropertyType) == null)
            {
                throw new ArgumentException("Лямбда-выражение должно ссылаться на существующее свойство элемента " + control.GetType());
            }
            T result = Activator.CreateInstance<T>();

            if (control.Dispatcher.CheckAccess())
            {
                result = (T)control.GetType().InvokeMember(propertyInfo.Name, BindingFlags.GetProperty, null, control, new object[] { });
            }
            else
            {
                control.Dispatcher.Invoke(new GetPropertyThreadSafeDelegateWPF<T>(GetPropertyThreadSafe), new object[] { control, property });
            }

            return result;
        }

        /// <summary>
        /// Создает событие Click для WPF кнопки. 
        /// </summary>
        /// <param name="button"> Текущаяя кнопка. </param>
        public static void PerformClick(this System.Windows.Controls.Button button)
        {
            if (!button.IsEnabled)
                return;
            button.RaiseEvent(new System.Windows.RoutedEventArgs(System.Windows.Controls.Button.ClickEvent, button));
        }

        /// <summary>
        /// Потокобезопасное задание свойства элемента <see cref="System.Windows.FrameworkElement" /> . 
        /// </summary>
        /// <typeparam name="T"> Тит задаваемого значения </typeparam>
        /// <param name="control"> Целевой элемент <see cref="System.Windows.FrameworkElement" /> . </param>
        /// <param name="property"> Целевое свойство элемента <see cref="System.Windows.FrameworkElement" /> . </param>
        /// <param name="value"> Значение свойства. </param>
        public static void SetPropertyThreadSafe<T>(this System.Windows.FrameworkElement control, Expression<Func<T>> property, T value)
        {
            var propertyInfo = (property.Body as MemberExpression).Member as PropertyInfo;

            if (propertyInfo == null ||
                !control.GetType().IsSubclassOf(propertyInfo.ReflectedType) ||
                control.GetType().GetProperty(propertyInfo.Name, propertyInfo.PropertyType) == null)
            {
                throw new ArgumentException("Лямбда-выражение должно ссылаться на существующее свойство элемента " + control.GetType());
            }

            if (control.Dispatcher.CheckAccess())
            {
                control.GetType().InvokeMember(propertyInfo.Name, BindingFlags.SetProperty, null, control, new object[] { value });
            }
            else
            {
                control.Dispatcher.Invoke(new SetPropertyThreadSafeDelegateWPF<T>(SetPropertyThreadSafe), new object[] { control, property, value });
            }
        }

        /// <summary>
        /// Открывает окно <see cref="System.Windows.Window" /> и возвращается не дожидаясь закрытия вновь открытого
        /// окна <see cref="System.Windows.Window" /> .
        /// </summary>
        /// <param name="window"> Открываемое окно <see cref="System.Windows.Window" /> . </param>
        /// <param name="owner">
        /// Окно <see cref="System.Windows.Window" /> , которое владеет данным окном
        /// <see cref="System.Windows.Window" /> .
        /// </param>
        public static void Show(this System.Windows.Window window, System.Windows.Window owner)
        {
            window.Owner = owner;
            window.Show();
        }

        /// <summary>
        /// Открывает окно <see cref="System.Windows.Window" /> и возвращается только после его закрытия. 
        /// </summary>
        /// <param name="window"> Открываемое окно <see cref="System.Windows.Window" /> . </param>
        /// <param name="owner">
        /// Окно <see cref="System.Windows.Window" /> , которое владеет данным окном
        /// <see cref="System.Windows.Window" /> .
        /// </param>
        /// <returns>
        /// Значение Nullable{T} типа Boolean, определяющее было действие принято <c> true </c> или отменено <c> false
        /// </c> . Возвращаемое значение - это значение свойства <see cref="System.Windows.Window.DialogResult" /> ,
        /// установленное перед закрытием окна.
        /// </returns>
        public static bool? ShowDialog(this System.Windows.Window window, System.Windows.Window owner)
        {
            window.Owner = owner;
            return window.ShowDialog();
        }

        private delegate void ExitFrameHandler(DispatcherFrame frame);

        #endregion WPF
    }
}
