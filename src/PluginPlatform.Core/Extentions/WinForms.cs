﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Threading;

namespace PluginPlatform.Core.Extentions
{
    public static class WinForms
    {
        #region Variables

        /// <summary>
        /// Делегат для метода <see cref="PluginPlatform.Core.Extentions.SafeInvoke" /> 
        /// </summary>
        public delegate void InvokeHandler();

        private delegate TResult GetPropertyThreadSafeDelegate<TResult>(System.Windows.Forms.Control control, Expression<Func<TResult>> property);

        private delegate void SetPropertyThreadSafeDelegate<TResult>(System.Windows.Forms.Control control, Expression<Func<TResult>> property, TResult value);


        #endregion Variables


        #region WinForms

        /// <summary>
        /// Потокобезопасное получение значения свойства элемента <see cref="System.Windows.Forms.Control" /> . 
        /// </summary>
        /// <typeparam name="T"> Тит задаваемого значения </typeparam>
        /// <param name="control"> Целевой элемент <see cref="System.Windows.Forms.Control" /> . </param>
        /// <param name="property"> Целевое свойство элемента <see cref="System.Windows.Forms.Control" /> . </param>
        /// <returns> Значение свойства элемента <see cref="System.Windows.Forms.Control" /> . </returns>
        public static TResult GetPropertyThreadSafe<TResult>(this System.Windows.Forms.Control control, Expression<Func<TResult>> property)
        {
            var propertyInfo = (property.Body as MemberExpression).Member as PropertyInfo;

            if (propertyInfo == null ||
                !control.GetType().IsSubclassOf(propertyInfo.ReflectedType) ||
                control.GetType().GetProperty(propertyInfo.Name, propertyInfo.PropertyType) == null)
            {
                throw new ArgumentException("Лямбда-выражение должно ссылаться на существующее свойство элемента " + control.GetType());
            }
            TResult result = Activator.CreateInstance<TResult>();
            if (control.InvokeRequired)
            {
                control.Invoke(new GetPropertyThreadSafeDelegate<TResult>(GetPropertyThreadSafe), new object[] { control, property });
            }
            else
            {
                result = (TResult)control.GetType().InvokeMember(propertyInfo.Name, BindingFlags.GetProperty, null, control, new object[] { });
            }

            return result;
        }

        /// <summary>
        /// Проверяет на наличие такой же уже открытой формы и выводит ее на передний план. 
        /// </summary>
        /// <param name="form"> Форма, наличие которой проверяется. </param>
        /// <returns> <c> true </c> , если форма ранее была открыта, <c> false </c> , если формы не найдено. </returns>
        public static bool IsFormAlreadyOpen(this System.Windows.Forms.Form form)
        {
            foreach (System.Windows.Forms.Form formItem in System.Windows.Forms.Application.OpenForms)
            {
                if (formItem.Text == form.Text)
                {
                    formItem.BringToFront();
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Выполняет заданный делегат в потоке, ассоциированном с <see cref="System.Windows.Forms.Control" /> (UI) 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="handler"> делегат, который должен выполниться в UI потоке </param>
        public static void SafeInvoke(this System.Windows.Forms.Control control, InvokeHandler handler)
        {
            if (control == null || control.IsDisposed)
                return;
            if (control.InvokeRequired)
            {
                control.Invoke(handler);
            }
            else
            {
                handler();
            }
        }

        /// <summary>
        /// Потокобезопасное задание свойства элемента <see cref="System.Windows.Forms.Control" /> . 
        /// </summary>
        /// <typeparam name="T"> Тит задаваемого значения </typeparam>
        /// <param name="control"> Целевой элемент <see cref="System.Windows.Forms.Control" /> . </param>
        /// <param name="property"> Целевое свойство элемента <see cref="System.Windows.Forms.Control" /> . </param>
        /// <param name="value"> Значение свойства. </param>
        public static void SetPropertyThreadSafe<TResult>(this System.Windows.Forms.Control control, Expression<Func<TResult>> property, TResult value)
        {
            var propertyInfo = (property.Body as MemberExpression).Member as PropertyInfo;

            if (propertyInfo == null ||
                !control.GetType().IsSubclassOf(propertyInfo.ReflectedType) ||
                control.GetType().GetProperty(propertyInfo.Name, propertyInfo.PropertyType) == null)
            {
                throw new ArgumentException("Лямбда-выражение должно ссылаться на существующее свойство элемента " + control.GetType());
            }

            if (control.InvokeRequired)
            {
                control.Invoke(new SetPropertyThreadSafeDelegate<TResult>(SetPropertyThreadSafe), new object[] { control, property, value });
            }
            else
            {
                control.GetType().InvokeMember(propertyInfo.Name, BindingFlags.SetProperty, null, control, new object[] { value });
            }
        }

        #endregion WinForms

    }
}