﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PluginPlatform.Core.Extentions
{
    public static class Common
    {
        #region Common

        /// <summary>
        /// Предпринимает несколько попыток выгрузить домен приложения с задержкой в 1 секунду. 
        /// </summary>
        /// <param name="domain"> Выгружаемый домен приложения. </param>
        /// <param name="attemptCount"> Количество попыток. </param>
        public static void ForceUnload(this AppDomain domain, int attemptCount)
        {
            System.Threading.Thread.Sleep(1000);
            if (attemptCount != 0)
            {
                try
                {
                    attemptCount--;
                    AppDomain.Unload(domain);
                }
                catch (CannotUnloadAppDomainException)
                {
                    ForceUnload(domain, attemptCount);
                }
                catch (AppDomainUnloadedException) { }
            }
        }

        /// <summary>
        /// Получает описание элемента <see cref="System.Enum" /> , заданное в
        /// <see cref="System.ComponentModel.DescriptionAttribute" /> , либо имя элемента.
        /// </summary>
        /// <param name="value"> Элемент <see cref="System.Enum" /> . </param>
        /// <returns> Опимание либо имя элемента <see cref="System.Enum" /> . </returns>
        public static string GetDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
        /// <summary>
        /// Преобразует заданную перечислимую коллекцию в строку, содержащую элементы данной коллекции. 
        /// </summary>
        /// <param name="list"> Преобразуемая коллекция элементов. </param>
        /// <param name="separator"> Разделитель. </param>
        /// <returns> Строка, содержащая элементы коллекции. </returns>
        public static string ToPLaingString<T>(this IEnumerable<T> list, string separator)
        {
            return String.Join(separator, list);
        }

        /// <summary>
        /// Преобразует заданный массив в строку, содержащую элементы массива. 
        /// </summary>
        /// <param name="array"> Преобразуемый массив. </param>
        /// <param name="separator"> Разделитель. </param>
        /// <returns> Строка, содержащая элементы массива. </returns>
        public static string ToPlainString(this object[] array, string separator)
        {
            return String.Join(separator, array);
        }

        #endregion Common
    }
}
