﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace PluginPlatform.Core.Http
{
    public abstract class BaseClient<TClient>
        where TClient : BaseClient<TClient>
    {
        private RestClient _client;

        public BaseClient(string baseAddress)
        {
            _client = new RestClient(baseAddress);
        }

        public TClient AddProxy(IWebProxy proxy)
        {
            _client.Proxy = proxy;
            return (TClient)this;
        }

        public TClient AddAuthentication(string sessionKey)
        {
            if (!string.IsNullOrWhiteSpace(sessionKey))
                _client.Authenticator = new JwtAuthenticator(sessionKey);
            return (TClient)this;
        }


        public T Execute<T>(IRestRequest request)
        {
            IRestResponse response = _client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            else
                HandleErrors(response);

            return default(T);
        }

        public void Execute(IRestRequest request)
        {
            IRestResponse response = _client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
                return;
            else
                HandleErrors(response);
        }

        private static void HandleErrors(IRestResponse response)
        {
            if (response.StatusCode == HttpStatusCode.Unauthorized)
                throw new InvalidCredentialException();
            else
            {
                ServiceError error = JsonConvert.DeserializeObject<ServiceError>(response.Content);
                throw ServiceError.GetException(error);
            }
            //else if (response.StatusCode == HttpStatusCode.BadRequest)
            //{
            //    throw new Exception();
            //}
            //else if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new Exception();
            //}
        }
    }
}
