﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace PluginPlatform.Core.Http
{

    [Serializable]
    public class ModelValidationException : Exception
    {
        public ModelValidationException() { }
        public ModelValidationException(string message) : base(message) { }
        public ModelValidationException(string message, Exception inner) : base(message, inner) { }
        public ModelValidationException(string message, string messageDetail, Dictionary<string, string[]> modelState)
            : base(message)
        {
            MessageDetail = MessageDetail;
            if (modelState == null)
                ModelState = new Dictionary<string, string[]>();
            else
                ModelState = CleanUpModelState(modelState);
        }

        public string MessageDetail { get; set; }
        public Dictionary<string, string[]> ModelState { get; set; }

        protected ModelValidationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }

        private Dictionary<string, string[]> CleanUpModelState(Dictionary<string, string[]> modelState)
        {
            Dictionary<string, string[]> result = new Dictionary<string, string[]>();

            foreach (var item in modelState)
            {
                var newKey = item.Key.Replace("model.", string.Empty);
                result.Add(newKey, item.Value);
            }

            return result;
        }

    }

    public class ServiceError
    {
        public string Message { get; set; }

        public string MessageDetail { get; set; }

        public Dictionary<string, string[]> ModelState { get; set; }

        public string ExceptionMessage { get; set; }

        public string ExceptionType { get; set; }

        public string StackTrace { get; set; }

        public ServiceError InnerException { get; set; }

        public bool IsException { get { return !string.IsNullOrWhiteSpace(ExceptionMessage); } }

        public static Exception GetException(ServiceError error)
        {
            if (error.IsException)
            {
                Type exType = Type.GetType(error.ExceptionType);

                Exception exception;
                if (exType == null)
                    exception = new Exception(error.ExceptionMessage, GetException(error.InnerException));
                else if (error.InnerException != null)
                    exception = (Exception)Activator.CreateInstance(
                    exType,
                    new object[]
                    {
                        error.ExceptionMessage,
                        GetException(error.InnerException)
                    });
                else
                    exception = (Exception)Activator.CreateInstance(
                    exType,
                    new object[]
                    {
                        error.ExceptionMessage
                    });

                return exception;
            }
            else
            {
                return new ModelValidationException(
                    error.Message,
                    error.MessageDetail,
                    error.ModelState);
            }
        }
    }
}
