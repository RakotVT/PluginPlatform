﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace PluginPlatform.Core.DllLoading
{
    public static class DllHelper
    {
        #region Private Fields

        //static XDocument _XappConfig;
        //static string _appConfigPath;
        private static XNamespace ns = "urn:schemas-microsoft-com:asm.v1";

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Записывает пути общих для приложения сборок в конфиг файл плагина. 
        /// </summary>
        /// <param name="commonAssembliesPath"> Папка общих сборок приложения. </param>
        /// <param name="pluginPath"> Путь файла плагина. </param>
        public static void WriteAssembliesToConfig(string commonAssembliesPath, string pluginPath)
        {
            string appDir = Directory.GetParent(commonAssembliesPath).FullName;

            List<string> commonAssemblies = new List<string>();
            string pluginConfigPath;
            XElement assemblyBinding;

            pluginConfigPath = pluginPath + ".config";

            //заполняем лист путями сборок.
            commonAssemblies = Directory.GetFiles(commonAssembliesPath, "*.dll", SearchOption.AllDirectories).ToList();
            commonAssemblies.AddRange(Directory.GetFiles(appDir, "*.dll", SearchOption.TopDirectoryOnly));

            XDocument document;

            assemblyBinding = new XElement(ns + "assemblyBinding");

            //проверяем на наличие конфиг файла, если есть заргужаем, если нет создаем новый и записываем в него сборки.
            if (File.Exists(pluginConfigPath))
                document = XDocument.Load(pluginConfigPath);
            else
            {
                WriteNewConfig(assemblyBinding, commonAssemblies, pluginConfigPath);
                return;
            }

            XElement runtime = document.Root.Element("runtime");
            if (runtime == null) //проверяем на наличие runtime элемента в конфиге.
            {
                //элемент не найден, создаем его и запивываем сборки.
                document.Root.Add(new XElement("runtime", assemblyBinding));
                runtime = document.Root.Element("runtime");
                AppendAssemblies(assemblyBinding, commonAssemblies);
            }
            else if ((runtime.Element(ns + "assemblyBinding")) == null) //проверяем на наличие assemblyBinding элемента в конфиге.
            {
                //элемент не найден, создаем его и запивываем сборки.
                runtime.Add(assemblyBinding);
                AppendAssemblies(assemblyBinding, commonAssemblies);
            }
            else if ((runtime.Element(ns + "assemblyBinding").Elements(ns + "dependentAssembly").Count() == 0)) //проверяем на наличие dependentAssembly элементов в конфиге.
            {
                //элементы не найдены, передаем в метод  родительский элемент dependentAssembly (assemblyBinding) и запивываем сборки.
                assemblyBinding = runtime.Element(ns + "assemblyBinding");
                AppendAssemblies(assemblyBinding, commonAssemblies);
            }
            else //структура конфига в порядке, получаем ссылку на assemblyBinding и обновляем элемены dependentAssembly.
            {
                assemblyBinding = runtime.Element(ns + "assemblyBinding");
                UpdateAssemblies(assemblyBinding, commonAssemblies);
            }

            //добавляем прочие параметры.
            AppendOtherNodes(document);

            //сохраняем конфиг.
            using (XmlTextWriter writer = new XmlTextWriter(pluginConfigPath, Encoding.UTF8))
            {
                writer.Formatting = System.Xml.Formatting.Indented;
                document.WriteTo(writer);
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Добавляет общие сборки к файлу конфигурации. 
        /// </summary>
        /// <param name="assemblyBinding"> элемент assemblyBinding документа </param>
        /// <param name="commonAssemblies"> список путей общих сборок </param>
        private static void AppendAssemblies(XElement assemblyBinding, List<string> commonAssemblies)
        {
            foreach (string dll in commonAssemblies)
            {
                assemblyBinding.Add(AppendDependentAssemblyElement(dll));
            }
        }

        /// <summary>
        /// Добавляет элемент с параметрами общей сборки. 
        /// </summary>
        /// <param name="assemblyPath"> путь общей сборки </param>
        /// <returns></returns>
        private static XElement AppendDependentAssemblyElement(string assemblyPath)
        {
            try
            {
                AssemblyName assemblyInfo = Assembly.LoadFrom(assemblyPath).GetName();
                XElement dependentAssembly = new XElement(ns + "dependentAssembly",
                    new XElement(ns + "assemblyIdentity",
                        new XAttribute("name", assemblyInfo.Name),
                        new XAttribute("culture", (string.IsNullOrWhiteSpace(assemblyInfo.CultureInfo.Name)) ? "neutral" : assemblyInfo.CultureInfo.Name),
                        new XAttribute("publicKeyToken", (BitConverter.ToString(assemblyInfo.GetPublicKeyToken())).Replace("-", string.Empty).ToLower())),
                    new XElement(ns + "codeBase",
                        new XAttribute("version", assemblyInfo.Version),
                        new XAttribute("href", string.Format(@"file:///{0}", ValidatePath(assemblyPath)))));
                return dependentAssembly;

            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Добавляет элемент работы с Devart.Data.Oracle провайдером, и другие вспомогательные параметры. 
        /// </summary>
        /// <param name="document"> XDocument конфиг файла </param>
        private static void AppendOtherNodes(XDocument document)
        {
            var runtime = document.Root.Element("runtime");

            if (runtime.Element("loadFromRemoteSources") == null)
                runtime.Add(new XElement("loadFromRemoteSources", new XAttribute("enabled", "true")));
            else
                runtime.Element("loadFromRemoteSources").Attribute("enabled").SetValue("true");
        }

        /// <summary>
        /// Обновляет параметры общих сборок конфига. 
        /// </summary>
        /// <param name="assemblyBinding"> элемент assemblyBinding документа </param>
        /// <param name="commonAssemblies"> список путей общих сборок </param>
        private static void UpdateAssemblies(XElement assemblyBinding, List<string> commonAssemblies)
        {
            var dependentAssemblies = assemblyBinding.Elements(ns + "dependentAssembly");

            foreach (string assembly in commonAssemblies)
            {
                bool isUpdated = false;
                foreach (var item in dependentAssemblies)
                {
                    if (isUpdated = UpdateElement(item, assembly))
                        break;
                }
                if (!isUpdated)
                    assemblyBinding.Add(AppendDependentAssemblyElement(assembly));
            }
        }

        /// <summary>
        /// Обновление параметров общей сборки. 
        /// </summary>
        /// <param name="dependentAssembly"> элемент dependentAssembly для обновления </param>
        /// <param name="assemblyPath"> путь общей сборки </param>
        /// <returns></returns>
        private static bool UpdateElement(XElement dependentAssembly, string assemblyPath)
        {
            try
            {
                AssemblyName assemblyInfo = Assembly.LoadFrom(assemblyPath).GetName();
                XElement assemblyIdentity = dependentAssembly.Element(ns + "assemblyIdentity");
                XElement codeBase = dependentAssembly.Element(ns + "codeBase");

                if (assemblyIdentity.Attribute("name").Value != assemblyInfo.Name)
                    return false;

                assemblyIdentity.Attribute("culture").SetValue((string.IsNullOrWhiteSpace(assemblyInfo.CultureInfo.Name)) ? "neutral" : assemblyInfo.CultureInfo.Name);
                assemblyIdentity.Attribute("publicKeyToken").SetValue((BitConverter.ToString(assemblyInfo.GetPublicKeyToken())).Replace("-", string.Empty).ToLower());
                codeBase.Attribute("version").SetValue(assemblyInfo.Version);
                codeBase.Attribute("href").SetValue(string.Format(@"file:///{0}", ValidatePath(assemblyPath)));
                return true;

            }
            catch (Exception)
            {
                return true;
            }
        }

        private static string ValidatePath(string path)
        {
            return path.Replace("&", "%26");
        }

        /// <summary>
        /// Создает новый конфиг файл плагина и записывает все необходимые элементы в него. 
        /// </summary>
        /// <param name="assemblyBinding"></param>
        /// <param name="commonAssemblies"></param>
        /// <param name="pluginConfigPath"></param>
        private static void WriteNewConfig(XElement assemblyBinding, List<string> commonAssemblies, string pluginConfigPath)
        {
            XDocument document = new XDocument(new XElement("configuration",
                new XElement("runtime", assemblyBinding)));
            AppendAssemblies(assemblyBinding, commonAssemblies);
            AppendOtherNodes(document);

            using (XmlTextWriter writer = new XmlTextWriter(pluginConfigPath, Encoding.UTF8))
            {
                writer.Formatting = System.Xml.Formatting.Indented;
                document.WriteTo(writer);
            }
        }

        #endregion Private Methods
    }
}