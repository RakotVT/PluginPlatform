﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace PluginPlatform.Core.Util
{
	/// <summary>
	/// Потокобезопасная реализация System.Collections.ObjectModel.ObservableCollection
	/// </summary>
	/// <typeparam _name="T"></typeparam>
	public class ThreadSafeCollection<T> : ObservableCollection<T>
	{
		#region Variables

		private Dispatcher _dispatcherUIThread;

		private delegate void ClearItemsCallback();

		private delegate void InsertItemCallback(int index, T item);

		private delegate void MoveItemCallback(int oldIndex, int newIndex);

		private delegate void RemoveItemCallback(int index);

		private delegate void SetItemCallback(int index, T item);

		#endregion Variables

		#region Constructors

		public ThreadSafeCollection()
		{
			_dispatcherUIThread = Dispatcher.CurrentDispatcher;
		}

		public ThreadSafeCollection(IEnumerable<T> innerList)
			: this()
		{
			foreach (T item in innerList)
				Add(item);
		}

		#endregion Constructors

		#region Ovveriden Methods

		protected override void ClearItems()
		{
			if (_dispatcherUIThread.CheckAccess())
			{
				base.ClearItems();
			}
			else
			{
				_dispatcherUIThread.Invoke(DispatcherPriority.Send,
					new ClearItemsCallback(ClearItems));
			}
		}

		protected override void InsertItem(int index, T item)
		{
			if (_dispatcherUIThread == null)
			{
				base.InsertItem(index, item);
			}
			else if (_dispatcherUIThread.CheckAccess())
			{
				base.InsertItem(index, item);
			}
			else
			{
				_dispatcherUIThread.Invoke(DispatcherPriority.Send,
					new InsertItemCallback(InsertItem), index, new object[] { item });
			}
		}

		protected override void MoveItem(int oldIndex, int newIndex)
		{
			if (_dispatcherUIThread.CheckAccess())
			{
				base.MoveItem(oldIndex, newIndex);
			}
			else
			{
				_dispatcherUIThread.Invoke(DispatcherPriority.Send,
					new MoveItemCallback(MoveItem), oldIndex, new object[] { newIndex });
			}
		}

		protected override void RemoveItem(int index)
		{
			if (_dispatcherUIThread.CheckAccess())
			{
				base.RemoveItem(index);
			}
			else
			{
				_dispatcherUIThread.Invoke(DispatcherPriority.Send,
					new RemoveItemCallback(RemoveItem), index);
			}
		}

		protected override void SetItem(int index, T item)
		{
			if (_dispatcherUIThread.CheckAccess())
			{
				base.SetItem(index, item);
			}
			else
			{
				_dispatcherUIThread.Invoke(DispatcherPriority.Send,
					new SetItemCallback(SetItem), index, new object[] { item });
			}
		}

		#endregion Ovveriden Methods

		public T AddNew()
		{
			T item = Activator.CreateInstance<T>();
			Add(item);
			return item;
		}
	}
}