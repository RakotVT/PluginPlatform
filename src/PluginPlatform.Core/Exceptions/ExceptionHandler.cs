﻿using System;
using System.Collections.Concurrent;
using System.Windows;
using System.Windows.Threading;
using PluginPlatform.Common.DataModels.Exceptions;
using PluginPlatform.Core.Util;

namespace PluginPlatform.Core.Exceptions
{
    public class ExceptionHandler
    {
        private static ConcurrentDictionary<ExceptionType, Action> _actions = new ConcurrentDictionary<ExceptionType, Action>();

        private static Action<Exception> _logger;

        public static void UseHandler()
        {
            Dispatcher.CurrentDispatcher.UnhandledException += CurrentDispatcherUnhandledException;
        }

        private static void CurrentDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            TypeSwitch.Switch(
                e.Exception,
                TypeSwitch.Case<NotAuthorizedException>(ex => HandleException(ex, ExceptionType.NotAuthorized)),
                TypeSwitch.Case<PluginException>(ex => HandleException(ex, ExceptionType.Plugin)),
                TypeSwitch.Case<ServiceException>(ex => HandleException(ex, ExceptionType.Service)),
                TypeSwitch.Case<ShellException>(ex => HandleException(ex, ExceptionType.Shell)),
                TypeSwitch.Case<Exception>(ex => HandleException(ex, ExceptionType.Other)));
        }

        public static void AddAction(ExceptionType type, Action action)
        {
            if (!_actions.TryAdd(type, action))
                _actions[type] = action;
        }

        public static void AddLogger(Action<Exception> logger)
        {
            _logger = logger;
        }

        public static void LogException(Exception ex)
        {
            if (_logger != null)
                _logger(ex);
        }


        public static void HandleException(Exception ex, ExceptionType type)
        {
            string caption = "Найдены проблемы";
            string description = "";
            MessageBoxButton buttons = MessageBoxButton.YesNoCancel;

            switch (type)
            {
                case ExceptionType.NotAuthorized:
                    description = "Возможно Вы не авторизованы в системе. Произвести авторизацию?";
                    break;
                case ExceptionType.Shell:
                    description = "Обнаружены пробемы в работе системы. Попытаться исправить?";
                    break;
                case ExceptionType.Plugin:
                    description = "В работе плагина обнаружены проблемы. Попытаться исправить?";
                    break;
                case ExceptionType.Service:
                    description = "В работе сервиса обнаружены проблемы. Проверить настройки подключения?";
                    break;
                case ExceptionType.Other:
                    description = "Обнаружена ранее неизвестная проблема. Ошибка передана разработчикам.";
                    buttons = MessageBoxButton.OK;
                    LogException(ex);
                    break;
                default:
                    description = "Обнаружена ранее неизвестная проблема. Ошибка передана разработчикам.";
                    type = ExceptionType.Other;
                    buttons = MessageBoxButton.OK;
                    LogException(ex);
                    break;
            }

            var msgResult = MessageBox.Show(description, caption, buttons, MessageBoxImage.Warning);

            if (msgResult == MessageBoxResult.Yes)
            {
                Action action;
                if (_actions.TryGetValue(type, out action))
                    action();
            }
        }
    }
}
