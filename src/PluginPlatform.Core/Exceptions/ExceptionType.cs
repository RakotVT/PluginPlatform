﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginPlatform.Core.Exceptions
{
    public enum ExceptionType
    {
        NotAuthorized,
        Shell,
        Plugin,
        Service,
        Other
    }
}
