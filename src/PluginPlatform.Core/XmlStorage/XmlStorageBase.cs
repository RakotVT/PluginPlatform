﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace PluginPlatform.Core.XmlStorage
{
    public class XmlContext<TStorage>
        where TStorage : class, new()
    {
        public TStorage Storage { get; set; }

        private string _storageFile;
        private string _defaultStorageFile;

        public XmlContext(string storageFile, string defaultStorageFile = null)
        {
            _storageFile = storageFile;
            _defaultStorageFile = defaultStorageFile;
            if (!File.Exists(storageFile))
            {
                if (!string.IsNullOrWhiteSpace(defaultStorageFile) && File.Exists(defaultStorageFile))
                {
                    LoadStorage(defaultStorageFile);
                    SaveChanges();
                }
                else
                    CreateStorage(storageFile);
            }
            LoadStorage(storageFile);
        }

        private void LoadStorage(string storageFile)
        {
            FileInfo info = new FileInfo(storageFile);

            if (info.Length == 0)
                CreateStorage(storageFile);

            using (FileStream file = new FileStream(storageFile, FileMode.Open, FileAccess.Read))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TStorage));
                Storage = (TStorage)serializer.Deserialize(file);
            }
        }

        private bool CreateStorage(string storageFile)
        {
            try
            {
                using (FileStream file = new FileStream(storageFile, FileMode.Create, FileAccess.Write))
                {
                    Storage = new TStorage();
                    XmlSerializer serializer = new XmlSerializer(typeof(TStorage));
                    serializer.Serialize(file, Storage, new XmlSerializerNamespaces(new XmlQualifiedName[] { new XmlQualifiedName(string.Empty) }));
                }

                Seed();

                return true;
            }
            catch (UnauthorizedAccessException)
            {
                throw;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual void Seed()
        {
        }

        public void SaveChanges()
        {
            using (FileStream file = new FileStream(_storageFile, FileMode.Create, FileAccess.Write))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TStorage));
                serializer.Serialize(file, Storage, new XmlSerializerNamespaces(new XmlQualifiedName[] { new XmlQualifiedName(string.Empty) }));
            }
        }
    }
}
