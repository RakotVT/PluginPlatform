﻿using System.Collections.Generic;
using System.ServiceModel.Channels;
using PluginPlatform.Common.DataModels;

namespace PluginPlatform.Core.LocalSettings
{
    /// <summary>
    /// Провайдер пользовательских настроек оболочки. 
    /// </summary>
    public interface ISettingsProvider
    {
        #region Public Methods

        /// <summary>
        /// Удаляет плагин из списка плагинов у пользователя. 
        /// </summary>
        /// <param name="pluginId"> Идентификатор плагина. </param>
        void DeleteExistingPlugin(int pluginId);

        /// <summary>
        /// Возвращает адрес конечной точки для выбранного сервиса. 
        /// </summary>
        /// <param name="service"> Тип сервиса. </param>
        /// <returns> адрес сервиса. </returns>
        string GetAddress(ServiceType service);

        /// <summary>
        /// Возвращает привязку для подключения к выбранному сервису. 
        /// </summary>
        /// <param name="service"> Тип сервиса. </param>
        /// <returns> <see cref="System.ServiceModel.Channels.Binding" /> биндинг сервиса. </returns>
        Binding GetBindings(ServiceType service);

        /// <summary>
        /// Возвращает текущий тип автоматической проверки обновлений. 
        /// </summary>
        /// <returns> Значение флага "Проверка обновлений". </returns>
        CheckUpdateType GetCheckUpdateType();

        /// <summary>
        /// Получает список плагинов и версий у пользователя. 
        /// </summary>
        /// <returns> Список плагинов у пользователя. </returns>
        List<PluginPlatform.Core.LocalSettings.UserStorage.Plugin> GetExistedPlugins();

        /// <summary>
        /// Получает значение, использовать ли параметры IE прокси для подключения. 
        /// </summary>
        /// <returns> true - использовать прокси IE, иначе не использовать. </returns>
        bool GetIsUseIEProxy();

        /// <summary>
        /// Получает значение, использовать ли параметры прокси для подключения. 
        /// </summary>
        /// <returns> true - использовать прокси, иначе не использовать. </returns>
        bool GetIsUseProxy();

        /// <summary>
        /// Возвращает список данных по лицензиям пользователя. 
        /// </summary>
        /// <param name="ownerId"> Владелец лицензии. </param>
        /// <returns> Список данных по лицензиям пользователя. </returns>
        List<byte[]> GetLicenseInfo(int ownerId);

        /// <summary>
        /// Возвращает путь файла логов пользователя. 
        /// </summary>
        /// <returns> Путь файла логов. </returns>
        string GetLogsStoragePath();

        /// <summary>
        /// Возвращает путь к папке с плагинами пользователя. 
        /// </summary>
        /// <returns> Путь к папке с плагинами пользователя. </returns>
        string GetPluginDir();

        /// <summary>
        /// Получает адрес прокси-сервера. 
        /// </summary>
        /// <returns> ip-адрес сервера. </returns>
        string GetProxyIpAddress();

        /// <summary>
        /// Получает логин пользователя прокси-сервера. 
        /// </summary>
        /// <returns> Логин пользователя. </returns>
        string GetProxyLogin();

        /// <summary>
        /// Получает пароль пользователя прокси-сервера. 
        /// </summary>
        /// <returns> Пароль пользователя. </returns>
        string GetProxyPassword();

        /// <summary>
        /// Получает порт прокси-сервера. 
        /// </summary>
        /// <returns> Порт прокси сервера. </returns>
        string GetProxyPort();

        /// <summary>
        /// Возвращает значение флага "Запомнить меня". 
        /// </summary>
        /// <returns> Значение флага "Запомнить меня". </returns>
        bool GetRememberMe();

        /// <summary>
        /// Получает адрес сервера. 
        /// </summary>
        /// <returns> ip-адрес сервера. </returns>
        string GetServerIpAddress();

        /// <summary>
        /// Возвращает номер порта подключения для сервиса. 
        /// </summary>
        /// <param name="service"> Тип сервиса. </param>
        /// <returns> Номер порта подключения. </returns>
        string GetServicePort(ServiceType service);

        /// <summary>
        /// Возвращает путь к временной папке пользователя. 
        /// </summary>
        /// <returns> Путь к временной папке. </returns>
        string GetTempDir();

        /// <summary>
        /// Возвращает значение флага "Проверка обновлений" при запуске. 
        /// </summary>
        /// <returns> Значение флага "Проверка обновлений". </returns>
        bool GetUpdateChecking(CheckUpdateType type);

        /// <summary>
        /// Возвращает значение логина пользователя. 
        /// </summary>
        /// <returns> Логин пользователя. </returns>
        string GetUserLogin();

        /// <summary>
        /// Возвращает значение пароля пользователя 
        /// </summary>
        /// <returns> Пароль пользователя. </returns>
        string GetUserPassword();

        /// <summary>
        /// Выполняет установку типа автоматической проверки обновлений при запуске. 
        /// </summary>
        /// <param name="type"> Тип проверки обновлений. </param>
        void SetCheckUpdateType(CheckUpdateType type);

        /// <summary>
        /// Добавяет плагин в список плагинов и версий у пользователя. 
        /// </summary>
        /// <param name="plugin"> Данные о плагине. </param>
        void SetExistingPlugins(PluginPlatform.Core.LocalSettings.UserStorage.Plugin plugin);

        /// <summary>
        /// Задает значение, использовать ли параметры IE прокси для подключения. 
        /// </summary>
        void SetIsUseIEProxy(bool isUse);

        /// <summary>
        /// Задает значение, использовать ли параметры прокси для подключения. 
        /// </summary>
        void SetIsUseProxy(bool isUse);

        /// <summary>
        /// Сохраняет в хранилище информацию о лицензии пользователя. 
        /// </summary>
        /// <param name="ownerId"> Владелец лицензии. </param>
        /// <param name="data"> Данные лицензии. </param>
        void SetLicenseInfo(int ownerId, byte[] data);

        /// <summary>
        /// Выполняет сохранение пути файла логов пользователя. 
        /// </summary>
        /// <param name="logsStoragePath"> Путь файла логов. </param>
        void SetLogsStoragePath(string logsStoragePath);

        /// <summary>
        /// Выполняет установку папки с плагинами пользователя. 
        /// </summary>
        /// <param name="dirPath"> Путь папки с плагинами. </param>
        void SetPluginDir(string dirPath);

        /// <summary>
        /// Задает значение адреса прокси-сервера. 
        /// </summary>
        /// <param name="proxyIpAddress"> Адрес прокси сервера. </param>
        void SetProxyIpAddress(string proxyIpAddress);

        /// <summary>
        /// Задает значение логина пользователя прокси-сервера. 
        /// </summary>
        /// <param name="proxyLogin"> Логин для прокси-сервера. </param>
        void SetProxylogin(string proxyLogin);

        /// <summary>
        /// Задает значение пароля пользователя прокси-сервера. 
        /// </summary>
        /// <param name="proxyPassword"> Пароль для прокси-сервера. </param>
        void SetProxyPassword(string proxyPassword);

        /// <summary>
        /// Задает значение порта прокси-сервера 
        /// </summary>
        void SetProxyPort(string proxyPort);

        /// <summary>
        /// Выполняет установку флага "Запомнить меня" при авторизации. 
        /// </summary>
        /// <param name="isRemember"> Значение флага "Запомнить меня". </param>
        void SetRememberMe(bool isRemember);

        /// <summary>
        /// Выполняет установку значения адреса сервера. 
        /// </summary>
        /// <param name="ipAddress"> ip-адрес сервера. </param>
        void SetServerIpAddress(string ipAddress);

        /// <summary>
        /// Задает номер порта подключения для сервиса. 
        /// </summary>
        /// <param name="service"> Тип сервиса. </param>
        /// <param name="value"> Номер порта. </param>
        void SetServicePort(ServiceType service, string value);

        /// <summary>
        /// Выполняет установку временной папки пользователя. 
        /// </summary>
        /// <param name="dirPath"> Путь временной папки пользователя. </param>
        void SetTempDir(string dirPath);

        /// <summary>
        /// Выполняет установку флага "Проверка обновлений" при запуске. 
        /// </summary>
        /// <param name="type"> Тип проверки обновлений. </param>
        /// <param name="isCheck"> Флаг провери обновлений. </param>
        void SetUpdateChecking(CheckUpdateType type, bool isCheck);

        /// <summary>
        /// Выполняет сохранение логина пользователя. 
        /// </summary>
        /// <param name="login"> Логин пользователя. </param>
        void SetUserLogin(string login);

        /// <summary>
        /// Выполняет сохранение пароля пользователя. 
        /// </summary>
        /// <param name="password"> Пароль пользователя. </param>
        void SetUserPassword(string password);

        /// <summary>
        /// Возвращает флаг отображения расширенных настроек.
        /// </summary>
        /// <returns> Флаг расширенных настроек. </returns>
        bool GetIsShowExtended();

        /// <summary>
        /// Выполняет сохранение флага отображения расширенных настроек. 
        /// </summary>
        /// <param name="IsShowExtended"> Флаг расширенных настроек. </param>
        void SetIsShowExtended(bool IsShowExtended);

        #endregion Public Methods
    }
}