﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using PluginPlatform.Common.DataModels;
using PluginPlatform.Core.LocalSettings.UserStorage;

namespace PluginPlatform.Core.LocalSettings
{
    public class SimpleSettingsProvider : ISettingsProvider
    {
        private string _appData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ITWebNet", "UMS");

        private UserStorageXmlContext _context;

        #region Constructors

        #region Конструктор Singleton SimpleSettingsProvider

        private static volatile SimpleSettingsProvider _instance;

        private static object _syncRoot = new Object();


        private SimpleSettingsProvider()
        {
            InitializeStorage();
        }

        /// <summary>
        /// Экземпляр <see cref="SimpleSettingsProvider" />. 
        /// </summary>
        public static SimpleSettingsProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new SimpleSettingsProvider();
                    }
                }
                return _instance;
            }
        }

        #endregion Конструктор Singleton SimpleSettingsProvider

        private void InitializeStorage()
        {
            if (!Directory.Exists(_appData))
                Directory.CreateDirectory(_appData);

            string settingsStoragePath = Path.Combine(_appData, "UserSettings.xml");

            string defaultStoragePath = Path.Combine(Environment.CurrentDirectory, "UserSettings.xml");

            _context = new UserStorageXmlContext(settingsStoragePath, defaultStoragePath);
        }

        #endregion Constructors

        #region Public Methods

        public void DeleteExistingPlugin(int pluginId)
        {
            Plugin plugin = _context.Storage.Plugins.Where(x => x.PluginId == pluginId).FirstOrDefault();
            if (plugin != null)
            {
                _context.Storage.Plugins.Remove(plugin);
                _context.SaveChanges();
            }
        }

        /// <summary>
        /// Возвращает адрес сервиса на сервере 
        /// </summary>
        /// <returns> Адрес сервиса </returns>
        public string GetAddress(ServiceType service)
        {
            try
            {
                return String.Format("http://{0}:{1}", GetServerIpAddress(), GetServicePort(service));

                //if (service == ServiceType.AuthorizationService)
                //    return String.Format("http://{0}:{1}", GetServerIpAddress(), GetServicePort(service));

                //string uri = String.Format("http://{0}:{1}/{2}.svc", GetServerIpAddress(), GetServicePort(service), service.ToString());
                //return uri;
            }
            catch (Exception e) { throw e; }
        }

        /// <summary>
        /// Возвращает настройки соединения с сервисом 
        /// </summary>
        /// <returns> Настройки соединения </returns>
        public Binding GetBindings(ServiceType service)
        {
            WSHttpBinding binding = new WSHttpBinding()
            {
                Security = new WSHttpSecurity()
                {
                    Mode = SecurityMode.None,
                    Transport = new HttpTransportSecurity()
                    {
                        ClientCredentialType = HttpClientCredentialType.None,
                        ProxyCredentialType = HttpProxyCredentialType.None
                    }
                }
            };

            binding.MaxReceivedMessageSize = 671088640;
            binding.MessageEncoding = WSMessageEncoding.Text;
            binding.TextEncoding = Encoding.UTF8;
            binding.ReaderQuotas.MaxArrayLength = 2147483646;
            binding.ReaderQuotas.MaxDepth = 32;
            binding.ReaderQuotas.MaxStringContentLength = 5242880;
            binding.ReaderQuotas.MaxBytesPerRead = 4096;
            binding.ReaderQuotas.MaxNameTableCharCount = 5242880;
            binding.CloseTimeout = TimeSpan.FromMinutes(5);
            binding.OpenTimeout = TimeSpan.FromMinutes(5);
            binding.SendTimeout = TimeSpan.FromMinutes(5);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(5);

            // настройки прокси 
            if (GetIsUseProxy())
            {
                binding.ProxyAddress = new Uri(string.Format("http://{0}:{1}", GetProxyIpAddress(), GetProxyPort()));
                binding.UseDefaultWebProxy = false;
            }
            if (GetIsUseIEProxy())
            {
                WebRequest.DefaultWebProxy.Credentials = new NetworkCredential(GetProxyLogin(), GetProxyPassword());
                binding.UseDefaultWebProxy = true;
            }
            if ((!GetIsUseProxy()) && (!GetIsUseIEProxy()))
                binding.UseDefaultWebProxy = false;

            return binding;
        }

        public CheckUpdateType GetCheckUpdateType()
        {
            string strValue = GetValue("UpdateChecking");
            CheckUpdateType result;
            if (!Enum.TryParse<CheckUpdateType>(strValue, true, out result))
                result = CheckUpdateType.All;

            return result;
        }

        public List<Plugin> GetExistedPlugins()
        {
            return _context.Storage.Plugins.ToList();
        }

        public bool GetIsUseIEProxy()
        {
            bool result;
            Boolean.TryParse(GetValue("IsUseIEproxy"), out result);

            return result;
        }

        public bool GetIsUseProxy()
        {
            bool result;
            Boolean.TryParse(GetValue("IsUseProxy"), out result);

            return result;
        }

        /// <summary>
        /// Возвращает список данных по лицензиям пользователя. 
        /// </summary>
        /// <param name="ownerId"> Владелец лицензии. </param>
        /// <returns> Список данных по лицензиям пользователя. </returns>
        public List<byte[]> GetLicenseInfo(int ownerId)
        {
            return _context.Storage.Licenses.Where(item => item.OwnerId == ownerId).Select(item => item.Data).ToList();
        }

        public string GetLogsStoragePath()
        {
            string dirLogs = GetValue("LogsStoragePath");
            if (String.IsNullOrWhiteSpace(dirLogs) || !Directory.Exists(dirLogs))
            {
                dirLogs = _appData;
                if (!Directory.Exists(dirLogs))
                    Directory.CreateDirectory(dirLogs);
                SetLogsStoragePath(dirLogs);
            }
            return dirLogs;
        }

        public string GetPluginDir()
        {
            string dirPlugin = GetValue("PluginDir");
            if (String.IsNullOrWhiteSpace(dirPlugin) || !Directory.Exists(dirPlugin))
            {
                string path = Path.Combine(_appData, "Plugins");
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                SetPluginDir(path);
            }
            return dirPlugin;
        }

        public string GetProxyIpAddress()
        {
            return GetValue("ProxyIpAddress");
        }

        public string GetProxyLogin()
        {
            return GetValue("ProxyLogin");
        }

        public string GetProxyPassword()
        {
            return GetValue("ProxyPassword");
        }

        public string GetProxyPort()
        {
            return GetValue("ProxyPort");
        }

        public bool GetRememberMe()
        {
            return GetValue("IsRememberMe") == "True" ? true : false;
        }

        public string GetServerIpAddress()
        {
            return GetValue("ServerIpAddress");
        }

        /// <summary>
        /// Возвращает номер порта подключения для сервиса. 
        /// </summary>
        /// <param name="service"> Тип сервиса. </param>
        /// <returns> Номер порта подключения. </returns>
        public string GetServicePort(ServiceType service)
        {
            return GetValue(service.ToString() + "Port");
        }

        public string GetTempDir()
        {
            string dirTemp = GetValue("TempDir");
            if (String.IsNullOrWhiteSpace(dirTemp) || !Directory.Exists(dirTemp))
            {
                SetTempDir(Path.Combine(Path.GetTempPath(), "UMS"));

                //SetValue("TempDir", Path.Combine(Path.GetTempPath(), "Shell"));
            }
            return GetValue("TempDir");
        }

        public bool GetUpdateChecking(CheckUpdateType type)
        {
            string strValue = GetValue("UpdateChecking");
            CheckUpdateType result;
            if (!Enum.TryParse<CheckUpdateType>(strValue, true, out result))
                result = CheckUpdateType.All;

            return result.HasFlag(type);
        }

        public string GetUserLogin()
        {
            return GetValue("UserLogin");
        }

        public string GetUserPassword()
        {
            return GetValue("UserPassword");
        }

        public void SetCheckUpdateType(CheckUpdateType type)
        {
            SetValue("UpdateChecking", type.ToString());
        }

        public void SetExistingPlugins(Plugin plugin)
        {
            Plugin exisedPlugin = _context.Storage.Plugins.FirstOrDefault(x => x.Name == plugin.Name);
            if (exisedPlugin != null)
            {
                if (exisedPlugin.Version == plugin.Version)
                    return;
                exisedPlugin.Version = plugin.Version.ToString();
                exisedPlugin.MainFile = plugin.MainFile;
                exisedPlugin.Caption = plugin.Caption;
                exisedPlugin.Description = plugin.Description;
                _context.SaveChanges();
            }
            else
            {
                _context.Storage.Plugins.Add(plugin);
                _context.SaveChanges();
            }
        }

        public void SetIsUseIEProxy(bool isUse)
        {
            SetValue("IsUseIEproxy", isUse.ToString());
        }

        public void SetIsUseProxy(bool isUse)
        {
            SetValue("IsUseProxy", isUse.ToString());
        }

        /// <summary>
        /// Сохраняет в хранилище информацию о лицензии пользователя. 
        /// </summary>
        /// <param name="ownerId"> Владелец лицензии. </param>
        /// <param name="data"> Данные лицензии. </param>
        public void SetLicenseInfo(int ownerId, byte[] data)
        {
            _context.Storage.Licenses.Add(new LicenseInfo() { OwnerId = ownerId, Data = data });
            _context.SaveChanges();
        }

        public void SetLogsStoragePath(string logsStoragePath)
        {
            SetValue("LogsStoragePath", logsStoragePath);
        }

        public void SetPluginDir(string dirPath)
        {
            SetValue("PluginDir", dirPath);
        }

        public void SetProxyIpAddress(string proxyIpAddress)
        {
            SetValue("ProxyIpAddress", proxyIpAddress);
        }

        public void SetProxylogin(string proxyLogin)
        {
            SetValue("ProxyLogin", proxyLogin.ToString());
        }

        public void SetProxyPassword(string proxyPassword)
        {
            SetValue("ProxyPassword", proxyPassword.ToString());
        }

        public void SetProxyPort(string proxyPort)
        {
            SetValue("ProxyPort", proxyPort);
        }

        public void SetRememberMe(bool isRemember)
        {
            SetValue("IsRememberMe", isRemember.ToString());
        }

        public void SetServerIpAddress(string ipAddress)
        {
            SetValue("ServerIpAddress", ipAddress);
        }

        /// <summary>
        /// Задает номер порта подключения для сервиса. 
        /// </summary>
        /// <param name="service"> Тип сервиса. </param>
        /// <param name="value"> Номер порта </param>
        public void SetServicePort(ServiceType service, string value)
        {
            SetValue(service.ToString() + "Port", value);
        }

        public void SetTempDir(string dirPath)
        {
            SetValue("TempDir", dirPath);
        }

        public void SetUpdateChecking(CheckUpdateType type, bool isCheck)
        {
            string strValue = GetValue("UpdateChecking");
            CheckUpdateType types;
            if (!Enum.TryParse<CheckUpdateType>(strValue, true, out types))
                types = CheckUpdateType.All;
            if (isCheck)
                types = types | type;
            else
                types = types ^ type;

            SetValue("UpdateChecking", types.ToString());
        }

        public void SetUserLogin(string login)
        {
            SetValue("UserLogin", login);
        }

        public void SetUserPassword(string password)
        {
            SetValue("UserPassword", password);
        }

        public bool GetIsShowExtended()
        {
            bool result;
            Boolean.TryParse(GetValue("IsShowExtended"), out result);

            return result;
        }

        public void SetIsShowExtended(bool IsShowExtended)
        {
            SetValue("IsShowExtended", IsShowExtended.ToString());
        }

        #endregion Public Methods

        #region Methods

        /// <summary>
        /// Возвращает значение параметра или создает его при отсутствии. 
        /// </summary>
        /// <param name="setting"> Имя параметра. </param>
        /// <returns> Значение параметра. </returns>
        public string GetValue(string setting)
        {
            UserSetting set = _context.Storage.Settings.Where(x => x.Key == setting).FirstOrDefault();
            if (set == null)
            {
                UserSetting newSetting = new UserSetting();
                newSetting.Key = setting;
                newSetting.Value = String.Empty;
                _context.Storage.Settings.Add(newSetting);
                _context.SaveChanges();
                return String.Empty;
            }
            else
                return set.Value;
        }

        /// <summary>
        /// Устанавливает новое значение параметру. 
        /// </summary>
        /// <param name="setting"> Имя параметра. </param>
        /// <param name="newValue"> Новое значение. </param>
        public void SetValue(string setting, string newValue)
        {
            try
            {
                UserSetting userSetting = _context.Storage.Settings.Where(x => x.Key == setting).FirstOrDefault();
                if (userSetting != null)
                {
                    userSetting.Value = newValue;
                }
                else
                {
                    UserSetting newSet = new UserSetting();
                    newSet.Key = setting;
                    newSet.Value = newValue;
                    _context.Storage.Settings.Add(newSet);
                }
                _context.SaveChanges();
            }
            catch (Exception e) { throw e; }
        }

        #endregion Methods
    }
}