﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PluginPlatform.Core.LocalSettings.UserStorage
{
    [Serializable]
    public class UserSetting
    {
        [Key]
        [XmlAttribute]
        public string Key { get; set; }
        [XmlElement]
        public string Value { get; set; }
    }
}
