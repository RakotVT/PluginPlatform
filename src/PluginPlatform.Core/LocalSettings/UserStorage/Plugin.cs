﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PluginPlatform.Core.LocalSettings.UserStorage
{
    [Serializable]
    public class Plugin
    {
        [Key]
        [XmlIgnore]
        public int Id { get; set; }
        [XmlAttribute]
        public string Caption { get; set; }
        [XmlAttribute]
        public string Description { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string MainFile { get; set; }
        [XmlAttribute]
        public int PluginId { get; set; }
        [XmlAttribute]
        public string Version { get; set; }
        [XmlAttribute]
        public bool Autorun { get; set; }
    }
}
