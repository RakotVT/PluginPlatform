﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServerCompact;
using System.Linq;
using System.Reflection;
using System.Text;
using ITWebNet.UMS.Core.Migrations;

namespace ITWebNet.UMS.Core.LocalSettings.UserStorage
{
    [DbConfigurationType(typeof(SQLCeConfiguration))]
    public class UserSettingsContext : DbContext
    {
        static UserSettingsContext()
        {
            Database.SetInitializer<UserSettingsContext>(new DbMigrationsInitializer());
            //Database.SetInitializer<UserSettingsContext>(new MigrateDatabaseToLatestVersion<UserSettingsContext, Configuration>());
            //Database.SetInitializer<UserSettingsContext>(new CreateDatabaseIfNotExists<UserSettingsContext>());
        }

        public UserSettingsContext() : base("UserSettingsContext") { }

        public UserSettingsContext(string connectionString)
            : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<UserSetting> UserSettings { get; set; }
        public DbSet<Plugin> Plugins { get; set; }
        public DbSet<LicenseInfo> Licenses { get; set; }
    }

    class DbMigrationsInitializer : IDatabaseInitializer<UserSettingsContext>
    {
        #region IDatabaseInitializer<UserSettingsContext> Members

        public void InitializeDatabase(UserSettingsContext context)
        {
            try
            {
                var config = new Configuration();
                config.TargetDatabase = new System.Data.Entity.Infrastructure.DbConnectionInfo(
                    context.Database.Connection.ConnectionString,
                    SqlCeProviderServices.ProviderInvariantName);

                var dbMigrator = new DbMigrator(config);

                if (dbMigrator.GetDatabaseMigrations().Count() == 0)
                    context.Database.Delete();

                if (dbMigrator.GetPendingMigrations().Count() != 0)
                    dbMigrator.Update();

                context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }

    class SQLCeConfiguration : DbConfiguration
    {
        public SQLCeConfiguration()
        {
            SetProviderServices(SqlCeProviderServices.ProviderInvariantName, SqlCeProviderServices.Instance);
        }
    }

    //public class SQLiteConfiguration : DbConfiguration
    //{
    //    public SQLiteConfiguration()
    //    {
    //        SetProviderFactory("System.Data.SQLite", SQLiteFactory.Instance);
    //        SetProviderFactory("System.Data.SQLite.EF6", SQLiteProviderFactory.Instance);
    //        Type t = Type.GetType(
    //                   "System.Data.SQLite.EF6.SQLiteProviderServices, System.Data.SQLite.EF6");
    //        FieldInfo fi = t.GetField("Instance", BindingFlags.NonPublic | BindingFlags.Static);
    //        SetProviderServices("System.Data.SQLite", (DbProviderServices)fi.GetValue(null));
    //    }
    //}
}
