﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PluginPlatform.Core.LocalSettings.UserStorage
{
    [XmlRoot("Storage")]
    public class SettingsStorage
    {
        [XmlArray("Settings"), XmlArrayItem("Setting")]
        public ObservableCollection<UserSetting> Settings { get; set; }

        [XmlArray("Plugins"), XmlArrayItem("Plugin")]
        public ObservableCollection<Plugin> Plugins { get; set; }

        [XmlArray("Licenses"), XmlArrayItem("License")]
        public ObservableCollection<LicenseInfo> Licenses { get; set; }

        public SettingsStorage()
        {
            Settings = new ObservableCollection<UserSetting>();
            Plugins = new ObservableCollection<Plugin>();
            Licenses = new ObservableCollection<LicenseInfo>();
        }
    }
}
