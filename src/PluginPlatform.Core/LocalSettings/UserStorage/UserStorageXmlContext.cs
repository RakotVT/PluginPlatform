﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using PluginPlatform.Core.XmlStorage;

namespace PluginPlatform.Core.LocalSettings.UserStorage
{
    public class UserStorageXmlContext : XmlContext<SettingsStorage>
    {
        public UserStorageXmlContext(string storageFile, string defaultStorageFile = null)
            : base(storageFile, defaultStorageFile) { }

        public override void Seed()
        {
            var initialData = new UserSetting[]
            {                
                new UserSetting
                {
                    Key = "UpdateServicePort",
                    Value = "8092"
                },
                new UserSetting
                {
                    Key = "AuthorizationServicePort",
                    Value = "8090"
                },
                new UserSetting
                {
                    Key = "LogServicePort",
                    Value = "8089"
                },
                new UserSetting
                {
                    Key = "LicensingServicePort",
                    Value = "8091"
                }
            };

            foreach (var item in initialData)
            {
                Storage.Settings.Add(item);
            }

            SaveChanges();
        }
    }
}
