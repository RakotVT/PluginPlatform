﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PluginPlatform.Core.LocalSettings.UserStorage
{
    [Serializable]
    public class LicenseInfo
    {
        [Key]
        [XmlIgnore]
        public int Id { get; set; }
        [XmlAttribute]
        public int OwnerId { get; set; }
        [XmlElement]
        public byte[] Data { get; set; }
    }
}
