﻿using System;
using System.ComponentModel;

namespace PluginPlatform.Core.LocalSettings
{
    [Flags]
    public enum CheckUpdateType
    {
        [Description("Ничего")]
        None = 0,
        [Description("Только оболочка")]
        Shell = 1,
        [Description("Только плагины")]
        Plugins = 2,
        [Description("Все")]
        All = 3,
    }
}
