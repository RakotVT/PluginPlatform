﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace PluginPlatform.Core.Hashing
{
    public class HashProvider
    {
        #region Constructors

        public HashProvider() { }

        #endregion

        #region Methods

        /// <summary>
        /// Вычисляет значение хеша файла
        /// </summary>
        /// <param name="fileName">Путь к файлу</param>
        /// <returns></returns>
        public string ComputeHash(string fileName)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(fileName);
                byte[] fileData;
                using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader binnaryReader = new BinaryReader(fileStream))
                    {
                        long fileLength = fileInfo.Length;
                        fileData = binnaryReader.ReadBytes((int)fileLength);
                    }
                }
                SHA512 shaM = new SHA512Managed();
                byte[] result = shaM.ComputeHash(fileData);
                string hex = BitConverter.ToString(result).Replace("-", string.Empty);
                return hex;
            }
            catch (Exception e) { throw e; }
        }

        #endregion
    }
}
