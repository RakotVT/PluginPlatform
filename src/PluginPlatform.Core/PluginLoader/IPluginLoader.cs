﻿using System;
using System.AddIn.Contract;
using System.Runtime.Remoting.Messaging;

namespace PluginPlatform.Core.PluginLoader
{
    public interface IPluginLoader
    {
        /// <summary>
        /// Загружает плагин в память и предоставляет контракт для визуального элемента плагина.
        /// </summary>
        /// <param name="assembly"> Путь до сборки для загрузки. </param>
        /// <returns> Контракт для визуального элемента плагина. </returns>
        INativeHandleContract LoadPlugin(string assembly);

        event EventHandler<PluginExceptionEventArgs> ExceptionOccured;

        [OneWay]
        void Terminate();
    }

    public class PluginExceptionEventArgs : EventArgs
    {
        private readonly string _pluginName;
        private readonly Exception _exception;

        public string PluginName { get { return _pluginName; } }
        public Exception Exception { get { return _exception; } }

        public PluginExceptionEventArgs(string pluginName, Exception exception)
        {
            _pluginName = pluginName;
            _exception = exception;
        }
    }
}
