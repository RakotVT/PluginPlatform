﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;

namespace PluginPlatform.Core.WinFunctions
{
    /// <summary>
    /// Агрегатор API функций Windows 
    /// </summary>
    public static class WindowsFunctions
    {
        #region Public Methods

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public extern static bool DestroyIcon(IntPtr handle);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowText(HandleRef hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(HandleRef hWnd);

        [DllImport("User32", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool MoveWindow(IntPtr Handle, int x, int y, int w, int h, bool repaint);

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenThread(ThreadAccess dwDesiredAccess, bool bInheritHandle, uint dwThreadId);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, UInt32 Msg, int wParam, int lParam);

        [DllImport("kernel32.dll")]
        public static extern int ResumeThread(IntPtr hThread);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, int wParam, int lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessageTimeout(HandleRef hWnd, int msg, IntPtr wParam, IntPtr lParam, SendMessageTimeoutFlags flags, int timeout, out IntPtr pdwResult);

        [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern IntPtr SetFocus(IntPtr hwnd);

        [DllImport("user32.dll")]
        public static extern IntPtr SetForegroundWindow(int hWnd);

        [DllImport("User32", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndParent);

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("kernel32.dll")]
        public static extern uint SuspendThread(IntPtr hThread);

        #endregion Public Methods
    }

    [Flags]
    public enum SendMessageTimeoutFlags : int
    {
        /// <summary>
        /// The calling thread is not prevented from processing other requests while waiting for the function to return. 
        /// </summary>
        SMTO_NORMAL = 0x0000,

        /// <summary>
        /// Prevents the calling thread from processing any other requests until the function returns. 
        /// </summary>
        SMTO_BLOCK = 0x0001,

        /// <summary>
        /// The function returns without waiting for the time-out period to elapse if the receiving thread appears to
        /// not respond or "hangs."
        /// </summary>
        SMTO_ABORTIFHUNG = 0x0002,

        /// <summary>
        /// The function does not enforce the time-out period as long as the receiving thread is processing messages. 
        /// </summary>
        SMTO_NOTIMEOUTIFNOTHUNG = 0x8,

        /// <summary>
        /// The function should return 0 if the receiving window is destroyed or its owning thread dies while the
        /// message is being processed.
        /// </summary>
        SMTO_ERRORONEXIT = 0x0020
    }

    [Flags()]
    public enum SetWindowPosFlags : uint
    {
        /// <summary>
        /// If the calling thread and the thread that owns the window are attached to different input queues, the system
        /// posts the request to the thread that owns the window. This prevents the calling thread from blocking its
        /// execution while other threads process the request.
        /// </summary>
        /// <remarks> SWP_ASYNCWINDOWPOS </remarks>
        AsynchronousWindowPosition = 0x4000,

        /// <summary>
        /// Prevents generation of the WM_SYNCPAINT message. 
        /// </summary>
        /// <remarks> SWP_DEFERERASE </remarks>
        DeferErase = 0x2000,

        /// <summary>
        /// Draws a frame (defined in the window's class description) around the window. 
        /// </summary>
        /// <remarks> SWP_DRAWFRAME </remarks>
        DrawFrame = 0x0020,

        /// <summary>
        /// Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to the window,
        /// even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE is sent only
        /// when the window's size is being changed.
        /// </summary>
        /// <remarks> SWP_FRAMECHANGED </remarks>
        FrameChanged = 0x0020,

        /// <summary>
        /// Hides the window. 
        /// </summary>
        /// <remarks> SWP_HIDEWINDOW </remarks>
        HideWindow = 0x0080,

        /// <summary>
        /// Does not activate the window. If this flag is not set, the window is activated and moved to the top of
        /// either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter
        /// parameter) .
        /// </summary>
        /// <remarks> SWP_NOACTIVATE </remarks>
        DoNotActivate = 0x0010,

        /// <summary>
        /// Discards the entire contents of the client area. If this flag is not specified, the valid contents of the
        /// client area are saved and copied back into the client area after the window is sized or repositioned.
        /// </summary>
        /// <remarks> SWP_NOCOPYBITS </remarks>
        DoNotCopyBits = 0x0100,

        /// <summary>
        /// Retains the current position (ignores X and Y parameters). 
        /// </summary>
        /// <remarks> SWP_NOMOVE </remarks>
        IgnoreMove = 0x0002,

        /// <summary>
        /// Does not change the owner window's position in the Z order. 
        /// </summary>
        /// <remarks> SWP_NOOWNERZORDER </remarks>
        DoNotChangeOwnerZOrder = 0x0200,

        /// <summary>
        /// Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to the client
        /// area, the nonclient area (including the title bar and scroll bars), and any part of the parent window
        /// uncovered as a types of the window being moved. When this flag is set, the application must explicitly
        /// invalidate or redraw any parts of the window and parent window that need redrawing.
        /// </summary>
        /// <remarks> SWP_NOREDRAW </remarks>
        DoNotRedraw = 0x0008,

        /// <summary>
        /// Same as the SWP_NOOWNERZORDER flag. 
        /// </summary>
        /// <remarks> SWP_NOREPOSITION </remarks>
        DoNotReposition = 0x0200,

        /// <summary>
        /// Prevents the window from receiving the WM_WINDOWPOSCHANGING message. 
        /// </summary>
        /// <remarks> SWP_NOSENDCHANGING </remarks>
        DoNotSendChangingEvent = 0x0400,

        /// <summary>
        /// Retains the current size (ignores the cx and cy parameters). 
        /// </summary>
        /// <remarks> SWP_NOSIZE </remarks>
        IgnoreResize = 0x0001,

        /// <summary>
        /// Retains the current Z order (ignores the hWndInsertAfter parameter). 
        /// </summary>
        /// <remarks> SWP_NOZORDER </remarks>
        IgnoreZOrder = 0x0004,

        /// <summary>
        /// Displays the window. 
        /// </summary>
        /// <remarks> SWP_SHOWWINDOW </remarks>
        ShowWindow = 0x0040,
    }

    [Flags]
    public enum ThreadAccess : int
    {
        TERMINATE = (0x0001),
        SUSPEND_RESUME = (0x0002),
        GET_CONTEXT = (0x0008),
        SET_CONTEXT = (0x0010),
        SET_INFORMATION = (0x0020),
        QUERY_INFORMATION = (0x0040),
        SET_THREAD_TOKEN = (0x0080),
        IMPERSONATE = (0x0100),
        DIRECT_IMPERSONATION = (0x0200)
    }
}