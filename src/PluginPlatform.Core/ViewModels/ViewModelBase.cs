﻿using System.ComponentModel;

namespace PluginPlatform.Core.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged, IDataErrorInfo
    {
        private bool _hasErrors;

        public bool HasErrors
        {
            get { return _hasErrors; }
            set { _hasErrors = value; RaisePropertyChanged("HasErrors"); }
        }


        #region INotifyPropertyChanged Members

        public virtual void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region IDataErrorInfo Members

        public virtual string Error
        {
            get { return null; }
        }

        public virtual string this[string columnName]
        {
            get { return null; }
        }

        #endregion
    }
}
