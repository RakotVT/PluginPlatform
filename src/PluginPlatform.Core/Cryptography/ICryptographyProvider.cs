﻿using System.IO;

namespace PluginPlatform.Core.Cryptography
{
    public interface ICryptographyProvider
    {
        void GetHash(byte[] data, out byte[] hash);
        void GetHash(MemoryStream data, out byte[] hash);
        void GetHash(FileStream data, out byte[] hash);

        void Encrypt(byte[] data, out byte[] encryptedData);
        void Encrypt(byte[] data, out MemoryStream encrypdedData);
        void Encrypt(byte[] data, out FileStream encrypdedData);
        void Encrypt(MemoryStream data, out byte[] encryptedData);
        void Encrypt(MemoryStream data, out MemoryStream encrypdedData);
        void Encrypt(MemoryStream data, out FileStream encrypdedData);
        void Encrypt(FileStream data, out byte[] encryptedData);
        void Encrypt(FileStream data, out MemoryStream encrypdedData);
        void Encrypt(FileStream data, out FileStream encrypdedData);

        void Decrypt(byte[] data, out byte[] decryptedData);
        void Decrypt(byte[] data, out MemoryStream decrypdedData);
        void Decrypt(byte[] data, out FileStream decrypdedData);
        void Decrypt(MemoryStream data, out byte[] decryptedData);
        void Decrypt(MemoryStream data, out MemoryStream decrypdedData);
        void Decrypt(MemoryStream data, out FileStream decrypdedData);
        void Decrypt(FileStream data, out byte[] decryptedData);
        void Decrypt(FileStream data, out MemoryStream decrypdedData);
        void Decrypt(FileStream data, out FileStream decrypdedData);
    }
}
