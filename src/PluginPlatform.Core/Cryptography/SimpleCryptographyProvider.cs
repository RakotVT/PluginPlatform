﻿using System;
using System.IO;

namespace PluginPlatform.Core.Cryptography
{
    public class SimpleCryptographyProvider : ICryptographyProvider
    {
        #region ICryptographyProvider Members

        public void GetHash(byte[] data, out byte[] hash)
        {
            throw new NotImplementedException();
        }

        public void GetHash(MemoryStream data, out byte[] hash)
        {
            throw new NotImplementedException();
        }

        public void GetHash(FileStream data, out byte[] hash)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(byte[] data, out byte[] encryptedData)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(byte[] data, out MemoryStream encrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(byte[] data, out FileStream encrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(MemoryStream data, out byte[] encryptedData)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(MemoryStream data, out MemoryStream encrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(MemoryStream data, out FileStream encrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(FileStream data, out byte[] encryptedData)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(FileStream data, out MemoryStream encrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Encrypt(FileStream data, out FileStream encrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(byte[] data, out byte[] decryptedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(byte[] data, out MemoryStream decrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(byte[] data, out FileStream decrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(MemoryStream data, out byte[] decryptedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(MemoryStream data, out MemoryStream decrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(MemoryStream data, out FileStream decrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(FileStream data, out byte[] decryptedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(FileStream data, out MemoryStream decrypdedData)
        {
            throw new NotImplementedException();
        }

        public void Decrypt(FileStream data, out FileStream decrypdedData)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
