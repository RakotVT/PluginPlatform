﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using PluginPlatform.Server.Data.Models;

namespace PluginPlatform.Server.Data
{
    public partial class UmsContext : DbContext
    {
        public UmsContext()
            : base("UmsContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserLicense>()
                .HasMany(c => c.PluginLicenses)
                .WithMany(p => p.UserLicenses)
                .Map(m =>
                {
                    m.MapLeftKey("UserLicenseId");
                    m.MapRightKey("PluginLicenseId");
                    m.ToTable("User2PluginLicenses");
                });
        }

        public virtual DbSet<License> Licenses { get; set; }
        public virtual DbSet<PluginFile> PluginFiles { get; set; }
        public virtual DbSet<Plugin> Plugins { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserSetting> UserSettings { get; set; }
        public virtual DbSet<UserTicket> UserTickets { get; set; }
        public virtual DbSet<Version> Versions { get; set; }
        public virtual DbSet<LicenseTemplate> LicenseTemplates { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserClaim> UserClaims { get; set; }
        public virtual DbSet<UserLogin> UserLogins { get; set; }
        public virtual DbSet<LogMessageSource> LogMessageSources { get; set; }
        public virtual DbSet<LogMinidump> LogMinidumps { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<LogScreenshot> LogScreenshots { get; set; }
    }
}
