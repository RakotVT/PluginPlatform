using System;
using System.Collections.Generic;

namespace PluginPlatform.Server.Data.Models
{
    public partial class Log
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> MessageSource { get; set; }
        public string MessageType { get; set; }
        public string MessageLevel { get; set; }
        public string StackTrace { get; set; }
        public string UserMessage { get; set; }
        public Nullable<int> Minidump { get; set; }
        public Nullable<int> Screenshot { get; set; }
        public Nullable<int> UserMessageId { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorLocation { get; set; }
        public virtual LogMessageSource LogMessageSource { get; set; }
        public virtual LogMinidump LogMinidump { get; set; }
        public virtual LogScreenshot LogScreenshot { get; set; }
        public virtual UserTicket UserTicket { get; set; }
    }
}
