using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginPlatform.Server.Data.Models
{
    public partial class PluginFile
    {
        public int Id { get; set; }
        public byte[] Content { get; set; }
        public string Hash { get; set; }
        public bool IsMainFile { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }
        public Nullable<int> VersionId { get; set; }
        [ForeignKey("VersionId")]
        public virtual BuildVersion BuildVersion { get; set; }
    }
}
