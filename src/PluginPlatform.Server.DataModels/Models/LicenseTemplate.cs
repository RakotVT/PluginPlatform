using System;
using System.Collections.Generic;

namespace PluginPlatform.Server.Data.Models
{
    public partial class LicenseTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public Nullable<decimal> Price { get; set; }
        public bool Timeless { get; set; }
        public bool CheckOnline { get; set; }
        public int VersionId { get; set; }
        public virtual MajorVersion MajorVersion { get; set; }
    }
}
