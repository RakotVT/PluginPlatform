using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginPlatform.Server.Data.Models
{
    [Table("UserLicenses")]
    public partial class UserLicense : License
    {
        public UserLicense()
        {
            this.PluginLicenses = new List<PluginLicense>();
        }

        public bool IsActivated { get; set; }
        public string SerialNumber { get; set; }
        public Nullable<int> UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<PluginLicense> PluginLicenses { get; set; }
    }
}
