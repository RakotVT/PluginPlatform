using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginPlatform.Server.Data.Models
{
    public partial class Plugin
    {
        public Plugin()
        {
            this.MajorVersions = new List<MajorVersion>();
            this.UserSettings = new List<UserSetting>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? DeveloperId { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public virtual ICollection<MajorVersion> MajorVersions { get; set; }
        [ForeignKey("DeveloperId")]
        public virtual User User { get; set; }
        public virtual ICollection<UserSetting> UserSettings { get; set; }
    }
}
