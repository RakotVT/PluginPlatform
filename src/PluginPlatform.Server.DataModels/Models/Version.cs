using System;
using System.Collections.Generic;

namespace PluginPlatform.Server.Data.Models
{
    public abstract partial class Version
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool IsPublished { get; set; }
    }
}
