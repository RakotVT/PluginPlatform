using System;
using System.Collections.Generic;

namespace PluginPlatform.Server.Data.Models
{
    public partial class LogMinidump
    {
        public LogMinidump()
        {
            this.Logs = new List<Log>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public virtual ICollection<Log> Logs { get; set; }
    }
}
