using System;
using System.Collections.Generic;

namespace PluginPlatform.Server.Data.Models
{
    public partial class UserTicket
    {
        public UserTicket()
        {
            this.Logs = new List<Log>();
        }

        public int id { get; set; }
        public Nullable<int> users_id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public virtual ICollection<Log> Logs { get; set; }
    }
}
