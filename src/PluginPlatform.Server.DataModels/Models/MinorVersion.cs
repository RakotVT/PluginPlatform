using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginPlatform.Server.Data.Models
{
    [Table("MinorVersions")]
    public partial class MinorVersion : Version
    {
        public MinorVersion()
        {
            this.BuildVersions = new List<BuildVersion>();
        }

        public int MajorVersionId { get; set; }
        public virtual ICollection<BuildVersion> BuildVersions { get; set; }
        public virtual MajorVersion MajorVersion { get; set; }
    }
}
