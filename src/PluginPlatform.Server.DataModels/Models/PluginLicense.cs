using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginPlatform.Server.Data.Models
{
    [Table("PluginLicenses")]
    public partial class PluginLicense : License
    {
        public PluginLicense()
        {
            this.MajorVersions = new List<MajorVersion>();
            this.UserLicenses = new List<UserLicense>();
        }

        public bool Timeless { get; set; }
        public int PluginId { get; set; }
        public string PluginName { get; set; }
        public virtual ICollection<MajorVersion> MajorVersions { get; set; }
        public virtual ICollection<UserLicense> UserLicenses { get; set; }
    }
}
