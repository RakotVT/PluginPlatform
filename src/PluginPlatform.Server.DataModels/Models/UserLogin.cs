using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginPlatform.Server.Data.Models
{
    public partial class UserLogin
    {
        [Key, Column(Order = 1)]
        public string LoginProvider { get; set; }
        [Key, Column(Order = 2)]
        public string ProviderKey { get; set; }
        [Key, Column(Order = 3)]
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
