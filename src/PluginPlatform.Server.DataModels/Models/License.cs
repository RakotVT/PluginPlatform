using System;
using System.Collections.Generic;

namespace PluginPlatform.Server.Data.Models
{
    public abstract partial class License
    {
        public int Id { get; set; }
        public Guid UniqueKey { get; set; }
        public decimal? Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public byte[] EULA { get; set; }
    }
}
