using System;
using System.Collections.Generic;

namespace PluginPlatform.Server.Data.Models
{
    public partial class UserSetting
    {
        public int Id { get; set; }
        public string Setting { get; set; }
        public string Value { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> PluginId { get; set; }
    }
}
