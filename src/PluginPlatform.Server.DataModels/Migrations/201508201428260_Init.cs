namespace PluginPlatform.Server.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Licenses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UniqueKey = c.Guid(nullable: false),
                        Price = c.Decimal(precision: 18, scale: 2),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        EULA = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Versions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Description = c.String(),
                        Name = c.String(),
                        IsPublished = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LicenseTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Duration = c.Int(nullable: false),
                        Price = c.Decimal(precision: 18, scale: 2),
                        Timeless = c.Boolean(nullable: false),
                        CheckOnline = c.Boolean(nullable: false),
                        VersionId = c.Int(nullable: false),
                        MajorVersion_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MajorVersions", t => t.MajorVersion_Id)
                .Index(t => t.MajorVersion_Id);
            
            CreateTable(
                "dbo.PluginFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.Binary(),
                        Hash = c.String(),
                        IsMainFile = c.Boolean(nullable: false),
                        Name = c.String(),
                        RelativePath = c.String(),
                        VersionId = c.Int(),
                        BuildVersion_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BuildVersions", t => t.BuildVersion_Id)
                .Index(t => t.BuildVersion_Id);
            
            CreateTable(
                "dbo.Plugins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DeveloperId = c.Int(),
                        Caption = c.String(),
                        Description = c.String(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Patronymic = c.String(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Setting = c.String(),
                        Value = c.String(),
                        UserId = c.Int(),
                        PluginId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Plugins", t => t.PluginId)
                .Index(t => t.PluginId);
            
            CreateTable(
                "dbo.LogMessageSources",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(),
                        MessageSource = c.Int(),
                        MessageType = c.String(),
                        MessageLevel = c.String(),
                        StackTrace = c.String(),
                        UserMessage = c.String(),
                        Minidump = c.Int(),
                        Screenshot = c.Int(),
                        UserMessageId = c.Int(),
                        ErrorMessage = c.String(),
                        ErrorLocation = c.String(),
                        LogMessageSource_Id = c.Int(),
                        LogMinidump_Id = c.Int(),
                        LogScreenshot_Id = c.Int(),
                        UserTicket_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LogMessageSources", t => t.LogMessageSource_Id)
                .ForeignKey("dbo.LogMinidumps", t => t.LogMinidump_Id)
                .ForeignKey("dbo.LogScreenshots", t => t.LogScreenshot_Id)
                .ForeignKey("dbo.UserTickets", t => t.UserTicket_id)
                .Index(t => t.LogMessageSource_Id)
                .Index(t => t.LogMinidump_Id)
                .Index(t => t.LogScreenshot_Id)
                .Index(t => t.UserTicket_id);
            
            CreateTable(
                "dbo.LogMinidumps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LogScreenshots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserTickets",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        users_id = c.Int(),
                        date = c.DateTime(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.RoleUsers",
                c => new
                    {
                        Role_Id = c.Int(nullable: false),
                        User_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_Id, t.User_Id })
                .ForeignKey("dbo.Roles", t => t.Role_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Role_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.UserLicensePluginLicenses",
                c => new
                    {
                        UserLicense_Id = c.Int(nullable: false),
                        PluginLicense_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserLicense_Id, t.PluginLicense_Id })
                .ForeignKey("dbo.UserLicenses", t => t.UserLicense_Id)
                .ForeignKey("dbo.PluginLicense", t => t.PluginLicense_Id)
                .Index(t => t.UserLicense_Id)
                .Index(t => t.PluginLicense_Id);
            
            CreateTable(
                "dbo.PluginLicense",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Timeless = c.Boolean(nullable: false),
                        PluginId = c.Int(nullable: false),
                        PluginName = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Licenses", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.MajorVersions",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PluginLicens_Id = c.Int(),
                        PluginId = c.Int(nullable: false),
                        LicenseId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Versions", t => t.Id)
                .ForeignKey("dbo.PluginLicense", t => t.PluginLicens_Id)
                .ForeignKey("dbo.Plugins", t => t.PluginId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.PluginLicens_Id)
                .Index(t => t.PluginId);
            
            CreateTable(
                "dbo.MinorVersions",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        MajorVersionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Versions", t => t.Id)
                .ForeignKey("dbo.MajorVersions", t => t.MajorVersionId)
                .Index(t => t.Id)
                .Index(t => t.MajorVersionId);
            
            CreateTable(
                "dbo.BuildVersions",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        MinorVersionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Versions", t => t.Id)
                .ForeignKey("dbo.MinorVersions", t => t.MinorVersionId)
                .Index(t => t.Id)
                .Index(t => t.MinorVersionId);
            
            CreateTable(
                "dbo.UserLicenses",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        IsActivated = c.Boolean(nullable: false),
                        SerialNumber = c.String(),
                        UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Licenses", t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.Id)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserLicenses", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLicenses", "Id", "dbo.Licenses");
            DropForeignKey("dbo.BuildVersions", "MinorVersionId", "dbo.MinorVersions");
            DropForeignKey("dbo.BuildVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.MinorVersions", "MajorVersionId", "dbo.MajorVersions");
            DropForeignKey("dbo.MinorVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.MajorVersions", "PluginId", "dbo.Plugins");
            DropForeignKey("dbo.MajorVersions", "PluginLicens_Id", "dbo.PluginLicense");
            DropForeignKey("dbo.MajorVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.PluginLicense", "Id", "dbo.Licenses");
            DropForeignKey("dbo.Logs", "UserTicket_id", "dbo.UserTickets");
            DropForeignKey("dbo.Logs", "LogScreenshot_Id", "dbo.LogScreenshots");
            DropForeignKey("dbo.Logs", "LogMinidump_Id", "dbo.LogMinidumps");
            DropForeignKey("dbo.Logs", "LogMessageSource_Id", "dbo.LogMessageSources");
            DropForeignKey("dbo.UserSettings", "PluginId", "dbo.Plugins");
            DropForeignKey("dbo.UserLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLicensePluginLicenses", "PluginLicense_Id", "dbo.PluginLicense");
            DropForeignKey("dbo.UserLicensePluginLicenses", "UserLicense_Id", "dbo.UserLicenses");
            DropForeignKey("dbo.UserClaims", "UserId", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.Plugins", "User_Id", "dbo.Users");
            DropForeignKey("dbo.PluginFiles", "BuildVersion_Id", "dbo.BuildVersions");
            DropForeignKey("dbo.LicenseTemplates", "MajorVersion_Id", "dbo.MajorVersions");
            DropIndex("dbo.UserLicenses", new[] { "UserId" });
            DropIndex("dbo.UserLicenses", new[] { "Id" });
            DropIndex("dbo.BuildVersions", new[] { "MinorVersionId" });
            DropIndex("dbo.BuildVersions", new[] { "Id" });
            DropIndex("dbo.MinorVersions", new[] { "MajorVersionId" });
            DropIndex("dbo.MinorVersions", new[] { "Id" });
            DropIndex("dbo.MajorVersions", new[] { "PluginId" });
            DropIndex("dbo.MajorVersions", new[] { "PluginLicens_Id" });
            DropIndex("dbo.MajorVersions", new[] { "Id" });
            DropIndex("dbo.PluginLicense", new[] { "Id" });
            DropIndex("dbo.UserLicensePluginLicenses", new[] { "PluginLicense_Id" });
            DropIndex("dbo.UserLicensePluginLicenses", new[] { "UserLicense_Id" });
            DropIndex("dbo.RoleUsers", new[] { "User_Id" });
            DropIndex("dbo.RoleUsers", new[] { "Role_Id" });
            DropIndex("dbo.Logs", new[] { "UserTicket_id" });
            DropIndex("dbo.Logs", new[] { "LogScreenshot_Id" });
            DropIndex("dbo.Logs", new[] { "LogMinidump_Id" });
            DropIndex("dbo.Logs", new[] { "LogMessageSource_Id" });
            DropIndex("dbo.UserSettings", new[] { "PluginId" });
            DropIndex("dbo.UserLogins", new[] { "UserId" });
            DropIndex("dbo.UserClaims", new[] { "UserId" });
            DropIndex("dbo.Plugins", new[] { "User_Id" });
            DropIndex("dbo.PluginFiles", new[] { "BuildVersion_Id" });
            DropIndex("dbo.LicenseTemplates", new[] { "MajorVersion_Id" });
            DropTable("dbo.UserLicenses");
            DropTable("dbo.BuildVersions");
            DropTable("dbo.MinorVersions");
            DropTable("dbo.MajorVersions");
            DropTable("dbo.PluginLicense");
            DropTable("dbo.UserLicensePluginLicenses");
            DropTable("dbo.RoleUsers");
            DropTable("dbo.UserTickets");
            DropTable("dbo.LogScreenshots");
            DropTable("dbo.LogMinidumps");
            DropTable("dbo.Logs");
            DropTable("dbo.LogMessageSources");
            DropTable("dbo.UserSettings");
            DropTable("dbo.UserLogins");
            DropTable("dbo.UserClaims");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.Plugins");
            DropTable("dbo.PluginFiles");
            DropTable("dbo.LicenseTemplates");
            DropTable("dbo.Versions");
            DropTable("dbo.Licenses");
        }
    }
}
