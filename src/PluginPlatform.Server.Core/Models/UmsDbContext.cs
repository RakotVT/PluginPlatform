﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PluginPlatform.ServicesCore.Models
{
    public class UmsDbContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public Guid InstanceId { get; set; }

        public UmsDbContext()
            : base("UmsDbContext")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            InstanceId = Guid.NewGuid();
        }

        public static UmsDbContext Create()
        {
            return new UmsDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles")
                .HasMany(r => r.Plugins)
                .WithMany(p => p.Roles)
                .Map(m => m.ToTable("RolePlugins")
                        .MapLeftKey("RoleId")
                        .MapRightKey("PluginId"));
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims");
            //modelBuilder.Entity<RolePlugin>().HasKey(r => new { r.RoleId, r.PluginId }).ToTable("RolePlugins");
        }

        public virtual DbSet<PluginFile> PluginFiles { get; set; }
        public virtual DbSet<Plugin> Plugins { get; set; }
        public virtual DbSet<Version> Versions { get; set; }
        public virtual DbSet<UserTicket> UserTickets { get; set; }
        public virtual DbSet<LogMessageSource> LogMessageSources { get; set; }
        public virtual DbSet<LogMinidump> LogMinidumps { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<LogScreenshot> LogScreenshots { get; set; }
    }
}
