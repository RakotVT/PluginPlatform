using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginPlatform.ServicesCore.Models
{
    public partial class Plugin
    {
        public Plugin()
        {
            Versions = new List<Version>();
            Roles = new List<Role>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? DeveloperId { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Version> Versions { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
