﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITWebNet.UMS.ServicesCore.Models
{
    public class RolePlugin
    {
        public int RoleId { get; set; }
        public int PluginId { get; set; }
    }
}
