﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginPlatform.ServicesCore.Models
{
    public partial class FileContent
    {
        [Key, ForeignKey("File")]
        public int Id { get; set; }
        public byte[] Data { get; set; }

        public PluginFile File { get; set; }
    }
}
