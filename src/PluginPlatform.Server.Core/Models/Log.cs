using System;
using System.Collections.Generic;

namespace PluginPlatform.ServicesCore.Models
{
    public partial class Log
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string MessageType { get; set; }
        public string MessageLevel { get; set; }
        public string StackTrace { get; set; }
        public string UserMessage { get; set; }
        public int? LogMessageSourceId { get; set; }
        public int? LogMinidumpId { get; set; }
        public int? LogScreenshotId { get; set; }
        public int? UserTicketId { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorLocation { get; set; }
        public virtual LogMessageSource LogMessageSource { get; set; }
        public virtual LogMinidump LogMinidump { get; set; }
        public virtual LogScreenshot LogScreenshot { get; set; }
        public virtual UserTicket UserTicket { get; set; }
    }
}
