﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PluginPlatform.ServicesCore.Models
{
    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public bool IsADUser { get; set; }
        public string UserDomain { get; set; }
    }
}