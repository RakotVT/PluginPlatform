using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginPlatform.ServicesCore.Models
{
    public partial class PluginFile
    {
        [ForeignKey("Content")]
        public int Id { get; set; }
        public string Hash { get; set; }
        public bool IsMainFile { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }

        [ForeignKey("Version")]
        public int VersionId { get; set; }
        public virtual Version Version { get; set; }

        public virtual FileContent Content { get; set; }
    }
}
