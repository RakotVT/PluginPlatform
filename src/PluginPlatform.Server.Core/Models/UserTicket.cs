using System;
using System.Collections.Generic;

namespace PluginPlatform.ServicesCore.Models
{
    public partial class UserTicket
    {
        public UserTicket()
        {
            this.Logs = new List<Log>();
        }

        public int Id { get; set; }
        public int? UserId { get; set; }
        public DateTime? Date { get; set; }
        public virtual ICollection<Log> Logs { get; set; }
        public User User { get; set; }
    }
}
