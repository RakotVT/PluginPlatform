﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PluginPlatform.ServicesCore.Models
{
    public class Role : IdentityRole<int, UserRole>
    {
        public Role()
        {
            Plugins = new List<Plugin>();
        }
        public string Description { get; set; }

        public virtual ICollection<Plugin> Plugins { get; set; }
    }
}