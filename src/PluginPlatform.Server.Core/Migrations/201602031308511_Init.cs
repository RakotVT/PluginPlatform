namespace PluginPlatform.ServicesCore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogMessageSources",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Logs",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Date = c.DateTime(),
                    MessageType = c.String(),
                    MessageLevel = c.String(),
                    StackTrace = c.String(),
                    UserMessage = c.String(),
                    LogMessageSourceId = c.Int(),
                    LogMinidumpId = c.Int(),
                    LogScreenshotId = c.Int(),
                    UserTicketId = c.Int(),
                    ErrorMessage = c.String(),
                    ErrorLocation = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LogMessageSources", t => t.LogMessageSourceId)
                .ForeignKey("dbo.LogMinidumps", t => t.LogMinidumpId)
                .ForeignKey("dbo.LogScreenshots", t => t.LogScreenshotId)
                .ForeignKey("dbo.UserTickets", t => t.UserTicketId)
                .Index(t => t.LogMessageSourceId)
                .Index(t => t.LogMinidumpId)
                .Index(t => t.LogScreenshotId)
                .Index(t => t.UserTicketId);

            CreateTable(
                "dbo.LogMinidumps",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    Content = c.Binary(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.LogScreenshots",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    Content = c.Binary(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.UserTickets",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserId = c.Int(),
                    Date = c.DateTime(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.Users",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    FirstName = c.String(),
                    LastName = c.String(),
                    Patronymic = c.String(),
                    IsADUser = c.Boolean(nullable: false, defaultValue: false),
                    UserDomain = c.String(),
                    Email = c.String(maxLength: 256),
                    EmailConfirmed = c.Boolean(nullable: false, defaultValue: false),
                    PasswordHash = c.String(),
                    SecurityStamp = c.String(),
                    PhoneNumber = c.String(),
                    PhoneNumberConfirmed = c.Boolean(nullable: false, defaultValue: false),
                    TwoFactorEnabled = c.Boolean(nullable: false, defaultValue: false),
                    LockoutEndDateUtc = c.DateTime(),
                    LockoutEnabled = c.Boolean(nullable: false, defaultValue: false),
                    AccessFailedCount = c.Int(nullable: false),
                    UserName = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");

            CreateTable(
                "dbo.UserClaims",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserId = c.Int(nullable: false),
                    ClaimType = c.String(),
                    ClaimValue = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.UserLogins",
                c => new
                {
                    LoginProvider = c.String(nullable: false, maxLength: 128),
                    ProviderKey = c.String(nullable: false, maxLength: 128),
                    UserId = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.UserRoles",
                c => new
                {
                    UserId = c.Int(nullable: false),
                    RoleId = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                "dbo.PluginFiles",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Content = c.Binary(),
                    Hash = c.String(),
                    IsMainFile = c.Boolean(nullable: false, defaultValue: false),
                    Name = c.String(),
                    RelativePath = c.String(),
                    VersionId = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BuildVersions", t => t.VersionId)
                .Index(t => t.VersionId);

            CreateTable(
                "dbo.Versions",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Number = c.Int(nullable: false),
                    Description = c.String(),
                    Name = c.String(),
                    IsPublished = c.Boolean(nullable: false, defaultValue: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Plugins",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    DeveloperId = c.Int(),
                    Caption = c.String(),
                    Description = c.String(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Roles",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Description = c.String(),
                    Name = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");

            CreateTable(
                "dbo.RolePlugins",
                c => new
                {
                    RoleId = c.Int(nullable: false),
                    PluginId = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.RoleId, t.PluginId })
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Plugins", t => t.PluginId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.PluginId);

            CreateTable(
                "dbo.BuildVersions",
                c => new
                {
                    Id = c.Int(nullable: false),
                    MinorVersionId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Versions", t => t.Id)
                .ForeignKey("dbo.MinorVersions", t => t.MinorVersionId)
                .Index(t => t.Id)
                .Index(t => t.MinorVersionId);

            CreateTable(
                "dbo.MinorVersions",
                c => new
                {
                    Id = c.Int(nullable: false),
                    MajorVersionId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Versions", t => t.Id)
                .ForeignKey("dbo.MajorVersions", t => t.MajorVersionId)
                .Index(t => t.Id)
                .Index(t => t.MajorVersionId);

            CreateTable(
                "dbo.MajorVersions",
                c => new
                {
                    Id = c.Int(nullable: false),
                    PluginId = c.Int(nullable: false),
                    LicenseId = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Versions", t => t.Id)
                .ForeignKey("dbo.Plugins", t => t.PluginId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.PluginId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.MajorVersions", "PluginId", "dbo.Plugins");
            DropForeignKey("dbo.MajorVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.MinorVersions", "MajorVersionId", "dbo.MajorVersions");
            DropForeignKey("dbo.MinorVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.BuildVersions", "MinorVersionId", "dbo.MinorVersions");
            DropForeignKey("dbo.BuildVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.PluginFiles", "VersionId", "dbo.BuildVersions");
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RolePlugins", "PluginId", "dbo.Plugins");
            DropForeignKey("dbo.RolePlugins", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.UserTickets", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserClaims", "UserId", "dbo.Users");
            DropForeignKey("dbo.Logs", "UserTicketId", "dbo.UserTickets");
            DropForeignKey("dbo.Logs", "LogScreenshotId", "dbo.LogScreenshots");
            DropForeignKey("dbo.Logs", "LogMinidumpId", "dbo.LogMinidumps");
            DropForeignKey("dbo.Logs", "LogMessageSourceId", "dbo.LogMessageSources");
            DropIndex("dbo.MajorVersions", new[] { "PluginId" });
            DropIndex("dbo.MajorVersions", new[] { "Id" });
            DropIndex("dbo.MinorVersions", new[] { "MajorVersionId" });
            DropIndex("dbo.MinorVersions", new[] { "Id" });
            DropIndex("dbo.BuildVersions", new[] { "MinorVersionId" });
            DropIndex("dbo.BuildVersions", new[] { "Id" });
            DropIndex("dbo.RolePlugins", new[] { "PluginId" });
            DropIndex("dbo.RolePlugins", new[] { "RoleId" });
            DropIndex("dbo.Roles", "RoleNameIndex");
            DropIndex("dbo.PluginFiles", new[] { "VersionId" });
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.UserLogins", new[] { "UserId" });
            DropIndex("dbo.UserClaims", new[] { "UserId" });
            DropIndex("dbo.Users", "UserNameIndex");
            DropIndex("dbo.UserTickets", new[] { "UserId" });
            DropIndex("dbo.Logs", new[] { "UserTicketId" });
            DropIndex("dbo.Logs", new[] { "LogScreenshotId" });
            DropIndex("dbo.Logs", new[] { "LogMinidumpId" });
            DropIndex("dbo.Logs", new[] { "LogMessageSourceId" });
            DropTable("dbo.MajorVersions");
            DropTable("dbo.MinorVersions");
            DropTable("dbo.BuildVersions");
            DropTable("dbo.RolePlugins");
            DropTable("dbo.Roles");
            DropTable("dbo.Plugins");
            DropTable("dbo.Versions");
            DropTable("dbo.PluginFiles");
            DropTable("dbo.UserRoles");
            DropTable("dbo.UserLogins");
            DropTable("dbo.UserClaims");
            DropTable("dbo.Users");
            DropTable("dbo.UserTickets");
            DropTable("dbo.LogScreenshots");
            DropTable("dbo.LogMinidumps");
            DropTable("dbo.Logs");
            DropTable("dbo.LogMessageSources");
        }
    }
}
