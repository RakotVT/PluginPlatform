namespace PluginPlatform.ServicesCore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class OptimizeVersions : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PluginFiles", "VersionId", "dbo.BuildVersions");
            DropForeignKey("dbo.BuildVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.BuildVersions", "MinorVersionId", "dbo.MinorVersions");
            DropForeignKey("dbo.MinorVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.MinorVersions", "MajorVersionId", "dbo.MajorVersions");
            DropForeignKey("dbo.MajorVersions", "Id", "dbo.Versions");
            DropForeignKey("dbo.MajorVersions", "PluginId", "dbo.Plugins");
            DropIndex("dbo.PluginFiles", new[] { "VersionId" });
            DropIndex("dbo.BuildVersions", new[] { "Id" });
            DropIndex("dbo.BuildVersions", new[] { "MinorVersionId" });
            DropIndex("dbo.MinorVersions", new[] { "Id" });
            DropIndex("dbo.MinorVersions", new[] { "MajorVersionId" });
            DropIndex("dbo.MajorVersions", new[] { "Id" });
            DropIndex("dbo.MajorVersions", new[] { "PluginId" });
            MoveTable("dbo.Versions", "bak");
            MoveTable("dbo.BuildVersions", "bak");
            MoveTable("dbo.MinorVersions", "bak");
            MoveTable("dbo.MajorVersions", "bak");

            CreateTable(
                "dbo.Versions",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Major = c.Int(nullable: false),
                    Minor = c.Int(nullable: false),
                    Build = c.Int(nullable: false),
                    Description = c.String(),
                    Name = c.String(),
                    IsPublished = c.Boolean(nullable: false, defaultValue: false),
                    PluginId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Plugins", t => t.PluginId, cascadeDelete: true)
                .Index(t => t.PluginId);

            SqlResource("PluginPlatform.ServicesCore.Migrations.Scripts.OptimizeVersions.sql");

            AlterColumn("dbo.PluginFiles", "VersionId", c => c.Int(nullable: false));
            CreateIndex("dbo.PluginFiles", "VersionId");
            AddForeignKey("dbo.PluginFiles", "VersionId", "dbo.Versions", "Id", cascadeDelete: true);
            DropTable("bak.Versions");
            DropTable("bak.BuildVersions");
            DropTable("bak.MinorVersions");
            DropTable("bak.MajorVersions");
        }

        public override void Down()
        {
            CreateTable(
                "dbo.MajorVersions",
                c => new
                {
                    Id = c.Int(nullable: false),
                    PluginId = c.Int(nullable: false),
                    LicenseId = c.Int(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.MinorVersions",
                c => new
                {
                    Id = c.Int(nullable: false),
                    MajorVersionId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.BuildVersions",
                c => new
                {
                    Id = c.Int(nullable: false),
                    MinorVersionId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Versions",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Number = c.Int(nullable: false),
                    Description = c.String(),
                    Name = c.String(),
                    IsPublished = c.Boolean(nullable: false, defaultValue: false),
                })
                .PrimaryKey(t => t.Id);

            DropForeignKey("dbo.PluginFiles", "VersionId", "dbo.Versions");
            DropForeignKey("dbo.Versions", "PluginId", "dbo.Plugins");
            DropIndex("dbo.Versions", new[] { "PluginId" });
            DropIndex("dbo.PluginFiles", new[] { "VersionId" });
            AlterColumn("dbo.PluginFiles", "VersionId", c => c.Int());
            DropTable("dbo.Versions");
            CreateIndex("dbo.MajorVersions", "PluginId");
            CreateIndex("dbo.MajorVersions", "Id");
            CreateIndex("dbo.MinorVersions", "MajorVersionId");
            CreateIndex("dbo.MinorVersions", "Id");
            CreateIndex("dbo.BuildVersions", "MinorVersionId");
            CreateIndex("dbo.BuildVersions", "Id");
            CreateIndex("dbo.PluginFiles", "VersionId");
            AddForeignKey("dbo.MajorVersions", "PluginId", "dbo.Plugins", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MajorVersions", "Id", "dbo.Versions", "Id");
            AddForeignKey("dbo.MinorVersions", "MajorVersionId", "dbo.MajorVersions", "Id");
            AddForeignKey("dbo.MinorVersions", "Id", "dbo.Versions", "Id");
            AddForeignKey("dbo.BuildVersions", "MinorVersionId", "dbo.MinorVersions", "Id");
            AddForeignKey("dbo.BuildVersions", "Id", "dbo.Versions", "Id");
            AddForeignKey("dbo.PluginFiles", "VersionId", "dbo.BuildVersions", "Id");
        }
    }
}
