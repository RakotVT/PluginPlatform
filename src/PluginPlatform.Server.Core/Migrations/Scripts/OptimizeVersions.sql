SET IDENTITY_INSERT [dbo].[Versions] ON
INSERT INTO [dbo].[Versions]
    ([Id],
    [Major],
    [Minor],
    [Build],
    [Description],
    [Name],
    [IsPublished],
    [PluginId])
SELECT DISTINCT build.Id, majv.Number, minv.Number, bv.Number, bv.Description, bv.Name, bv.IsPublished, plugin.Id 
    FROM [dbo].[PluginFiles] pf
    INNER JOIN [bak].[BuildVersions] build ON pf.VersionId = build.Id
    INNER JOIN [bak].[MinorVersions] minor ON build.MinorVersionId = minor.Id
    INNER JOIN [bak].[MajorVersions] major ON minor.MajorVersionId = major.Id
    INNER JOIN [dbo].[Plugins] plugin ON major.PluginId = plugin.Id
    INNER JOIN [bak].[Versions] bv ON build.Id = bv.Id
    INNER JOIN [bak].[Versions] minv ON minor.Id = minv.Id
    INNER JOIN [bak].[Versions] majv ON major.Id = majv.Id
SET IDENTITY_INSERT [dbo].[Versions] OFF

DELETE 
  FROM [dbo].[PluginFiles]
  WHERE VersionId is NULL

