namespace PluginPlatform.ServicesCore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class SeparateFileContent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileContents",
                c => new
                {
                    Id = c.Int(nullable: false),
                    Data = c.Binary(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PluginFiles", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);

            SqlResource("PluginPlatform.ServicesCore.Migrations.Scripts.SeparateFileContent.sql");

            DropColumn("dbo.PluginFiles", "Content");
        }

        public override void Down()
        {
            AddColumn("dbo.PluginFiles", "Content", c => c.Binary());
            DropForeignKey("dbo.FileContents", "Id", "dbo.PluginFiles");
            DropIndex("dbo.FileContents", new[] { "Id" });
            DropTable("dbo.FileContents");
        }
    }
}
