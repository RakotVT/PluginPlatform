// <auto-generated />
namespace PluginPlatform.ServicesCore.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class SeparateFileContent : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SeparateFileContent));
        
        string IMigrationMetadata.Id
        {
            get { return "201602091601068_SeparateFileContent"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
