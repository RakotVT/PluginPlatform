namespace PluginPlatform.ServicesCore.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;

    // ���������� �������� �� ������
    //Update-Database -Verbose -StartUpProjectName ums.ServicesCore -ProjectName ums.ServicesCore -ConnectionStringName <�����>
    internal sealed class Configuration : DbMigrationsConfiguration<PluginPlatform.ServicesCore.Models.UmsDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PluginPlatform.ServicesCore.Models.UmsDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
