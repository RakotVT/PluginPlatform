﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace PluginPlatform.Common.DataModels
{
    [DataContract]
    public enum ServiceType
    {
        [EnumMember]
        PluginService,
        [EnumMember]
        AuthorizationService,
        [EnumMember]
        LogService,
        [EnumMember]
        LicensingService
    }
}
