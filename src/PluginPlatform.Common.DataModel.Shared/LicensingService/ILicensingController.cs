﻿using PluginPlatform.Common.DataModels.LicensingService;

namespace PluginPlatform.Common.DataModels.LicensingService
{
    public interface ILicensingController
    {
        LicenseInfo ActivateLicense(string activationKey);
        int[] GetAvailablePlugins();
    }
}