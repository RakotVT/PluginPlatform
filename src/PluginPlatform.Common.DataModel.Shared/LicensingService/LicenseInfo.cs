﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PluginPlatform.Common.DataModels.LicensingService
{
    [DataContract]
    public class LicenseInfo
    {
        #region Properties

        /// <summary>
        /// Дата окончания действия лицензии. 
        /// </summary>
        [DataMember]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Номер активации. 
        /// </summary>
        [DataMember]
        public Guid Number { get; set; }

        /// <summary>
        /// Собственник лицензии. 
        /// </summary>
        [DataMember]
        public string Owner { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        /// <summary>
        /// Информация о лицензии плагинов.
        /// </summary>
        [DataMember]
        public List<PluginLicenseInfo> PluginsInfo { get; set; }

        /// <summary>
        /// Дата начала действия лицензии. 
        /// </summary>
        [DataMember]
        public DateTime StartDate { get; set; }

        #endregion Properties
    }

}
