﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PluginPlatform.Common.DataModels.LicensingService
{
    [DataContract]
    public enum TermTypeEnum : int
    {
        [EnumMember]
        Trial = 1,

        [EnumMember]
        Unfixed,

        [EnumMember]
        FixedDate,

        [EnumMember]
        Free
    }

    [DataContract]
    public enum CheckResult
    {
        /// <summary>
        /// Лицензия верна.
        /// </summary>
        [EnumMember]
        LicenseValid = 0,

        /// <summary>
        /// Лицензия не прошла проверку.
        /// </summary>
        [EnumMember]
        LicenseNotValid = 1,

        /// <summary>
        /// Срок действия лицензии истек.
        /// </summary>
        [EnumMember]
        LisenceExpired = 2,
    }
}
