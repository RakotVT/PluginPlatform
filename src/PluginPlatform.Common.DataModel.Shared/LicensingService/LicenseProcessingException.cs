﻿using System.Runtime.Serialization;

namespace PluginPlatform.Common.DataModels.LicensingService
{
    /// <summary>
    /// Используется для предоставления клиентскому приложению информации об ошибке обработки лицензии.
    /// </summary>
    [System.Serializable]
    public class LicenseProcessingException : System.Exception
    {
        public LicenseProcessingException() { }
        public LicenseProcessingException(string message) : base(message) { }
        public LicenseProcessingException(string message, System.Exception inner) : base(message, inner) { }
        protected LicenseProcessingException(
          SerializationInfo info,
          StreamingContext context) : base(info, context)
        { }
    }
}
