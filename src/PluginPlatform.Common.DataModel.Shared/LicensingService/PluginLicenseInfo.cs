﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PluginPlatform.Common.DataModels.LicensingService
{
    [DataContract]
    public class PluginLicenseInfo
    {
        #region Public Properties

        /// <summary>
        /// Дата окончания действия лицензии. 
        /// </summary>
        [DataMember]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Имя плагина.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Номер активации. 
        /// </summary>
        [DataMember]
        public Guid Number { get; set; }

        /// <summary>
        /// Дата начала действия лицензии. 
        /// </summary>
        [DataMember]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Флаг, показыващий является ли лицензия бессрочной.
        /// </summary>
        [DataMember]
        public bool Timeless { get; set; }

        /// <summary>
        /// Версия плагина.
        /// </summary>
        [DataMember]
        public string Version { get; set; }

        #endregion Public Properties
    }
}
