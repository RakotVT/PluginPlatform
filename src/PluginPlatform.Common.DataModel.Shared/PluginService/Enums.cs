﻿using System.Runtime.Serialization;

namespace PluginPlatform.Common.DataModels.PluginService
{
    [DataContract(Name = "UpdateAction")]
    public enum UpdateAction
    {
        [EnumMember]
        Add,

        [EnumMember]
        Overwrite,

        [EnumMember]
        Remove,

        [EnumMember]
        Rename
    }
}
