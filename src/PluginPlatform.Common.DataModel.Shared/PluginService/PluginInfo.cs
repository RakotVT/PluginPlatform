﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PluginPlatform.Common.DataModels.PluginService
{
    /// <summary>
    /// Предоставляет базовую инофрмацию о плагине. 
    /// </summary>
    [DataContract]
    public class PluginInfo
    {
        #region Public Properties

        [DataMember]
        public string Caption { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string LatestVersion { get; set; }

        [DataMember]
        public List<string> AllVersions { get; set; }

        [DataMember]
        public string MainFileName { get; set; }

        [DataMember]
        public string Name { get; set; }

        #endregion Public Properties
    }
}
