﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PluginPlatform.Common.DataModels.PluginService
{
    public interface IShellController
    {
        UpdateEntry GetInstaller(string currentVersion = null);
        bool IsShellUpdatesAvaliable(string currentVersion);
        PluginInfo GetShellInfo();
    }
}
