﻿using System.Collections.Generic;

namespace PluginPlatform.Common.DataModels.PluginService
{
    public interface IPluginsController
    {
        PluginInfo GetPluginInfo(int pluginId = 0, string pluginName = null);
        List<UpdateEntry> GetPluginLatestUpdates(int pluginId);
        List<UpdateEntry> GetPluginUpdates(int pluginId, string currentVersion);
        bool IsUpdatesAvailable(int pluginId, string currentVersion);
    }
}