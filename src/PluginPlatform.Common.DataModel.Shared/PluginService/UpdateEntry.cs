﻿using System.Runtime.Serialization;

namespace PluginPlatform.Common.DataModels.PluginService
{
    /// <summary>
    /// Файл обновления 
    /// </summary>
    [DataContract]
    public class UpdateEntry
    {
        #region Public Properties

        /// <summary>
        /// Тип дествия для файла 
        /// </summary>
        [DataMember]
        public UpdateAction EntryAction { get; set; }

        [DataMember]
        public byte[] EntryBinaryData { get; set; }

        /// <summary>
        /// Hash-код 
        /// </summary>
        [DataMember]
        public string EntryHash { get; set; }

        /// <summary>
        /// Имя файла 
        /// </summary>
        [DataMember]
        public string EntryName { get; set; }

        /// <summary>
        /// Относительный путь к файлу 
        /// </summary>
        [DataMember]
        public string EntryPath { get; set; }

        /// <summary>
        /// Имя плагина 
        /// </summary>
        [DataMember]
        public string EntryPluginName { get; set; }

        /// <summary>
        /// Версия 
        /// </summary>
        [DataMember]
        public string EntryVersion { get; set; }

        #endregion Public Properties
    }
}
