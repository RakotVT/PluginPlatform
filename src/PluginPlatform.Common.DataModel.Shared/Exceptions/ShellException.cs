﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginPlatform.Common.DataModels.Exceptions
{

    [Serializable]
    public class ShellException : Exception
    {
        public ShellException() { }
        public ShellException(string message) : base(message) { }
        public ShellException(string message, Exception inner) : base(message, inner) { }
        protected ShellException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }
    }
}
