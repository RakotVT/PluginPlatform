﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PluginPlatform.Common.DataModels.LogService
{
    [DataContract]
    public class LogMessage
    {
        /// <summary>
        /// Идентификатор пакета
        /// </summary>
        [DataMember]
        public int TicketId { get; set; }
        /// <summary>
        /// Дата возникновения ошибки
        /// </summary>
        [DataMember]
        public DateTime Date { get; set; }
        /// <summary>
        /// Источник сообщения об ошибке
        /// </summary>
        [DataMember]
        public string MessageSource { get; set; }
        /// <summary>
        /// Уровень ошибки (error | critical error)
        /// </summary>
        [DataMember]
        public string MessageLevel { get; set; }
        /// <summary>
        /// Сообщение об ошибке (e.Message)
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Трассировка стека
        /// </summary>
        [DataMember]
        public string StackTrace { get; set; }
        /// <summary>
        /// Местоположение ошибки
        /// </summary>
        [DataMember]
        public string ErrorLocation { get; set; }
        /// <summary>
        /// Имя файла минидампа
        /// </summary>
        [DataMember]
        public string MinidumpName { get; set; }
        /// <summary>
        /// Содержимое минидампа
        /// </summary>
        [DataMember]
        public byte[] MinidumpContent { get; set; }
        /// <summary>
        /// Имя скриншота
        /// </summary>
        [DataMember]
        public string ScreenshotName { get; set; }
        /// <summary>
        /// Содержимое скриншота
        /// </summary>
        [DataMember]
        public byte[] ScreenshotContent { get; set; }
        /// <summary>
        /// Пользовательское сообщение.
        /// </summary>
        [DataMember]
        public string UserMessage { get; set; }
    }
}
