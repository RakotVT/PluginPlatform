using System;
using System.Collections.Generic;

namespace PluginPlatform.Common.DataModels.UserService
{
    public partial class UserModel
    {
        public UserModel()
        {
            this.Roles = new List<RoleModel>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public bool IsADUser { get; set; }
        public virtual ICollection<RoleModel> Roles { get; set; }
        public string UserDomain { get; set; }
    }
}
