﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PluginPlatform.Common.DataModels.UserService
{
    /// <summary>
    /// Используется для предоставления клиентскому приложению информации об ошибках сервиса.
    /// </summary>
    [DataContract]
    public class UserServiceException
    {
        [DataMember]
        public List<string> Errors { get; set; }

        public UserServiceException()
        {
            Errors = new List<string>();
        }

        public UserServiceException(List<string> errors)
        {
            Errors = errors;
        }

        public UserServiceException(string error)
        {
            Errors = new List<string>() { error };        
        }
    }
}
