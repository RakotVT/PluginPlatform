﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PluginPlatform.Common.DataModels.UserService
{
    [DataContract]
    public class LoginModel
    {
        [Required]
        [Display(Name = "Логин")]
        [DataMember]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        [DataMember]
        public string Password { get; set; }
    }

    [DataContract]
    public class RegisterModel
    {
        [Required]
        [Display(Name = "Логин")]
        [DataMember]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "{0} должен быть не менее {2} символов")]
        [Display(Name = "Пароль")]
        [DataMember]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль (еще раз)")]
        [DataMember]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Некорректный {0}")]
        [Display(Name = "Email")]
        [DataMember]
        public string Email { get; set; }
    }

    [DataContract]
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        [DataMember]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "{0} должен быть не менее {2} символов")]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        [DataMember]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль (еще раз)")]
        [DataMember]
        public string ConfirmPassword { get; set; }
    }

    [DataContract]
    public class ForgotPasswordModel
    {
        [DataType(DataType.EmailAddress, ErrorMessage = "Некорректный {0}")]
        [Display(Name = "Email")]
        [DataMember]
        public string Email { get; set; }
    }

    [DataContract]
    public class SetPasswordModel
    {
        [Required]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "{0} должен быть не менее {2} символов")]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        [DataMember]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль (еще раз)")]
        [DataMember]
        public string ConfirmPassword { get; set; }
    }
}