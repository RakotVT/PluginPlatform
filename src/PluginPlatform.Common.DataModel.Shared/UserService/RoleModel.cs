﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PluginPlatform.Common.DataModels.UserService
{
    public class RoleModel
    {
        public RoleModel()
        {
            this.Users = new List<UserModel>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public List<UserModel> Users { get; set; }
        public List<int> Plugins { get; set; }
    }
}
