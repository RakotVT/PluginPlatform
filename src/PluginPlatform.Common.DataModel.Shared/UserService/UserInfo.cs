﻿using System.Runtime.Serialization;

namespace PluginPlatform.Common.DataModels.UserService
{
    [DataContract]
    public class UserInfo
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string Email { get; set; }
    }
}
