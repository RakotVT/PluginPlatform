﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ITWebNet.UMS.ServicesApi.LogService.UserStorage
{
    public class MessageSource
    {
        [Key]
        public int Id { get; set; }
        public string SourceName { get; set; }
    }
}
