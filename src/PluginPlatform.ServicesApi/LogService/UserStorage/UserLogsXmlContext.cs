﻿using PluginPlatform.Core.XmlStorage;

namespace PluginPlatform.ServicesApi.LogService.UserStorage
{
    class UserLogsXmlContext : XmlContext<UserLogsStorage>
    {
        public UserLogsXmlContext(string storageFile, string defaultStorageFile = null)
            : base(storageFile, defaultStorageFile)
        { }
    }
}
