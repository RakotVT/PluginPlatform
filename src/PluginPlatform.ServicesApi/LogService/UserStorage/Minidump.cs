﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ITWebNet.UMS.ServicesApi.LogService.UserStorage
{
    public class Minidump
    {
        [Key]
        public int Id { get; set; }
        public byte[] Content { get; set; }
        public string Name { get; set; }
    }
}
