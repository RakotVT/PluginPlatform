﻿using System;
using System.Xml.Serialization;

namespace PluginPlatform.ServicesApi.LogService.UserStorage
{
    [Serializable]
    public class Log
    {
        #region Properties

        [XmlElement]
        public DateTime Date { get; set; }
        [XmlElement]
        public string ErrorMessage { get; set; }
        [XmlElement]
        public string ErrorLocation { get; set; }
        [XmlElement]
        public bool IsSended { get; set; }
        [XmlElement]
        public string MessageSource { get; set; }
        [XmlElement]
        public string MessageType { get; set; }
        [XmlElement]
        public string Level { get; set; }
        [XmlElement]
        public string UserMessage { get; set; }
        [XmlElement]
        public string StackTrace { get; set; }

        #endregion
    }
}
