﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using ITWebNet.UMS.ServicesApi.Migrations;

namespace ITWebNet.UMS.ServicesApi.LogService.UserStorage
{
    class UserLogsContext : DbContext
    {
        static UserLogsContext()
        {
            Database.SetInitializer<UserLogsContext>(new DbMigrationsInitializer());
        }

        public UserLogsContext()
            : base("UserLogsContext")
        {        
            try
            {
                Database.Initialize(false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public UserLogsContext(string connectionString) : base(connectionString) { }


        public DbSet<Log> Logs { get; set; }
        public DbSet<Minidump> Minidumps { get; set; }
        public DbSet<ScreenShot> ScreenShots { get; set; }
        public DbSet<MessageSource> MessageSources { get; set; }
    }

    class DbMigrationsInitializer : IDatabaseInitializer<UserLogsContext>
    {
        #region IDatabaseInitializer<UserSettingsContext> Members

        public void InitializeDatabase(UserLogsContext context)
        {
            try
            {
                var config = new Configuration();
                config.TargetDatabase = new System.Data.Entity.Infrastructure.DbConnectionInfo(
                    context.Database.Connection.ConnectionString,
                    "System.Data.SQLite.EF6");

                var dbMigrator = new DbMigrator(config);

                if (dbMigrator.GetDatabaseMigrations().Count() == 0)
                    context.Database.Delete();

                if (dbMigrator.GetPendingMigrations().Count() != 0)
                    dbMigrator.Update();

                context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }

}
