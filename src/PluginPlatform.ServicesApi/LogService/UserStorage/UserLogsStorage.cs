﻿using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace PluginPlatform.ServicesApi.LogService.UserStorage
{
    [XmlRoot("Storage")]
    public class UserLogsStorage
    {
        [XmlArray("Logs"), XmlArrayItem("Entry")]
        public ObservableCollection<Log> Logs { get; set; }

        public UserLogsStorage()
        {
            Logs = new ObservableCollection<Log>();
        }
    }
}
