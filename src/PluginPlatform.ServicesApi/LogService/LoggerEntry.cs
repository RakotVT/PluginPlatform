﻿using System;
using System.Collections.Generic;


namespace PluginPlatform.ServicesApi.LogService
{
    /// <summary>
    /// Тип сообщения
    /// </summary>
    public enum MessageType
    {
        Action,
        Error
    }

    /// <summary>
    /// Уровень важности сообщения
    /// </summary>
    public enum MessageLevel
    {
        Debug,
        Info,
        Warning,
        Error,
        CriticalError
    }

    /// <summary>
    /// Запись лога ошибок и сообщений.
    /// </summary>
    public class LoggerEntry
    {
        /// <summary>
        /// Тип сообщения.
        /// </summary>
        public MessageType MessageType { get; set; }
        
        /// <summary>
        /// Уровень важности сообщения.
        /// </summary>
        public MessageLevel MessageLevel { get; set; }

        /// <summary>
        /// Исключение
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Сообщение от разработчика.
        /// </summary>
        public string UserMessage { get; set; }

        /// <summary>
        /// Создавать минидамп.
        /// </summary>
        public bool MakeDump { get; set; }

        /// <summary>
        /// Создавать скриншот.
        /// </summary>
        public bool MakeScreen { get; set; }

        /// <summary>
        /// Открыть форму сообщения.
        /// </summary>
        public bool ShowMessageform { get; set; }

        /// <summary>
        /// Сохранять лог в локалюное хранилище.
        /// </summary>
        public bool SaveLogInDB { get; set; }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// StackTrace ошибки.
        /// </summary>
        public string StackTrace { get; private set; }

        /// <summary>
        /// Место возникновения ошибки.
        /// </summary>
        public string Location { get; private set; }

        /// <summary>
        /// Источник ошибки.
        /// </summary>
        public string Source { get; private set; }

        /// <summary>
        /// Минидамп памяти.
        /// </summary>
        public KeyValuePair<string, byte[]> MiniDump { get; private set; }

        /// <summary>
        /// Скриншот рабочего стола.
        /// </summary>
        public KeyValuePair<string, byte[]> Screenshot { get; private set; }

        /// <summary>
        /// Заполняет сущность лога дополнительными параметрами.
        /// </summary>
        /// <param name="exMessage">Сообщение об ошибке.</param>
        /// <param name="exStackTrace">StackTrace ошибки.</param>
        /// <param name="exLocation">Место возникновения ошибки.</param>
        /// <param name="exSource">Источник ошибки.</param>
        /// <param name="screenName">Имя снимка экрана.</param>
        /// <param name="screenContent">Бинарные данные снимка экрана.</param>
        /// <param name="dumpName">Имя минидампа памяти.</param>
        /// <param name="dumpContent">Бинарные данные минидампа памяти.</param>
        public void SetAdditionalInfo(string exMessage,
                string exStackTrace,
                string exLocation,
                string exSource,
                string fileName,
                byte[] fileContent,
                string dumpName,
                byte[] dumpContent)
        {
            Message = exMessage;
            StackTrace = exStackTrace;
            Location = exLocation;
            Source = exSource;
            Screenshot = new KeyValuePair<string, byte[]>(fileName, fileContent);
            MiniDump = new KeyValuePair<string, byte[]>(dumpName, dumpContent);
        }
    }
}
