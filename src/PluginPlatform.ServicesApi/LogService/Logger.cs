﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using PluginPlatform.Core.LocalSettings;
using PluginPlatform.ServicesApi.LogService.UserStorage;

namespace PluginPlatform.ServicesApi.LogService
{
    public class Logger
    {
        #region Private Fields

        private static ISettingsProvider _settingsProvider;

        private UserLogsXmlContext _context;
        private string _sessionKey;

        #endregion Private Fields

        #region Public Properties

        public string SessionKey
        {
            get { return _sessionKey; }
            set { _sessionKey = value; }
        }

        #endregion Public Properties

        #region Constructors

        #region Конструктор Singleton Logger

        private static volatile Logger _instance;

        private static object _syncRoot = new Object();

        private Logger()
        {
        }

        /// <summary>
        /// Экземпляр <see cref="Logger" />. 
        /// </summary>
        public static Logger Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new Logger();
                    }
                }
                return _instance;
            }
        }

        #endregion Конструктор Singleton Logger

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Выполняет логирование отладочного сообщения 
        /// </summary>
        /// <param name="userMessage"> Сообщение </param>
        public void Debug(string userMessage)
        {
            WriteLog(new LoggerEntry()
            {
                UserMessage = userMessage,
                MessageType = MessageType.Action,
                MessageLevel = MessageLevel.Debug
            });
        }

        /// <summary>
        /// Выполняет логирование информационного сообщения 
        /// </summary>
        /// <param name="userMessage"> Сообщение </param>
        public void Info(string userMessage)
        {
            WriteLog(new LoggerEntry()
            {
                UserMessage = userMessage,
                MessageType = MessageType.Action,
                MessageLevel = MessageLevel.Info
            });
        }

        public void Exception(Exception ex)
        {
            WriteLog(new LoggerEntry
            {
                Exception = ex,
                MessageLevel = MessageLevel.Error,
                MessageType = MessageType.Error,
            });
        }

        public void Initialize(ISettingsProvider settingsProvider)
        {
            try
            {
                _settingsProvider = settingsProvider;
                string storagePath = _settingsProvider.GetLogsStoragePath();
                if (!Directory.Exists(storagePath))
                    Directory.CreateDirectory(storagePath);

                string logsStoragePath = Path.Combine(storagePath, "Messages.log");

                _context = new UserLogsXmlContext(logsStoragePath);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Выполняет отправку последней зафиксированной ошибки. 
        /// </summary>
        /// <param name="loggerEntry"> Сущность с информацией об ошибке </param>
        public void SendLastError(LoggerEntry loggerEntry)
        {
            try
            {
                if (loggerEntry.MessageLevel != MessageLevel.CriticalError &&
                    loggerEntry.MessageLevel != MessageLevel.Error &&
                    loggerEntry.MessageLevel != MessageLevel.Warning)
                    return;
                //EndpointAddress address = new EndpointAddress(_settingsProvider.GetAddress(ServiceType.LogService));
                //Binding binding = _settingsProvider.GetBindings(ServiceType.LogService);

                //using (LogServiceClient client = new LogServiceClient(binding, address))
                //{
                //    int userMessageId = client.GetUserTicketId(SessionKey);

                //    LogMessage msg = new LogMessage
                //    { 
                //        Date = DateTime.Now,
                //        TicketId = userMessageId,
                //        MessageSource = loggerEntry.Source,
                //        MessageLevel = loggerEntry.MessageLevel.ToString(),
                //        ErrorMessage = loggerEntry.Message,
                //        StackTrace = loggerEntry.StackTrace,
                //        ErrorLocation = loggerEntry.Location,
                //        MinidumpName = loggerEntry.MiniDump.Key,
                //        MinidumpContent = loggerEntry.MiniDump.Value,
                //        ScreenshotName = loggerEntry.Screenshot.Key,
                //        ScreenshotContent = loggerEntry.Screenshot.Value,
                //        UserMessage = loggerEntry.UserMessage
                //    };

                //    client.SendLog(msg);
                //    //request.AddJsonBody(msg);

                //    //client.Execute(request); 
                //}
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Осуществляет логирование сообщения 
        /// </summary>
        /// <param name="loggerEntry"></param>
        public void WriteLog(LoggerEntry loggerEntry)
        {
            try
            {

                string exMessage = string.Empty;
                string exStackTrace = string.Empty;
                string exLocation = string.Empty;
                string exSource = string.Empty;

                string dumpName = string.Empty;
                byte[] screenContent = new byte[] { };

                string screenName = string.Empty;
                byte[] dumpContent = new byte[] { };

                if (loggerEntry.Exception != null)
                {
                    // Заполнение данных об исключении 
                    StackTrace st = new StackTrace(loggerEntry.Exception, true);
                    StackFrame[] frames = st.GetFrames();
                    StackFrame targetFrame = new StackFrame(true);
                    foreach (StackFrame frame in frames)
                    {
                        if (frame.GetFileLineNumber() != 0)
                        {
                            targetFrame = frame;
                            break;
                        }
                    }

                    StringBuilder exMessageBuilder = new StringBuilder();
                    exMessageBuilder.AppendLine(string.Format("Исключение: {0}", loggerEntry.Exception.Message));

                    GetInnerExceptionText(loggerEntry.Exception, ref exMessageBuilder);

                    if (loggerEntry.Exception is System.Reflection.ReflectionTypeLoadException)
                    {
                        var loaderExceptions = (loggerEntry.Exception as System.Reflection.ReflectionTypeLoadException).LoaderExceptions;
                        exMessageBuilder.AppendLine(string.Format("Исключения загрузчика: {0}", string.Join("\r\n", loaderExceptions.Select(e => e.Message))));
                    }

                    exMessage = exMessageBuilder.ToString();
                    exStackTrace = loggerEntry.Exception.StackTrace;
                    exSource = targetFrame.GetFileName();
                    exLocation = String.Format("Метод: {0} (строка {1})", targetFrame.GetMethod(), targetFrame.GetFileLineNumber());
                }

                if (loggerEntry.MakeScreen)
                {
                    // Создание скриншота 
                    screenName = String.Format("ScreenShot_{0}_{1}", DateTime.Today.ToShortDateString(), DateTime.Now.Ticks);
                    screenContent = DoScreenShot();
                }

                if (loggerEntry.MakeDump)
                {
                    // Создание дампа 
                    dumpName = string.Format(@"CrushDump_{0}_{1}.dmp", DateTime.Today.ToShortDateString(), DateTime.Now.Ticks);
                    dumpContent = CreateMiniDump();
                }

                loggerEntry.SetAdditionalInfo(exMessage, exStackTrace, exLocation, exSource, screenName, screenContent, dumpName, dumpContent);

                //if (loggerEntry.ShowMessageform)
                //{
                //    // Запуск сообщения 
                //    FrmMessage frmMessage = new FrmMessage(this, loggerEntry);
                //    frmMessage.ShowDialog();
                //}

                if (loggerEntry.SaveLogInDB)
                    SaveLog(loggerEntry);
                SendLastError(loggerEntry);
            }
            catch (Exception) { }
        }

        #endregion Public Methods

        #region Private Methods

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hWnd, out Rectangle lpRect);

        /// <summary>
        /// Возвращает снимок экрана в виде массива байт 
        /// </summary>
        /// <returns></returns>
        private byte[] DoScreenShot()
        {
            IntPtr mainHandle = Process.GetCurrentProcess().MainWindowHandle;
            Rectangle screen;

            if (mainHandle == IntPtr.Zero || !GetWindowRect(mainHandle, out screen))
                return null;

            Bitmap bmp = new Bitmap(screen.Width, screen.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.CopyFromScreen(screen.Location, screen.Location, screen.Size, CopyPixelOperation.SourceCopy);
            using (MemoryStream stream = new MemoryStream())
            {
                bmp.Save(stream, ImageFormat.Bmp);
                return stream.ToArray();
            }
        }

        private void GetInnerExceptionText(Exception ex, ref StringBuilder exMessageBuilder)
        {
            if (ex.InnerException != null)
            {
                Exception innerEx = ex.InnerException;
                exMessageBuilder.AppendLine(string.Format("Внутреннее исключение: {0}", innerEx.Message));
                GetInnerExceptionText(innerEx, ref exMessageBuilder);
            }
        }

        private void SaveLog(LoggerEntry loggerEntry)
        {
            // Cохранение лога в локальной БД 
            Log log = new Log();
            log.Date = DateTime.Now;
            log.MessageSource = loggerEntry.Source;
            log.MessageType = loggerEntry.MessageType.ToString();
            log.Level = loggerEntry.MessageLevel.ToString();
            log.UserMessage = loggerEntry.UserMessage;
            log.ErrorLocation = loggerEntry.Location;
            log.ErrorMessage = loggerEntry.Message;
            log.StackTrace = loggerEntry.StackTrace;//.Length > 2000 ? loggerEntry.StackTrace.Substring(0, 2000) : loggerEntry.StackTrace;
            log.IsSended = false;

            _context.Storage.Logs.Add(log);
            _context.SaveChanges();
        }

        //Создание минидампа

        #region makeminiDump

        [DllImport("kernel32.dll")]
        private static extern uint GetCurrentThreadId();

        [DllImport("Dbghelp.dll")]
        private static extern bool MiniDumpWriteDump(
            IntPtr hProcess,
            uint ProcessId,
            IntPtr hFile,
            int DumpType,
            ref MINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
            IntPtr UserStreamParam,
            IntPtr CallbackParam
            );

        /// <summary>
        /// Создает минидамп 
        /// </summary>
        private byte[] CreateMiniDump()
        {
            byte[] dumpbytes;
            using (System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess())
            {
                string FileName = string.Format(@"CrashDump_{0}_{1}.dmp", DateTime.Today.ToShortDateString(), DateTime.Now.Ticks);

                MINIDUMP_EXCEPTION_INFORMATION Mdinfo = new MINIDUMP_EXCEPTION_INFORMATION();

                Mdinfo.ThreadId = GetCurrentThreadId();
                Mdinfo.ExceptionPointers = Marshal.GetExceptionPointers();
                Mdinfo.ClientPointers = 1;

                using (FileStream fs = new FileStream(FileName, FileMode.Create))
                {
                    {
                        MiniDumpWriteDump(
                            process.Handle,
                            (uint)process.Id,
                            fs.SafeFileHandle.DangerousGetHandle(),
                            MINIDUMP_TYPE.MiniDumpNormal,
                            ref Mdinfo,
                            IntPtr.Zero,
                            IntPtr.Zero);
                    }
                    dumpbytes = new byte[fs.Length];
                    fs.Read(dumpbytes, 0, dumpbytes.Length);
                }
                File.Delete(FileName);
            }
            return dumpbytes;
        }

        private class MINIDUMP_TYPE
        {
            #region Public Fields

            public const int MiniDumpFilterMemory = 0x00000008;
            public const int MiniDumpFilterModulePaths = 0x00000080;
            public const int MiniDumpIgnoreInaccessibleMemory = 0x00020000;
            public const int MiniDumpNormal = 0x00000000;
            public const int MiniDumpScanMemory = 0x00000010;
            public const int MiniDumpWithCodeSegs = 0x00002000;
            public const int MiniDumpWithDataSegs = 0x00000001;
            public const int MiniDumpWithFullAuxiliaryState = 0x00008000;
            public const int MiniDumpWithFullMemory = 0x00000002;
            public const int MiniDumpWithFullMemoryInfo = 0x00000800;
            public const int MiniDumpWithHandleData = 0x00000004;
            public const int MiniDumpWithIndirectlyReferencedMemory = 0x00000040;
            public const int MiniDumpWithoutAuxiliaryState = 0x00004000;
            public const int MiniDumpWithoutOptionalData = 0x00000400;
            public const int MiniDumpWithPrivateReadWriteMemory = 0x00000200;
            public const int MiniDumpWithPrivateWriteCopyMemory = 0x00010000;
            public const int MiniDumpWithProcessThreadData = 0x00000100;
            public const int MiniDumpWithThreadInfo = 0x00001000;
            public const int MiniDumpWithTokenInformation = 0x00040000;
            public const int MiniDumpWithUnloadedModules = 0x00000020;

            #endregion Public Fields
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        private struct MINIDUMP_EXCEPTION_INFORMATION
        {
            public uint ThreadId;
            public IntPtr ExceptionPointers;
            public int ClientPointers;
        }

        #endregion makeminiDump

        #endregion Private Methods
    }
}