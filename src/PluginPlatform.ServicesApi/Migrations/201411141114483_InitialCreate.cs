namespace ITWebNet.UMS.ServicesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Date = c.DateTime(nullable: false),
                    ErrorMessage = c.String(maxLength: 4000),
                    ErrorLocation = c.String(maxLength: 4000),
                    IsSended = c.Boolean(nullable: false),
                    MessageSourceId = c.Int(nullable: false),
                    MessageType = c.String(maxLength: 4000),
                    MinidumpId = c.Int(nullable: false),
                    Level = c.String(maxLength: 4000),
                    UserMessage = c.String(maxLength: 4000),
                    ScreenShotId = c.Int(nullable: false),
                    StackTrace = c.String(maxLength: 4000),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MessageSources", t => t.MessageSourceId, cascadeDelete: true)
                .ForeignKey("dbo.Minidumps", t => t.MinidumpId, cascadeDelete: true)
                .ForeignKey("dbo.ScreenShots", t => t.ScreenShotId, cascadeDelete: true)
                .Index(t => t.MessageSourceId)
                .Index(t => t.MinidumpId)
                .Index(t => t.ScreenShotId);

            CreateTable(
                "dbo.MessageSources",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    SourceName = c.String(maxLength: 4000),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Minidumps",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Content = c.Binary(maxLength: 4000),
                    Name = c.String(maxLength: 4000),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.ScreenShots",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Content = c.Binary(maxLength: 4000),
                    Name = c.String(maxLength: 4000),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Logs", "ScreenShotId", "dbo.ScreenShots");
            DropForeignKey("dbo.Logs", "MinidumpId", "dbo.Minidumps");
            DropForeignKey("dbo.Logs", "MessageSourceId", "dbo.MessageSources");
            DropIndex("dbo.Logs", new[] { "ScreenShotId" });
            DropIndex("dbo.Logs", new[] { "MinidumpId" });
            DropIndex("dbo.Logs", new[] { "MessageSourceId" });
            DropTable("dbo.ScreenShots");
            DropTable("dbo.Minidumps");
            DropTable("dbo.MessageSources");
            DropTable("dbo.Logs");
        }

    }
}
