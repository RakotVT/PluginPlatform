// <auto-generated />
namespace ITWebNet.UMS.ServicesApi.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class EnableMigrations : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EnableMigrations));
        
        string IMigrationMetadata.Id
        {
            get { return "201411141113502_EnableMigrations"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
