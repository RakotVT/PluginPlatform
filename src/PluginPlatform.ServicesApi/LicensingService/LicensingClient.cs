﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using ITWebNet.UMS.Core.LocalSettings;
using ITWebNet.UMS.Common.DataModels.LicensingService;
using ITWebNet.UMS.Common.DataModels.PluginService;
using System.ServiceModel.Channels;
using System.Net;
using RestSharp;

namespace ITWebNet.UMS.ServicesApi.LicensingService
{
    public class LicensingClient : BaseClient<LicensingClient>, ILicensingController
    {

        #region Fields

        ISettingsProvider _settingsProvider;
        private string _licExtention = ".license";

        #endregion

        #region Constructors

        public LicensingClient(ISettingsProvider settingsProvider, string baseAddress)
            : base(baseAddress)
        {
            _settingsProvider = settingsProvider;
        }

        #endregion

        #region Public Methods

        public LicenseInfo ActivateLicense(string activationKey)
        {
            LicenseInfo info = Execute<LicenseInfo>(
                new RestRequest("api/licensing/ActivateLicense", Method.POST)
                    .AddParameter("activationKey", activationKey));

            using (MemoryStream stream = new MemoryStream())
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(LicenseInfo));
                serializer.WriteObject(stream, info);
                _settingsProvider.SetLicenseInfo(info.OwnerId, stream.ToArray());
            }

            return info;
        }

        public int[] GetAvailablePlugins()
        {
            return Execute<List<int>>(
                 new RestRequest("api/licensing/GetAvailablePlugins", Method.GET)).ToArray();
        }

        public List<LicenseInfoWrapped> GetAllUserLicenses(string sessionKey)
        {
            //var info = new UserService.AuthorizeProvider(_settingsProvider).GetUserInfo(sessionKey);
            //if (info == null)
            //    return new List<LicenseInfoWrapped>();
            //List<LicenseInfoWrapped> licenses = new List<LicenseInfoWrapped>();
            //foreach (var item in _settingsProvider.GetLicenseInfo(info.Id))
            //{
            //    using (MemoryStream stream = new MemoryStream(item))
            //    {
            //        DataContractSerializer serializer = new DataContractSerializer(typeof(LicenseInfo));
            //        var license = serializer.ReadObject(stream) as LicenseInfo;

            //        licenses.Add(LicenseInfoWrapped.WrapLincenseInfo(license));
            //    }
            //}

            //return licenses;
            return null;
        }

        #endregion
    }
}
