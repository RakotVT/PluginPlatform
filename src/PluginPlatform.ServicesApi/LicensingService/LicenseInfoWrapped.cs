﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ITWebNet.UMS.Common.DataModels.LicensingService;

namespace ITWebNet.UMS.ServicesApi.LicensingService
{

    public class LicenseInfoWrapped
    {
        #region Properties

        /// <summary>
        /// Дата окончания действия лицензии. 
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Номер активации. 
        /// </summary>
        public Guid Number { get; set; }

        /// <summary>
        /// Собственник лицензии. 
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Информация о лицензии плагинов.
        /// </summary>
        public List<PluginDetails> PluginsInfo { get; set; }

        /// <summary>
        /// Дата начала действия лицензии. 
        /// </summary>
        public DateTime StartDate { get; set; }

        #endregion Properties

        #region Internal Methods

        /// <summary>
        /// Преобразовывает объект <see cref="ITWebNet.UMS.ServicesApi.LicensingService.LicenseInfo" />
        /// публично доступный.
        /// </summary>
        /// <param name="info"> Объект файла лицензии. </param>
        /// <returns> Публично доступный объект лицензии. </returns>
        internal static LicenseInfoWrapped WrapLincenseInfo(LicenseInfo info)
        {
            return new LicenseInfoWrapped()
            {
                EndDate = info.EndDate,
                Number = info.Number,
                Owner = info.Owner,
                PluginsInfo = info.PluginsInfo.ToList().Select(item => new PluginDetails()
                {
                    EndDate = item.EndDate,
                    Name = item.Name,
                    Number = item.Number,
                    StartDate = item.StartDate,
                    Timeless = item.Timeless,
                    Version = item.Version
                }).ToList(),
                StartDate = info.StartDate
            };
        }

        #endregion Internal Methods
    }

    public class PluginDetails
    {
        #region Public Properties

        /// <summary>
        /// Дата окончания действия лицензии. 
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Имя плагина.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер активации. 
        /// </summary>
        public Guid Number { get; set; }

        /// <summary>
        /// Дата начала действия лицензии. 
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Флаг, показыващий является ли лицензия бессрочной.
        /// </summary>
        public bool Timeless { get; set; }

        /// <summary>
        /// Версия плагина.
        /// </summary>
        public string Version { get; set; }

        #endregion Public Properties
    }

}