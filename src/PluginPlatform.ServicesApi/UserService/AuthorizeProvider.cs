﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.Text;
using PluginPlatform.Core.LocalSettings;
using PluginPlatform.Common.DataModels.UserService;
using RestSharp;
using PluginPlatform.Common.DataModels;
using PluginPlatform.Core.Http;

namespace PluginPlatform.ServicesApi.UserService
{
    public class AuthorizeProvider : BaseClient<AuthorizeProvider>
    {
        #region Fields

        ISettingsProvider _settingsProvider;

        #endregion

        #region Constructors

        public AuthorizeProvider(ISettingsProvider settingsProvider, string baseAddress)
            : base(baseAddress)
        {
            _settingsProvider = settingsProvider;
            string uri = _settingsProvider.GetAddress(ServiceType.AuthorizationService);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Выполянет авторизацию пользователя и выдает ему ключ сессии.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>Уникальный идентификатор сессии.</returns>
        public Token AuthorizeUser(string login, string password)
        {
            try
            {
                LoginModel model = new LoginModel() { UserName = login, Password = password };
                return Execute<Token>(new RestRequest("api/account/login", Method.POST)
                    .AddJsonBody(model));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<int> GetAvailablePlugins()
        {
            return Execute<List<int>>(new RestRequest("api/Account/AvailablePlugins", Method.GET));
        }


        internal UserInfo GetUserInfo(string sessionKey)
        {
            AddAuthentication(sessionKey);
            return Execute<UserInfo>(new RestRequest("api/account/userinfo", Method.POST));
        }


        #endregion
    }
}
