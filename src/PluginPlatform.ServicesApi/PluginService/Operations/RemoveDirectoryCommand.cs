﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PluginPlatform.ServicesApi.UpdateService.Operations
{
    class RemoveDirectoryCommand : IUpdateCommand
    {
        private string _dirName;
        private bool _deleteSubDirs;

        public RemoveDirectoryCommand(string dirName, bool deleteSubDirectories)
        {
            _dirName = dirName;
            _deleteSubDirs = deleteSubDirectories;
        }

        public string GetName()
        {
            return "RemoveDirectoryCommand";
        }

        public string GetDescription()
        {
            return "Удаляет каталог с диска";
        }

        public void Execute()
        {
            try
            {
                Directory.Delete(_dirName, _deleteSubDirs);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ExecuteAsync()
        {
            throw new NotImplementedException();
        }
    }
}
