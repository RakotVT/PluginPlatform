﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PluginPlatform.ServicesApi.UpdateService.Operations
{
    class RenameFileCommand : IUpdateCommand
    {
        private string _srcPath;
        private string _dstPath;

        public RenameFileCommand(string srcPath, string dstPath)
        {
            _srcPath = srcPath;
            _dstPath = dstPath;
        }

        public string GetName()
        {
            return "RenameFileCommand";
        }

        public string GetDescription()
        {
            return "Переименовывает файл";
        }

        public void Execute()
        {
            try
            {
                File.Move(_srcPath, _dstPath);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ExecuteAsync()
        {
            throw new NotImplementedException();
        }
    }
}
