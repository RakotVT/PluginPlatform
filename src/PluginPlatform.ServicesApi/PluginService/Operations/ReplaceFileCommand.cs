﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PluginPlatform.ServicesApi.UpdateService.Operations
{
    class ReplaceFileCommand : IUpdateCommand
    {
        private string _srcPath;
        private string _dstPath;

        public ReplaceFileCommand(string srcPath, string dstPath)
        {
            _srcPath = srcPath;
            _dstPath = dstPath;
        }

        public string GetName()
        {
            return "ReplaceFileCommand";
        }

        public string GetDescription()
        {
            return "Перезаписывает файл новым файлом.";
        }

        public void Execute()
        {
            try
            {
                File.Replace(_srcPath, _dstPath, null);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ExecuteAsync()
        {
            throw new NotImplementedException();
        }
    }
}
