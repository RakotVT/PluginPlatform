﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PluginPlatform.ServicesApi.UpdateService.Operations
{
    class DeleteFileCommand : IUpdateCommand
    {
        private string _filePath;

        public DeleteFileCommand(string filePath)
        {
            _filePath = filePath;
        }

        public string GetName()
        {
            return "RemoveFileCommand";
        }

        public string GetDescription()
        {
            return "Удаляет файл";
        }

        public void Execute()
        {
            try
            {
                File.Delete(_filePath);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ExecuteAsync()
        {
            throw new NotImplementedException();
        }
    }
}
