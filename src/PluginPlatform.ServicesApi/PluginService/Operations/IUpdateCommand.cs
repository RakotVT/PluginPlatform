﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginPlatform.ServicesApi.UpdateService.Operations
{
    public interface IUpdateCommand
    {
        /// <summary>
        /// Имя команды
        /// </summary>
        string GetName();

        /// <summary>
        /// Описание команды
        /// </summary>
        string GetDescription();

        /// <summary>
        /// Выполняет команду
        /// </summary>
        void Execute();

        /// <summary>
        /// Выполняет команду асинхронно
        /// </summary>
        void ExecuteAsync();
    }
}
