﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PluginPlatform.ServicesApi.UpdateService.Operations
{
    class CopyFileCommand : IUpdateCommand
    {
        private string _filePath;
        private string _dstPath;
        private const int _bufferSize = 8196;

        public CopyFileCommand(string filePath, string dstPath)
        {
            _filePath = filePath;
            _dstPath = dstPath;
        }

        public string GetName()
        {
            return "CopyFileCommand";
        }

        public string GetDescription()
        {
            return "Копирует файл в файловую систему.";
        }

        public void Execute()
        {
            try
            {
                File.Copy(_filePath, _dstPath, true);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                //TODO логирование операции
            }
        }

        public void ExecuteAsync()
        {
            throw new NotImplementedException();
        }
    }
}
