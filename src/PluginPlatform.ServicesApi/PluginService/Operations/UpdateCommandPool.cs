﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace PluginPlatform.ServicesApi.UpdateService.Operations
{
    public class UpdateCommandPool
    {
        private List<IUpdateCommand> _commands;     // Список команд
        public event OnCommandExecuteEventHandler OnCommandExecuted;


        public UpdateCommandPool()
        {
            _commands = new List<IUpdateCommand>();
        }

        public UpdateCommandPool(List<IUpdateCommand> commandList)
        {
            _commands = commandList;
        }

        /// <summary>
        /// Выполняет добавление команды в список команд
        /// </summary>
        /// <param name="command"></param>
        public void AddCommand(IUpdateCommand command)
        {
            _commands.Add(command);
        }

        /// <summary>
        /// Выполняет добавление нескольких команд в список команд
        /// </summary>
        /// <param name="commandList">Список команд для добавления</param>
        public void AddCommands(List<IUpdateCommand> commandList)
        {
            if (_commands.Count > 0)
                _commands.AddRange(commandList);
            else
                _commands = commandList;
        }

        /// <summary>
        /// Производит выполнение каждой команды в списке команд
        /// </summary>
        public void ExecuteCommands()
        {
            try
            {
                foreach (IUpdateCommand command in _commands)
                {
                    command.Execute();
                    if (OnCommandExecuted != null)
                        OnCommandExecuted(new CommandPoolEventInfo { Message = string.Format("{0} выполнена.", command.GetName()) });
                }
            }
            catch (Exception)
            {
                //TODO Логировать сообщенеи об ошибке команд
            }
        }
    }

    /// <summary>
    /// Делегат для отслеживания выполнения команды
    /// </summary>
    /// <param name="info">Информация о выполненной команде</param>
    public delegate void OnCommandExecuteEventHandler(CommandPoolEventInfo info);

    public class CommandPoolEventInfo
    {
        public string Message { get; set; }
    }
}
