﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PluginPlatform.ServicesApi.UpdateService.Operations
{
    class CreateDirectoryCommand : IUpdateCommand
    {
        private string _dirName;

        public CreateDirectoryCommand(string dirName)
        {
            _dirName = dirName;
        }

        public string GetName()
        {
            return "CreateDirectoryCommand";
        }

        public string GetDescription()
        {
            return "Создает каталог на диске";
        }

        public void Execute()
        {
            try
            {
                Directory.CreateDirectory(_dirName);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ExecuteAsync()
        {
            throw new NotImplementedException();
        }
    }
}
