﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Threading;
using PluginPlatform.Core.LocalSettings;
using PluginPlatform.Common.DataModels.PluginService;
using System.Collections.Generic;
using System.Linq;
using PluginPlatform.Common.DataModels;
using System.Diagnostics;
using System.Runtime.InteropServices;
using PluginPlatform.ServicesApi.UserService;

namespace PluginPlatform.ServicesApi.UpdateService
{
    public class UpdateProgressChangedEventArgs : EventArgs
    {
        #region Public Constructors

        public UpdateProgressChangedEventArgs(double progress, string status)
        {
            Progress = progress;
            Status = status;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Прогресс в процентах от 100% 
        /// </summary>
        public double Progress { get; private set; }

        public string Status { get; private set; }

        #endregion Public Properties
    }

    public class CheckUpdatesCompletedEventArgs : EventArgs
    {
        #region Public Constructors

        public CheckUpdatesCompletedEventArgs(bool updatesAvailable)
        {
            UpdatesAvailable = updatesAvailable;
        }

        #endregion Public Constructors

        #region Public Properties

        public bool UpdatesAvailable { get; private set; }

        #endregion Public Properties
    }

    public class Updater
    {
        private string _pluginsDir;
        private string _tempDir;
        private PluginServiceClient _client;
        private ISettingsProvider _settingsProvider;
        private string _sessionKey;
        private string _baseAddress;
        private IWebProxy _proxy;

        #region ctors

        public Updater(ISettingsProvider settingsProvider, string baseAddress)
        {
            _client = new PluginServiceClient(baseAddress);
            _baseAddress = baseAddress;
            _settingsProvider = settingsProvider;
        }

        #endregion

        #region Fluent Initialization

        public Updater AddAuthorization(string sessionKey)
        {
            _client.AddAuthentication(sessionKey);
            _sessionKey = sessionKey;

            return this;
        }

        public Updater AddPluginsDir(string pluginsDir)
        {
            _pluginsDir = pluginsDir;
            return this;
        }

        public Updater AddTempDir(string tempDir)
        {
            _tempDir = tempDir;
            return this;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Получает массив доступных пользователю плагинов. 
        /// </summary>
        /// <returns> Массив доступных плагинов. </returns>
        public List<int> GetAvailablePlugins()
        {
            try
            {

                return new AuthorizeProvider(_settingsProvider, _settingsProvider.GetAddress(ServiceType.AuthorizationService))
                    .AddAuthentication(_sessionKey)
                    .GetAvailablePlugins();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Проверяет наличие обновлений плагинов пользователя. 
        /// </summary>
        /// <param name="sessionKey"> Ключ сессии. </param>
        /// <returns> true - если обновления доступны, в противном случае false </returns>
        public bool IsAnyUpdateAvailable()
        {
            var plugins = GetAvailablePlugins();
            var listExistingPlugins = _settingsProvider.GetExistedPlugins();

            foreach (var pluginId in plugins)
            {
                Version currentVersion;
                var plugin = listExistingPlugins.Where(item => item.PluginId == pluginId).FirstOrDefault();
                if (plugin == null)
                    return true;

                string destinationPath = Path.Combine(_pluginsDir, plugin.Name);

                if (!Directory.Exists(destinationPath))
                    return true;

                string versionStr = plugin.Version;
                if (!Version.TryParse(versionStr, out currentVersion))
                    return true;

                if (IsUpdatesAvailable(pluginId, currentVersion, _client))
                    return true;
            }

            return false;

        }

        /// <summary>
        /// Иницирует асинхронную проверку обновлений плагинов пользователя. 
        /// </summary>
        /// <param name="sessionKey"> Ключ сессии. </param>
        public void IsAnyUpdateAvailableAsync()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += (sender, e) =>
            {
                e.Result = IsAnyUpdateAvailable();
            };
            worker.RunWorkerCompleted += (sender, e) =>
            {
                bool result;
                if (e.Error != null || e.Cancelled || !Boolean.TryParse(e.Result.ToString(), out result))
                    result = false;
                OnCheckUpdatesCompleted(result);
            };
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Обновляет плагины пользователя, для которых доступны обновления. 
        /// </summary>
        public void UpdatePlugins()
        {
            UpdatePlugins(null);
        }

        /// <summary>
        /// Инициирует асинхронное обновление плагинов пользователя, для которых доступны обновления. 
        /// </summary>
        public void UpdatePluginsAsync()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += (sender, e) =>
            {
                UpdatePlugins(worker);
            };
            worker.ProgressChanged += (sender, e) =>
            {
                OnUpdateProgressChanged(e.ProgressPercentage, e.UserState.ToString());
            };
            worker.RunWorkerCompleted += OnUpdateCompleted;

            worker.RunWorkerAsync();
        }

        public void DeleteSubscribers()
        {
            UpdateProgressChanged = null;
            CheckUpdatesCompleted = null;
            UpdateCompleted = null;
        }

        public bool IsShellUpdatesAvailable(Version currentVersion)
        {
            if (currentVersion == null)
                throw new ArgumentNullException("currentVersion");
            return _client.IsShellUpdatesAvaliable(currentVersion.ToString());
        }

        public void IsShellUpdatesAvailableAsync(Version currentVersion)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += (sender, e) =>
            {
                e.Result = IsShellUpdatesAvailable(currentVersion);
            };
            worker.RunWorkerCompleted += (sender, e) =>
            {
                bool result;
                if (e.Error != null || e.Cancelled || !Boolean.TryParse(e.Result.ToString(), out result))
                    result = false;
                OnCheckUpdatesCompleted(result);
            };
            worker.RunWorkerAsync();
        }

        public void UpdateShell(Version currentVersion)
        {
            UpdateShell(currentVersion, null);
        }

        public void UpdateShellAsync(Version currentVersion)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;

            worker.DoWork += (sender, e) =>
            {
                UpdateShell(currentVersion, worker);
            };
            worker.ProgressChanged += (sender, e) =>
            {
                OnUpdateProgressChanged(e.ProgressPercentage, e.UserState.ToString());
            };
            worker.RunWorkerCompleted += OnUpdateCompleted;
            worker.RunWorkerAsync();
        }

        #endregion Public Methods

        #region Private Methods

        private List<UpdateEntry> DownloadShellUpdates(Version currentVersion, BackgroundWorker worker)
        {
            int currentState = 0;
            try
            {
                string pluginName = "Shell";
                if (worker != null)
                    worker.ReportProgress(currentState, string.Format("Получение обновлений для плагина {0}", pluginName));

                string updateOutputPath = Path.Combine(_tempDir, pluginName);

                if (!Directory.Exists(updateOutputPath))
                    Directory.CreateDirectory(updateOutputPath);

                List<UpdateEntry> updates;
                //if (currentVersion == null)
                //    updates = _client.GetShellLatestUpdates();
                //else
                //    updates = _client.GetShellUpdates(currentVersion.ToString());

                updates = _client.GetShellLatestUpdates();

                //currentState = SaveUpdates(worker, currentState, updateOutputPath, updates.ToArray());

                return updates;
            }
            catch (Exception ex)
            {
                worker.ReportProgress(currentState, ex.Message);
                return null;
            }
        }

        private void UpdateShell(Version currentVersion, BackgroundWorker worker)
        {
            if (currentVersion == null)
                throw new ArgumentNullException("currentVersion");

            string updateOutputPath = Path.Combine(_tempDir, "Shell");
            //string destinationPath = Environment.CurrentDirectory;
            //string backupPath = Path.Combine(destinationPath, "Old", currentVersion.ToString());

            //var backupVersions = Directory.GetParent(backupPath).GetDirectories();
            //foreach (var item in backupVersions)
            //{
            //    item.Delete(true);
            //}

            var updates = DownloadShellUpdates(currentVersion, worker);
            if (updates == null)
                return;

            string installerPath = null;
            foreach (var entry in updates)
            {
                string fileDestinationPath = Path.Combine(updateOutputPath, entry.EntryName);
                using (FileStream outStream = new FileStream(fileDestinationPath, FileMode.Create, FileAccess.Write))
                    outStream.Write(entry.EntryBinaryData, 0, entry.EntryBinaryData.Length);

                if (Path.GetExtension(fileDestinationPath) == ".exe")
                    installerPath = fileDestinationPath;
            }


            if (worker != null)
                worker.ReportProgress(0, "Применение обновлений для UMS.");

            if (!string.IsNullOrEmpty(installerPath))
                LaunchInstaller(installerPath);

            //ApplyUpdates(worker, updateOutputPath, destinationPath, backupPath);

            //Directory.Delete(updateOutputPath, true);
        }

        private void LaunchInstaller(string installerPath)
        {
            ProcessStartInfo info = new ProcessStartInfo
            {
                FileName = installerPath,
                Arguments = "/SILENT /NORESTART /SUPPRESSMSGBOXES /RESTARTAPPLICATIONS"
            };

            Process.Start(info);

            RegisterApplicationRestart(string.Empty, RestartRestrictions.None);
        }

        private PluginInfo GetPluginInfo(int pluginId, PluginServiceClient client)
        {
            return client.GetPluginInfo(pluginId, null);
        }

        private PluginInfo GetPluginInfo(string pluginName, PluginServiceClient client)
        {
            return client.GetPluginInfo(0, pluginName);
        }

        private bool IsUpdatesAvailable(int pluginId, Version currentVersion, PluginServiceClient client)
        {
            return client.IsUpdatesAvailable(pluginId, currentVersion.ToString());
        }

        private void UpdatePlugins(BackgroundWorker worker)
        {
            var plugins = GetAvailablePlugins();
            var listExistingPlugins = _settingsProvider.GetExistedPlugins();

            foreach (var pluginId in plugins)
            {
                var pluginInfo = GetPluginInfo(pluginId, _client);

                string pluginName = pluginInfo.Name;

                Version currentVersion = null;
                string versionStr = listExistingPlugins.Where(item => item.PluginId == pluginId).Select(item => item.Version).FirstOrDefault();
                string pluginDir = Path.Combine(_pluginsDir, pluginName);
                bool downloadCompleted = false;
                if (!Directory.Exists(pluginDir))
                {
                    downloadCompleted = DownloadPluginUpdates(pluginId, null, worker);
                }
                else if (!Version.TryParse(versionStr, out currentVersion))
                {
                    downloadCompleted = DownloadPluginUpdates(pluginId, currentVersion, worker);

                    // Так как не удалось извлечь версию плагина из локального хранилища, считаем что записи плагина
                    // в хранилище нет, проверяем наличие папки плагина и удалем ее.
                    if (Directory.Exists(pluginDir))
                        Directory.Delete(pluginDir, true);
                }
                else if (IsUpdatesAvailable(pluginId, currentVersion, _client))
                {
                    downloadCompleted = DownloadPluginUpdates(pluginId, currentVersion, worker);
                }
                else
                    continue;

                if (!downloadCompleted)
                    continue;

                if (worker != null)
                    worker.ReportProgress(0, string.Format("Применение обновлений для плагина {0}", pluginName));

                string updateOutputPath = Path.Combine(_tempDir, pluginName);
                string destinationPath = Path.Combine(_pluginsDir, pluginName);
                string backupPath = null;
                if (currentVersion != null)
                    backupPath = Path.Combine(destinationPath, "Old", currentVersion.ToString());

                ApplyUpdates(worker, updateOutputPath, destinationPath, backupPath);

                Directory.Delete(updateOutputPath, true);

                // сохраняем информацию о плагине в локальное хранинище. 
                Core.LocalSettings.UserStorage.Plugin plugin = new Core.LocalSettings.UserStorage.Plugin()
                {
                    Caption = pluginInfo.Caption,
                    Description = pluginInfo.Description,
                    MainFile = pluginInfo.MainFileName,
                    Name = pluginInfo.Name,
                    PluginId = pluginInfo.Id,
                    Version = pluginInfo.LatestVersion
                };

                _settingsProvider.SetExistingPlugins(plugin);

            }
        }

        private bool DownloadPluginUpdates(int pluginId, Version currentVersion, BackgroundWorker worker)
        {
            int currentState = 0;
            try
            {
                string pluginName = _client.GetPluginInfo(pluginId)?.Name;
                if (worker != null)
                    worker.ReportProgress(currentState, string.Format("Получение обновлений для плагина {0}", pluginName));

                string updateOutputPath = Path.Combine(_tempDir, pluginName);

                if (!Directory.Exists(updateOutputPath))
                    Directory.CreateDirectory(updateOutputPath);

                List<UpdateEntry> updates;
                if (currentVersion == null)
                    updates = _client.GetPluginLatestUpdates(pluginId);
                else
                    updates = _client.GetPluginUpdates(pluginId, currentVersion.ToString());

                currentState = SaveUpdates(worker, currentState, updateOutputPath, updates.ToArray());

                return true;
            }
            catch (Exception ex)
            {
                worker.ReportProgress(currentState, ex.Message);
                return false;
            }
        }

        private void CreateOrClearDir(string destinationPath)
        {
            if (Directory.Exists(destinationPath))
            {
                Directory.Delete(destinationPath, true);
                Directory.CreateDirectory(destinationPath);
            }
            else
                Directory.CreateDirectory(destinationPath);
        }

        private int SaveUpdates(BackgroundWorker worker, int currentState, string updateOutputPath, UpdateEntry[] updates)
        {
            double step = 1;
            foreach (var update in updates)
            {

                currentState = (int)Math.Round((step / updates.Length) * 100);
                step++;
                if (worker != null)
                    worker.ReportProgress(currentState, string.Format("Сохранение файла {0} плагина {1}", update.EntryName, update.EntryPluginName));

                using (FileStream file = new FileStream(Path.Combine(updateOutputPath, update.EntryName + ".update"), FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(UpdateEntry));
                    serializer.WriteObject(file, update);
                }
            }

            return currentState;
        }

        private void ApplyUpdates(BackgroundWorker worker, string updateOutputPath, string destinationPath, string backupPath)
        {
            double step = 1;
            var files = Directory.GetFiles(updateOutputPath, "*.update", SearchOption.TopDirectoryOnly);
            foreach (var item in files)
            {
                UpdateEntry entry;
                using (FileStream file = new FileStream(item, FileMode.Open, FileAccess.Read))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(UpdateEntry));
                    entry = (UpdateEntry)serializer.ReadObject(file);
                }

                string fileDestinationPath;
                if (string.IsNullOrWhiteSpace(entry.EntryPath))
                    fileDestinationPath = Path.Combine(destinationPath, entry.EntryName);
                else
                    fileDestinationPath = Path.Combine(destinationPath, entry.EntryPath, entry.EntryName);
                if (!Directory.Exists(Path.GetDirectoryName(fileDestinationPath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(fileDestinationPath));

                // создаем бэкап файлов, подлежащих перезаписи и/или удалению.
                if (!string.IsNullOrWhiteSpace(backupPath))
                {
                    if (!Directory.Exists(backupPath))
                        Directory.CreateDirectory(backupPath);

                    if (File.Exists(fileDestinationPath))
                        File.Move(fileDestinationPath, Path.Combine(backupPath, entry.EntryName + ".old"));
                }

                int current = (int)Math.Round((step / files.Length) * 100);
                step++;
                switch (entry.EntryAction)
                {
                    case UpdateAction.Add:
                        if (worker != null)
                            worker.ReportProgress(current, string.Format("Создание файла {0}", entry.EntryName));
                        using (FileStream outStream = new FileStream(fileDestinationPath, FileMode.Create, FileAccess.Write))
                            outStream.Write(entry.EntryBinaryData, 0, entry.EntryBinaryData.Length);
                        break;
                    case UpdateAction.Overwrite:
                        if (worker != null)
                            worker.ReportProgress(current, string.Format("Обновление файла {0}", entry.EntryName));
                        using (FileStream outStream = new FileStream(fileDestinationPath, FileMode.Create, FileAccess.Write))
                            outStream.Write(entry.EntryBinaryData, 0, entry.EntryBinaryData.Length);
                        break;
                    case UpdateAction.Remove:
                        if (worker != null)
                            worker.ReportProgress(current, string.Format("Удаление файла {0}", entry.EntryName));
                        File.Delete(fileDestinationPath);
                        break;

                    case UpdateAction.Rename:
                    default:
                        break;
                }
            }
        }

        [DllImport("kernel32.dll")]
        private static extern int RegisterApplicationRestart([MarshalAs(UnmanagedType.BStr)] string commandLineArgs, RestartRestrictions flags);

        [Flags]
        private enum RestartRestrictions
        {
            None = 0,
            NotOnCrash = 1,
            NotOnHang = 2,
            NotOnPatch = 4,
            NotOnReboot = 8
        }
        #endregion Private Methods



        #region Events

        internal void OnUpdateProgressChanged(int progress, string status)
        {
            EventHandler<UpdateProgressChangedEventArgs> temp = Interlocked.CompareExchange(
                ref UpdateProgressChanged,
                null,
                null);
            if (temp != null)
                temp(this, new UpdateProgressChangedEventArgs(progress, status));
        }

        public event EventHandler<UpdateProgressChangedEventArgs> UpdateProgressChanged;

        internal void OnCheckUpdatesCompleted(bool result)
        {
            EventHandler<CheckUpdatesCompletedEventArgs> temp = Interlocked.CompareExchange(
                ref CheckUpdatesCompleted,
                null,
                null);
            if (temp != null)
                temp(this, new CheckUpdatesCompletedEventArgs(result));
        }

        public event EventHandler<CheckUpdatesCompletedEventArgs> CheckUpdatesCompleted;

        public event RunWorkerCompletedEventHandler UpdateCompleted;

        internal void OnUpdateCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RunWorkerCompletedEventHandler temp = Interlocked.CompareExchange(
                ref UpdateCompleted,
                null,
                null);
            if (temp != null)
                temp(this, e);
        }

        #endregion Events
    }
}