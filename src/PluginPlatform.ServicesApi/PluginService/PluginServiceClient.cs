﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using PluginPlatform.Common.DataModels.PluginService;
using PluginPlatform.Core.Http;
using RestSharp;

namespace PluginPlatform.ServicesApi.UpdateService
{
    internal class PluginServiceClient : BaseClient<PluginServiceClient>, IPluginsController
    {
        public PluginServiceClient(string baseAddress)
            : base(baseAddress)
        { }

        public PluginInfo GetPluginInfo(int pluginId = 0, string pluginName = null)
        {
            var request = new RestRequest("api/plugins/GetPluginInfo", Method.GET);
            if (pluginId > 0)
                request.AddParameter("pluginId", pluginId);
            if (!string.IsNullOrEmpty(pluginName))
                request.AddParameter("pluginName", pluginName);

            return Execute<PluginInfo>(request);
        }

        public List<UpdateEntry> GetPluginLatestUpdates(int pluginId)
        {
            return Execute<List<UpdateEntry>>(
                new RestRequest("api/plugins/GetPluginLatestUpdates", Method.GET)
                    .AddParameter("pluginId", pluginId));
        }

        public List<UpdateEntry> GetPluginUpdates(int pluginId, string currentVersion)
        {
            return Execute<List<UpdateEntry>>(
                new RestRequest("api/plugins/GetPluginUpdates", Method.GET)
                    .AddParameter("pluginId", pluginId)
                    .AddParameter("currentVersion", currentVersion));
        }

        public List<UpdateEntry> GetShellLatestUpdates()
        {
            return Execute<List<UpdateEntry>>(
                new RestRequest("api/shell/GetShellLatestUpdates", Method.GET));
        }

        public List<UpdateEntry> GetShellUpdates(string currentVersion)
        {
            return Execute<List<UpdateEntry>>(
                new RestRequest("api/shell/GetShellUpdates", Method.GET)
                    .AddParameter("currentVersion", currentVersion));
        }

        public bool IsShellUpdatesAvaliable(string currentVersion)
        {
            return Execute<bool>(
                new RestRequest("api/shell/IsShellUpdatesAvaliable", Method.GET)
                    .AddParameter("currentVersion", currentVersion));
        }

        public bool IsUpdatesAvailable(int pluginId, string currentVersion)
        {
            return Execute<bool>(
                new RestRequest("api/plugins/IsUpdatesAvailable", Method.GET)
                    .AddParameter("pluginId", pluginId)
                    .AddParameter("currentVersion", currentVersion));
        }
    }
}
