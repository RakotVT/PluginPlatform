﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using PluginPlatform.Common.DataModels.LogService;
using PluginPlatform.Common.DataModels.UserService;
using PluginPlatform.ServicesCore.Models;

namespace PluginPlatform.LogService.Controllers
{
    [Authorize]
    public class LogsController : ApiController
    {
        private UmsDbContext context = new UmsDbContext();

        public int GetUserTicketId(string sessionKey)
        {
            int userId;
            UserTicket ticket;

            UserInfo userInfo = GetUserInfo().Result;
            userId = userInfo.Id;
            ticket = new UserTicket();
            ticket.UserId = userId;
            ticket.Date = DateTime.Now;
            context.UserTickets.Add(ticket);
            context.SaveChanges();
            return ticket.Id;
        }

        public void SendLog(LogMessage message)
        {
            int messageSourceId;
            LogMessageSource messSource = context.LogMessageSources.Where(x => x.Name == message.MessageSource).FirstOrDefault();
            if (messSource == null)
            {
                messSource = new LogMessageSource();
                messSource.Name = message.MessageSource;
                context.LogMessageSources.Add(messSource);
                context.SaveChanges();
                messageSourceId = messSource.Id;
            }
            else
                messageSourceId = messSource.Id;

            LogMinidump miniDump = new LogMinidump();
            miniDump.Content = message.MinidumpContent;
            miniDump.Name = message.MinidumpName;
            context.LogMinidumps.Add(miniDump);
            context.SaveChanges();

            LogScreenshot screenShot = new LogScreenshot();
            screenShot.Content = message.ScreenshotContent;
            screenShot.Name = message.ScreenshotName;
            context.LogScreenshots.Add(screenShot);
            context.SaveChanges();

            Log logEntry = new Log();
            logEntry.Date = message.Date;
            logEntry.ErrorLocation = message.ErrorLocation;
            logEntry.ErrorMessage = message.ErrorMessage;
            logEntry.MessageLevel = message.MessageLevel;
            logEntry.StackTrace = message.StackTrace;
            logEntry.UserTicketId = message.TicketId;
            logEntry.LogScreenshotId = screenShot.Id;
            logEntry.LogMinidumpId = miniDump.Id;
            logEntry.LogMessageSourceId = messageSourceId;
            logEntry.UserMessage = message.UserMessage;
            context.Logs.Add(logEntry);
            context.SaveChanges();
        }

        public IHttpActionResult GetAll()
        {
            var result = context.Logs;
            return Ok(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }

        private async Task<UserInfo> GetUserInfo()
        {
            if (Request.Headers.Authorization == null)
                return null;

            var requestUrl = Request.RequestUri;
            string baseAddress = $"{requestUrl.Scheme}://{requestUrl.Host}:8090";
            UserInfo userInfo = null;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseAddress);
            client.DefaultRequestHeaders.Authorization = Request.Headers.Authorization;

            var response = await client.GetAsync("api/account/userinfo");

            if (response.IsSuccessStatusCode)
                userInfo = await response.Content.ReadAsAsync<UserInfo>();
            return userInfo;
        }
    }
}
