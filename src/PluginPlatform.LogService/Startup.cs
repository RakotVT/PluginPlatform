﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Extensions;
using Owin;

[assembly: OwinStartup(typeof(PluginPlatform.LogService.Startup))]

namespace PluginPlatform.LogService
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);

            app.UseStageMarker(PipelineStage.Authenticate);
            app.UseOAuthBearerAuthentication(new Microsoft.Owin.Security.OAuth.OAuthBearerAuthenticationOptions());

            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(config);
        }
    }
}
