﻿using System;
using System.AddIn.Contract;
using System.AddIn.Pipeline;
using System.Reflection;
using System.Windows.Forms.Integration;
using System.Windows.Interop;
using System.Windows.Threading;
using PluginPlatform.Core.PluginLoader;
using PluginPlatform.Core.WinFunctions;
using PluginPlatform.PluginCore.API;
using WinForms = System.Windows.Forms;
using WPF = System.Windows;

namespace ums.PluginHost
{
    /// <summary>
    /// Представляет функционал изоляции внутри отдельного процесса загружаемого плагина. 
    /// </summary>
    internal class PluginLoader : MarshalByRefObject, IPluginLoader, IUserInfo
    {
        public event EventHandler<PluginExceptionEventArgs> ExceptionOccured;

        //PluginLoader(string localAssemblyStorage)
        //{
        //    AssemblyResolver.StartAssemblyResolving(localAssemblyStorage);
        //}

        #region IPluginLoader Members

        /// <summary>
        /// Получает форму плагина преобразованную в INativeHandleContract 
        /// </summary>
        /// <param name="assembly"> путь сборки плагина </param>
        /// <returns></returns>
        [STAThread]
        public INativeHandleContract LoadPlugin(string assembly)
        {
            if (String.IsNullOrEmpty(assembly))
                throw new ArgumentNullException("assembly");

            //подписываемся на события необработанных исключений для их вывода пользователю
            //
            //System.Windows.Forms.Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

            HostLoader.Dispatcher.UnhandledException += new DispatcherUnhandledExceptionEventHandler(Dispatcher_UnhandledException);

            //AppDomain.CurrentDomain.DomainUnload += new EventHandler(CurrentDomain_DomainUnload);

            try
            {
                //System.Diagnostics.Debugger.Launch();
                //System.Diagnostics.Debugger.Break();

                //получаем  упакованную форму
                //
                Func<string, INativeHandleContract> createOnUiThread = GetPluginUI;

                var contract = (INativeHandleContract)HostLoader.Dispatcher.Invoke(createOnUiThread, assembly);
                var insulator = new NativeHandleContractInsulator(contract);
                return insulator;
            }
            catch (Exception ex)
            {
                var message = String.Format("Ошибка загрузки плагина из сборки '{0}'. {1}",
                    System.IO.Path.GetFileNameWithoutExtension(assembly), ex.Message);

                throw new ApplicationException(message, ex);
            }
        }

        /// <summary>
        ///Завершает процесс плагина
        /// </summary>
        public void Terminate()
        {
            if (AppDomain.CurrentDomain.IsDefaultAppDomain())
                Environment.Exit(0);
            else
                AppDomain.Unload(AppDomain.CurrentDomain);
        }

        #endregion IPluginLoader Members

        #region Private Methods

        private INativeHandleContract GetContractFromPlugin(IWinFormsPlugin plugin)
        {
            WinForms.Control uiElement = plugin.GetMainPluginUI();
            if (uiElement is IUserInfo && !CurrentSessionID.Equals(Guid.Empty))
                (uiElement as IUserInfo).CurrentSessionID = CurrentSessionID;

            uiElement.Dock = WinForms.DockStyle.Fill;

            WindowsFormsHost host = new WindowsFormsHost();

            //создаем контейнер WinForms для возможности корректной работы MDI формы плагина,
            //а также на случай загрузки плагина в AppDomain для корректной отработки событий
            WinForms.Form container = new WinForms.Form()
            {
                IsMdiContainer = false,
                TopLevel = false,
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.None,
                BackColor = System.Drawing.Color.Black,
                Text = "Plugin Container",
            };

            if (uiElement is WinForms.Form) //если объект не является FrameworkElement, то
            {
                host.Child = container;

                WinForms.Form pluginForm = uiElement as WinForms.Form;
                pluginForm.WindowState = WinForms.FormWindowState.Normal; // выставляем состояние окна в Normal для того, чтобы оно могло менять размер при измении контейнера
                pluginForm.FormBorderStyle = WinForms.FormBorderStyle.None; //убираем границы окна.

                if (pluginForm.IsMdiContainer) // если плагин - это Mdi форма, то сохраняем ее функционал.
                {
                    WindowsFunctions.SetParent(pluginForm.Handle, container.Handle); // привязываем форму плагина к контейнеру P/Invoke SetParent.

                    //привязываем размеры формы плагина к размерам контейнера.
                    pluginForm.DataBindings.Add(new WinForms.Binding("Size", container, "ClientSize", false, WinForms.DataSourceUpdateMode.OnPropertyChanged));
                    pluginForm.Location = new System.Drawing.Point(0, 0);
                    pluginForm.Show();
                }
                else // если нет, то
                {
                    // делаем возможным присоединение фомры к хосту в качестве дочернего элемента. 
                    pluginForm.TopLevel = false;
                    host.Child = pluginForm;
                }

                return FrameworkElementAdapters.ViewToContractAdapter(host); //преобразуем контейнер с плагином в INativeHandleContract
            }
            else
            {
                host.Child = uiElement;
                return FrameworkElementAdapters.ViewToContractAdapter(host); //преобразуем контейнер с плагином в INativeHandleContract
            }
        }

        private INativeHandleContract GetContractFromPlugin(IWPFPlugin plugin)
        {
            WPF.FrameworkElement uiElement = plugin.GetMainPluginUI();
            if (uiElement is IUserInfo && !CurrentSessionID.Equals(Guid.Empty))
                (uiElement as IUserInfo).CurrentSessionID = CurrentSessionID;

            if (uiElement is WPF.Window) //если объект является окном, то
            {
                WindowsFormsHost host = new WindowsFormsHost();

                //создаем контейнер WinForms для возможности корректной работы MDI формы плагина,
                //а также на случай загрузки плагина в AppDomain для корректной отработки событий
                WinForms.Form container = new WinForms.Form()
                {
                    IsMdiContainer = false,
                    TopLevel = false,
                    FormBorderStyle = System.Windows.Forms.FormBorderStyle.None,
                    BackColor = System.Drawing.Color.Black,
                    Text = "Plugin Container",
                };

                host.Child = container;

                WPF.Window windowPlugin = uiElement as WPF.Window;

                //убираем границы и хаголовок окна плагина
                windowPlugin.ResizeMode = WPF.ResizeMode.NoResize;
                windowPlugin.WindowStyle = WPF.WindowStyle.None;
                windowPlugin.Left = windowPlugin.Top = 0;

                //сворачиваем окно для того, чтобы оно не было видно при загрузке плагина
                windowPlugin.WindowState = WPF.WindowState.Minimized;
                windowPlugin.Show();

                container.SizeChanged += (sender, e) =>
                    {
                        windowPlugin.Width = container.ClientSize.Width;
                        windowPlugin.Height = container.ClientSize.Height;
                    };

                IntPtr windowsHandle = new WindowInteropHelper(windowPlugin).Handle;
                WindowsFunctions.SetParent(windowsHandle, container.Handle);  //привязываем окно плагина к контейнеру P/Invoke SetParent

                return FrameworkElementAdapters.ViewToContractAdapter(host); //преобразуем контейнер с плагином в INativeHandleContract
            }
            else //если объект является FrameworkElement, то
            {
                return FrameworkElementAdapters.ViewToContractAdapter(uiElement);
            }
        }

        private INativeHandleContract GetPluginUI(string assemblyPath)
        {
            bool errorsOnCreate = false;

            try
            {
                Assembly assembly = Assembly.LoadFrom(assemblyPath);
                HostLoader.PluginName = assembly.GetName().Name;
                Type[] allTypes = assembly.GetTypes();
                foreach (Type item in allTypes)
                {
                    if (item.IsClass && item.GetInterface("IPlugin`1") != null)
                    {
                        if (item.GetInterface("IWPFPlugin", true) != null)
                        {
                            IWPFPlugin plugin = (IWPFPlugin)Activator.CreateInstance(item);
                            return GetContractFromPlugin(plugin);
                        }
                        else if (item.GetInterface("IWinFormsPlugin", true) != null)
                        {
                            IWinFormsPlugin plugin = (IWinFormsPlugin)Activator.CreateInstance(item);
                            return GetContractFromPlugin(plugin);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }

                return null;
            }
            catch (Exception)
            {
                errorsOnCreate = true;
                throw;
            }
            finally
            {
                if (errorsOnCreate)
                    Terminate();
            }
        }

        #endregion Private Methods

        #region EventHandlers

        private void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            HostLoader.ShowErrorMessage(e.Exception);
        }

        private void CurrentDomain_DomainUnload(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(2000);
        }

        private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            HostLoader.ShowErrorMessage(e.Exception);
        }

        #endregion EventHandlers

        #region MarshalByRefObject Override

        public override object InitializeLifetimeService()
        {
            return null;
        }

        #endregion MarshalByRefObject Override

        #region IUserInfo Members

        public string CurrentSessionID { get; set; }

        #endregion
    }
}