﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.Integration;
using System.Reflection;
using System.Windows.Forms;
using ums.Core.WinFunctions;

namespace ums.PluginHost
{
    public class KeyEventsWindowsFormsHost : WindowsFormsHost
    {
        public KeyEventsWindowsFormsHost(): base()
        {
            KeyDown += new System.Windows.Input.KeyEventHandler(KeyEventsWindowsFormsHost_KeyDown);
            EnableWindowsFormsInterop();
        }

        void KeyEventsWindowsFormsHost_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //WindowsFunctions.PostMessage(this.Child.Handle, 0x0100, _wParam, _lParam);
        }

        int _wParam, _lParam;



        private void DoMethodByReflection()
        {
            Type t = typeof(WindowsFormsHost);
            FieldInfo[] properties =  t.GetFields( BindingFlags.GetField | BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (FieldInfo property in properties)
            {
                if (property.Name == "_hostContainerInternal")
                {
                    //object obj = Activator.CreateInstance(property.FieldType, new object[] { this });
                    var tmp = property.GetValue(this);
                    MethodInfo method = property.FieldType.GetMethod("HandleChildActivate");
                    method.Invoke(tmp, new object[] { });
                }
            }
        }


        protected override IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == 256)
            {
            //    DoMethodByReflection();
            //    if (((Form)this.Child).ActiveControl != null)
            //        ((Form)this.Child).ActiveControl.Focus();
            //    //handled = true;
            }
            //WindowsFunctions.PostMessage(this.Child.Handle, (uint)msg, (int)wParam, (int)lParam);
            //if (msg == 0x0100)
            //WindowsFunctions.PostMessage(this.Child.Handle, (uint)msg, (int)wParam, (int)lParam);

            return base.WndProc(hwnd, msg, wParam, lParam, ref handled);
        }


        protected override bool TranslateAcceleratorCore(ref System.Windows.Interop.MSG msg, System.Windows.Input.ModifierKeys modifiers)
        {
            return base.TranslateAcceleratorCore(ref msg, modifiers);
        }
    }
}
