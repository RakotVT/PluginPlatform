﻿using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Serialization.Formatters;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Windows.Threading;

namespace ums.PluginHost
{
    internal class HostLoader
    {
        private static Dispatcher _dispatcher;

        public static Dispatcher Dispatcher
        {
            get
            {
                if (_dispatcher == null)
                    return System.Windows.Threading.Dispatcher.CurrentDispatcher;
                return _dispatcher;
            }
            private set
            {
                _dispatcher = value;
            }
        }

        private static string _pluginName;

        public static string PluginName
        {
            get { return HostLoader._pluginName; }
            set { HostLoader._pluginName = value; }
        }

        [STAThread]
        public static void StartHost(string name)
        {
            try
            {
                //System.Diagnostics.Debugger.Launch();
                //System.Diagnostics.Debugger.Break();

                int bits = IntPtr.Size * 8;
                Console.WriteLine("Starting ums.PluginHost {0}, {1} bit", name, bits);

                Dispatcher = Dispatcher.CurrentDispatcher;

                RegisterServerChannel(name);

                RemotingConfiguration.RegisterWellKnownServiceType(
                    typeof(PluginLoader), "PluginLoader", WellKnownObjectMode.Singleton);

                SignalReady(name);

                Dispatcher.Run();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
        }

        private static void SignalReady(string name)
        {
            var eventName = name + ".Ready";
            var readyEvent = EventWaitHandle.OpenExisting(eventName);
            readyEvent.Set();
        }

        private static void RegisterServerChannel(string name)
        {
            var serverProvider = new BinaryServerFormatterSinkProvider { TypeFilterLevel = TypeFilterLevel.Full };
            var clientProvider = new BinaryClientFormatterSinkProvider();
            var properties = new Hashtable();
            properties["portName"] = name;
            properties["authorizedGroup"] = Environment.UserName;
            properties["secure"] = "true";
            properties["impersonate"] = "true";

            var channel = new IpcChannel(properties, clientProvider, serverProvider);
            ChannelServices.RegisterChannel(channel, true);
        }

        private static void RegisterServerChannel2(string name)
        {
            var serverProvider = new BinaryServerFormatterSinkProvider { TypeFilterLevel = TypeFilterLevel.Full };
            var clientProvider = new BinaryClientFormatterSinkProvider();
            var properties = new Hashtable();
            properties["portName"] = name;

            // Current user sid
            SecurityIdentifier currentUserSid = WindowsIdentity.GetCurrent().User;

            // All users sid
            SecurityIdentifier allUsersSid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            // Network sid
            SecurityIdentifier networkSid = new SecurityIdentifier(@"S-1-5-2");

            // Настройки ACL
            DiscretionaryAcl dAcl = new DiscretionaryAcl(false, false, 1);
            //dAcl.AddAccess(AccessControlType.Deny, networkSid, -1, InheritanceFlags.None, PropagationFlags.None);
            dAcl.AddAccess(AccessControlType.Deny, allUsersSid, -1, InheritanceFlags.None, PropagationFlags.None);
            dAcl.AddAccess(AccessControlType.Allow, currentUserSid, -1, InheritanceFlags.None, PropagationFlags.None);

            CommonSecurityDescriptor securityDescriptor =
                new CommonSecurityDescriptor(false, false, ControlFlags.OwnerDefaulted | ControlFlags.DiscretionaryAclPresent, currentUserSid, null, null, dAcl);

            var channel = new IpcChannel(properties, clientProvider, serverProvider, securityDescriptor);
            ChannelServices.RegisterChannel(channel, false);
        }

        private static void RegisterServerChannel3(string name)
        {
            IDictionary props = new Hashtable();
            props["portName"] = name;

            // This is the wellknown sid for network sid
            string networkSidSddlForm = @"S-1-5-2";
            // Local administrators sid
            SecurityIdentifier localAdminSid = new SecurityIdentifier(
                WellKnownSidType.BuiltinAdministratorsSid, null);
            // Local Power users sid
            SecurityIdentifier powerUsersSid = new SecurityIdentifier(
                WellKnownSidType.BuiltinPowerUsersSid, null);
            // Network sid
            SecurityIdentifier networkSid = new SecurityIdentifier(networkSidSddlForm);

            DiscretionaryAcl dacl = new DiscretionaryAcl(false, false, 1);

            //// Disallow access from off machine
            dacl.AddAccess(AccessControlType.Deny, networkSid, -1,
                InheritanceFlags.None, PropagationFlags.None);

            // Allow acces only from local administrators and power users
            dacl.AddAccess(AccessControlType.Allow, localAdminSid, -1,
                InheritanceFlags.None, PropagationFlags.None);
            dacl.AddAccess(AccessControlType.Allow, powerUsersSid, -1,
                InheritanceFlags.None, PropagationFlags.None);

            CommonSecurityDescriptor securityDescriptor =
                new CommonSecurityDescriptor(false, false,
                        ControlFlags.GroupDefaulted |
                        ControlFlags.OwnerDefaulted |
                        ControlFlags.DiscretionaryAclPresent,
                        null, null, null, dacl);

            IpcServerChannel channel = new IpcServerChannel(
                                                    props,
                                                    null,
                                                    securityDescriptor);

            ChannelServices.RegisterChannel(channel, false /*ensureSecurity*/);
        }

        /// <summary>
        /// Формирует окно с сообщением о необработанном исключении в плагине
        /// </summary>
        /// <param name="ex"></param>
        public static void ShowErrorMessage(Exception ex)
        {
            string exMessage;
            if (ex.InnerException != null)
                exMessage = string.Format("Было перехвачено необработанное исключение:{0}{1}{0}Внутреннее исключение:{0}{2}{0}Перезагрузите плагин.",
                    Environment.NewLine,
                    ex.Message,
                    ex.InnerException.Message);
            else
                exMessage = string.Format("Было перехвачено необработанное исключение:{0}{1}{0}Перезагрузите плагин.", Environment.NewLine, ex.Message);
            string header = string.Format("Ошибка плагина [{0}]", _pluginName);
            Console.WriteLine("============================");
            Console.WriteLine(header);
            Console.WriteLine(exMessage);
            System.Windows.MessageBox.Show(exMessage, header, System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
        }
    }
}
