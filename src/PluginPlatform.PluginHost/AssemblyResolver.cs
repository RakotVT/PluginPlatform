﻿using System;
using System.IO;
using System.Reflection;

namespace ums.PluginHost
{
    public static class AssemblyResolver
    {
        private static string _localAssemblyStorage;

        public static void StartAssemblyResolving(string localAssemblyStorage)
        {
            _localAssemblyStorage = localAssemblyStorage;
            AppDomain.CurrentDomain.AssemblyResolve -= AssemblyResolve;
            AppDomain.CurrentDomain.AssemblyResolve += AssemblyResolve;
        }

        private static Assembly AssemblyResolve(object sender, ResolveEventArgs args)
        {
            Assembly resolvedAssembly = null;
            string assemblyPath = null;
            AssemblyName assemblyName = new AssemblyName(args.Name);
            // проверим не загружена ли сборка ранее
            Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly item in loadedAssemblies)
            {
                if (item.GetName().FullName.Equals(assemblyName.FullName))
                    return item;
            }

            // ищем сборку в хранилище
            string[] assemblies = Directory.GetFiles(
                _localAssemblyStorage,
                string.Format("{0}.dll", assemblyName.Name), 
                SearchOption.AllDirectories);
            foreach (string item in assemblies)
            {
                try
                {
                    AssemblyName itemName = AssemblyName.GetAssemblyName(item);
                    if (itemName.FullName.Equals(assemblyName.FullName))
                    {
                        assemblyPath = item;
                        break;
                    }
                }
                catch (Exception) { }
                
            }

            // попробуем загрузить сборку из локального хранилища
            if (File.Exists(assemblyPath))
                resolvedAssembly = Assembly.LoadFrom(assemblyPath);

            // возвращаем то, что нашли, а может быть и null
            return resolvedAssembly;
        }
    }
}
