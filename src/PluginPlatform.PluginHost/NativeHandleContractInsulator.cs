﻿using System;
using System.AddIn.Contract;

namespace ums.PluginHost
{
    class NativeHandleContractInsulator : MarshalByRefObject, INativeHandleContract
    {
        #region Private Fields

        private readonly INativeHandleContract _source;

        #endregion Private Fields

        #region Public Constructors

        public NativeHandleContractInsulator(INativeHandleContract source)
        {
            _source = source;    
        }

        #endregion Public Constructors

        #region Public Methods

        public int AcquireLifetimeToken()
        {
            return _source.AcquireLifetimeToken();
        }

        public IntPtr GetHandle()
        {
            return _source.GetHandle();
        }
        public int GetRemoteHashCode()
        {
            return _source.GetRemoteHashCode();
        }

        public override object InitializeLifetimeService()
        {
            return null; // live forever
        }

        public IContract QueryContract(string contractIdentifier)
        {
            return _source.QueryContract(contractIdentifier);
        }

        public bool RemoteEquals(IContract contract)
        {
            return _source.RemoteEquals(contract);
        }

        public string RemoteToString()
        {
            return _source.RemoteToString();
        }

        public void RevokeLifetimeToken(int token)
        {
            _source.RevokeLifetimeToken(token);
        }

        #endregion Public Methods
    }
}
