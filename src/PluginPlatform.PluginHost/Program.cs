﻿using System;
using System.Windows.Forms;

namespace ums.PluginHost
{
    class Program
    {
        static Program()
        {
            string[] args = Environment.GetCommandLineArgs();
            AssemblyResolver.StartAssemblyResolving(args[2]);
        }

        [STAThread]
        static void Main(string[] args)
        {

            if (args.Length == 0)
            {
                Console.Error.WriteLine("Usage: ums.PluginHost name");
                return;
            }

            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += Application_ThreadException;

            HostLoader.StartHost(args[0]);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            HostLoader.ShowErrorMessage(e.Exception);
        }

    }
}
