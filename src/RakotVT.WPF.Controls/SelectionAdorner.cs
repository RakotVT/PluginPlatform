﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace ITWebNet.WPF.Controls
{
    internal sealed class SelectionAdorner : System.Windows.Documents.Adorner
    {
        // Initializes a new instance of the SelectionAdorner class.
        public SelectionAdorner(UIElement parent)
            : base(parent)
        {
            // Make sure the mouse doesn't see us.
            this.IsHitTestVisible = false;

            // We only draw a rectangle when we're enabled.
            this.IsEnabledChanged += delegate { this.InvalidateVisual(); };
        }

        public Rect HighlightArea
        {
            get { return (Rect)GetValue(HighlightAreaProperty); }
            set { SetValue(HighlightAreaProperty, value); }
        }

        public static readonly DependencyProperty HighlightAreaProperty = DependencyProperty.Register(
            "HighlightArea",
            typeof(Rect),
            typeof(SelectionAdorner),
            new FrameworkPropertyMetadata(
                new Rect(), FrameworkPropertyMetadataOptions.AffectsRender));

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (this.IsEnabled)
            {
                // Make the lines snap to pixels (add half the pen width [0.5])
                double[] x = { this.HighlightArea.Left + 0.5, this.HighlightArea.Right + 0.5 };
                double[] y = { this.HighlightArea.Top + 0.5, this.HighlightArea.Bottom + 0.5 };
                drawingContext.PushGuidelineSet(new GuidelineSet(x, y));

                Brush fill = SystemColors.HighlightBrush.Clone();
                fill.Opacity = 0.4;
                drawingContext.DrawRectangle(
                    fill,
                    new Pen(SystemColors.HighlightBrush, 1.0),
                    this.HighlightArea);
            }
        }
    }
}
