﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace ITWebNet.WPF.Controls
{
    [TemplatePart(Name = "PART_CloseButton", Type = typeof(ButtonBase))]
    public class TabItem : System.Windows.Controls.TabItem
    {
        #region Dependency Properties

        public static readonly DependencyProperty AllowDeleteProperty = DependencyProperty.Register(
            "AllowDelete",
            typeof(bool),
            typeof(TabItem),
            new UIPropertyMetadata(true));

        public static readonly DependencyProperty CloseCommandParameterProperty = DependencyProperty.Register(
            "CloseCommandParameter",
            typeof(object),
            typeof(TabItem),
            new UIPropertyMetadata(null));

        public static readonly DependencyProperty CloseCommandProperty = DependencyProperty.Register(
            "CloseCommand"
            , typeof(ICommand)
            , typeof(TabItem)
            , new UIPropertyMetadata(null));

        public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
            "Icon",
            typeof(object),
            typeof(TabItem),
            new UIPropertyMetadata(null));

        [Category("Common Properties")]
        [Description("Возвращает или задает значение, показывающее, разрешено ли удаление вкладок.")]
        public bool AllowDelete
        {
            get { return (bool)GetValue(AllowDeleteProperty); }
            set { SetValue(AllowDeleteProperty, value); }
        }

        [DefaultValue(null)]
        [Category("Common Properties")]
        [Description("Возвращает или задает комманду, которая выполняет при закрытии вкладки.")]
        public ICommand CloseCommand
        {
            get { return (ICommand)GetValue(CloseCommandProperty); }
            set { SetValue(CloseCommandProperty, value); }
        }

        [DefaultValue(null)]
        [Category("Common Properties")]
        [Description("Возвращает или задает параметр для комманды, которая выполняет при закрытии вкладки.")]
        public object CloseCommandParameter
        {
            get { return (object)GetValue(CloseCommandParameterProperty); }
            set { SetValue(CloseCommandParameterProperty, value); }
        }

        [Category("Common Properties")]
        [Description("Возвращает или задает иконку вкладки.")]
        public object Icon
        {
            get { return (object)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        #endregion Dependency Properties

        #region Public Constructors

        static TabItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TabItem), new FrameworkPropertyMetadata(typeof(TabItem)));
        }

        public TabItem() 
            : base() {}

        #endregion Public Constructors

        #region Public Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();


            TabControl tc = this.FindVisualParent<TabControl>();
            if (tc == null)
                return;

            //CompositeCommand commands = new CompositeCommand();
            //commands.ExecutionList.Add(new DelegateCommand(tc.CloseTab));

            if (this.CloseCommand == null)
                this.CloseCommand = new DelegateCommand(tc.CloseTab);

            //this.CloseCommand = commands;

            if (this.CloseCommandParameter == null)
                this.CloseCommandParameter = this;
        }

        public void Close()
        {
            if (CloseCommand != null && CloseCommandParameter != null && CloseCommand.CanExecute(CloseCommandParameter))
                CloseCommand.Execute(CloseCommandParameter);
        }

        #endregion Public Methods
    }
}