﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ITWebNet.WPF.Controls.Converters
{
    public class ListToStringConverter : ConverterBase<ListToStringConverter>
    {
        public string JoinProperty { get; set; }
        public string Separator { get; set; }

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            IList list = value as IList;

            if (list == null || list.Count == 0)
                return null;

            if (JoinProperty == null)
                return string.Join(Separator ?? ", ", list);

            var property = list[0].GetType().GetProperty(JoinProperty);

            List<string> result = new List<string>();

            foreach (var item in list)
            {
                result.Add(property.GetValue(item, null).ToString());
            }

            return string.Join(Separator ?? ", ", result);
        }
    }
}
