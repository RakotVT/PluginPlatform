﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ITWebNet.WPF.Controls.Converters.WinApi;

namespace ITWebNet.WPF.Controls.Converters
{
    public class FileToThumbnailConverter : ConverterBase<FileToThumbnailConverter>
    {
        #region IValueConverter Members

        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
                return GetThumbnail(value as string);
            else
                return GetThumbnail("");
        }


        #endregion

        private ImageSource GetThumbnail(string filePath)
        {
            List<string> imageFilter = new List<string>() { ".jpg", ".png", ".gif", ".bmp", ".tiff", ".ico" };

            if (System.IO.File.Exists(filePath) && imageFilter.Contains(System.IO.Path.GetExtension(filePath)))
            {
                Bitmap image = new Bitmap(filePath);
                var thumbnail = image.GetThumbnailImage(32, 32, null, IntPtr.Zero) as Bitmap;

                try
                {
                    return Imaging.CreateBitmapSourceFromHBitmap(
                        thumbnail.GetHbitmap(),
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                }
                finally
                {
                    UnmanagedMethods.DeleteObject(thumbnail.GetHbitmap());
                    image.Dispose();
                    thumbnail.Dispose();
                }
            }

            string fName = filePath;

            SHFILEINFO shinfo = new SHFILEINFO();

            IntPtr hImgLarge = UnmanagedMethods.SHGetFileInfo(
                fName,
                0,
                ref shinfo,
                (uint)Marshal.SizeOf(shinfo),
                SHGFI.Icon | SHGFI.LargeIcon | SHGFI.UseFileAttributes);

            System.Drawing.Icon myIcon = System.Drawing.Icon.FromHandle(shinfo.hIcon);

            ImageSource img = Imaging.CreateBitmapSourceFromHIcon(
                                    myIcon.Handle,
                                    Int32Rect.Empty,
                                    BitmapSizeOptions.FromEmptyOptions());

            UnmanagedMethods.DestroyIcon(myIcon.Handle);

            return img;
        }
    }
}
