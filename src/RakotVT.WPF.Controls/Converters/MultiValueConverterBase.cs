﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace ITWebNet.WPF.Controls.Converters
{
    /// <summary>
    /// Представляет собой базовый класс для создания конвертеров <see cref="IMultiValueConverter"/>.
    /// </summary>
    /// <typeparam name="T">Тип конвертера.</typeparam>
    public abstract class MultiValueConverterBase<T> : MarkupExtension, IMultiValueConverter
            where T : class, new()
    {
        #region MarkupExtension Members

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        #endregion

        #region IMultiValueConverter Members

        public abstract object Convert(object[] values, Type targetType, object parameter, CultureInfo culture);

        public virtual object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
