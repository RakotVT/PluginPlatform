﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;

namespace ITWebNet.WPF.Controls.Converters
{
    public class BooleanToVisibilityConverter : ConverterBase<VisibilityToBooleanConverter>
    {
        public bool IsReversed { get; set; }
        public bool UseHidden { get; set; }

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool visibility = System.Convert.ToBoolean(value, culture);

            if (IsReversed)
                visibility = !visibility;

            return visibility ? Visibility.Visible : 
                    UseHidden ? Visibility.Hidden : Visibility.Collapsed;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;

            bool result = visibility == Visibility.Visible;

            if (IsReversed)
                result = !result;

            return result;
        }
    }
}
