﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace ITWebNet.WPF.Controls.Converters
{
    /// <summary>
    /// Представляет собой базовый класс для создания конвертеров <see cref="IValueConverter"/> .
    /// </summary>
    /// <typeparam name="T">Тип конвертера.</typeparam>
    public abstract class ConverterBase<T> : MarkupExtension, IValueConverter
        where T : class, new()
    {
        #region MarkupExtension Members

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        #endregion

        #region IValueConverter Members

        public abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
