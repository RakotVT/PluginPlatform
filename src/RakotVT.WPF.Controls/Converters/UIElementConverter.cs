﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Markup;
using System.Xml;

namespace ITWebNet.WPF.Controls.Converters
{
    internal class UIElementConverter : ConverterBase<UIElementConverter>
    {
        #region IValueConverter Members

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            try
            {
                string xaml = XamlWriter.Save(value);
                var result = XamlReader.Load(XmlReader.Create(new StringReader(xaml)));
                if (result == null)
                    return value;
                else
                    return result;

            }
            catch (Exception)
            {
                return value;
            }
        }

        #endregion
    }
}
