﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ITWebNet.WPF.Controls.Converters
{
    public class VisibilityToBooleanConverter : ConverterBase<VisibilityToBooleanConverter>
    {
        #region IValueConverter Members

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibility = (Visibility)value;
            return visibility == Visibility.Visible;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var @true = (bool)value;
            return @true ? Visibility.Visible : Visibility.Collapsed;
        }

        #endregion
    }
}
