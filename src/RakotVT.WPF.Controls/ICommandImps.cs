﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ITWebNet.WPF.Controls
{
    public class DelegateCommand : ICommand
    {
        private Action<object> _execute;

        private Predicate<object> _canExecute;

        private Action<Exception> _handleException;

        public DelegateCommand(Action<object> execute)
            : this(execute, null, null)
        { }

        public DelegateCommand(Action<object> execute, Action<Exception> handleException)
            : this(execute, null, handleException)
        { }

        public DelegateCommand(Action<object> execute, Predicate<object> canExecute)
            : this(execute, canExecute, null)
        { }

        public DelegateCommand(Action<object> execute, Predicate<object> canExecute, Action<Exception> handleException)
        {
            _execute = execute;
            _canExecute = canExecute;
            _handleException = handleException;
        }

        public DelegateCommand WithCanExecute(Predicate<object> canExecute)
        {
            _canExecute = canExecute;
            return this;
        }

        public DelegateCommand WithHandler(Action<Exception> handleException)
        {
            _handleException = handleException;
            return this;
        }

        public bool CanExecute(object parameter)
        {
            try
            {
                if (_canExecute != null)
                    return _canExecute(parameter);
                else
                    return true;
            }
            catch (Exception ex)
            {
                if (_handleException != null)
                    _handleException(ex);
                else
                    throw ex;

                return false;
            }
        }

        public void Execute(object parameter)
        {
            try
            {
                if (_execute != null)
                    _execute(parameter);

            }
            catch (Exception ex)
            {
                if (_handleException != null)
                    _handleException(ex);
                else
                    throw ex;
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }

    public class CompositeCommand : ICommand
    {
        public ObservableCollection<ICommand> ExecutionList { get; private set; }

        public CompositeCommand(ObservableCollection<ICommand> executionList)
        {
            ExecutionList = executionList;
        }

        public CompositeCommand()
        {
            ExecutionList = new ObservableCollection<ICommand>();
        }

        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            bool result = true;
            foreach (var item in ExecutionList)
            {
                result &= item.CanExecute(parameter);
            }

            return result;
        }

        public void Execute(object parameter)
        {
            foreach (var item in ExecutionList)
            {
                item.Execute(parameter);
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        #endregion
    }

    //public class DelegateCommandOptions
    //{
    //    public Action<object> Execute { get; set; }
    //    public Predicate<bool> CanExecute { get; set; }
    //    public Action<Exception> Handler { get; set; }

    //}

}
