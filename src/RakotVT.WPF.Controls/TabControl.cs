﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace ITWebNet.WPF.Controls
{
    [TemplatePart(Name = "PART_TabNavigator", Type = typeof(MenuItem))]
    [TemplatePart(Name = "PART_RepeatLeft", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "PART_RepeatRight", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "PART_NewTabButton", Type = typeof(ButtonBase))]
    [TemplatePart(Name = "PART_NewTabButtonOnTab", Type = typeof(ButtonBase))]
    [TemplatePart(Name = "PART_ScrollViewer", Type = typeof(ScrollViewer))]
    [TemplatePart(Name = "PART_TabPanel", Type = typeof(UniformStackPanel))]
    public class TabControl : System.Windows.Controls.TabControl
    {
        #region Dependency Properties

        [Category("Common Properties")]
        [Description("Возвращает или задает значение, показывающее, разрешено ли добавление пустой вкладки после закрытия последней.")]
        public bool AddEmptyTab
        {
            get { return (bool)GetValue(AddEmptyTabProperty); }
            set { SetValue(AddEmptyTabProperty, value); }
        }

        public static readonly DependencyProperty AddEmptyTabProperty = DependencyProperty.Register(
            "AddEmptyTab"
            , typeof(bool)
            , typeof(TabControl)
            , new UIPropertyMetadata(true));
        [Category("Common Properties")]
        [Description("Возвращает или задает объект,который должен быть добавлен в коллекцию вкладок.")]
        public object AddTabCommandParameter
        {
            get { return (object)GetValue(AddTabCommandParameterProperty); }
            set { SetValue(AddTabCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty AddTabCommandParameterProperty = DependencyProperty.Register(
            "AddTabCommandParameter"
            , typeof(object)
            , typeof(TabControl));

        [Category("Common Properties")]
        [Description("Возвращает комманду, выполняемую для создания новых вкладок.")]
        public ICommand AddTabCommand
        {
            get { return (ICommand)GetValue(AddTabCommandProperty); }
            set { SetValue(AddTabCommandProperty, value); }
        }

        public static readonly DependencyProperty AddTabCommandProperty = DependencyProperty.Register(
            "AddTabCommand", typeof(ICommand)
            , typeof(TabControl)
            , new UIPropertyMetadata(null));

        [Category("Common Properties")]
        [Description("Возвращает или задает значение, показывающее, разрешено ли создание новых вкладок.")]
        public bool AllowAddNew
        {
            get { return (bool)GetValue(AllowAddNewProperty); }
            set { SetValue(AllowAddNewProperty, value); }
        }

        public static readonly DependencyProperty AllowAddNewProperty = DependencyProperty.Register(
            "AllowAddNew"
            , typeof(bool)
            , typeof(TabControl)
            , new FrameworkPropertyMetadata(
                true
                , new PropertyChangedCallback(OnAllowAddNewChanged)
                , OnCoerceAllowAddNewCallback));

        [Category("Common Properties")]
        [Description("Возвращает или задает значение, показывающее, разрешено ли удаление вкладок.")]
        public bool AllowDelete
        {
            get { return (bool)GetValue(AllowDeleteProperty); }
            set { SetValue(AllowDeleteProperty, value); }
        }

        public static readonly DependencyProperty AllowDeleteProperty = DependencyProperty.Register(
            "AllowDelete"
            , typeof(bool)
            , typeof(TabControl)
            , new FrameworkPropertyMetadata(
                true
                , new PropertyChangedCallback(OnAllowDeleteChanged)
                , OnCoerceAllowDeleteCallback));

        [Category("Brush")]
        [Description("Возвращает или задает фон контента вкладки.")]
        public Brush ContentBackground
        {
            get { return (Brush)GetValue(ContentBackgroundProperty); }
            set { SetValue(ContentBackgroundProperty, value); }
        }

        public static readonly DependencyProperty ContentBackgroundProperty =
            DependencyProperty.Register("ContentBackground", typeof(Brush), typeof(TabControl), new PropertyMetadata(Brushes.White));

        [Category("Common Properties")]
        [Description("Возвращает значение, показывающее, используется ли привязка к источнику данных.")]
        public bool IsUsingItemsSource
        {
            get { return (bool)GetValue(IsUsingItemsSourceProperty); }
            private set { SetValue(IsUsingItemsSourcePropertyKey, value); }
        }

        public static readonly DependencyPropertyKey IsUsingItemsSourcePropertyKey = DependencyProperty.RegisterReadOnly(
            "IsUsingItemsSource"
            , typeof(bool)
            , typeof(TabControl)
            , new UIPropertyMetadata(false));

        public static readonly DependencyProperty IsUsingItemsSourceProperty = IsUsingItemsSourcePropertyKey.DependencyProperty;

        public Visibility NavigationPanelVisibility
        {
            get { return (Visibility)GetValue(NavigationPanelVisibilityProperty); }
            set { SetValue(NavigationPanelVisibilityProperty, value); }
        }

        public static readonly DependencyProperty NavigationPanelVisibilityProperty =
            DependencyProperty.Register("NavigationPanelVisibility", typeof(Visibility), typeof(TabControl), new PropertyMetadata(Visibility.Visible));

        /// <summary>
        /// Возвращает или задает максимальную высоту вкладки.
        /// </summary>
        [Category("Layout")]
        [Description("Возвращает или задает максимальную высоту вкладки.")]
        public double TabItemMaxHeight
        {
            get { return (double)GetValue(TabItemMaxHeightProperty); }
            set { SetValue(TabItemMaxHeightProperty, value); }
        }

        public static readonly DependencyProperty TabItemMaxHeightProperty = DependencyProperty.Register(
            "TabItemMaxHeight",
            typeof(double),
            typeof(TabControl),
            new FrameworkPropertyMetadata(
                50.0,
                new PropertyChangedCallback(OnMinMaxChanged),
                CoerceMaxHeight));

        [Category("Layout")]
        [Description("Возвращает или задает максимальную ширину вкладки.")]
        public double TabItemMaxWidth
        {
            get { return (double)GetValue(TabItemMaxWidthProperty); }
            set { SetValue(TabItemMaxWidthProperty, value); }
        }

        public static readonly DependencyProperty TabItemMaxWidthProperty = DependencyProperty.Register(
            "TabItemMaxWidth"
            , typeof(double)
            , typeof(TabControl)
            , new FrameworkPropertyMetadata(
                150.0
                , new PropertyChangedCallback(OnMinMaxChanged)));

        /// <summary>
        /// Возвращает или задает минимальную высоту вкладки. 
        /// </summary>
        [Category("Layout")]
        [Description("Возвращает или задает минимальную высоту вкладки.")]
        public double TabItemMinHeight
        {
            get { return (double)GetValue(TabItemMinHeightProperty); }
            set { SetValue(TabItemMinHeightProperty, value); }
        }

        public static readonly DependencyProperty TabItemMinHeightProperty = DependencyProperty.Register(
            "TabItemMinHeight",
            typeof(double),
            typeof(TabControl),
            new FrameworkPropertyMetadata(
                20.0,
                new PropertyChangedCallback(OnMinMaxChanged),
                CoerceMinHeight));

        [Category("Layout")]
        [Description("Возвращает или задает минимальную ширину вкладки.")]
        public double TabItemMinWidth
        {
            get { return (double)GetValue(TabItemMinWidthProperty); }
            set { SetValue(TabItemMinWidthProperty, value); }
        }

        public static readonly DependencyProperty TabItemMinWidthProperty = DependencyProperty.Register(
            "TabItemMinWidth"
            , typeof(double)
            , typeof(TabControl)
            , new FrameworkPropertyMetadata(
                50.0
                , new PropertyChangedCallback(OnMinMaxChanged)));

        [Category("Brush")]
        [Description("Возвращает или задает фон дочернего элемента при наведении мыши.")]
        public Brush TabItemMouseOverBackground
        {
            get { return (Brush)GetValue(TabItemMouseOverBackgroundProperty); }
            set { SetValue(TabItemMouseOverBackgroundProperty, value); }
        }

        public static readonly DependencyProperty TabItemMouseOverBackgroundProperty = DependencyProperty.Register(
            "TabItemMouseOverBackground"
            , typeof(Brush)
            , typeof(TabControl)
            , new UIPropertyMetadata(null));

        [Category("Brush")]
        [Description("Возвращает или задает фон дочернего элемента.")]
        public Brush TabItemNormalBackground
        {
            get { return (Brush)GetValue(TabItemNormalBackgroundProperty); }
            set { SetValue(TabItemNormalBackgroundProperty, value); }
        }

        public static readonly DependencyProperty TabItemNormalBackgroundProperty = DependencyProperty.Register(
            "TabItemNormalBackground"
            , typeof(Brush)
            , typeof(TabControl)
            , new UIPropertyMetadata(null));

        [Category("Brush")]
        [Description("Возвращает или задает фон дочернего элемента при нажатии на него.")]
        public Brush TabItemPressedBackground
        {
            get { return (Brush)GetValue(TabItemPressedBackgroundProperty); }
            set { SetValue(TabItemPressedBackgroundProperty, value); }
        }

        public static readonly DependencyProperty TabItemPressedBackgroundProperty = DependencyProperty.Register(
            "TabItemPressedBackground"
            , typeof(Brush)
            , typeof(TabControl)
            , new UIPropertyMetadata(null));

        [Category("Brush")]
        [Description("Возвращает или задает фон выбранной вкладки.")]
        public Brush TabItemSelectedBackground
        {
            get { return (Brush)GetValue(TabItemSelectedBackgroundProperty); }
            set { SetValue(TabItemSelectedBackgroundProperty, value); }
        }

        public static readonly DependencyProperty TabItemSelectedBackgroundProperty = DependencyProperty.Register(
            "TabItemSelectedBackground"
            , typeof(Brush)
            , typeof(TabControl)
            , new UIPropertyMetadata(null));

        [Category("Common Properties")]
        [Description("Возвращает коллекцию элементов TabItem.")]
        public ObservableCollection<TabItem> TabItems
        {
            get { return (ObservableCollection<TabItem>)GetValue(TabItemsProperty); }
            private set { SetValue(TabItemsPropertyKey, value); }
        }

        public static readonly DependencyPropertyKey TabItemsPropertyKey = DependencyProperty.RegisterReadOnly(
            "TabItems",
            typeof(ObservableCollection<TabItem>),
            typeof(TabControl),
            new PropertyMetadata(new ObservableCollection<TabItem>()));

        public static readonly DependencyProperty TabItemsProperty = TabItemsPropertyKey.DependencyProperty;

        #endregion Dependency Properties

        #region Public Constructors

        static TabControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(TabControl),
                new FrameworkPropertyMetadata(typeof(TabControl)));
        }

        public TabControl()
            : base()
        {
            Loaded += (sender, e) =>
                {
                    IsUsingItemsSource = BindingOperations.IsDataBound(this, ItemsSourceProperty);
                    if (this.Items.Count == 0 && AddEmptyTab && !IsFixedSize)
                        AddTabCommand.Execute(AddTabCommandParameter);
                    //if (IsUsingItemsSource && IsFixedSize)
                    //    AllowAddNew = AllowDelete = false;

                    SetAddNewButtonVisibility();
                    SetDeleteButtonVisibility();
                    if (this.SelectedIndex == -1)
                        this.SelectedIndex = 0;
                };
            SelectCommand = new DelegateCommand(SelectTab);
            AddTabCommand = new DelegateCommand(AddNewTab);
        }

        #endregion Public Constructors

        #region Private Properties

        private bool IsFixedSize
        {
            get
            {
                IEnumerable items = GetItems();
                return items as IList == null || (items as IList).IsFixedSize;
            }
        }

        public ICommand SelectCommand { get; private set; }

        #endregion Private Properties

        #region Protected Override Methods

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TabItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TabItem;
        }

        protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            base.OnItemsChanged(e);

            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    TabItem tabItem = GetTabItemFromObject(e.NewItems[e.NewItems.Count - 1]);
                    if (tabItem != null)
                        tabItem.Focus();
                    break;

                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    if (this.Items.Count == 0 && AddEmptyTab && !IsFixedSize && !Dispatcher.HasShutdownStarted)
                        AddTabCommand.Execute(AddTabCommandParameter);
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Move:
                case System.Collections.Specialized.NotifyCollectionChangedAction.Replace:
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    break;

                default:
                    break;
            }

            TabItems = new ObservableCollection<TabItem>(GetTabItems());
        }

        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            base.OnItemsSourceChanged(oldValue, newValue);

            IsUsingItemsSource = newValue != null;
            //if (IsFixedSize)
            //    AllowAddNew = AllowDelete = false;

            SetAddNewButtonVisibility();
            SetDeleteButtonVisibility();
        }

        protected override void OnPreviewMouseWheel(System.Windows.Input.MouseWheelEventArgs e)
        {
            //Добавление прокрутки табов при прокрутке колеса мыши над табами.
            ScrollViewer scrollViewer = this.Template.FindName("PART_ScrollViewer", this) as ScrollViewer;
            if (scrollViewer != null && VisualTreeHelper.HitTest(scrollViewer, e.GetPosition(scrollViewer)) != null)
            {
                if (e.Delta < 0)
                    scrollViewer.LineRight();
                else if (e.Delta > 0)
                    scrollViewer.LineLeft();
            }
            base.OnPreviewMouseWheel(e);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            MenuItem tabNavigator = this.Template.FindName("PART_TabNavigator", this) as MenuItem;

            if (tabNavigator != null)
            {
                tabNavigator.Click += (sender, e) =>
                    {
                        if (tabNavigator.HasItems)
                            tabNavigator.Items.Clear();

                        foreach (var item in GetTabItems())
                        {
                            //MenuItem menuItem = new MenuItem();
                            //menuItem.DataContext = itemContainer;
                            tabNavigator.Items.Add(item);
                        }
                    };
            }
        }

        #endregion Protected Override Methods

        #region Private Methods

        private static object CoerceMaxHeight(DependencyObject d, object value)
        {
            TabControl tc = (TabControl)d;
            double newValue = (double)value;

            if (newValue < tc.TabItemMinHeight)
                return tc.TabItemMinHeight;

            return newValue;
        }

        private static object CoerceMinHeight(DependencyObject d, object value)
        {
            TabControl tc = (TabControl)d;
            double newValue = (double)value;

            if (newValue > tc.TabItemMaxHeight)
                return tc.TabItemMaxHeight;

            return (newValue > 0 ? newValue : 0);
        }

        private static void OnAllowAddNewChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((TabControl)d).SetAddNewButtonVisibility();
        }

        private static void OnAllowDeleteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((TabControl)d).SetDeleteButtonVisibility();
        }

        private static object OnCoerceAllowAddNewCallback(DependencyObject d, object baseValue)
        {
            return ((TabControl)d).OnCoerceAllowAddNewCallback(baseValue);
        }

        private static object OnCoerceAllowDeleteCallback(DependencyObject d, object baseValue)
        {
            return ((TabControl)d).OnCoerceAllowDeleteCallback(baseValue);
        }

        private static void OnMinMaxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TabControl tc = d as TabControl;
            if (tc.Template == null)
                return;

            UniformStackPanel panel = tc.Template.FindName("PART_TabPanel", tc) as UniformStackPanel;
            if (panel != null)
                panel.InvalidateMeasure();
        }

        private void AddNewTab(object obj)
        {
            CancelEventArgs cancel = new CancelEventArgs();
            if (TabItemAdding != null)
                TabItemAdding(this, cancel);

            if (cancel.Cancel)
                return;

            TabItem tabItem;
            IList list = (IList)GetItems();

            if (obj != null)
            {
                this.SelectedIndex = list.Add(obj);
                tabItem = (TabItem)this.ItemContainerGenerator.ContainerFromItem(obj);
            }
            else
            {
                if (IsUsingItemsSource)
                {
                    throw new InvalidOperationException("Вы должны передать добавляемый объект как параметр вызова команды в свойстве \"AddTabCommandParameter\"");
                }
                else
                {
                    tabItem = new TabItem();
                    tabItem.Header = "Новая вкладка";
                    this.SelectedIndex = list.Add(tabItem);
                }
            }

            if (tabItem != null && TabItemAdded != null)
                TabItemAdded(this, new TabItemEventArgs(tabItem));
        }

        private IEnumerable GetItems()
        {
            if (IsUsingItemsSource)
                return ItemsSource;
            return Items;
        }

        private IEnumerable<TabItem> GetTabItems()
        {
            IEnumerator enumerator = GetItems().GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Current is TabItem)
                    yield return enumerator.Current as TabItem;
                else
                    yield return this.ItemContainerGenerator.ContainerFromItem(enumerator.Current) as TabItem;
            }
        }

        private object OnCoerceAllowAddNewCallback(object baseValue)
        {
            if (ItemsSource == null)
                return baseValue;

            IList list = ItemsSource as IList;
            if (list != null && list.IsFixedSize)
                return false;

            return baseValue;
        }

        private object OnCoerceAllowDeleteCallback(object baseValue)
        {
            if (ItemsSource == null)
                return baseValue;

            IList list = ItemsSource as IList;
            if (list != null && list.IsFixedSize)
                return false;

            return baseValue;
        }

        private void SelectTab(object obj)
        {
            TabItem item;
            if (obj is TabItem)
                item = obj as TabItem;
            else
                item = this.ItemContainerGenerator.ContainerFromItem(obj) as TabItem;

            if (item != null)
            {
                //itemContainer.IsSelected = true;
                item.Focus();
            }
        }

        private void SetAddNewButtonVisibility()
        {
            if (this.Template == null)
                return;

            ButtonBase addNew = this.Template.FindName("PART_NewTabButton", this) as ButtonBase;

            if (addNew == null)
                return;

            if (IsFixedSize)
                addNew.Visibility = System.Windows.Visibility.Collapsed;
            else
                addNew.Visibility = AllowAddNew ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
        }

        private void SetDeleteButtonVisibility()
        {
            if (this.Template == null)
                return;

            UniformStackPanel panel = this.Template.FindName("PART_TabPanel", this) as UniformStackPanel;
            if (panel == null)
                return;
            foreach (var item in panel.Children)
            {
                TabItem tabItem;
                if (item is TabItem)
                    tabItem = item as TabItem;
                else
                    tabItem = this.ItemContainerGenerator.ContainerFromItem(item) as TabItem;

                if (tabItem != null)
                    tabItem.AllowDelete = !IsFixedSize && this.AllowDelete && tabItem.IsEnabled;
            }
        }

        #endregion Private Methods

        #region Public Events

        public event EventHandler<TabItemEventArgs> TabItemAdded;

        public event EventHandler<CancelEventArgs> TabItemAdding;

        public event EventHandler<TabItemEventArgs> TabItemClosed;

        public event EventHandler<TabItemCancelEventArgs> TabItemClosing;

        #endregion Public Events

        #region Public Methods

        public void AddNewTabItem(TabItem item)
        {
            AddNewTab(item);
        }

        public void CloseAll()
        {
            List<TabItem> toClose = new List<TabItem>();
            foreach (var item in GetItems())
            {
                toClose.Add(GetTabItemFromObject(item));
            }

            foreach (var item in toClose)
            {
                item.Close();
            }
        }

        public void CloseTab(object obj)
        {
            TabItem tabItem;
            if (obj is TabItem)
                tabItem = obj as TabItem;
            else
                tabItem = ItemContainerGenerator.ContainerFromItem(obj) as TabItem;

            if (tabItem == null)
                return;

            TabItemCancelEventArgs cancel = new TabItemCancelEventArgs(tabItem);

            if (TabItemClosing != null)
                TabItemClosing(this, cancel);

            if (cancel.Cancel)
                return;

            if (IsUsingItemsSource)
            {
                var list = ItemsSource as IList;
                object listItem = ItemContainerGenerator.ItemFromContainer(tabItem);
                if (listItem != null && list != null)
                    list.Remove(listItem);
            }
            else
                this.Items.Remove(tabItem);

            if (TabItemClosed != null)
                TabItemClosed(this, new TabItemEventArgs(tabItem));
        }

        public TabItem GetTabItemFromObject(object obj)
        {
            if (obj is TabItem)
                return obj as TabItem;
            else
                return this.ItemContainerGenerator.ContainerFromItem(obj) as TabItem;
        }

        #endregion Public Methods
    }

    public class TabItemCancelEventArgs : CancelEventArgs
    {
        #region Public Constructors


        public TabItemCancelEventArgs(TabItem item)
        {
            TabItem = item;
        }

        #endregion Public Constructors

        #region Public Properties

        public TabItem TabItem { get; private set; }

        #endregion Public Properties
    }

    public class TabItemEventArgs : EventArgs
    {
        #region Public Constructors

        public TabItemEventArgs(TabItem item)
        {
            TabItem = item;
        }

        #endregion Public Constructors

        #region Public Properties

        public TabItem TabItem { get; private set; }

        #endregion Public Properties
    }
}