﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace ITWebNet.WPF.Controls
{
    [TemplatePart(Name = "PART_ContentHost", Type = typeof(System.Windows.Controls.ScrollViewer))]
    [TemplatePart(Name = "PART_Placeholder", Type = typeof(System.Windows.Controls.TextBlock))]
    [TemplatePart(Name = "PART_Button", Type = typeof(System.Windows.Controls.Button))]
    public class TextBox : System.Windows.Controls.TextBox
    {
        #region DependencyProperties

        public Brush ButtonBackground
        {
            get { return (Brush)GetValue(ButtonBackgroundProperty); }
            set { SetValue(ButtonBackgroundProperty, value); }
        }

        public static readonly DependencyProperty ButtonBackgroundProperty =
            DependencyProperty.Register("ButtonBackground", typeof(Brush), typeof(TextBox), new PropertyMetadata(Brushes.Transparent));
        
        /// <summary>
        /// Возвращает или задает контент кнопки.
        /// </summary>
        [Category("Button")]
        [Description("Возвращает или задает контент кнопки.")]
        public object ButtonContent
        {
            get { return (object)GetValue(ButtonContentProperty); }
            set { SetValue(ButtonContentProperty, value); }
        }

        public static readonly DependencyProperty ButtonContentProperty =
            DependencyProperty.Register("ButtonContent", typeof(object), typeof(TextBox), new PropertyMetadata(null));

        /// <summary>
        /// Возвращает или задает всплывающую подсказку для кнопки.
        /// </summary>
        [Category("Button")]
        [Description("Возвращает или задает всплывающую подсказку для кнопки.")]
        public string ButtonToolTip
        {
            get { return (string)GetValue(ButtonToolTipProperty); }
            set { SetValue(ButtonToolTipProperty, value); }
        }

        public static readonly DependencyProperty ButtonToolTipProperty =
            DependencyProperty.Register("ButtonToolTip", typeof(string), typeof(TextBox), new PropertyMetadata(null));

        [Category("Button")]
        [Description("Возвращает или задаёт плотность или толщину шрифта текста кнопки.")]
        public FontWeight ButtonFontWeight
        {
            get { return (FontWeight)GetValue(ButtonFontWeightProperty); }
            set { SetValue(ButtonFontWeightProperty, value); }
        }

        public static readonly DependencyProperty ButtonFontWeightProperty =
            DependencyProperty.Register("ButtonFontWeight", typeof(FontWeight), typeof(TextBox), new PropertyMetadata(FontWeights.Bold));

        /// <summary>
        /// Возвращает значение, указывающее содержит ли TextBox текст.
        /// </summary>
        [Category("Appearance")]
        [Description("Возвращает значение, указывающее содержит ли TextBox текст.")]
        public bool HasText
        {
            get { return (bool)GetValue(HasTextProperty); }
            private set { SetValue(HasTextPropertyKey, value); }
        }

        public static readonly DependencyPropertyKey HasTextPropertyKey =
            DependencyProperty.RegisterReadOnly("HasText", typeof(bool), typeof(TextBox), new PropertyMetadata(false));

        public static readonly DependencyProperty HasTextProperty = HasTextPropertyKey.DependencyProperty;

        [Category("Common Properties")]
        [Description("Возвращает или задает описание текстового поля.")]
        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(TextBox), new PropertyMetadata(null));


        /// <summary>
        /// Возвращает или задает замещающий текст, если в поле Text отсутствует значение.
        /// </summary>
        [Category("Common Properties")]
        [Description("Возвращает или задает замещающий текст, если в поле Text отсутствует значение.")]
        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public static readonly DependencyProperty PlaceholderProperty =
            DependencyProperty.Register("Placeholder", typeof(string), typeof(TextBox), new PropertyMetadata(null));

        /// <summary>
        /// Возвращает или задает цвет замещающего текста.
        /// </summary>
        [Category("Brush")]
        [Description("Возвращает или задает цвет замещающего текста.")]
        public Brush PlaceholderForeground
        {
            get { return (Brush)GetValue(PlaceholderColorProperty); }
            set { SetValue(PlaceholderColorProperty, value); }
        }

        public static readonly DependencyProperty PlaceholderColorProperty =
            DependencyProperty.Register("PlaceholderForeground", typeof(Brush), typeof(TextBox), new PropertyMetadata(Brushes.Gray));

        /// <summary>
        /// Возвращает или задаёт команду, которую следует вызывать по нажатию этой кнопки. 
        /// </summary>
        [Category("Button")]
        [Description("Возвращает или задаёт команду, которую следует вызывать по нажатию этой кнопки. ")]
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(TextBox), new PropertyMetadata(null));

        /// <summary>
        /// Возвращает или задаёт параметр для передачи свойству <see cref="System.Windows.Controls.Primitives.ButtonBase.Command"/>.
        /// </summary>
        [Category("Button")]
        [Description("Возвращает или задаёт параметр для передачи свойству Command.")]
        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(TextBox), new PropertyMetadata(null));

        /// <summary>
        /// Возвращает или задает значение, указывающее следует ли очищать текстовое поле при получении фокуса.
        /// </summary>
        [Category("Appearance")]
        [Description("Возвращает или задает значение, указывающее следует ли очищать текстовое поле при получении фокуса.")]
        public bool CleanTextOnFocus
        {
            get { return (bool)GetValue(CleanTextOnFocusProperty); }
            set { SetValue(CleanTextOnFocusProperty, value); }
        }

        public static readonly DependencyProperty CleanTextOnFocusProperty =
            DependencyProperty.Register("CleanTextOnFocus", typeof(bool), typeof(TextBox), new PropertyMetadata(false));

        /// <summary>
        /// Возвращает или задает значение, указывающее следует ли выделять весь текст в текстовом поле при получении фокуса.
        /// </summary>
        [Category("Appearance")]
        [Description("Возвращает или задает значение, указывающее следует ли выделять весь текст в текстовом поле при получении фокуса.")]
        public bool SelectAllOnFocus
        {
            get { return (bool)GetValue(SelectAllOnFocusProperty); }
            set { SetValue(SelectAllOnFocusProperty, value); }
        }

        public static readonly DependencyProperty SelectAllOnFocusProperty =
            DependencyProperty.Register("SelectAllOnFocus", typeof(bool), typeof(TextBox), new PropertyMetadata(true));
        #endregion

        #region Public Constructors

        static TextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(TextBox),
                new FrameworkPropertyMetadata(typeof(TextBox)));
        }

        #endregion Public Constructors

        #region Public Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            System.Windows.Controls.Button btn = this.Template.FindName("PART_Button", this) as System.Windows.Controls.Button;
            if (btn != null)
                btn.Click += (s, e) => { OnClick(); };
        }

        #endregion Public Methods

        #region Override Methods

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            if (SelectAllOnFocus)
                this.SelectAll();
            if (CleanTextOnFocus)
                this.Text = String.Empty;

            base.OnGotFocus(e);
        }

        protected override void OnTextChanged(System.Windows.Controls.TextChangedEventArgs e)
        {
            HasText = Text.Length != 0;

            base.OnTextChanged(e);
        }

        #endregion Protected Methods

        #region Events

        /// <summary>
        /// Возникает при нажатии кнопки. 
        /// </summary>
        [Category("Behavior")]
        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }

        public static readonly RoutedEvent ClickEvent =
                    EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TextBox));

        protected virtual void OnClick()
        {
            RoutedEventArgs newEvent = new RoutedEventArgs(ClickEvent, this);
            RaiseEvent(newEvent);
        }


        #endregion Events
    }
}