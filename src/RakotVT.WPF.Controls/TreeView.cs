﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ITWebNet.WPF.Controls
{
    [StyleTypedPropertyAttribute(Property = "ItemContainerStyle", StyleTargetType = typeof(TreeViewItem))]
    public class TreeView : System.Windows.Controls.TreeView
    {
        public object TreeViewSelectedItem
        {
            get { return (object)GetValue(TreeViewSelectedItemProperty); }
            set { SetValue(TreeViewSelectedItemProperty, value); }
        }

        public static readonly DependencyProperty TreeViewSelectedItemProperty =
            DependencyProperty.Register("TreeViewSelectedItem", typeof(object), typeof(TreeView), new PropertyMetadata(new object(), TreeViewSelectedItemChanged));

        static void TreeViewSelectedItemChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            TreeView treeView = sender as TreeView;
            if (treeView == null)
            {
                return;
            }

            treeView.SelectedItemChanged -= new RoutedPropertyChangedEventHandler<object>(treeView_SelectedItemChanged);
            treeView.SelectedItemChanged += new RoutedPropertyChangedEventHandler<object>(treeView_SelectedItemChanged);

            TreeViewItem thisItem = treeView.ItemContainerGenerator.ContainerFromItem(e.NewValue) as TreeViewItem;
            if (thisItem != null)
            {
                thisItem.IsSelected = true;
                return;
            }

            foreach (var item in treeView.Items)
                if (SelectInnerItem(e.NewValue, treeView.ItemContainerGenerator.ContainerFromItem(item) as TreeViewItem))
                    break;            
        }

        static void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeView treeView = sender as TreeView;
            treeView.TreeViewSelectedItem = e.NewValue;
            //SetTreeViewSelectedItem(treeView, e.NewValue);
        }

        private static bool SelectInnerItem(object o, TreeViewItem parentItem)
        {
            if (parentItem == null)
                return false;

            TreeViewItem itemContainer;
            itemContainer = parentItem.ItemContainerGenerator.ContainerFromItem(o) as TreeViewItem;

            if (itemContainer != null)
            {
                parentItem.IsExpanded = true;
                itemContainer.IsSelected = true;
                return true;
            }

            foreach (var item in parentItem.Items)
            {
                itemContainer = parentItem.ItemContainerGenerator.ContainerFromItem(item) as TreeViewItem;
                if (itemContainer != null)
                {
                    if (SelectInnerItem(o, itemContainer))
                    {
                        parentItem.IsExpanded = true;
                        return true;
                    }
                }
            }

            return false;
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeViewItem;
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeViewItem();
        }
    }
}
