﻿using System;
using System.ComponentModel;
using System.Windows;

namespace ITWebNet.WPF.Controls
{
    public class UniformWrapPanel : System.Windows.Controls.WrapPanel
    {
        #region Public Fields

        public static readonly DependencyProperty IsAutoUniformProperty =
            DependencyProperty.Register(
            "IsAutoUniform",
            typeof(bool), typeof(UniformWrapPanel),
            new FrameworkPropertyMetadata(
                true,
                new PropertyChangedCallback(IsAutoUniformChanged)));

        #endregion Public Fields

        #region Public Properties

        /// <summary>
        /// Возвращает или задает значение, показывающее, требуетлся ли выравнивание дочерних элементов по максимальному.
        /// </summary>
        [Description("Возвращает или задает значение, показывающее, требуетлся ли выравнивание дочерних элементов по максимальному.")]
        public bool IsAutoUniform
        {
            get { return (bool)GetValue(IsAutoUniformProperty); }
            set { SetValue(IsAutoUniformProperty, value); }
        }

        #endregion Public Properties

        #region Protected Methods

        protected override Size MeasureOverride(Size constraint)
        {
            if (Children.Count > 0 && IsAutoUniform)
            {
                switch (Orientation)
                {
                    case System.Windows.Controls.Orientation.Horizontal:
                        ItemWidth = 0.0;
                        foreach (UIElement item in Children)
                        {
                            item.Measure(constraint);
                            Size next = item.DesiredSize;
                            if (!(Double.IsInfinity(next.Width) || Double.IsNaN(next.Width)))
                                ItemWidth = Math.Max(next.Width, ItemWidth);
                        }
                        break;

                    case System.Windows.Controls.Orientation.Vertical:
                        ItemHeight = 0.0;
                        foreach (UIElement item in Children)
                        {
                            item.Measure(constraint);
                            Size next = item.DesiredSize;
                            if (!(Double.IsInfinity(next.Height) || Double.IsNaN(next.Height)))
                                ItemHeight = Math.Max(next.Height, ItemHeight);
                        }
                        break;

                    default:
                        break;
                }
            }

            return base.MeasureOverride(constraint);
        }

        #endregion Protected Methods

        #region Private Methods

        private static void IsAutoUniformChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is UniformWrapPanel)
                ((UniformWrapPanel)d).InvalidateVisual();
        }

        #endregion Private Methods
    }
}