﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ITWebNet.WPF.Controls
{
    public class SearchBox : TextBox
    {
        #region DependencyProperties

        public IEnumerable SourceToSearch
        {
            get { return (IEnumerable)GetValue(SourceToSearchProperty); }
            set { SetValue(SourceToSearchProperty, value); }
        }

        public static readonly DependencyProperty SourceToSearchProperty =
            DependencyProperty.Register(
                "SourceToSearch",
                typeof(IEnumerable),
                typeof(SearchBox),
                new PropertyMetadata(null)
            );

        public static readonly DependencyProperty SearchModeProperty =
            DependencyProperty.Register(
                "SearchMode",
                typeof(SearchMode),
                typeof(SearchBox),
                new PropertyMetadata(SearchMode.Instant)
            );

        public static readonly DependencyProperty SearchFieldProperty =
            DependencyProperty.Register(
                "SearchField",
                typeof(string),
                typeof(SearchBox),
                new PropertyMetadata(null)
            );  


        public SearchMode SearchMode
        {
            get { return (SearchMode)GetValue(SearchModeProperty); }
            set { SetValue(SearchModeProperty, value); }
        }

        
        public string SearchField
        {
            get { return (string)GetValue(SearchFieldProperty); }
            set { SetValue(SearchFieldProperty, value); }
        }

        
        #endregion

        #region Events

        /// <summary>
        /// Возникает при старте поиска элемента.
        /// </summary>
        public event RoutedEventHandler SearchStarted
        {
            add { AddHandler(SearchStartedEvent, value); }
            remove { RemoveHandler(SearchStartedEvent, value); }
        }

        public static readonly RoutedEvent SearchStartedEvent =
                    EventManager.RegisterRoutedEvent("SearchStarted", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(SearchBox));

        protected void OnSearchStarted()
        {
            RoutedEventArgs newEvent = new RoutedEventArgs(SearchStartedEvent, this);
            RaiseEvent(newEvent);
        }

        #endregion


        #region Override Methods

        protected override void OnTextChanged(System.Windows.Controls.TextChangedEventArgs e)
        {
            if (SearchMode == SearchMode.Instant)
            {
                OnSearchStarted();
                Search();
            }

            if (Text.Length == 0 && SearchMode == SearchMode.Delayed)
                Search();

            base.OnTextChanged(e);
        }

        protected override void OnClick()
        {
            base.OnClick();
            if (SearchMode == SearchMode.Delayed)
            {
                OnSearchStarted();
                Search();
            }
            else if (SearchMode == SearchMode.Instant && HasText)
                Text = null;
        }

        protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                OnClick();
            if (e.Key == System.Windows.Input.Key.Escape)
                Text = null;

            base.OnPreviewKeyDown(e);
        }

        #endregion

        private void Search()
        {
            var source = CollectionViewSource.GetDefaultView(SourceToSearch);
            source.Filter = p =>
            {
                string value = p.GetType().GetProperty(SearchField).GetValue(p, null).ToString();

                return value.ToLowerInvariant().Contains(Text.ToLowerInvariant());
            };
        }

        static SearchBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SearchBox), new FrameworkPropertyMetadata(typeof(SearchBox)));
        }

        public SearchBox() : base() { }

    }

    public enum SearchMode
    {
        Instant,
        Delayed,
    }
}
