﻿using System.Windows;
using System.Windows.Media;

namespace ITWebNet.WPF.Controls
{
    public static class VisualHelper
    {
        #region Public Methods

        /// <summary>
        /// Находит заданного по типу родителя у передаваемого родителя в визуальном дереве. 
        /// </summary>
        /// <typeparam name="T"> Тип родителя. </typeparam>
        /// <param name="obj"> Объект, у которого требуется найти родительский элемент. </param>
        /// <returns> Родительский элемент заданного типа. </returns>
        public static T FindVisualParent<T>(this DependencyObject obj)
            where T : DependencyObject
        {
            DependencyObject dObj = VisualTreeHelper.GetParent(obj);
            if (dObj == null)
                return null;

            if (dObj is T)
                return dObj as T;

            while ((dObj = VisualTreeHelper.GetParent(dObj)) != null)
            {
                if (dObj is T)
                    return dObj as T;
            }

            return null;
        }

        /// <summary>
        /// Находит первый заданный по типу дочерний элемент в визуальном дереве. 
        /// </summary>
        /// <typeparam name="T"> Тип дочернего элемента. </typeparam>
        /// <param name="obj"> Объект, у которого требуется найти дочерний элемент. </param>
        /// <returns> Дочерний элемент заданного типа. </returns>
        public static T FindVisualChild<T>(this DependencyObject obj)
            where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child != null && child is T)

                    return (T)child;
                else
                {
                    T childOfChild = FindVisualChild<T>(child);

                    if (childOfChild != null)

                        return childOfChild;
                }
            }

            return null;
        }

        #endregion Public Methods
    }
}