﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace ITWebNet.WPF.Controls
{
    public class UniformStackPanel : Panel, IScrollInfo, INotifyPropertyChanged
    {
        #region Private Fields

        private bool _canScroll;
        private Size _extent;
        private Vector _offset;
        private ScrollViewer _scrollOwner;
        private Vector _scrollStep;
        private TranslateTransform _translateTransform;
        private Size _viewport;

        #endregion Private Fields

        #region Public Dependency Properties

        public static readonly DependencyProperty BoundsProperty =
            DependencyProperty.RegisterAttached("Bounds", typeof(Rect), typeof(UniformStackPanel), new PropertyMetadata(Rect.Empty));

        public static readonly DependencyProperty IsAutoUniformProperty =
            DependencyProperty.Register("IsAutoUniform", typeof(bool), typeof(UniformStackPanel), new PropertyMetadata(true, new PropertyChangedCallback(IsAutoUniformCallback)));

        public static readonly DependencyProperty MaxChildHeightProperty =
            DependencyProperty.Register("MaxChildHeight", typeof(double), typeof(UniformStackPanel), new FrameworkPropertyMetadata(Double.MaxValue));

        public static readonly DependencyProperty MaxChildWidthProperty =
            DependencyProperty.Register("MaxChildWidth", typeof(double), typeof(UniformStackPanel), new FrameworkPropertyMetadata(Double.MaxValue));

        public static readonly DependencyProperty MinChildHeightProperty =
            DependencyProperty.Register("MinChildHeight", typeof(double), typeof(UniformStackPanel), new FrameworkPropertyMetadata(0.0));

        public static readonly DependencyProperty MinChildWidthProperty =
            DependencyProperty.Register("MinChildWidth", typeof(double), typeof(UniformStackPanel), new FrameworkPropertyMetadata(0.0));

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(UniformStackPanel), new PropertyMetadata(Orientation.Horizontal));

        public bool IsAutoUniform
        {
            get { return (bool)GetValue(IsAutoUniformProperty); }
            set { SetValue(IsAutoUniformProperty, value); }
        }

        /// <summary>
        /// Возвращает или задает максимальную высоту дочерних элементов. 
        /// </summary>
        [Description("Возвращает или задает максимальную высоту дочерних элементов.")]
        public double MaxChildHeight
        {
            get { return (double)GetValue(MaxChildHeightProperty); }
            set { SetValue(MaxChildHeightProperty, value); }
        }

        /// <summary>
        /// Возвращает или задает максимальную ширину дочерних элементов. 
        /// </summary>
        [Description("Возвращает или задает максимальную ширину дочерних элементов.")]
        public double MaxChildWidth
        {
            get { return (double)GetValue(MaxChildWidthProperty); }
            set { SetValue(MaxChildWidthProperty, value); }
        }

        /// <summary>
        /// Возвращает или задает минимальную высоту дочерних элементов. 
        /// </summary>
        [Description("Возвращает или задает минимальную высоту дочерних элементов.")]
        public double MinChildHeight
        {
            get { return (double)GetValue(MinChildHeightProperty); }
            set { SetValue(MinChildHeightProperty, value); }
        }

        /// <summary>
        /// Возвращает или задает минимальную ширину дочерних элементов. 
        /// </summary>
        [Description("Возвращает или задает минимальную ширину дочерних элементов.")]
        public double MinChildWidth
        {
            get { return (double)GetValue(MinChildWidthProperty); }
            set { SetValue(MinChildWidthProperty, value); }
        }

        [Description("Получает или задает значение, указывающее размерность, с которой укладываются дочерние элементы. ")]
        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        private static void IsAutoUniformCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is UniformStackPanel)
                ((UniformStackPanel)d).InvalidateVisual();
        }

        [AttachedPropertyBrowsableForChildren(IncludeDescendants = false)]
        public static Rect GetBounds(DependencyObject obj)
        {
            return (Rect)obj.GetValue(BoundsProperty);
        }

        public static void SetBounds(DependencyObject obj, Rect value)
        {
            obj.SetValue(BoundsProperty, value);
        }

        #endregion Public Dependency Properties

        #region Public Constructors

        public UniformStackPanel()
        {
            _translateTransform = new TranslateTransform();
            this.RenderTransform = _translateTransform;
        }

        #endregion Public Constructors

        #region Protected Methods

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            bool invalidateMeasure = false;
            if (_extent.Width <= _viewport.Width && _offset.X > 0)
            {
                _offset.X = 0;
                _translateTransform.X = 0;

                InvalidateScrollInfo();
                invalidateMeasure = true;
            }
            if (_extent.Height <= _viewport.Height && _offset.Y > 0)
            {
                _offset.Y = 0;
                _translateTransform.Y = 0;

                InvalidateScrollInfo();
                invalidateMeasure = true;
            }
            if (invalidateMeasure)
                InvalidateMeasure();

            foreach (UIElement item in InternalChildren)
            {
                Rect childRect = UniformStackPanel.GetBounds(item);
                item.Arrange(childRect);
            }

            return arrangeSize;
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            switch (Orientation)
            {
                case Orientation.Horizontal:
                    return MeasureHorizontal(availableSize);

                case Orientation.Vertical:
                    return MeasureVertical(availableSize);

                default:
                    return new Size();
            }
        }

        #endregion Protected Methods

        #region Private Methods
        /// <summary>
        /// Рассчитывает новое горизонтальное смещение для элемента прокрутки панели. 
        /// </summary>
        /// <param name="viewportLeft"></param>
        /// <param name="viewportRight"></param>
        /// <param name="childLeft"></param>
        /// <param name="childRigth"></param>
        /// <returns></returns>
        private double CalculateNewScrollOffset(double viewportLeft, double viewportRight, double childLeft, double childRigth)
        {
            bool isFurtherToLeft = (childLeft < viewportLeft) && (childRigth < viewportRight);
            bool isFurtherToRight = (childRigth > viewportRight) && (childLeft > viewportLeft);
            bool isWiderThanViewport = (childRigth - childLeft) > (viewportRight - viewportLeft);
            if (!isFurtherToRight && !isFurtherToLeft)
                return viewportLeft;

            if (isFurtherToLeft && !isWiderThanViewport)
                return childLeft;
            return (childRigth - (viewportRight - viewportLeft));
        }

        private void InvalidateScrollInfo()
        {
            if (_scrollOwner != null)
                _scrollOwner.InvalidateScrollInfo();
        }

        /// <summary>
        /// Перерасчитывает размер вкладок при горизонтальном расположении вкладок. 
        /// </summary>
        /// <param name="availableSize"></param>
        /// <returns></returns>
        private Size MeasureHorizontal(Size availableSize)
        {
            double extentWidthSize = 0.0;
            double itemHeight = 0.0;
            double itemWidth = 0.0;
            double XPoint = 0.0;
            double YPoint = 0.0;
            double availableWidth = availableSize.Width;
            int itemsCount = InternalChildren.Count;

            foreach (UIElement item in InternalChildren)
            {
                item.Measure(availableSize);
                itemHeight = Math.Max(itemHeight, Math.Ceiling(item.DesiredSize.Height));
            }

            // Получаем максимальную ширину панели с учетом размеров дочерних элементов. 
            extentWidthSize = MaxChildWidth * itemsCount;

            // Получаем высоту элементов в заданных пределах. 
            itemHeight = Math.Max(MinChildHeight, Math.Min(MaxChildHeight, itemHeight));

            // Проверяем, вмещаются ли все элементы в видимую ширину панели. 
            if (extentWidthSize <= availableWidth) // Элементы помещаются.
            {
                // Задаем всем элементам максмальную ширину и высоту. 
                foreach (UIElement item in InternalChildren)
                {
                    Rect childRect = new Rect(XPoint, YPoint, MaxChildWidth, itemHeight);
                    UniformStackPanel.SetBounds(item, childRect);
                    item.Measure(childRect.Size);
                    XPoint += childRect.Size.Width;
                }
            }
            else // Элементы не помещаются.
            {
                // Вычисляем ширину неактивного элемента. 
                itemWidth = Math.Max(MinChildWidth, (availableWidth / itemsCount));

                foreach (UIElement item in InternalChildren)
                {
                    Rect childRect = new Rect(XPoint, YPoint, itemWidth, itemHeight);
                    UniformStackPanel.SetBounds(item, childRect);
                    item.Measure(childRect.Size);
                    XPoint += childRect.Size.Width;
                }
            }

            // расчитываем конечные параметры для элемента прокрутки. 
            extentWidthSize = 0.0;
            foreach (UIElement item in InternalChildren)
                extentWidthSize += UniformStackPanel.GetBounds(item).Width;

            Size extent = new Size(extentWidthSize, itemHeight);
            Size viewport = new Size(availableWidth, itemHeight);
            Vector scrollStep = new Vector(extentWidthSize / itemsCount, itemHeight);

            UpdateScrollInfo(extent, viewport, scrollStep);

            if (availableSize.Width == double.PositiveInfinity || availableSize.Height == double.PositiveInfinity)
            {
                return Size.Empty;
            }

            return viewport;
        }

        /// <summary>
        /// Перерасчитывает размер элементов при вертикальном расположении вкладок. 
        /// </summary>
        /// <param name="availableSize"></param>
        /// <returns></returns>
        private Size MeasureVertical(Size availableSize)
        {
            //if (!IsInitialized)
            //    return availableSize;

            double extentHeightSize = 0.0;
            double itemHeight = 0.0;
            double itemWidth = 0.0;
            double XPoint = 0.0;
            double YPoint = 0.0;
            double availableHeight = availableSize.Height;
            int itemsCount = InternalChildren.Count;

            foreach (UIElement item in InternalChildren)
            {
                item.Measure(availableSize);
                itemHeight = Math.Max(itemHeight, Math.Ceiling(item.DesiredSize.Height));
                itemWidth = Math.Max(itemWidth, Math.Ceiling(item.DesiredSize.Width));
            }

            itemHeight = Math.Max(MinChildHeight, Math.Min(MaxChildHeight, itemHeight));
            itemWidth = Math.Max(MinChildWidth, Math.Min(MaxChildWidth, itemWidth));

            foreach (UIElement item in InternalChildren)
            {
                if (item.Visibility == System.Windows.Visibility.Collapsed)
                    continue;
                Rect childRect = new Rect(XPoint, YPoint, itemWidth, itemHeight);
                UniformStackPanel.SetBounds(item, childRect);
                item.Measure(childRect.Size);
                YPoint += childRect.Size.Height;
            }

            // расчитываем конечные параметры для элемента прокрутки. 
            foreach (UIElement item in InternalChildren)
            {
                if (item.Visibility == System.Windows.Visibility.Collapsed)
                    continue;
                extentHeightSize += UniformStackPanel.GetBounds(item).Height;
            }

            Size extent = new Size(itemWidth, extentHeightSize);
            Size viewport = new Size(itemWidth, availableHeight);
            Vector scrollStep = new Vector(itemWidth, extentHeightSize / itemsCount);

            UpdateScrollInfo(extent, viewport, scrollStep);

            return viewport;
        }

        /// <summary>
        /// Обновляет параметры элемента прокрутки. 
        /// </summary>
        /// <param name="extent"></param>
        /// <param name="viewport"></param>
        /// <param name="scrollStep"></param>
        private void UpdateScrollInfo(Size extent, Size viewport, Vector scrollStep)
        {
            bool needInvalidation = false;

            if (_extent != extent)
            {
                _extent = extent;
                needInvalidation = true;
            }
            if (_viewport != viewport)
            {
                _viewport = viewport;
                needInvalidation = true;
            }

            if (this.HorizontalOffset + this.ViewportWidth > this.ExtentWidth)
                SetHorizontalOffset(HorizontalOffset + this.ViewportWidth);

            _scrollStep = scrollStep;

            if (needInvalidation)
                InvalidateScrollInfo();

            CanScroll = Math.Floor(ExtentWidth) > Math.Floor(ViewportWidth);
            CanScroll |= Math.Floor(ExtentHeight) > Math.Floor(ViewportHeight);
        }

        #endregion Private Methods

        #region Public Properties

        /// <summary>
        /// Возвращает значение, указывающее доступна ли прокрутка элементов панели. 
        /// </summary>
        [Description("Возвращает значение, указывающее доступна ли прокрутка элементов панели.")]
        public bool CanScroll
        {
            get
            {
                return _canScroll;
            }

            private set
            {
                if (_canScroll != value)
                    InvalidateMeasure();

                _canScroll = value;

                NotifyPropertyChanged("CanScroll");
                NotifyPropertyChanged("CanScrollLeft");
                NotifyPropertyChanged("CanScrollRight");
                NotifyPropertyChanged("CanScrollUp");
                NotifyPropertyChanged("CanScrollDown");
            }
        }

        /// <summary>
        /// Возвращает значение, указывающее доступна ли прокрутка элементов панели влево. 
        /// </summary>
        [Description("Возвращает значение, указывающее доступна ли прокрутка элементов панели влево.")]
        public bool CanScrollLeft { get { return this.CanScroll && !this.IsOnFarLeft; } }

        /// <summary>
        /// Возвращает значение, указывающее доступна ли прокрутка элементов панели вправо. 
        /// </summary>
        [Description("Возвращает значение, указывающее доступна ли прокрутка элементов панели вправо.")]
        public bool CanScrollRight { get { return this.CanScroll && !this.IsOnFarRight; } }

        /// <summary>
        /// Вовзращает значение, указывающее, что прокрутка достигла левой границы области прокрутки. 
        /// </summary>
        [Description("Вовзращает значение, указывающее, что прокрутка достигла левой границы области прокрутки.")]
        public bool IsOnFarLeft { get { return this.HorizontalOffset == 0; } }

        /// <summary>
        /// Вовзращает значение, указывающее, что прокрутка достигла правой границы области прокрутки. 
        /// </summary>
        [Description("Возвращает значение, указывающее, что прокрутка достигла правой границы области прокрутки.")]
        public bool IsOnFarRight { get { return (this.HorizontalOffset + this.ViewportWidth) == this.ExtentWidth; } }

        /// <summary>
        /// Возвращает значение, указывающее доступна ли прокрутка элементов панели вверх. 
        /// </summary>
        [Description("Возвращает значение, указывающее доступна ли прокрутка элементов панели вверх.")]
        public bool CanScrollUp { get { return this.CanScroll && !this.IsOnFarUp; } }

        /// <summary>
        /// Возвращает значение, указывающее доступна ли прокрутка элементов панели вниз. 
        /// </summary>
        [Description("Возвращает значение, указывающее доступна ли прокрутка элементов панели вниз.")]
        public bool CanScrollDown { get { return this.CanScroll && !this.IsOnFarDown; } }

        /// <summary>
        /// Вовзращает значение, указывающее, что прокрутка достигла верхней границы области прокрутки. 
        /// </summary>
        [Description("Вовзращает значение, указывающее, что прокрутка достигла верхней границы области прокрутки.")]
        public bool IsOnFarUp { get { return this.VerticalOffset == 0; } }

        /// <summary>
        /// Вовзращает значение, указывающее, что прокрутка достигла нижней границы области прокрутки. 
        /// </summary>
        [Description("Вовзращает значение, указывающее, что прокрутка достигла нижней границы области прокрутки.")]
        public bool IsOnFarDown { get { return (this.VerticalOffset + this.ViewportHeight) == this.ExtentHeight; } }

        #endregion Public Properties

        #region IScrollInfo Members

        /// <summary>
        /// Возвращает или задает значение, указывающее, возможна ли горизонтальная прокрутка. 
        /// </summary>
        public bool CanHorizontallyScroll { get; set; }

        /// <summary>
        /// Возвращает или задает значение, указывающее, возможна ли вертикальная прокрутка. 
        /// </summary>
        public bool CanVerticallyScroll { get; set; }

        /// <summary>
        /// Возвращает вертикальный размер экстента. 
        /// </summary>
        public double ExtentHeight
        {
            get { return _extent.Height; }
        }

        /// <summary>
        /// Возвращает горизонтальный размер экстента. 
        /// </summary>
        public double ExtentWidth
        {
            get { return _extent.Width; }
        }

        /// <summary>
        /// Возвращает горизонтальное смещение прокручиваемого содержимого. 
        /// </summary>
        public double HorizontalOffset
        {
            get { return _offset.X; }
        }

        /// <summary>
        /// Возвращает или задает элемент <see cref="System.Windows.Controls.ScrollViewer" />, управляющий поведением прокрутки. 
        /// </summary>
        public ScrollViewer ScrollOwner
        {
            get { return _scrollOwner; }
            set { _scrollOwner = value; }
        }

        /// <summary>
        /// Возвращает вертикальное смещение прокручиваемого содержимого. 
        /// </summary>
        public double VerticalOffset
        {
            get { return _offset.Y; }
        }

        /// <summary>
        /// Возвращает вертикальный размер видимой области содержимого панели. 
        /// </summary>
        public double ViewportHeight
        {
            get { return _viewport.Height; }
        }

        /// <summary>
        /// Возвращает горизонтальный размер видимой области содержимого панели. 
        /// </summary>
        public double ViewportWidth
        {
            get { return _viewport.Width; }
        }

        /// <summary>
        /// Выполняет прокрутку вниз в содержимом на высоту одного дочернего элемента 
        /// </summary>
        public void LineDown()
        {
            SetVerticalOffset(_offset.Y + _scrollStep.Y);
        }

        /// <summary>
        /// Выполняет прокрутку влево в содержимом на среднюю ширину дочерних элементов. 
        /// </summary>
        public void LineLeft()
        {
            SetHorizontalOffset(_offset.X - _scrollStep.X);
        }

        /// <summary>
        /// Выполняет прокрутку вправо в содержимом на среднюю ширину дочерних элементов. 
        /// </summary>
        public void LineRight()
        {
            SetHorizontalOffset(_offset.X + _scrollStep.X);
        }

        /// <summary>
        /// Выполняет прокрутку вверх в содержимом на высоту одногодочернего элемента. 
        /// </summary>
        public void LineUp()
        {
            SetVerticalOffset(_offset.Y - _scrollStep.Y);
        }

        /// <summary>
        /// Принудительно прокручивает содержимое пока координатное пространство объекта
        /// <see cref="System.Windows.Media.Visual" /> не станет видимым.
        /// </summary>
        /// <param name="visual"> <see cref="System.Windows.Media.Visual" /> , который становится видимым. </param>
        /// <param name="rectangle">
        /// Ограничивающий прямоугольник, идентифицирующий пространство координат, которое необходимо сделать видимым.
        /// </param>
        /// <returns></returns>
        public Rect MakeVisible(Visual visual, Rect rectangle)
        {
            if (rectangle.IsEmpty || visual == null
              || visual == this || !base.IsAncestorOf(visual))
            { return Rect.Empty; }

            double offsetX = 0.0;
            double offsetY = 0.0;
            UIElement uieControlToMakeVisible = null;
            for (int i = 0; i < this.InternalChildren.Count; i++)
            {
                if ((Visual)this.InternalChildren[i] == visual)
                {
                    uieControlToMakeVisible = this.InternalChildren[i];
                    offsetX = UniformStackPanel.GetBounds(this.InternalChildren[i]).Left;
                    break;
                }
            }

            if (uieControlToMakeVisible != null)
            {
                Rect itemBounds = UniformStackPanel.GetBounds(uieControlToMakeVisible);

                switch (Orientation)
                {
                    case Orientation.Horizontal:
                        if (uieControlToMakeVisible == this.InternalChildren[0])
                            offsetX = 0;
                        else if (uieControlToMakeVisible == this.InternalChildren[this.InternalChildren.Count - 1])
                            offsetX = this.ExtentWidth - _viewport.Width;
                        else
                            offsetX = CalculateNewScrollOffset(
                                     this.HorizontalOffset,
                                     this.HorizontalOffset + _viewport.Width,
                                     offsetX,
                                     offsetX + itemBounds.Width);
                        rectangle = new Rect(this.HorizontalOffset, 0, itemBounds.Width, itemBounds.Height);
                        SetHorizontalOffset(offsetX);
                        break;
                    case Orientation.Vertical:
                        if (uieControlToMakeVisible == this.InternalChildren[0])
                            offsetY = 0;
                        else if (uieControlToMakeVisible == this.InternalChildren[this.InternalChildren.Count - 1])
                            offsetY = this.ExtentHeight - _viewport.Height;
                        else
                            offsetX = CalculateNewScrollOffset(
                                     this.VerticalOffset,
                                     this.VerticalOffset + _viewport.Height,
                                     offsetY,
                                     offsetY + itemBounds.Width);

                        rectangle = new Rect(this.VerticalOffset, 0, itemBounds.Width, itemBounds.Height);
                        SetVerticalOffset(offsetY);
                        break;
                    default:
                        break;
                }

            }

            return rectangle;
        }

        /// <summary>
        /// Выполняет прокрутку вниз в содержимом на высоту одного дочернего элемента. 
        /// </summary>
        public void MouseWheelDown()
        {
            LineDown();
        }

        /// <summary>
        /// Выполняет прокрутку влево в содержимом на среднюю ширину дочерних элементов. 
        /// </summary>
        public void MouseWheelLeft()
        {
            LineLeft();
        }

        /// <summary>
        /// Выполняет прокрутку вправо в содержимом на среднюю ширину дочерних элементов. 
        /// </summary>
        public void MouseWheelRight()
        {
            LineRight();
        }

        /// <summary>
        /// Выполняет прокрутку вверх в содержимом на высоту одного дочернего элемента. 
        /// </summary>
        public void MouseWheelUp()
        {
            LineUp();
        }

        /// <summary>
        /// Выполняет прокрутку вниз в содержимом на одну страницу. 
        /// </summary>
        public void PageDown()
        {
            SetVerticalOffset(_offset.Y + _viewport.Height);
        }

        /// <summary>
        /// Выполняет прокрутку влево в содержимом на одну страницу. 
        /// </summary>
        public void PageLeft()
        {
            SetHorizontalOffset(_offset.X - _viewport.Width);
        }

        /// <summary>
        /// Выполняет прокрутку вправо в содержимом на одну страницу. 
        /// </summary>
        public void PageRight()
        {
            SetHorizontalOffset(_offset.X + _viewport.Width);
        }

        /// <summary>
        /// Выполняет прокрутку вверх в содержимом на одну страницу. 
        /// </summary>
        public void PageUp()
        {
            SetVerticalOffset(_offset.Y - _viewport.Height);
        }

        /// <summary>
        /// Задает величину горизонтального смещения. 
        /// </summary>
        /// <param name="offset">
        /// Величина, на которую содержимое смещается по горизонтали от контейнерного окна просмотра.
        /// </param>
        public void SetHorizontalOffset(double offset)
        {
            _offset.X = Math.Max(0.0, Math.Min(ExtentWidth - ViewportWidth, Math.Max(0.0, offset)));

            InvalidateScrollInfo();
            _translateTransform.X = -_offset.X;

            InvalidateMeasure();
        }

        /// <summary>
        /// Задает величину вертикального смещения. 
        /// </summary>
        /// <param name="offset"> Величина, на которую содержимое смещается по вертикали от контейнерного окна просмотра. </param>
        public void SetVerticalOffset(double offset)
        {
            _offset.Y = Math.Max(0.0, Math.Min(ExtentHeight - ViewportHeight, Math.Max(0.0, offset)));

            InvalidateScrollInfo();
            _translateTransform.Y = -_offset.Y;

            InvalidateMeasure();
        }

        #endregion IScrollInfo Members

        #region INotifyPropertyChanged Members

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members
    }
}