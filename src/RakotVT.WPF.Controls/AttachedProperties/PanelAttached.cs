﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ITWebNet.WPF.Controls.AttachedProperties
{
    public class PanelAttached
    {
        [AttachedPropertyBrowsableForType(typeof(System.Windows.Controls.Panel))]
        public static Thickness GetPadding(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(PaddingProperty);
        }

        public static void SetPadding(DependencyObject obj, Thickness value)
        {
            obj.SetValue(PaddingProperty, value);
        }

        public static readonly DependencyProperty PaddingProperty =
            DependencyProperty.RegisterAttached("Padding", typeof(Thickness), typeof(PanelAttached), new PropertyMetadata(new Thickness(0), new PropertyChangedCallback(PaddingChanged)));

        private static void PaddingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Panel panel = d as Panel;
            object value = e.NewValue;

            SetPadding(panel, value);
        }

        private static void SetPadding(Panel panel, object value)
        {
            if (panel == null)
                return;
            Thickness zero = new Thickness(0);
            if (panel.Children.Count > 0)
                foreach (FrameworkElement item in panel.Children)
                {
                    if (item is Panel && GetUseInNastedPanels(panel))
                        SetPadding(item as Panel, value);
                    if (item.Margin.Equals(zero) && !GetIgnorePadding(item))
                        item.Margin = (Thickness)value;
                }
            else
                panel.Initialized += (s, args) =>
                {
                    foreach (FrameworkElement item in panel.Children)
                    {
                        if (item is Panel && GetUseInNastedPanels(panel))
                            SetPadding(item as Panel, value);
                        if (item.Margin.Equals(zero) && !GetIgnorePadding(item))
                            item.Margin = (Thickness)value;
                    }
                };
        }

        public static bool GetIgnorePadding(DependencyObject obj)
        {
            return (bool)obj.GetValue(IgnorePaddingProperty);
        }

        public static void SetIgnorePadding(DependencyObject obj, bool value)
        {
            obj.SetValue(IgnorePaddingProperty, value);
        }

        public static readonly DependencyProperty IgnorePaddingProperty =
            DependencyProperty.RegisterAttached("IgnorePadding", typeof(bool), typeof(PanelAttached), new PropertyMetadata(false));



        public static bool GetUseInNastedPanels(DependencyObject obj)
        {
            return (bool)obj.GetValue(UseInNastedPanelsProperty);
        }

        public static void SetUseInNastedPanels(DependencyObject obj, bool value)
        {
            obj.SetValue(UseInNastedPanelsProperty, value);
        }

        // Using a DependencyProperty as the backing store for UseInNastedPanels.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UseInNastedPanelsProperty =
            DependencyProperty.RegisterAttached("UseInNastedPanels", typeof(bool), typeof(PanelAttached), new PropertyMetadata(false));


    }
}
