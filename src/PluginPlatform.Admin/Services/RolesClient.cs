﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using PluginPlatform.Admin.Models;
using PluginPlatform.Core.Http;
using Newtonsoft.Json;
using RestSharp;

namespace PluginPlatform.Admin.Services
{
    public class RolesClient
        : BaseClient<RolesClient>
    {
        public RolesClient()
            : base(ConfigurationManager.AppSettings["UserService"])
        { }


        // api/Roles/Create 
        public Role Create(Role model)
        {
            var request = new RestRequest("api/Roles/Create/", Method.POST)
                .AddJsonBody(model);
            return Execute<Role>(request);
        }

        // api/Roles/Delete/id 
        public void Delete(int id)
        {
            var request = new RestRequest("api/Roles/Delete/" + id, Method.POST);
            Execute(request);
        }

        // api/Roles/GetAll 
        public List<Role> GetAll()
        {
            var request = new RestRequest("api/Roles/GetAll/", Method.GET);
            return Execute<List<Role>>(request);
        }

        //api/Roles/Update/id
        public Role Update(int id, Role model)
        {
            var request = new RestRequest("api/Roles/Update/" + id, Method.POST)
                .AddJsonBody(model);
            return Execute<Role>(request);
        }

        // api/Roles/SetPlugins/id
        public Role SetPlugins(int id, List<int> plugins)
        {
            var request = new RestRequest("api/Roles/SetPlugins/" + id, Method.POST)
                .AddJsonBody(plugins);

            return Execute<Role>(request);
        }
    }
}
