﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using PluginPlatform.Admin.Models;
using PluginPlatform.Core.Http;
using Newtonsoft.Json;
using RestSharp;

namespace PluginPlatform.Admin.Services
{
    public class UsersClient
        : BaseClient<UsersClient>
    {
        public UsersClient()
            : base(ConfigurationManager.AppSettings["UserService"])
        { }

        // api/Users/Lock/id 
        public void Lock(int id, int? forDays)
        {
            var request = new RestRequest("api/Users/Lock/" + id, Method.POST)
                .AddJsonBody(forDays);
            Execute(request);
        }

        // api/Users/Unlock/id 
        public void Unlock(int id)
        {
            var request = new RestRequest("api/Users/Unlock/" + id, Method.POST);
            Execute(request);
        }

        // api/Users/IsLocked/id 
        public bool IsLocked(int id)
        {
            var request = new RestRequest("api/Users/IsLocked/" + id, Method.POST);
            return Execute<bool>(request);
        }


        // api/Users/Create 
        public User Create(User model)
        {
            var request = new RestRequest("api/Users/Create/", Method.POST)
                .AddJsonBody(model);
            return Execute<User>(request);
        }

        // api/Users/Delete/id 
        public void Delete(int id)
        {
            var request = new RestRequest("api/Users/Delete/" + id, Method.POST);
            Execute(request);
        }

        // api/Users/GetAll 
        public List<User> GetAll()
        {
            var request = new RestRequest("api/Users/GetAll/", Method.GET);
            return Execute<List<User>>(request);
        }

        // api/Users/GetFromAD
        public List<User> GetFromAD(string domain)
        {
            var request = new RestRequest("api/Users/GetFromAD/", Method.GET);
            return Execute<List<User>>(request);
        }

        // POST api/Users/SetPassword 
        public string SetPassword(string login, string password, string confirmPassword)
        {
            SetUserPasswordModel model = new SetUserPasswordModel
            {
                UserName = login,
                NewPassword = password,
                ConfirmPassword = confirmPassword
            };

            var request = new RestRequest("api/Users/SetPassword/", Method.POST)
                .AddJsonBody(model);
            return Execute<string>(request);

        }

        //api/Users/Update/id
        public User Update(int id, User model)
        {
            var request = new RestRequest("api/Users/Update/" + id, Method.POST)
                .AddJsonBody(model);
            return Execute<User>(request);
        }

        internal class SetUserPasswordModel
        {
            public string UserName { get; set; }
            public string NewPassword { get; set; }
            public string ConfirmPassword { get; set; }
        }

    }
}
