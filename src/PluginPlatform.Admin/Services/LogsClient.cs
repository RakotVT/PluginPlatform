﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using PluginPlatform.Admin.Models;
using PluginPlatform.Core.Http;
using RestSharp;

namespace PluginPlatform.Admin.Services
{
    public class LogsClient
        : BaseClient<LogsClient>
    {
        public LogsClient()
            : base(ConfigurationManager.AppSettings["LogService"])
        {

        }

        public List<Log> GetAll()
        {
            RestRequest request = new RestRequest("api/Logs/GetAll");
            return Execute<List<Log>>(request);
        }
    }
}
