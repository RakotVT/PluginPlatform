﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using PluginPlatform.Admin.Models;
using PluginPlatform.Core.Http;
using Newtonsoft.Json;
using RestSharp;

namespace PluginPlatform.Admin.Services
{
    public class PluginsClient
        : BaseClient<PluginsClient>
    {
        public PluginsClient()
            : base(ConfigurationManager.AppSettings["PluginService"])
        { }


        // api/Manage/Create 
        public Plugin Create(Plugin model)
        {
            var request = new RestRequest("api/Manage/Create/", Method.POST)
                .AddJsonBody(model);
            return Execute<Plugin>(request);
        }

        // api/Manage/Delete/id 
        public Plugin Delete(int id)
        {
            var request = new RestRequest("api/Manage/Delete/" + id, Method.POST);
            return Execute<Plugin>(request);
        }

        // api/Manage/GetAll 
        public List<Plugin> GetAll()
        {
            var request = new RestRequest("api/Manage/GetAll/", Method.GET);
            return Execute<List<Plugin>>(request);
        }

        //api/Manage/Update/id
        public Plugin Update(int id, Plugin model)
        {
            var request = new RestRequest("api/Manage/Update/" + id, Method.POST)
                .AddJsonBody(model);
            return Execute<Plugin>(request);
        }

        // api/Manage/UploadFiles?versionId=id
        public Models.Version UploadFiles(int versionId, List<PluginFile> files)
        {
            var request = new RestRequest("api/Manage/UploadFiles", Method.POST)
                .AddQueryParameter("versionId", versionId.ToString());
            request.AddJsonBody(files);
            return Execute<Models.Version>(request);
        }

        // api/Manage/DeleteFiles?versionId=id
        public Models.Version DeleteFiles(int versionId, List<int> filesId)
        {
            var request = new RestRequest("api/Manage/DeleteFiles", Method.POST)
                .AddQueryParameter("versionId", versionId.ToString())
                .AddJsonBody(filesId);
            return Execute<Models.Version>(request);
        }

        // api/Manage/SetMainFile?pluginFileId=id
        public Models.Version SetMainFile(int pluginFileId)
        {
            var request = new RestRequest("api/Manage/SetMainFile", Method.POST)
                .AddQueryParameter("pluginFileId", pluginFileId.ToString());
            return Execute<Models.Version>(request);
        }

        // api/Manage/PublishVersion?versionId=id
        public Models.Version PublishVersion(int versionId)
        {
            var request = new RestRequest("api/Manage/PublishVersion", Method.POST)
                .AddQueryParameter("versionId", versionId.ToString());
            return Execute<Models.Version>(request);
        }
    }
}
