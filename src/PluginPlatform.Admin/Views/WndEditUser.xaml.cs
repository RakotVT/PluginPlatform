﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.ViewModels;

namespace PluginPlatform.Admin.Views
{
    /// <summary>
    /// Interaction logic for WndEditUser.xaml
    /// </summary>
    public partial class WndEditUser : Window
    {
        public WndEditUser(UserViewModel userViewModel)
        {
            InitializeComponent();
            DataContext = userViewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
