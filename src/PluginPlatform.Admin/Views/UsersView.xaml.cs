﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PluginPlatform.Admin.ViewModels;
using PluginPlatform.Core.Extentions;

namespace PluginPlatform.Admin.Views
{
    /// <summary>
    /// Interaction logic for UsersView.xaml
    /// </summary>
    public partial class UsersView : UserControl
    {
        public UsersViewModel ViewModel { get { return new UsersViewModel(); } }

        public UsersView()
        {
            InitializeComponent();                
        }
    }
}
