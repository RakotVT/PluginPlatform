﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PluginPlatform.Admin.ViewModels;

namespace PluginPlatform.Admin.Views
{
    /// <summary>
    /// Interaction logic for PluginsView.xaml
    /// </summary>
    public partial class PluginsView : UserControl
    {
        public PluginsViewModel ViewModel { get { return new PluginsViewModel(); } }

        public PluginsView()
        {
            InitializeComponent();
        }
    }
}
