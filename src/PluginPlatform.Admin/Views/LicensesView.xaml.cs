﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ITWebNet.UMS.Admin.ViewModels;

namespace ITWebNet.UMS.Admin.Views
{
    /// <summary>
    /// Interaction logic for LicensesView.xaml
    /// </summary>
    public partial class LicensesView : UserControl
    {
        public LicensesViewModel ViewModel { get { return new LicensesViewModel(); } }

        public LicensesView()
        {
            InitializeComponent();
        }
    }
}
