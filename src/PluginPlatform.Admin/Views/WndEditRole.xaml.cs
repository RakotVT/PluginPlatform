﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.ViewModels;

namespace PluginPlatform.Admin.Views
{
    /// <summary>
    /// Interaction logic for WndEditRole.xaml
    /// </summary>
    public partial class WndEditRole : Window
    {
        public WndEditRole()
        {
            InitializeComponent();
        }

        public WndEditRole(RoleViewModel selectedRole)
        {
            InitializeComponent();

            DataContext = selectedRole;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
