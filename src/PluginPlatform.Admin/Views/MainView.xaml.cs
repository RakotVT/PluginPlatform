﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using PluginPlatform.PluginCore.API;

namespace PluginPlatform.Admin.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
            DataContext = this;
        }

        #region IPlugin<FrameworkElement> Members

        public FrameworkElement GetMainPluginUI()
        {
            return this;
        }

        public ICollection<FrameworkElement> GetPlguinUIElements()
        {
            throw new NotImplementedException();
        }

        public bool StartUIInOneProcess
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IUserInfo Members

        public string CurrentSessionID { get; set; }

        #endregion
    }
}
