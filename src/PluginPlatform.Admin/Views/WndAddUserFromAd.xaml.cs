﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.ViewModels;

namespace PluginPlatform.Admin.Views
{
    /// <summary>
    /// Interaction logic for WndAddUserFromAd.xaml
    /// </summary>
    /// 

    public partial class WndAddUserFromAd : Window
    {
        public AdUsersViewModel ViewModel {
            get { return new AdUsersViewModel(); } 
        }

        public WndAddUserFromAd()
        {
            InitializeComponent();
        }

    }
}
