﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.ViewModels;

namespace PluginPlatform.Admin.Views
{
    /// <summary>
    /// Interaction logic for WndEditLicense.xaml
    /// </summary>
    public partial class WndEditLicense : Window
    {
        public WndEditLicense()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
