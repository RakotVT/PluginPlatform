﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PluginPlatform.Admin.Views
{
    /// <summary>
    /// Interaction logic for WndMain.xaml
    /// </summary>
    public partial class WndMain : Window
    {
        public WndMain()
        {
            InitializeComponent();
        }

        #region IPlugin

        /// <summary>
        /// Имя плагина.
        /// </summary>
        public string PluginName
        {
            get { return "ums.Admin"; }
        }

        /// <summary>
        /// Версия плагина.
        /// </summary>
        public string PluginVersion
        {
            get { return "v1.00"; }
        }

        /// <summary>
        /// Разработчик плагина.
        /// </summary>
        public string PluginDeveloper
        {
            get { return "ITWebNet"; }
        }

        /// <summary>
        /// Название плагина.
        /// </summary>
        public string PluginCaption
        {
            get { return "Админка UMS"; }
        }

        /// <summary>
        /// Значок плагина.
        /// </summary>
        public System.Drawing.Image PluginIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Описание плагина.
        /// </summary>
        public string PluginDescription
        {
            get { return "Плагин администирования пользователей, лицензий и плагинов, а так же просмотра логов ошибок плагинов"; }
        }

        #endregion
    }
}
