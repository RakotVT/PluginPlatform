﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using PluginPlatform.Admin.Views;
using PluginPlatform.PluginCore.API;

namespace PluginPlatform.Admin
{
    public class UmsAdminPlugin : IWPFPlugin, IUserInfo
    {
        #region Конструктор Singleton UmsPlugin

        private static volatile UmsAdminPlugin _instance;

        private static object _syncRoot = new Object();

        public UmsAdminPlugin()
        {

        }

        /// <summary>
        /// Экземпляр <see cref="UmsAdminPlugin"/>. 
        /// </summary>
        public static UmsAdminPlugin Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new UmsAdminPlugin();
                    }
                }
                return _instance;
            }
        }

        #endregion

        #region IPlugin<FrameworkElement> Members

        public FrameworkElement GetMainPluginUI()
        {
            return new MainView();
        }

        public ICollection<FrameworkElement> GetPlguinUIElements()
        {
            throw new NotImplementedException();
        }

        public bool StartUIInOneProcess
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IUserInfo Members

        public string CurrentSessionID { get; set; }

        #endregion

    }
}
