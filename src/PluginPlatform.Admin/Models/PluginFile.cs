using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class PluginFile
    {
        public int Id { get; set; }
        public string Hash { get; set; }
        public bool IsMainFile { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }

        public int VersionId { get; set; }
        public virtual Version Version { get; set; }

        public virtual FileContent Content { get; set; }
    }
}
