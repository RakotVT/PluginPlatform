﻿namespace PluginPlatform.Admin.Models
{
    public partial class FileContent
    {
        public int Id { get; set; }
        public byte[] Data { get; set; }

        public PluginFile File { get; set; }
    }
}
