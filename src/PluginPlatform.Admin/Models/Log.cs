using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class Log
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string MessageType { get; set; }
        public string MessageLevel { get; set; }
        public string StackTrace { get; set; }
        public string UserMessage { get; set; }
        public Nullable<int> LogMessageSourceId { get; set; }
        public Nullable<int> LogMinidumpId { get; set; }
        public Nullable<int> LogScreenshotId { get; set; }
        public Nullable<int> UserTicketId { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorLocation { get; set; }
        public virtual LogMessageSource LogMessageSource { get; set; }
        public virtual LogMinidump LogMinidump { get; set; }
        public virtual LogScreenshot LogScreenshot { get; set; }
        public virtual UserTicket UserTicket { get; set; }
    }
}
