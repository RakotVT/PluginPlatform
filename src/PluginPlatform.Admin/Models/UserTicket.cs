using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class UserTicket
    {
        public UserTicket()
        {
            this.Logs = new List<Log>();
        }

        public int Id { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public virtual ICollection<Log> Logs { get; set; }
        public virtual User User { get; set; }
    }
}
