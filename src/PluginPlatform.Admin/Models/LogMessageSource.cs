using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class LogMessageSource
    {
        public LogMessageSource()
        {
            this.Logs = new List<Log>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Log> Logs { get; set; }
    }
}
