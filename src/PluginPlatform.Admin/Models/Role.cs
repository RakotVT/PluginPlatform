using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class Role
    {
        public Role()
        {
            this.Plugins = new List<Plugin>();
            this.Users = new List<User>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public virtual List<Plugin> Plugins { get; set; }
        public virtual List<User> Users { get; set; }
    }
}
