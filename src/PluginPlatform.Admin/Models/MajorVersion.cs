using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITWebNet.UMS.Admin.Models
{
    [Table("MajorVersions")]
    public partial class MajorVersion : Version
    {
        public MajorVersion()
        {
            this.MinorVersions = new List<MinorVersion>();
        }

        public int PluginId { get; set; }
        public Nullable<int> LicenseId { get; set; }
        public virtual ICollection<MinorVersion> MinorVersions { get; set; }
        public virtual Plugin Plugin { get; set; }
    }
}
