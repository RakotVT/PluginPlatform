using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class User
    {
        public User()
        {
            this.Roles = new List<UserRole>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public bool IsADUser { get; set; }
        public string UserDomain { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public virtual ICollection<UserRole> Roles { get; set; }

        // ���� ��� ������� ������ ������������
        public string Password { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1} {2}", LastName, FirstName, Patronymic);
            }
        }
    }
}
