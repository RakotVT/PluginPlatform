﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginPlatform.Admin.Models
{
    public class UserRole
    {
        public int RoleId { get; set; }

        public int UserId { get; set; }
    }
}
