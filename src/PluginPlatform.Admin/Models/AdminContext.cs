using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace ITWebNet.UMS.Admin.Models
{
    public partial class AdminContext : DbContext
    {
        static AdminContext()
        {
            Database.SetInitializer<AdminContext>(null);
        }

        public AdminContext()
            : base("Name=AdminContext")
        {
        }

        public DbSet<LogMessageSource> LogMessageSources { get; set; }
        public DbSet<LogMinidump> LogMinidumps { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<LogScreenshot> LogScreenshots { get; set; }
        public DbSet<PluginFile> PluginFiles { get; set; }
        public DbSet<Plugin> Plugins { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserClaim> UserClaims { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserTicket> UserTickets { get; set; }
        public DbSet<Version> Versions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserLogin>()
                .HasKey(l => new { l.LoginProvider, l.ProviderKey, l.UserId });
        }
    }
}
