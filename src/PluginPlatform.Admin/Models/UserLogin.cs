using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class UserLogin
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
