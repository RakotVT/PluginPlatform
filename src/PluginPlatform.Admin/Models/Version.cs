using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class Version
    {
        public Version()
        {
            PluginFiles = new List<PluginFile>();
        }

        public int Id { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool IsPublished { get; set; }

        public int PluginId { get; set; }
        public virtual Plugin Plugin { get; set; }

        public virtual ICollection<PluginFile> PluginFiles { get; set; }
    }
}
