using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITWebNet.UMS.Admin.Models
{
    [Table("BuildVersions")]
    public partial class BuildVersion : Version
    {
        public BuildVersion()
        {
            this.PluginFiles = new List<PluginFile>();
        }

        public int MinorVersionId { get; set; }
        public virtual MinorVersion MinorVersion { get; set; }
        public virtual ICollection<PluginFile> PluginFiles { get; set; }
    }
}
