using System;
using System.Collections.Generic;

namespace PluginPlatform.Admin.Models
{
    public partial class Plugin
    {
        public Plugin()
        {
            Versions = new List<Version>();
            Roles = new List<Role>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? DeveloperId { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public virtual IList<Version> Versions { get; set; }
        public virtual IList<Role> Roles { get; set; }
    }
}
