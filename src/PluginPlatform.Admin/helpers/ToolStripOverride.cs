﻿using System.Windows.Forms;

namespace ums.Admin.helpers
{
    /// <summary>
    /// Класс, переопределяющй <see cref="ToolStripProfessionalRenderer"/>, для получения панели ToolStrip без рамок.
    /// </summary>
    public class ToolStripOverride : ToolStripProfessionalRenderer
    {
        /// <summary>
        /// Конструктор по-умолчанию.
        /// </summary>
        public ToolStripOverride() { }

        /// <summary>
        /// Переопределяет базовый метод <see cref="ToolStripProfessionalRenderer.OnRenderToolStripBorder"/>.
        /// </summary>
        /// <param name="e">Аргументы.</param>
        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e) { }
    }
}
