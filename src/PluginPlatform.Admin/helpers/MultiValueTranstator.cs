﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ITWebNet.WPF.Controls.Converters;

namespace PluginPlatform.Admin.Helpers
{
    public class MultiValueTranstator : MultiValueConverterBase<MultiValueTranstator>
    {
        #region IMultiValueConverter Members

        public override object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // просто передаем значения без преобразований, ибо мультибиндинг без конвертера жить не может.
            return values;
        }

        #endregion
    }
}
