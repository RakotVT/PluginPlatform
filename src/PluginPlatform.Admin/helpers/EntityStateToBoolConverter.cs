﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using PluginPlatform.Admin.Models;
using ITWebNet.WPF.Controls.Converters;

namespace PluginPlatform.Admin.Helpers
{
    public class EntityStateToBoolConverter : ConverterBase<EntityStateToBoolConverter>
    {
        public bool IsReversed { get; set; }

        #region IMultiValueConverter Members

        public override object Convert(object values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null)
                return false;

            bool result = (int)values == 0;

            return IsReversed ? !result : result;
        }

        #endregion
    }
}
