﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using ITWebNet.UMS.Admin.Models;
using System.Collections.ObjectModel;

namespace ITWebNet.UMS.Admin.Helpers
{
    static class ActionDirecotryExtensionMethods
    {
        public static string GetPropertyValue(
           this SearchResult sr, string propertyName)
        {
            string ret = string.Empty;

            if (sr.Properties[propertyName].Count > 0)
                ret = sr.Properties[propertyName][0].
                         ToString();

            return ret;
        }
    }

    class ActiveDirectoryHelper
    {
        public ObservableCollection<ActiveDirectoryUser> GetAllUsers()
        {
            ObservableCollection<ActiveDirectoryUser> AdUsers = new ObservableCollection<ActiveDirectoryUser>();
            SearchResultCollection results;
            DirectorySearcher ds = null;
            DirectoryEntry de = new
                  DirectoryEntry(GetCurrentDomainPath());

            ds = new DirectorySearcher(de);

            // Email Address
            ds.PropertiesToLoad.Add("mail");
            // First Name
            ds.PropertiesToLoad.Add("givenname");
            // Last Name (Surname)
            ds.PropertiesToLoad.Add("sn");
            // AD login
            ds.PropertiesToLoad.Add("sAMAccountName");

            ds.Filter = "(&(objectCategory=User)(objectClass=person))";

            results = ds.FindAll();

            foreach (SearchResult sr in results)
            {
                AdUsers.Add(
                    new ActiveDirectoryUser
                    {
                        EMail
                            = (sr.GetPropertyValue("mail") != null)
                                ? sr.GetPropertyValue("mail")
                                : sr.GetPropertyValue("userPrincipalName"),
                        Login = sr.GetPropertyValue("sAMAccountName"),
                        Name = sr.GetPropertyValue("givenname"),
                        Surname = sr.GetPropertyValue("sn")
                    }
                );
            }

            return AdUsers;
        }

        public string GetCurrentDomainPath()
        {
            DirectoryEntry de =
               new DirectoryEntry("LDAP://RootDSE");

            return "LDAP://" +
               de.Properties["defaultNamingContext"][0].
                   ToString();
        }
    }
}
