﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PluginPlatform.Admin.Helpers
{
    /// <summary>
    /// Interaction logic for WaitPlaceholder.xaml
    /// </summary>
    public partial class WaitPlaceholder : UserControl
    {
        public WaitPlaceholder()
        {
            InitializeComponent();

            const double offset = Math.PI;
            const double step = Math.PI * 2 / 10.0;

            for (double i = 0.0; i < 10.0; i++)
            {
                var x = (25.0 + Math.Sin(offset + i * step) * 25.0);
                var y = (25.0 + Math.Cos(offset + i * step) * 25.0);

                Ellipse elipse = new Ellipse();
                elipse.Width = elipse.Height = 15;
                elipse.Stretch = Stretch.Fill;
                elipse.Fill = elipse.Stroke = Brushes.Black;
                elipse.SetValue(Canvas.LeftProperty, x);
                elipse.SetValue(Canvas.TopProperty, y);
                elipse.Opacity = 1.0 - (i * 0.1);
                WaitCircle.Children.Add(elipse);
            }
        }
    }
}
