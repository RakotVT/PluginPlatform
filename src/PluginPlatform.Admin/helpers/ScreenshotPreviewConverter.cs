﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ITWebNet.WPF.Controls.Converters;

namespace PluginPlatform.Admin.Helpers
{
    public class ScreenshotPreviewConverter : ConverterBase<ScreenshotPreviewConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            byte[] buffer = (byte[])value;
            if (buffer == null || buffer.Length == 0)
                return null;


            var imageSource = new BitmapImage();
            using (MemoryStream ms = new MemoryStream(buffer))
            {
                imageSource.BeginInit();
                imageSource.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                imageSource.CacheOption = BitmapCacheOption.OnLoad;
                imageSource.StreamSource = ms;
                imageSource.EndInit(); 
            }
            imageSource.Freeze();
            return imageSource;
        }
    }
}
