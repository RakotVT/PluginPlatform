﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ITWebNet.WPF.Controls.Converters;

namespace PluginPlatform.Admin.Helpers
{
    public class NameVersionConverter : MultiValueConverterBase<NameVersionConverter>
    {
        #region IMultiValueConverter Members

        public override object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.Format("{0}.{1}", values[0], values[1]);
        }

        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            string source = value as string;
            int separatorIndex = source.LastIndexOf('.');
            string versionStr = source.Substring(separatorIndex + 1).Trim();
            int version;
            object[] result = null;
            if (Int32.TryParse(versionStr, out version))
            {
                if (separatorIndex < 0)
                    result = new object[] { string.Empty, version };
                else
                    result = new object[] { source.Substring(0, separatorIndex).Trim(), version };
                            
            }
            else
                result = new object[] { source };

            return result;
        }

        #endregion
    }
}
