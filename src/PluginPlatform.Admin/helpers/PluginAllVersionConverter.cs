﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using PluginPlatform.Admin.Models;
using ITWebNet.WPF.Controls.Converters;

namespace PluginPlatform.Admin.Helpers
{
    public class PluginAllVersionConverter : ConverterBase<PluginAllVersionConverter>
    {
        public PluginAllVersionConverter() { }

        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IEnumerable<Models.Version> majors = value as IEnumerable<Models.Version>;
            if (majors == null)
                return null;

            return majors;
        }
    }
}
