﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PluginPlatform.Core.Http;
using PluginPlatform.Core.ViewModels;

namespace PluginPlatform.Admin.Helpers
{
    public static class Extentions
    {
        //public static void RejectChanges(this DbContext context)
        //{
        //    foreach (var item in context.ChangeTracker.Entries())
        //    {
        //        switch (item.State)
        //        {
        //            case EntityState.Added:
        //                item.State = EntityState.Detached;
        //                break;
        //            case EntityState.Deleted:
        //                item.State = EntityState.Unchanged;
        //                break;
        //            case EntityState.Modified:
        //                item.CurrentValues.SetValues(item.OriginalValues);
        //                item.State = EntityState.Unchanged;
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //    context.SaveChanges();
        //}

        public static void Replace<T>(this IList<T> collection, T oldValue, T newValue)
        {
            var index = collection.IndexOf(oldValue);
            if (index != -1)
                collection[index] = newValue;
        }


        public static void HandleException(this ViewModelBase viewModel, Exception e)
        {
            var modelEx = e as ModelValidationException;

            if (modelEx != null)
            {
                string exText = string.Join(Environment.NewLine,
                    modelEx.ModelState.Select(
                        item =>
                        string.Format(
                            "{0}:\t{1}",
                            string.IsNullOrWhiteSpace(item.Key) ? "Общие" : item.Key,
                            string.Join("\r\n\t", item.Value))));

                MessageBox.Show(
                    string.Join(Environment.NewLine, exText),
                    modelEx.Message,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show(
                    string.Join(Environment.NewLine, e.Message),
                    "Что-то пошло не так",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

            }
        }
    }
}
