﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ITWebNet.WPF.Controls.Converters;

namespace PluginPlatform.Admin.Helpers
{
    public class VersionToStringConverter
        : MultiValueConverterBase<VersionToStringConverter>
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Format("{0}.{1}.{2}", values);
        }

        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            var version = value.ToString()
                .Split('.')
                .Select(v => int.Parse(v) as object)
                .ToArray();

            return version;
        }
    }
}
