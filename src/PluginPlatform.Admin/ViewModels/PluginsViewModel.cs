﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Xml.Linq;
using PluginPlatform.Admin.Helpers;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.Services;
using PluginPlatform.Admin.Views;
using PluginPlatform.Core.Hashing;
using PluginPlatform.Core.ViewModels;
using ITWebNet.WPF.Controls;
using Microsoft.Win32;

namespace PluginPlatform.Admin.ViewModels
{
    public class PluginsViewModel : ViewModelBase
    {

        private List<PluginFile> _copiedFiles;
        private Action _filesAction = Action.None;
        private ObservableCollection<Plugin> _plugins;
        private ObservableCollection<PluginFile> _pluginFiles;
        private object _selectedItem;
        private Plugin _selectedPlugin;
        private Models.Version _selectedVersion;
        PluginsClient _client;

        public PluginsViewModel()
        {
            _client = new PluginsClient().AddAuthentication(UmsAdminPlugin.Instance.CurrentSessionID);

            CopyFilesCommand = new DelegateCommand(CopyFiles, CanCopyFiles)
                .WithHandler(this.HandleException);

            CutFilesCommand = new DelegateCommand(CutFiles, CanCutFiles)
                .WithHandler(this.HandleException);

            DeleteFilesCommand = new DelegateCommand(DeleteFiles, CanDeleteFiles)
                .WithHandler(this.HandleException);

            EditPluginCommand = new DelegateCommand(EditPlugin, CanEditPlugin)
                .WithHandler(this.HandleException);

            NewCommand = new DelegateCommand(CreateNew, CanCreateNew)
                .WithHandler(this.HandleException);

            PasteFilesCommand = new DelegateCommand(PasteFiles, CanPasteFiles)
                .WithHandler(this.HandleException);

            PublishCommand = new DelegateCommand(PublishVersion, CanPublishVersion)
                .WithHandler(this.HandleException);

            UploadFilesCommand = new DelegateCommand(UploadFiles, CanUploadFiles)
                .WithHandler(this.HandleException);

            CheckIsMainCommand = new DelegateCommand(CheckIsMain, CanCheckIsMain)
                .WithHandler(this.HandleException);

            MakePluginArchiveCommand = new DelegateCommand(MakePluginArchive, CanMakePluginArchive)
                .WithHandler(this.HandleException);

            UploadPluginArchiveCommand = new DelegateCommand(UploadPluginArchive, CanUploadPluginArchive)
                .WithHandler(this.HandleException);

            RefreshCommand = new DelegateCommand(Refresh)
                .WithHandler(this.HandleException);


            try
            {
                Plugins = new ObservableCollection<Plugin>(_client.GetAll());
                SelectedPlugin = Plugins.FirstOrDefault();
                PluginFiles = new ObservableCollection<PluginFile>();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void Refresh(object obj)
        {
            Plugins = new ObservableCollection<Plugin>(_client.GetAll());
            SelectedPlugin = Plugins.FirstOrDefault();
        }

        public ICommand MakePluginArchiveCommand { get; private set; }

        public ICommand UploadPluginArchiveCommand { get; private set; }


        public ICommand CheckIsMainCommand { get; private set; }

        public List<PluginFile> CopiedFiles
        {
            get { return _copiedFiles; }
            set { _copiedFiles = value; RaisePropertyChanged("CopiedFiles"); }
        }

        public ICommand CopyAsLinkTemplatesCommand { get; private set; }

        public ICommand CopyFilesCommand { get; private set; }

        public ICommand CopyTemplatesCommand { get; private set; }

        public ICommand CutFilesCommand { get; private set; }

        public ICommand DeleteFilesCommand { get; private set; }

        public ICommand EditPluginCommand { get; private set; }

        public ICommand NewCommand { get; private set; }

        public ICommand PasteFilesCommand { get; private set; }

        public ICommand PasteTemplatesCommand { get; private set; }

        public ObservableCollection<PluginFile> PluginFiles
        {
            get { return _pluginFiles; }
            set { _pluginFiles = value; RaisePropertyChanged("PluginFiles"); }
        }

        public ObservableCollection<Plugin> Plugins
        {
            get { return _plugins; }
            set
            {
                _plugins = value;
                RaisePropertyChanged("Plugins");
            }
        }

        public ICommand PublishCommand { get; private set; }

        public object SelectedItem
        {
            get
            {
                return _selectedItem;
            }

            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    if (_selectedItem is Plugin)
                        SelectedPlugin = _selectedItem as Plugin;
                    else if (_selectedItem is Models.Version)
                    {
                        var version = _selectedItem as Models.Version;
                        var plugin = Plugins.FirstOrDefault(item => item.Id == version.PluginId);
                        SelectedPlugin = plugin;
                        SelectedVersion = version;
                    }

                    RaisePropertyChanged("SelectedItem");
                }
            }
        }

        public Plugin SelectedPlugin
        {
            get
            {
                return _selectedPlugin;
            }

            set
            {
                if (_selectedPlugin != value)
                {
                    _selectedPlugin = value;
                    if (_selectedPlugin != null)
                        SelectedVersion = _selectedPlugin.Versions
                            .OrderByDescending(v => v.Major)
                            .ThenByDescending(v => v.Minor)
                            .ThenByDescending(v => v.Build)
                            .FirstOrDefault();
                    RaisePropertyChanged("SelectedPlugin");
                }
            }
        }
        public Models.Version SelectedVersion
        {
            get { return _selectedVersion; }
            set
            {
                _selectedVersion = value;
                if (_selectedVersion != null)
                    PluginFiles = new ObservableCollection<PluginFile>(_selectedVersion.PluginFiles);
                RaisePropertyChanged("SelectedVersion");
            }
        }


        public ICommand UploadFilesCommand { get; private set; }

        public ICommand RefreshCommand { get; private set; }

        private void MakePluginArchive(object obj)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.ValidateNames = true;
            dlg.AddExtension = true;
            dlg.DefaultExt = ".zip";
            dlg.Filter = "Zip архив|*.zip";
            bool? result = dlg.ShowDialog();
            if (result.HasValue && result.Value)
                MakePluginArchive(dlg.FileName, SelectedVersion);
        }

        private void MakePluginArchive(string fileName, Models.Version build)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            using (Package package = Package.Open(fileName, FileMode.Create))
            {
                Uri documentUri = PackUriHelper.CreatePartUri(new Uri("description.xml", UriKind.Relative));
                PackagePart documentPart = package.CreatePart(documentUri, MediaTypeNames.Text.Xml, CompressionOption.Maximum);
                CreatePluginDescription(build, documentPart.GetStream());

                foreach (PluginFile item in build.PluginFiles)
                {
                    Uri fileUri = PackUriHelper.CreatePartUri(new Uri(Path.Combine(item.RelativePath, item.Name), UriKind.Relative));
                    PackagePart filePart = package.CreatePart(fileUri, MediaTypeNames.Application.Octet, CompressionOption.Maximum);
                    filePart.GetStream().Write(item.Content.Data, 0, item.Content.Data.Length);
                }
            }
            Mouse.OverrideCursor = null;
        }

        private void CreatePluginDescription(Models.Version version, Stream stream)
        {
            Plugin plugin = version.Plugin;

            XDocument description = new XDocument();
            XElement pluginElement = new XElement(
                "plugin",
                string.IsNullOrWhiteSpace(plugin.Name) ? null : new XAttribute("name", plugin.Name),
                plugin.DeveloperId == null ? null : new XAttribute("developer", plugin.DeveloperId),
                string.IsNullOrWhiteSpace(plugin.Caption) ? null : new XAttribute("caption", plugin.Caption),
                string.IsNullOrWhiteSpace(plugin.Description) ? null : new XAttribute("description", plugin.Description));
            description.Add(pluginElement);
            XElement buildElement = new XElement(
                "version",
                new XAttribute("Major", version.Major),
                new XAttribute("Minor", version.Minor),
                new XAttribute("Build", version.Build),
                string.IsNullOrWhiteSpace(version.Description) ? null : new XAttribute("description", version.Description),
                string.IsNullOrWhiteSpace(version.Name) ? null : new XAttribute("name", version.Name),
                new XAttribute("isPublished", version.IsPublished),
                new XAttribute("mainFile", version.PluginFiles.FirstOrDefault(item => item.IsMainFile).Name));
            pluginElement.Add(buildElement);

            description.Save(stream, System.Xml.Linq.SaveOptions.None);
        }

        private bool CanMakePluginArchive(object obj)
        {
            return SelectedVersion != null && PluginFiles != null && SelectedVersion.IsPublished && PluginFiles.Any(item => item.IsMainFile);
        }

        private bool CanUploadPluginArchive(object obj)
        {
            return true;
        }

        private bool CanCheckIsMain(object obj)
        {
            PluginFile file = obj as PluginFile;
            if (file == null)
                return false;
            string ext = Path.GetExtension(file.Name);
            if (!(ext.Equals(".exe") || ext.Equals(".dll")))
                return false;

            return true;
        }

        private bool CanCopyFiles(object obj)
        {
            bool result = false;
            IList<object> selectedFiles = null;
            if (obj is IList<object>)
            {
                selectedFiles = obj as IList<object>;
                result = selectedFiles.Count > 0 && SelectedVersion != null;
            }

            return result;
        }

        private bool CanCreateNew(object obj)
        {
            return true;
        }

        private bool CanCutFiles(object obj)
        {
            bool result = false;
            IList<object> selectedFiles = null;
            if (obj is IList<object>)
            {
                selectedFiles = obj as IList<object>;
                result = selectedFiles.Count > 0 && SelectedVersion != null && !SelectedVersion.IsPublished;
            }

            return result;
        }

        private bool CanDeleteFiles(object obj)
        {
            bool result = false;
            IList<object> selectedFiles = null;
            if (obj is IList<object>)
            {
                selectedFiles = obj as IList<object>;
                result = selectedFiles.Count > 0 && SelectedVersion != null && !SelectedVersion.IsPublished;
            }

            return result;
        }

        private bool CanEditPlugin(object obj)
        {
            return true;
        }

        private bool CanPasteFiles(object obj)
        {
            return SelectedVersion != null &&
                CopiedFiles != null &&
                CopiedFiles.Count > 0 &&
                CopiedFiles.FirstOrDefault().VersionId != SelectedVersion.Id;
        }

        private bool CanPublishVersion(object obj)
        {
            return SelectedVersion != null && !SelectedVersion.IsPublished && PluginFiles.Any(item => item.IsMainFile);
        }

        private bool CanUploadFiles(object obj)
        {
            return SelectedVersion != null && !SelectedVersion.IsPublished;
        }

        private void CheckIsMain(object obj)
        {
            PluginFile file = obj as PluginFile;
            foreach (var item in PluginFiles)
            {
                item.IsMainFile = false;
            }
            file.IsMainFile = true;
            var version = _client.SetMainFile(file.Id);
            SelectedPlugin.Versions.Replace(SelectedVersion, version);
            SelectedVersion = version;
        }

        private void CopyFiles(object obj)
        {
            IList<object> selectedFiles = obj as IList<object>;
            CopiedFiles = selectedFiles.Cast<PluginFile>().ToList();
            _filesAction = Action.Copy;
        }

        private void CreateNew(object obj)
        {
            string newObject = obj as string;
            Plugin plugin;
            Models.Version version = null;
            Models.Version last;
            switch (newObject)
            {
                case "Plugin":
                    plugin = new Plugin();
                    version = new Models.Version { Major = 1, Minor = 0, Build = 0 };
                    Plugins.Add(plugin);
                    SelectedPlugin = plugin;
                    break;
                case "Version":
                    if (SelectedPlugin == null)
                        break;
                    last = SelectedPlugin.Versions
                        .OrderByDescending(item => item.Major)
                        .ThenByDescending(item => item.Minor)
                        .ThenByDescending(item => item.Build)
                        .FirstOrDefault();
                    if (last == null)
                        version = new Models.Version { Major = 1, Minor = 0, Build = 0 };
                    else
                        version = new Models.Version()
                        {
                            Major = last.Major,
                            Minor = last.Minor,
                            Build = last.Build + 1,
                            Name = last.Name,
                            Description = last.Description
                        };
                    break;
                //case "Minor":
                //    if (SelectedPlugin == null)
                //        break;
                //    last = SelectedPlugin.Versions
                //        .OrderByDescending(item => item.Major)
                //        .ThenByDescending(item => item.Minor)
                //        .FirstOrDefault();
                //    if (last == null)
                //        version = new Models.Version { Major = 1, Minor = 0, Build = 0 };
                //    else
                //        version = new Models.Version()
                //        {
                //            Major = last.Major,
                //            Minor = last.Minor + 1,
                //            Build = 0,
                //            Name = last.Name,
                //            Description = last.Description
                //        };
                //    break;
                //case "Build":
                //    if (SelectedPlugin == null)
                //        break;
                //    last = SelectedPlugin.Versions
                //        .OrderByDescending(item => item.Major)
                //        .ThenByDescending(item => item.Minor)
                //        .ThenByDescending(item => item.Build)
                //        .FirstOrDefault();
                //    if (last == null)
                //        version = new Models.Version { Major = 1, Minor = 0, Build = 0 };
                //    else
                //        version = new Models.Version()
                //        {
                //            Major = last.Major,
                //            Minor = last.Minor,
                //            Build = last.Build + 1,
                //            Name = last.Name,
                //            Description = last.Description
                //        };
                //    break;
                default:
                    break;
            }

            version.PluginId = SelectedPlugin.Id;
            SelectedPlugin.Versions.Add(version);
            SelectedVersion = version;

        }

        private void CutFiles(object obj)
        {
            IList<object> selectedFiles = obj as IList<object>;
            CopiedFiles = selectedFiles.Cast<PluginFile>().ToList();
            _filesAction = Action.Cut;
        }

        private void DeleteFiles(object obj)
        {
            var selectedFiles = (obj as IList<object>).Cast<PluginFile>().ToList();
            if (selectedFiles.FirstOrDefault().VersionId != SelectedVersion.Id)
                return;
            var version = _client.DeleteFiles(SelectedVersion.Id, selectedFiles.Select(f => f.Id).ToList());

            SelectedPlugin.Versions.Replace(SelectedVersion, version);
            SelectedVersion = version;
        }

        private void EditPlugin(object obj)
        {
            bool? result = new WndEditPlugin() { DataContext = this }.ShowDialog();
            if (result.HasValue && result.Value)
            {
                Plugin newPlugin;
                if (SelectedPlugin.Id == 0)
                    newPlugin = _client.Create(SelectedPlugin);
                else
                    newPlugin = _client.Update(SelectedPlugin.Id, SelectedPlugin);

                Plugins.Replace(SelectedPlugin, newPlugin);
                SelectedPlugin = newPlugin;
            }
            else
                Plugins = new ObservableCollection<Plugin>(_client.GetAll());

            SelectedItem = SelectedPlugin;
        }

        private void PasteFiles(object obj)
        {
            Models.Version version = null;
            switch (_filesAction)
            {
                case Action.Copy:
                    version = _client.UploadFiles(SelectedVersion.Id, CopiedFiles);
                    break;

                case Action.Cut:
                    int prevVersionId = CopiedFiles[0].VersionId;
                    _client.UploadFiles(SelectedVersion.Id, CopiedFiles);
                    version = _client.DeleteFiles(prevVersionId, CopiedFiles.Select(f => f.Id).ToList());
                    break;

                case Action.None:
                default:
                    break;
            }

            SelectedPlugin.Versions.Replace(SelectedVersion, version);
            SelectedVersion = version;
        }

        private void PublishVersion(object obj)
        {
            var version = _client.PublishVersion(SelectedVersion.Id);
            SelectedPlugin.Versions.Replace(SelectedVersion, version);
            SelectedVersion = version;
        }

        private void UploadFiles(object obj)
        {
            string parameter = obj as string;
            if (parameter.Equals("Files"))
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Multiselect = true;
                dialog.CheckPathExists = true;
                dialog.CheckFileExists = true;
                dialog.ValidateNames = false;

                dialog.Filter = @"Файлы плагина|*.exe;*.dll;*.config|Все файлы|*.*";
                bool? result = dialog.ShowDialog();

                if (result.HasValue && result.Value)
                {
                    UploadFiles(dialog.FileNames);
                }
            }
            else
            {
                OpenFolderDialog dialog = new OpenFolderDialog();
                bool? result = dialog.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    UploadFiles(Directory.GetFiles(dialog.Folder, "*.*", SearchOption.AllDirectories), dialog.Folder);
                }
            }
        }

        private void UploadFiles(string[] fileNames, string root = null)
        {
            var files = PluginFiles.ToDictionary(item => item.Name);

            HashProvider hasher = new HashProvider();
            foreach (var item in fileNames)
            {
                FileInfo info = new FileInfo(item);

                string relativePath = null;
                if (!string.IsNullOrWhiteSpace(root))
                    relativePath = info.DirectoryName.Replace(root, null);

                PluginFile file;
                string hash = hasher.ComputeHash(item);

                if (files.TryGetValue(item, out file))
                {
                    if (!file.Hash.Equals(hash))
                    {
                        file.Hash = hash;
                        file.Content.Data = File.ReadAllBytes(item);
                    }
                }
                else
                {
                    PluginFile newFile = new PluginFile()
                    {
                        Content = new FileContent() { Data = File.ReadAllBytes(item) },
                        Hash = hash,
                        Name = Path.GetFileName(item),
                        RelativePath = relativePath,
                    };
                    PluginFiles.Add(newFile);
                }
            }

            var version = _client.UploadFiles(SelectedVersion.Id, PluginFiles.ToList());
            SelectedPlugin.Versions.Replace(SelectedVersion, version);
            SelectedVersion = version;
        }

        private void UploadPluginArchive(object obj)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = false;
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            dlg.ValidateNames = true;
            dlg.Filter = "Zip архив|*.zip";

            bool? result = dlg.ShowDialog();
            if (result.HasValue && result.Value)
                UploadArchive(dlg.FileName);
        }

        private void UploadArchive(string fileName)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            string descriptionName = "description.xml";
            string tempDir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDir);
            Uri baseUri = new Uri(tempDir);

            using (Package package = Package.Open(fileName, FileMode.Open))
            {

                var packageParts = package.GetParts();
                if (!packageParts.Any(item => item.Uri.OriginalString.Contains(descriptionName)))
                {
                    MessageBox.Show(
                        "Архив имеет неверный формат. Отсутствует файл описания плагина.",
                        "Загрузка плагина",
                        MessageBoxButton.OK,
                        MessageBoxImage.Warning);
                    return;
                }

                foreach (PackagePart part in packageParts)
                {
                    UriBuilder builder = new UriBuilder(baseUri);
                    builder.Path += part.Uri.OriginalString;

                    string filePath = builder.Uri.LocalPath; //new Uri(baseUri, part.Uri).LocalPath;
                    using (FileStream file = File.Create(filePath))
                    {
                        Stream partStream = part.GetStream();
                        byte[] content = new byte[partStream.Length];
                        partStream.Read(content, 0, content.Length);
                        file.Write(content, 0, content.Length);
                    }
                }
            }

            string[] files = Directory.GetFiles(tempDir, "*.*", SearchOption.AllDirectories)
                .Where(item => !Path.GetFileName(item).Equals(descriptionName, StringComparison.InvariantCultureIgnoreCase))
                .ToArray();

            string descriptionFile = Directory.GetFiles(tempDir, descriptionName, SearchOption.TopDirectoryOnly).FirstOrDefault();

            Models.Version build = CreateOrUpdatePlugin(descriptionFile);
            if (build == null)
            {
                MessageBox.Show(
                    "Загружаемая версия плагина уже существует.",
                    "Загрузка плагина",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            SelectedItem = build;

            UploadFiles(files, tempDir);
            string mainFileName = GetMainFile(descriptionFile);
            PluginFile mainFile = PluginFiles.FirstOrDefault(item => item.Name.Equals(mainFileName, StringComparison.InvariantCultureIgnoreCase));
            CheckIsMain(mainFile);

            Mouse.OverrideCursor = null;
        }

        private string GetMainFile(string descriptionFile)
        {
            XDocument description = XDocument.Load(descriptionFile);
            return (string)description.Descendants("version").FirstOrDefault().Attribute("mainFile");
        }

        private Models.Version CreateOrUpdatePlugin(string descriptionFile)
        {
            XDocument description = XDocument.Load(descriptionFile);
            XElement pluginElement = description.Root;
            if (pluginElement == null)
            {
                MessageBox.Show(
                    "Неверный формат описания",
                    "Загрузка плагина",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return null;
            }
            XElement versionElement = pluginElement.Element("version");
            if (versionElement == null)
            {
                MessageBox.Show(
                    "Неверный формат описания",
                    "Загрузка плагина",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return null;
            }

            Plugin plugin = Plugins
                .FirstOrDefault(item => item.Name.Equals(
                    (string)pluginElement.Attribute("name"),
                    StringComparison.InvariantCultureIgnoreCase));
            if (plugin == null)
            {
                plugin = new Plugin()
                {
                    Name = (string)pluginElement.Attribute("name"),
                    Caption = (string)pluginElement.Attribute("caption"),
                    Description = (string)pluginElement.Attribute("description"),
                    DeveloperId = (int?)pluginElement.Attribute("developerId")
                };

                Plugins.Add(plugin);
            }

            Models.Version build = plugin.Versions
                 .FirstOrDefault(
                    item =>
                            item.Major == (int)versionElement.Attribute("major") &&
                            item.Minor == (int)versionElement.Attribute("minor") &&
                            item.Build == (int)versionElement.Attribute("build"));
            if (build == null)
            {
                build = new Models.Version()
                {
                    Major = (int)versionElement.Attribute("major"),
                    Minor = (int)versionElement.Attribute("minor"),
                    Build = (int)versionElement.Attribute("build"),
                    Name = (string)versionElement.Attribute("name"),
                    Description = (string)versionElement.Attribute("description"),
                    IsPublished = (bool)versionElement.Attribute("isPublished")
                };
                plugin.Versions.Add(build);
            }
            else
                return null;

            Plugin newPlugin;
            if (plugin.Id == 0)
                newPlugin = _client.Create(plugin);
            else
                newPlugin = _client.Update(plugin.Id, plugin);

            Plugins.Replace(plugin, newPlugin);
            SelectedPlugin = newPlugin;

            return build;
        }

        private enum Action
        {
            None,
            Copy,
            CopyLink,
            Cut
        }

    }
}