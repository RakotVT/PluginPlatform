﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using PluginPlatform.Admin.Helpers;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.Services;
using PluginPlatform.Core.ViewModels;
using ITWebNet.WPF.Controls;

namespace PluginPlatform.Admin.ViewModels
{
    public class UserViewModel : ViewModelBase
    {
        private User _origin;
        private RolesClient _rolesClient;

        public string FirstName
        {
            get { return _origin.FirstName; }
            set { _origin.FirstName = value; }
        }


        public string LastName
        {
            get { return _origin.LastName; }
            set { _origin.LastName = value; }
        }

        public string Patronymic
        {
            get { return _origin.Patronymic; }
            set { _origin.Patronymic = value; }
        }

        public string Email
        {
            get { return _origin.Email; }
            set { _origin.Email = value; }
        }

        public string UserName
        {
            get { return _origin.UserName; }
            set { _origin.UserName = value; }
        }

        public string Password
        {
            get { return _origin.Password; }
            set { _origin.Password = value; }
        }

        public ObservableCollection<Role> ExistedRoles { get; set; }

        public ObservableCollection<Role> AvailableRoles { get; set; }

        public ICommand AddCommand { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        public UserViewModel(User user)
        {
            _rolesClient = new RolesClient().AddAuthentication(UmsAdminPlugin.Instance.CurrentSessionID);

            _origin = user;

            AddCommand = new DelegateCommand(AddPlugins, CanMovePlugins).WithHandler(this.HandleException);
            DeleteCommand = new DelegateCommand(DeletePlugins, CanMovePlugins).WithHandler(this.HandleException);

            var allRoles = _rolesClient.GetAll();

            IEnumerable<Role> availableRoles;
            IEnumerable<Role> existedRoles = new List<Role>();
            if (_origin.Roles == null || _origin.Roles.Count == 0)
                availableRoles = allRoles;
            else
            {
                var existedIds = _origin.Roles.Select(item => item.RoleId);
                availableRoles = allRoles.Where(item => !(existedIds.Contains(item.Id)));
                existedRoles = allRoles.Where(item => (existedIds.Contains(item.Id)));
            }

            AvailableRoles = new ObservableCollection<Role>(availableRoles);
            ExistedRoles = new ObservableCollection<Role>(existedRoles);
        }

        private bool CanMovePlugins(object obj)
        {
            var selected = obj as IEnumerable;
            if (selected == null)
                return false;
            var roles = selected.Cast<Role>();

            return roles != null && roles.Count() > 0;
        }

        private void DeletePlugins(object obj)
        {
            var selected = obj as IEnumerable;
            if (selected == null)
                return;
            var roles = selected.Cast<Role>().ToList();

            foreach (var item in roles)
            {
                ExistedRoles.Remove(item);
                var role = _origin.Roles.FirstOrDefault(r => r.RoleId == item.Id);
                _origin.Roles.Remove(role);
                AvailableRoles.Add(item);
            }
        }

        private void AddPlugins(object obj)
        {
            var selected = obj as IEnumerable;
            if (selected == null)
                return;
            var roles = selected.Cast<Role>().ToList();

            foreach (var item in roles)
            {
                ExistedRoles.Add(item);
                _origin.Roles.Add(new UserRole
                {
                    RoleId = item.Id,
                    UserId = _origin.Id
                });
                AvailableRoles.Remove(item);
            }
        }
    }
}
