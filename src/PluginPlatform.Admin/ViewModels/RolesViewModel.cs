﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using PluginPlatform.Admin.Helpers;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.Services;
using PluginPlatform.Admin.Views;
using PluginPlatform.Core.ViewModels;
using ITWebNet.WPF.Controls;

namespace PluginPlatform.Admin.ViewModels
{
    public class RolesViewModel : ViewModelBase
    {
        private RolesClient _client;

        public ICommand AddCommand { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        public ICommand EditCommand { get; private set; }

        public ICommand RefreshCommand { get; private set; }


        private ObservableCollection<Role> _roles;

        public ObservableCollection<Role> Roles
        {
            get { return _roles; }
            set { _roles = value; RaisePropertyChanged("Roles"); }
        }


        private Role _selectedRole;

        public Role SelectedRole
        {
            get { return _selectedRole; }
            set { _selectedRole = value; RaisePropertyChanged("SelectedRole"); }
        }

        public RolesViewModel()
        {
            _client = new RolesClient().AddAuthentication(UmsAdminPlugin.Instance.CurrentSessionID);

            AddCommand = new DelegateCommand(AddRole)
                .WithHandler(this.HandleException);

            EditCommand = new DelegateCommand(EditRole, CanEditRole)
                .WithHandler(this.HandleException);

            DeleteCommand = new DelegateCommand(DeleteRole, CanDeleteRole)
                .WithHandler(this.HandleException);

            RefreshCommand = new DelegateCommand(Refresh)
                .WithHandler(this.HandleException);

            Roles = new ObservableCollection<Role>(_client.GetAll());
        }

        private void AddRole(object obj)
        {
            Role role = new Role();
            Roles.Add(role);
            SelectedRole = role;
            EditRole(null);
        }

        private bool CanEditRole(object obj)
        {
            if (SelectedRole == null)
                return false;
            return true;
        }

        private void EditRole(object obj)
        {
            WndEditRole wnd = new WndEditRole(new RoleViewModel(SelectedRole));
            bool? result = wnd.ShowDialog();
            if (result.HasValue && result.Value)
            {
                Role newRole;
                if (SelectedRole.Id == 0)
                    newRole = _client.Create(SelectedRole);
                else
                    newRole = _client.Update(SelectedRole.Id, SelectedRole);

                Roles.Replace(SelectedRole, newRole);
                SelectedRole = newRole;
            }

            HasErrors = false;
        }

        private bool CanDeleteRole(object obj)
        {
            if (SelectedRole == null)
                return false;
            return true;
        }

        private void DeleteRole(object obj)
        {
            var result = MessageBox.Show(
                "Вы уверены, что хотите удалить выбранную роль?",
                "Удаление роли " + SelectedRole.Name,
                MessageBoxButton.YesNo,
                MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                _client.Delete(SelectedRole.Id);
                Roles.Remove(SelectedRole);
            }
        }

        private void Refresh(object obj)
        {
            Roles = new ObservableCollection<Role>(_client.GetAll());
        }
    }
}
