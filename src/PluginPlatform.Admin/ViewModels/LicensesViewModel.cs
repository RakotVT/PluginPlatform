﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Input;
using ITWebNet.UMS.Admin.Helpers;
using ITWebNet.UMS.Admin.Models;
using ITWebNet.UMS.Admin.Views;
using ITWebNet.UMS.Core.Extentions;
using ITWebNet.UMS.Core.ViewModels;
using ITWebNet.WPF.Controls;

namespace ITWebNet.UMS.Admin.ViewModels
{
    public class LicensesViewModel : ViewModelBase
    {
        #region Private Fields

        private AdminContext _context;

        private DateTime? _endDate;

        private ObservableCollection<Plugin> _plugins;

        private UserLicense _selectedLicense;

        private Plugin _selectedPlugin;

        private PluginLicense _selectedPluginLicense;

        private LicenseTemplate _selectedTemplate;

        private MajorVersion _selectedVersion;

        private DateTime? _startDate;

        #endregion Private Fields

        #region Public Constructors

        public LicensesViewModel()
        {
            AddCommand = new DelegateCommand(AddLicense);
            EditCommand = new DelegateCommand(EditLicense, CanEditLicense);
            AddPluginLicenseCommand = new DelegateCommand(AddPluginLicense, CanAddPluginLicense);
            DeletePluginLicenseCommand = new DelegateCommand(DeletePluginLicense, CanDeletePluginLicense);
            DeleteCommand = new DelegateCommand(DeleteLicense, CanDeleteLicense);
            GenerateKeyCommand = new DelegateCommand(GenereateNewKey, CanGenerateNewKey);
            RefreshCommand = new DelegateCommand(Refresh);

            try
            {
                _context = new AdminContext();
                _context.Set<UserLicense>().Load();
                Licenses = _context.Set<UserLicense>().Local;
                _context.Users.Load();
                Users = _context.Users.Local;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void Refresh(object obj)
        {
            _context = new AdminContext();
            _context.Set<UserLicense>().Load();
            Licenses = _context.Set<UserLicense>().Local;
            _context.Users.Load();
            Users = _context.Users.Local;
        }

        #endregion Public Constructors

        #region Public Properties

        public ICommand AddCommand { get; private set; }

        public ICommand AddPluginLicenseCommand { get; private set; }

        public AdminContext Context
        {
            get { return _context; }
            private set { _context = value; RaisePropertyChanged("Context"); }
        }

        public ICommand DeleteCommand { get; private set; }

        public ICommand DeletePluginLicenseCommand { get; private set; }

        public ICommand EditCommand { get; private set; }

        public ICommand RefreshCommand { get; private set; }

        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public ICommand GenerateKeyCommand { get; private set; }


        private ObservableCollection<UserLicense> _licenses;

        public ObservableCollection<UserLicense> Licenses
        {
            get { return _licenses; }
            set
            {
                if (_licenses != value)
                {
                    _licenses = value;
                    RaisePropertyChanged("Licenses");
                }
            }
        }

        public ObservableCollection<Plugin> Plugins
        {
            get
            {
                return _plugins;
            }

            set
            {
                _plugins = value;
                RaisePropertyChanged("Plugins");
            }
        }

        public UserLicense SelectedLicense
        {
            get
            {
                return _selectedLicense;
            }

            set
            {
                _selectedLicense = value;
                RaisePropertyChanged("SelectedLicense");
                RaisePropertyChanged("SelectedPluginLicense");
                ValidateModel();
            }
        }

        public User LicenseOwner
        {
            get { return SelectedLicense == null ? null : SelectedLicense.User; }
            set
            {
                if (SelectedLicense.User != value)
                {
                    SelectedLicense.User = value;
                    RaisePropertyChanged("LicenseOwner");
                    ValidateModel();
                }
            }
        }

        public Plugin SelectedPlugin
        {
            get
            {
                return _selectedPlugin;
            }

            set
            {
                if (_selectedPlugin != value)
                {
                    _selectedPlugin = value;
                    if (_selectedPlugin != null)
                        SelectedVersion = _selectedPlugin.MajorVersions.
                            OrderByDescending(item => item.Number).FirstOrDefault();
                    RaisePropertyChanged("SelectedPlugin");
                }
            }
        }

        public PluginLicense SelectedPluginLicense
        {
            get
            {
                return _selectedPluginLicense;
            }

            set
            {
                _selectedPluginLicense = value;
                RaisePropertyChanged("SelectedPluginLicense");
                ValidateModel();
            }
        }

        public LicenseTemplate SelectedTemplate
        {
            get
            {
                return _selectedTemplate;
            }

            set
            {
                _selectedTemplate = value;
                RaisePropertyChanged("SelectedTemplate");
                UpdateLicenseParameters();
            }
        }

        public MajorVersion SelectedVersion
        {
            get
            {
                return _selectedVersion;
            }

            set
            {
                if (_selectedVersion != value)
                {
                    _selectedVersion = value;
                    RaisePropertyChanged("SelectedVersion");
                }
            }
        }

        public DateTime? StartDate
        {
            get
            {
                if (!_startDate.HasValue)
                    _startDate = DateTime.Now;
                return _startDate;
            }

            set
            {
                _startDate = value;
                RaisePropertyChanged("StartDate");
                UpdateLicenseParameters();
            }
        }

        private ObservableCollection<User> _users;

        public ObservableCollection<User> Users
        {
            get { return _users; }
            set
            {
                if (_users != value)
                {
                    _users = value;
                    RaisePropertyChanged("Users");
                }
            }
        }

        #endregion Public Properties

        #region Private Methods

        private void AddLicense(object obj)
        {
            try
            {
                UserLicense license = new UserLicense();
                license.UniqueKey = Guid.NewGuid();
                Licenses.Add(license);
                SelectedLicense = license;

                bool? result = new WndEditLicense() { DataContext = this }.ShowDialog();
                if (result.HasValue && result.Value)
                    _context.SaveChanges();
                else
                    _context.RejectChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    StringBuilder builder = new StringBuilder();

                    builder.AppendFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    builder.AppendLine();
                    foreach (var ve in eve.ValidationErrors)
                    {
                        builder.AppendFormat("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        builder.AppendLine();
                    }

                    System.Windows.MessageBox.Show(builder.ToString());
                }
            }
        }

        private void AddPluginLicense(object obj)
        {
            PluginLicense pluginLicense = new PluginLicense() { UniqueKey = Guid.NewGuid() };
            var existedPlugins = SelectedLicense.PluginLicenses.Select(item => item.PluginName).ToList();
            var items = _context.Plugins.Where(item => !existedPlugins.Contains(item.Name) &&
                item.MajorVersions.Count > 0 &&
                item.MajorVersions.SelectMany(mv => mv.LicenseTemplates).Count() > 0).AsEnumerable<Plugin>();

            Plugins = new ObservableCollection<Plugin>(items);

            SelectedLicense.PluginLicenses.Add(pluginLicense);
            SelectedPluginLicense = pluginLicense;
        }

        private bool CanAddPluginLicense(object obj)
        {
            return true;
        }

        private bool CanDeleteLicense(object obj)
        {
            return SelectedLicense != null && !SelectedLicense.IsActivated;
        }

        private bool CanDeletePluginLicense(object obj)
        {
            return obj as PluginLicense != null && !SelectedLicense.IsActivated;
        }

        private bool CanEditLicense(object obj)
        {
            return SelectedLicense != null;
        }

        private bool CanGenerateNewKey(object obj)
        {
            return SelectedLicense != null && !SelectedLicense.IsActivated && SelectedLicense.PluginLicenses.Count > 0;
        }

        private void DeleteLicense(object obj)
        {
            _context.Licenses.Remove(SelectedLicense);
            _context.SaveChanges();
        }

        private void DeletePluginLicense(object obj)
        {
            SelectedLicense.PluginLicenses.Remove(SelectedPluginLicense);
        }

        private void EditLicense(object obj)
        {
            bool? result = new WndEditLicense() { DataContext = this }.ShowDialog();
            if (result.HasValue && result.Value)
                _context.SaveChanges();
            else
                _context.RejectChanges();
        }

        private void GenereateNewKey(object obj)
        {
            var guids = SelectedLicense.PluginLicenses.Select(item => item.UniqueKey).ToList();
            guids.Add(SelectedLicense.UniqueKey);
            HashAlgorithm hasher = MD5.Create();
            byte[] data = hasher.ComputeHash(Encoding.Default.GetBytes(String.Join(";", guids)));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            string hash = sBuilder.ToString();

            SelectedLicense.SerialNumber = hash.Substring(0, 12);
            RaisePropertyChanged("SelectedLicense");
            ValidateModel();
        }

        private void UpdateLicenseParameters()
        {
            if (SelectedTemplate == null)
                return;

            SelectedPluginLicense.PluginId = SelectedPlugin.Id;
            SelectedPluginLicense.PluginName = SelectedPlugin.Name;
            SelectedPluginLicense.Price = SelectedTemplate.Price;
            SelectedPluginLicense.StartDate = StartDate.Value;
            SelectedPluginLicense.Timeless = SelectedTemplate.Timeless;
            if (!SelectedTemplate.Timeless)
            {
                SelectedPluginLicense.EndDate = SelectedPluginLicense.StartDate.AddMonths(SelectedTemplate.Duration);
                EndDate = SelectedPluginLicense.EndDate;
            }

            SelectedLicense.Price = SelectedLicense.PluginLicenses.Sum(item => item.Price);
            SelectedLicense.StartDate = SelectedLicense.PluginLicenses.Min(item => item.StartDate);
            if (SelectedLicense.PluginLicenses.Any(item => item.Timeless))
                SelectedLicense.EndDate = null;
            else
                SelectedLicense.EndDate = SelectedLicense.PluginLicenses.Max(item => item.EndDate);
            RaisePropertyChanged("SelectedPluginLicense");
            ValidateModel();
        }

        private void ValidateModel()
        {
            HasErrors = SelectedLicense == null || LicenseOwner == null || SelectedLicense.PluginLicenses.Count == 0 || string.IsNullOrEmpty(SelectedLicense.SerialNumber);
        }

        #endregion Private Methods
    }
}