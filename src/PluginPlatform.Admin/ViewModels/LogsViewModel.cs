﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.Services;
using PluginPlatform.Core.ViewModels;
using ITWebNet.WPF.Controls;

namespace PluginPlatform.Admin.ViewModels
{
    public class LogsViewModel : ViewModelBase
    {
        #region Private Fields

        //private AdminContext _context;
        private LogsClient _client = new LogsClient();

        private Log _selectedLog;

        #endregion Private Fields

        #region Public Constructors

        public LogsViewModel()
        {
            ViewMinidumpCommand = new DelegateCommand(ViewMinidump, CanViewMinidump);
            ViewScreenshotCommand = new DelegateCommand(ViewScreenshot, CanViewScreenshot);

            Logs = new ObservableCollection<Log>(_client.GetAll());
        }

        #endregion Public Constructors

        #region Public Properties

        public ObservableCollection<Log> Logs { get; private set; }

        public Log SelectedLog
        {
            get { return _selectedLog; }
            set { _selectedLog = value; RaisePropertyChanged("SelectedLog"); }
        }

        public ICommand ViewMinidumpCommand { get; private set; }

        public ICommand ViewScreenshotCommand { get; private set; }

        #endregion Public Properties

        #region Private Methods

        private bool CanViewMinidump(object obj)
        {
            return SelectedLog.LogMinidump != null && SelectedLog.LogMinidump.Content != null && SelectedLog.LogMinidump.Content.Length != 0;
        }

        private bool CanViewScreenshot(object obj)
        {
            return SelectedLog.LogScreenshot != null && SelectedLog.LogScreenshot.Content != null && SelectedLog.LogScreenshot.Content.Length != 0;
        }

        private void ViewMinidump(object obj)
        {
            Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();
            dialog.ShowDialog();
        }

        private void ViewScreenshot(object obj)
        {
            string fileDestinationPath = Path.Combine(Path.GetTempPath(), SelectedLog.LogScreenshot.Name + ".bmp");
            using (FileStream outStream = new FileStream(fileDestinationPath, FileMode.Create, FileAccess.Write))
                outStream.Write(SelectedLog.LogScreenshot.Content, 0, SelectedLog.LogScreenshot.Content.Length);

            System.Diagnostics.Process.Start(fileDestinationPath);
        }

        #endregion Private Methods
    }
}