﻿using System.Linq;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PluginPlatform.Admin.Models;
using PluginPlatform.Core.ViewModels;
using ITWebNet.WPF.Controls;
using System.Windows.Threading;
using System.Collections.Generic;
using PluginPlatform.Admin.Services;
using System.DirectoryServices.ActiveDirectory;

namespace PluginPlatform.Admin.ViewModels
{
    public class AdUsersViewModel : ViewModelBase
    {
        #region Private Fields

        private Dispatcher curDisp;

        private ObservableCollection<User> _adUsers;

        private ObservableCollection<User> _selectedAdUsers;

        private ObservableCollection<User> _users;

        private ObservableCollection<User> _selectedUsers;

        UsersClient _client;
        #endregion Private Fields

        #region Public Constructors

        public AdUsersViewModel()
        {
            _client = new UsersClient().AddAuthentication(UmsAdminPlugin.Instance.CurrentSessionID);

            List<User> existedUsers = _client.GetAll().Where(u => u.IsADUser).ToList();
            Users = new ObservableCollection<User>(existedUsers);
            SelectedUsers = new ObservableCollection<User>();

            List<User> adUsers = _client.GetFromAD(Domain.GetCurrentDomain().Name);
            adUsers = adUsers.Where(u => !existedUsers.Select(eu => eu.UserName).Contains(u.UserName)).ToList();
            AdUsers = new ObservableCollection<User>(adUsers);
            SelectedAdUsers = new ObservableCollection<User>();

            MoveToUms = new DelegateCommand(MoveAdUsersToUmsUsers);
            MoveFromUms = new DelegateCommand(DeleteUsersFromUmsUsers);
            //Save = new DelegateCommand(SaveUsers);
        }
        #endregion Public Constructors

        #region Public Properties

        public ICommand MoveToUms { get; private set; }

        public ICommand MoveFromUms { get; private set; }

        public ICommand DisplaySelectionCountCommand { get; private set; }

        //public ICommand Save { get; private set; }

        public ObservableCollection<User> AdUsers
        {
            get { return _adUsers; }
            set
            {
                if (_adUsers != value)
                {
                    _adUsers = value;
                    RaisePropertyChanged("AdUsers");
                }
            }
        }

        public ObservableCollection<User> SelectedAdUsers { get; private set; }

        public ObservableCollection<User> Users
        {
            get { return _users; }
            set
            {
                if (_users != value)
                {
                    _users = value;
                    RaisePropertyChanged("Users");
                }
            }
        }

        public ObservableCollection<User> SelectedUsers { get; private set; }

        #endregion Public Properties

        #region private Methods

        private void MoveAdUsersToUmsUsers(object obj)
        {
            foreach (User item in SelectedAdUsers)
            {
                if (!Users.Any(
                        u => u.UserName == item.UserName
                             && u.Email == item.Email))
                {
                    Users.Add(item);
                    _client.Create(item);
                }
            }
        }

        private void DeleteUsersFromUmsUsers(object obj)
        {
            foreach (User item in SelectedUsers)
            {
                Users.Remove(item);
                _client.Delete(item.Id);
            }
        }

        //private void SaveUsers(object obj)
        //{
        //    foreach (User item in Users)
        //    {
        //        _context.Users.Add(item);
        //    }

        //    _context.SaveChanges();
        //}

        #endregion
    }
}
