﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using PluginPlatform.Admin.Helpers;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.Views;
using PluginPlatform.Core.ViewModels;
using ITWebNet.WPF.Controls;
using PluginPlatform.Admin.Services;

namespace PluginPlatform.Admin.ViewModels
{
    public class UsersViewModel : ViewModelBase
    {
        #region Private Fields

        UsersClient _client;

        private User _selectedUser;

        #endregion Private Fields

        #region Public Constructors

        public UsersViewModel()
        {
            _client = new UsersClient().AddAuthentication(UmsAdminPlugin.Instance.CurrentSessionID);

            AddCommand = new DelegateCommand(AddUser)
                .WithHandler(this.HandleException);

            AddAdCommand = new DelegateCommand(AddAdUser)
                .WithHandler(this.HandleException);

            EditCommand = new DelegateCommand(EditUser, CanEditUser)
                .WithHandler(this.HandleException);

            DeleteCommand = new DelegateCommand(DeleteUser, CanDeleteUser)
                .WithHandler(this.HandleException);

            RefreshCommand = new DelegateCommand(Refresh)
                .WithHandler(this.HandleException);

            try
            {
                Users = new ObservableCollection<User>(_client.GetAll());

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void Refresh(object obj)
        {
            Users = new ObservableCollection<User>(_client.GetAll());
        }

        #endregion Public Constructors

        #region Public Properties

        public ICommand AddCommand { get; private set; }

        public ICommand AddAdCommand { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        public ICommand EditCommand { get; private set; }

        public ICommand RefreshCommand { get; private set; }

        public User SelectedUser
        {
            get { return _selectedUser; }
            set { _selectedUser = value; RaisePropertyChanged("SelectedUser"); }
        }


        private ObservableCollection<User> _users;

        public ObservableCollection<User> Users
        {
            get { return _users; }
            set
            {
                if (_users != value)
                {
                    _users = value;
                    RaisePropertyChanged("Users");
                }
            }
        }

        #endregion Public Properties

        #region Private Methods

        private void AddUser(object obj)
        {
            User user = new User();
            Users.Add(user);
            SelectedUser = user;
            EditUser(null);
        }

        private void AddAdUser(object obj)
        {
            WndAddUserFromAd wnd = new WndAddUserFromAd();
            bool? result = wnd.ShowDialog();
            Refresh(null);
        }

        private bool CanDeleteUser(object obj)
        {
            if (SelectedUser == null)
                return false;
            return true;
        }

        private bool CanEditUser(object obj)
        {
            if (SelectedUser == null || SelectedUser.IsADUser)
                return false;
            return true;
        }

        private void DeleteUser(object obj)
        {
            var result = MessageBox.Show(
                "Вы уверены, что хотите удалить выбранного пользователя?",
                "Удаление пользователя",
                MessageBoxButton.YesNo,
                MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Users.Remove(SelectedUser);
                _client.Delete(SelectedUser.Id);
            }
        }

        private void EditUser(object obj)
        {
            WndEditUser wnd = new WndEditUser(new UserViewModel(SelectedUser));
            bool? result = wnd.ShowDialog();
            if (result.HasValue && result.Value)
            {
                //if (Users.Any(item => item.UserName.Equals(SelectedUser.UserName, StringComparison.InvariantCultureIgnoreCase) && item.Id != SelectedUser.Id))
                //{
                //    MessageBox.Show(
                //        "Такой пользователь уже существует.",
                //        "Пользователь",
                //        MessageBoxButton.OK,
                //        MessageBoxImage.Exclamation);
                //    EditUser(null);
                //}
                //else
                //{

                User newUser;
                if (SelectedUser.Id == 0)
                    newUser = _client.Create(SelectedUser);
                else
                    newUser = _client.Update(SelectedUser.Id, SelectedUser);

                Users.Replace(SelectedUser, newUser);
                SelectedUser = newUser;

                if (!string.IsNullOrWhiteSpace(SelectedUser.Password))
                    _client.SetPassword(SelectedUser.UserName, SelectedUser.Password, SelectedUser.Password);
                //}
            }

            HasErrors = false;
        }

        #endregion Private Methods
    }
}