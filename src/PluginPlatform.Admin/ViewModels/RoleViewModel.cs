﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using PluginPlatform.Admin.Helpers;
using PluginPlatform.Admin.Models;
using PluginPlatform.Admin.Services;
using PluginPlatform.Core.ViewModels;
using ITWebNet.WPF.Controls;

namespace PluginPlatform.Admin.ViewModels
{
    public class RoleViewModel : ViewModelBase
    {
        private Role _origin;
        private PluginsClient _client;


        public int Id
        {
            get { return _origin.Id; }
            set { _origin.Id = value; }
        }

        public string Name
        {
            get { return _origin.Name; }
            set { _origin.Name = value; }
        }

        public string Description
        {
            get { return _origin.Description; }
            set { _origin.Description = value; }
        }

        public ObservableCollection<Plugin> Plugins { get; set; }

        public ObservableCollection<Plugin> AvailablePlugins { get; set; }

        public ICommand AddCommand { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        public RoleViewModel(Role role)
        {
            _client = new PluginsClient().AddAuthentication(UmsAdminPlugin.Instance.CurrentSessionID);

            _origin = role;
            if (_origin.Plugins == null)
                _origin.Plugins = new List<Plugin>();
            AddCommand = new DelegateCommand(AddPlugins, CanMovePlugins).WithHandler(this.HandleException);
            DeleteCommand = new DelegateCommand(DeletePlugins, CanMovePlugins).WithHandler(this.HandleException);

            var allPlugins = _client.GetAll();

            IEnumerable<Plugin> availablePlguins;
            if (_origin.Plugins == null || _origin.Plugins.Count == 0)
                availablePlguins = allPlugins;
            else
            {
                var existed = _origin.Plugins.Select(item => item.Id);
                availablePlguins = allPlugins.Where(item => !(existed.Contains(item.Id))).ToList();
            }

            AvailablePlugins = new ObservableCollection<Plugin>(availablePlguins);
            Plugins = new ObservableCollection<Plugin>(_origin.Plugins);
        }

        private bool CanMovePlugins(object obj)
        {
            var selected = obj as IEnumerable;
            if (selected == null)
                return false;
            var plugins = selected.Cast<Plugin>();

            return plugins != null && plugins.Count() > 0;
        }

        private void DeletePlugins(object obj)
        {
            var selected = obj as IEnumerable;
            if (selected == null)
                return;
            var plugins = selected.Cast<Plugin>().ToList();

            foreach (var item in plugins)
            {
                Plugins.Remove(item);
                _origin.Plugins.Remove(item);
                AvailablePlugins.Add(item);
            }
        }

        private void AddPlugins(object obj)
        {
            var selected = obj as IEnumerable;
            if (selected == null)
                return;
            var plugins = selected.Cast<Plugin>().ToList();

            foreach (var item in plugins)
            {
                Plugins.Add(item);
                _origin.Plugins.Add(item);
                AvailablePlugins.Remove(item);
            }
        }
    }
}
