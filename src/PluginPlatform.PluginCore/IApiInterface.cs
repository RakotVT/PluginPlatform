﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ums.PluginCore
{
    /// <summary>
    /// Интерфейс от которого будут наследоваться все реализации api оболочки, а так же api плагинов, если будет необходимость их использования.
    /// </summary>
    public interface IApiInterface
    {
        /// <summary>
        /// Название API
        /// </summary>
        string ApiName { get; }

        /// <summary>
        /// Информация о версии API
        /// </summary>
        int ApiVersion { get; }

        /// <summary>
        /// Разработчик API
        /// </summary>
        string ApiDeveloper { get; }

        /// <summary>
        /// Описание API
        /// </summary>
        string ApiDescription { get; }

        /// <summary>
        /// 
        /// </summary>
        IPlugin Plugin { get; }
    }
}
