﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginPlatform.PluginCore.API
{
    public interface IUserInfo
    {
        string CurrentSessionID { get; set; }
    }
}
