﻿using System.Collections.Generic;

namespace PluginPlatform.PluginCore.API
{
    /// <summary>
    /// Интрерфейс для реализации WPF плагина.
    /// </summary>
    public interface IWPFPlugin : IPlugin<System.Windows.FrameworkElement>
    {
    }
}
