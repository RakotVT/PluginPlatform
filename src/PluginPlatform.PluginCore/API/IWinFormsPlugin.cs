﻿using System.Collections.Generic;

namespace PluginPlatform.PluginCore.API
{
    /// <summary>
    /// Интерфейс для реализации WinForms плагина.
    /// </summary>
    public interface IWinFormsPlugin : IPlugin<System.Windows.Forms.Control>
    {
    }
}
