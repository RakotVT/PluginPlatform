﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace PluginPlatform.PluginCore.ToolbarsEx
{
	/// <summary>
	/// Этот класс дополняет функциональные возможности, предоставляемые System.Windows.Forms.BindingNavigator.
	/// <seealso cref="http://blogs.msdn.com/b/rickbrew/archive/2006/01/09/511003.aspx"/>
	/// </summary>
	public class BindingNavigatorEx 
		: BindingNavigator
	{
		#region Constructors

		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="BindingNavigatorEx"/>
		/// </summary>
		public BindingNavigatorEx()
			: base() { }

		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="ums.PluginCore.ToolbarsEx.BindingNavigatorEx"/> 
		/// с заданным объектом <see cref="System.Windows.Forms.BindingSource"/> в качестве источника данных.
		/// </summary>
		/// <param name="bindingSource">Объект <see cref="System.Windows.Forms.BindingSource"/>, использованный в качестве источника данных.</param>
		public BindingNavigatorEx(BindingSource bindingSource)
			: base(bindingSource) { }

		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="ums.PluginCore.ToolbarsEx.BindingNavigatorEx"/>,
		/// указывая, следует ли отображать стандартный навигационный пользовательский интерфейс.
		/// </summary>
		/// <param name="addStandardItems">Значение <c>true</c>, чтобы отображать стандартный навигационный пользовательский интерфейс, в противном случае — значение <c>false</c>.</param>
		public BindingNavigatorEx(bool addStandardItems)
			: base(addStandardItems) { }

		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="ums.PluginCore.ToolbarsEx.BindingNavigatorEx"/>
		/// и добавляет этот экземпляр в указанный контейнер.
		/// </summary>
		/// <param name="container">Объект <see cref="System.ComponentModel.IContainer"/>, используемый для добавления нового элемента управления <see cref="ums.PluginCore.ToolbarsEx.BindingNavigatorEx"/>.</param>
		public BindingNavigatorEx(IContainer container)
			: base(container) { }

		#endregion

		private bool clickThrough;

		/// <summary>
		/// Возвращает или задает значение, указывающее, обрабатывает ли BindingNavigatorEx нажатия элементов, 
		/// когда содержащая его форма не владеет фокусом ввода
		/// </summary>
		/// <remarks>
		/// Значение по умолчанию <c>false</c>, что эквивалентно поведению базового класса <see cref="System.Windows.Forms.BindingNavigator"/>
		/// </remarks>
		/// 
		[Description("Возвращает или задает значение, указывающее, обрабатывает ли BindingNavigatorEx нажатия элементов, когда содержащая его форма не владеет фокусом ввода")]
		[DefaultValue(false)]
		public bool ClickThrough
		{
			get
			{
				return this.clickThrough;
			}

			set
			{
				this.clickThrough = value;
			}
		}

		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);

			if (this.clickThrough &&
				m.Msg == NativeConstants.WM_MOUSEACTIVATE &&
				m.Result == (IntPtr)NativeConstants.MA_ACTIVATEANDEAT)
			{
				m.Result = (IntPtr)NativeConstants.MA_ACTIVATE;
			}
		}
	}
}
