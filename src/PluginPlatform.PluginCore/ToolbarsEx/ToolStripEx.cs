﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace PluginPlatform.PluginCore.ToolbarsEx
{
	/// <summary>
	/// Этот класс дополняет функциональные возможности, предоставляемые System.Windows.Forms.ToolStrip.
	/// <seealso cref="http://blogs.msdn.com/b/rickbrew/archive/2006/01/09/511003.aspx"/>
	/// </summary>
	public class ToolStripEx
		: ToolStrip
	{
		private bool clickThrough;

		/// <summary>
		/// Возвращает или задает значение, указывающее, обрабатывает ли ToolStripEx нажатия элементов, 
		/// когда содержащая его форма не владеет фокусом ввода
		/// </summary>
		/// <remarks>
		/// Значение по умолчанию <c>false</c>, что эквивалентно поведению базового класса <see cref="System.Windows.Forms.ToolStrip"/>
		/// </remarks>
		/// 
		[Description("Возвращает или задает значение, указывающее, обрабатывает ли ToolStripEx нажатия элементов, когда содержащая его форма не владеет фокусом ввода")]
		[DefaultValue(false)]
		public bool ClickThrough
		{
			get
			{
				return this.clickThrough;
			}

			set
			{
				this.clickThrough = value;
			}
		}

		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);

			if (this.clickThrough &&
				m.Msg == NativeConstants.WM_MOUSEACTIVATE &&
				m.Result == (IntPtr)NativeConstants.MA_ACTIVATEANDEAT)
			{
				m.Result = (IntPtr)NativeConstants.MA_ACTIVATE;
			}
		}
	}
}
