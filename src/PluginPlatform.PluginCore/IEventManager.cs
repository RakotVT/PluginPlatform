﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ums.PluginCore
{
    public interface IEventManager
    {
        void SendEvent(EventArgs e);
    }
}
