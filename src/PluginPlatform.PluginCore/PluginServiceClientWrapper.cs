﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ums.PluginCore.PluginServiceReference;
using System.ServiceModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;


namespace ums.PluginCore
{
    public class PluginServiceClientWrapper
    {
        #region Variable

        PluginServiceClient _client;
        string _pluginName;

        #endregion

        public PluginServiceClientWrapper()
        {
            NetNamedPipeBinding binding = new NetNamedPipeBinding();
            EndpointAddress address = new EndpointAddress("net.pipe://localhost/qwerty/Plugins");
            _client = new PluginServiceClient(binding, address);
        }

        #region Methods

        public void GetPluginHandle(string pluginName, Form form)
        {
            using (StreamWriter sr = new StreamWriter(@"C:\\Docs\1.txt", true))
            {
                sr.WriteLine(Process.GetCurrentProcess().ProcessName);
            }
            if (Process.GetCurrentProcess().ProcessName.ToLower() != String.Format("{0}.dll", pluginName.ToLower())) return;
            form.ShowInTaskbar = false;
            form.WindowState = FormWindowState.Minimized;
            form.FormBorderStyle = FormBorderStyle.None;
            form.Shown += new EventHandler(form_Shown);
            _pluginName = pluginName;
        }

        void form_Shown(object sender, EventArgs e)
        {
            _client.GetPluginHandle(_pluginName, ((Form)sender).Handle, Process.GetCurrentProcess().Id);
        }

        public void KillProcessById(int processId)
        {
            _client.KillProcessById(processId);
        }

        #endregion
    }
}
