﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ums.PluginCore
{
    /// <summary>
    /// Интерфейс описывающий функции по доступу к API оболочки.
    /// </summary>
    public interface IApiController
    {
        /// <summary>
        /// Возвращающий api по имени и версии.
        /// </summary>
        /// <param name="name">имя интерфейса</param>
        /// <param name="version">версия интерфейса</param>
        /// <returns></returns>
        IApiInterface GetApi(string name, int version);

    }
}
