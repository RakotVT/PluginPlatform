﻿using System.Windows;
using ITWebNet.UMS.Core.LocalSettings;
using ITWebNet.UMS.Core.Extentions;
using ITWebNet.UMS.Shell.ViewModels;
using ITWebNet.UMS.ServicesApi.LicensingService;
using ITWebNet.UMS.Common.DataModels;

namespace ITWebNet.UMS.Shell.Views
{
    public partial class WndLicenseManager : Window
    {
        LicensesViewModel _model;

        public WndLicenseManager(string authorization)
        {
            InitializeComponent();
            _model = new LicensesViewModel(
                authorization,
                new LicensingClient(SimpleSettingsProvider.Instance, SimpleSettingsProvider.Instance.GetAddress(ServiceType.LicensingService))
                    .AddAuthentication(authorization));

            this.DataContext = _model;
        }

        private void btnActivate_Click(object sender, RoutedEventArgs e)
        {
            new WndActivatePlugin(_model).ShowDialog(this);
        }
    }
}
