﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using PluginPlatform.Core.LocalSettings;
using PluginPlatform.Common.DataModels.UserService;
using PluginPlatform.ServicesApi.UserService;
using PluginPlatform.Core.Exceptions;
using PluginPlatform.Common.DataModels;
using PluginPlatform.Common.DataModels.Exceptions;

namespace PluginPlatform.Shell.Views
{
    /// <summary>
    /// Логика взаимодействия для WndAuthorization.xaml
    /// </summary>
    public partial class WndAuthorization : Window
    {

        public Token sessionKey;
        ISettingsProvider settingsProvider;

        public WndAuthorization()
        {
            InitializeComponent();
            settingsProvider = SimpleSettingsProvider.Instance;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string login = tbxLogin.Text;
                string password = tbxPassword.Password;
                Mouse.OverrideCursor = Cursors.Wait;

                AuthorizeProvider autorizeProvider = new AuthorizeProvider(settingsProvider, settingsProvider.GetAddress(ServiceType.AuthorizationService));
                sessionKey = autorizeProvider.AuthorizeUser(login, password);
                if (sessionKey != null && !string.IsNullOrWhiteSpace(sessionKey.AccessToken))
                {
                    // Сохранение настроек пользователя
                    //
                    settingsProvider.SetRememberMe(chbRememberMe.IsChecked.Value);
                    if (chbRememberMe.IsChecked.Value)
                    {
                        settingsProvider.SetUserLogin(tbxLogin.Text);
                        settingsProvider.SetUserPassword(tbxPassword.Password);
                    }
                    DialogResult = true;
                    AuthFailedMessage.Visibility = Visibility.Collapsed;
                    Close();
                }
                else
                    AuthFailedMessage.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                //Mouse.OverrideCursor = null;
                //WndConnectionSettings frmSettings = new WndConnectionSettings();
                //frmSettings.ShowDialog();
                throw new ServiceException("Ошибка при авторизации", ex);
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Загрузка настроек пользователя
            //
            bool isRemember = settingsProvider.GetRememberMe();
            if (isRemember)
            {
                chbRememberMe.IsChecked = isRemember;
                tbxLogin.Text = settingsProvider.GetUserLogin();
                tbxPassword.Password = settingsProvider.GetUserPassword();
            }
        }

        private void tbxLogin_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (sender is TextBox)
                (sender as TextBox).SelectAll();
            else if (sender is PasswordBox)
                (sender as PasswordBox).SelectAll();
        }

        private void textField_GotMouseCapture(object sender, MouseEventArgs e)
        {
            if (sender is TextBox)
                (sender as TextBox).SelectAll();
            else if (sender is PasswordBox)
                (sender as PasswordBox).SelectAll();
        }

    }
}
