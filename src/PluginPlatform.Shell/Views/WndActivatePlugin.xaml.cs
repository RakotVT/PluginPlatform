﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITWebNet.UMS.Shell.ViewModels;

namespace ITWebNet.UMS.Shell.Views
{
    /// <summary>
    /// Interaction logic for WndActivatePlugin.xaml
    /// </summary>
    public partial class WndActivatePlugin : Window
    {
        LicensesViewModel _model;

        public WndActivatePlugin(LicensesViewModel model)
        {
            InitializeComponent();
            _model = model;
        }

        private void btnActivate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_model.ActivateNewKey(txtLicense.Text))
                    Close();

            }
            catch (InvalidOperationException ex)
            {
                ActivationFailed.Text = ex.Message;
                ActivationFailedBorder.Visibility = System.Windows.Visibility.Visible;
            }
        }
    }
}
