﻿using System.Windows;

namespace PluginPlatform.Shell.Views
{
    /// <summary>
    /// Interaction logic for UpdateMessage.xaml
    /// </summary>
    public partial class UpdateMessage : Window
    {
        public bool CheckUpdates
        {
            get
            {
                return checkUpdate.IsChecked.HasValue ? !checkUpdate.IsChecked.Value : true;
            }
        }

        #region Public Constructors

        public UpdateMessage(string updateText, UpdateMessageUI controls)
        {
            InitializeComponent();

            textUpdateAvaulable.Text = updateText;

            switch (controls)
            {
                case UpdateMessageUI.OK:
                    checkUpdate.Visibility = System.Windows.Visibility.Collapsed;
                    btnCancel.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case UpdateMessageUI.OKCancel:
                    checkUpdate.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case UpdateMessageUI.All:
                    break;
                default:
                    break;
            }
        }

        #endregion Public Constructors

        #region Private Methods

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        #endregion Private Methods
    }

    public enum UpdateMessageUI
    {
        OK,
        OKCancel,
        All
    }
}