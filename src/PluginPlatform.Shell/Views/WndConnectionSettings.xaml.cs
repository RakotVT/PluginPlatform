﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Input;
using PluginPlatform.Core.LocalSettings;
using PluginPlatform.ServicesApi.LogService;

namespace PluginPlatform.Shell.Views
{
    /// <summary>
    /// Логика взаимодействия для WndConnectionSettings.xaml
    /// </summary>
    public partial class WndConnectionSettings : Window
    {
        public WndConnectionSettings()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
