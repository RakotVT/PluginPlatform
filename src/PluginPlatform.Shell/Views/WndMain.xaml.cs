﻿using System;
using System.Windows;
using PluginPlatform.Core.Exceptions;
using PluginPlatform.ServicesApi.LogService;
using PluginPlatform.Shell.ViewModels;

namespace PluginPlatform.Shell.Views
{
    public partial class WndMain : Window
    {
        #region Public Constructors

        public MainViewModel ViewModel { get; set; }

        public WndMain()
        {
            InitializeComponent();
            ViewModel = new MainViewModel();
            DataContext = ViewModel;
        }

        #endregion Public Constructors

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.IsViewLoaded = true;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ViewModel.ClosePlugins();
        }
    }
}