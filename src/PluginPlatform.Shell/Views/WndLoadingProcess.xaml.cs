﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using PluginPlatform.Shell.ViewModels;

namespace PluginPlatform.Shell.Views
{
    /// <summary>
    /// Логика взаимодействия для WndLoadingProcess.xaml
    /// </summary>
    public partial class WndLoadingProcess : Window
    {
        public UpdatesViewModel UpdateData { get; set; }
        public WndLoadingProcess()
        {
            InitializeComponent();
            UpdateData = new UpdatesViewModel();
            DataContext = UpdateData;
        }

        private void btnHide_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }
    }
}
