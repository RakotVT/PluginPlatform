﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using PluginPlatform.Common.DataModels;
using PluginPlatform.Common.DataModels.Exceptions;
using PluginPlatform.Core.Exceptions;
using PluginPlatform.Core.Extentions;
using PluginPlatform.Core.LocalSettings;
using PluginPlatform.Core.Util;
using PluginPlatform.Core.ViewModels;
using PluginPlatform.ServicesApi.LogService;
using PluginPlatform.ServicesApi.UpdateService;
using PluginPlatform.Shell.Model;
using PluginPlatform.Shell.Services;
using PluginPlatform.Shell.Views;
using ITWebNet.WPF.Controls;

namespace PluginPlatform.Shell.ViewModels
{
    /// <summary>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region Private Fields

        private const bool IS_DEBUG = false;
        private bool _isUpdateBegin;
        private bool _isViewLoaded;
        private Updater _updater;
        private Plugin _selectedPlugin;
        private ISettingsProvider _settingsProvider = SimpleSettingsProvider.Instance;
        private Version SHELL_VERSION = Assembly.GetExecutingAssembly().GetName().Version;

        #endregion Private Fields

        #region Public Constructors

        public MainViewModel()
        {
            ExceptionHandler.AddLogger(Logger.Instance.Exception);
            ExceptionHandler.AddAction(ExceptionType.NotAuthorized, Authorize);
            ExceptionHandler.AddAction(ExceptionType.Service, ShowSettings);

            ExceptionHandler.UseHandler();

            ListPlugins = new ThreadSafeCollection<Plugin>();
            ListPluginDescriptions = new ThreadSafeCollection<PluginDescription>();

            AuthorizeCommand = new DelegateCommand(Authorize);
            CloseApplicationCommand = new DelegateCommand(CloseApplication);
            CloseCommand = new DelegateCommand(ClosePlugin);
            LoadInCurrentPageCommand = new DelegateCommand(LoadPlugin, CanLoadPlugin);
            LoadInNewPageCommand = new DelegateCommand(LoadInNewPage, CanLoadPlugin);
            OpenNewCommand = new DelegateCommand(OpenStartPage);
            //ShowLicenseManagerCommand = new DelegateCommand(ShowLicenseManager);
            ShowSettingsCommand = new DelegateCommand(ShowSettings);
            UpdatePluginsCommand = new DelegateCommand(UpdatePlugins);
            UpdateShellCommand = new DelegateCommand(UpdateShell);
            ShowAboutCommand = new DelegateCommand(ShowAbout);
            PluginDispatcher.Instance.PluginProcessStatusChanged += PluginDispatcher_PluginProcessStatusChanged;

            _updater = new Updater(_settingsProvider, _settingsProvider.GetAddress(ServiceType.PluginService))
                .AddPluginsDir(_settingsProvider.GetPluginDir())
                .AddTempDir(_settingsProvider.GetTempDir());
        }

        #endregion Public Constructors

        #region Public Properties

        public ICommand AuthorizeCommand { get; private set; }

        public ICommand CloseApplicationCommand { get; private set; }

        public ICommand CloseCommand { get; private set; }

        public bool IsViewLoaded
        {
            get
            {
                return _isViewLoaded;
            }

            set
            {
                _isViewLoaded = value;
                if (_isViewLoaded)
                    Initialize();
            }
        }

        public ThreadSafeCollection<PluginDescription> ListPluginDescriptions { get; private set; }

        public ThreadSafeCollection<Plugin> ListPlugins { get; set; }

        public ICommand LoadInCurrentPageCommand { get; private set; }

        public ICommand LoadInNewPageCommand { get; private set; }

        public ICommand OpenNewCommand { get; private set; }

        public int SelectedIndex { get; set; }

        public Plugin SelectedPlugin
        {
            get { return _selectedPlugin; }
            set { _selectedPlugin = value; RaisePropertyChanged("SelectedPlugin"); }
        }

        private string _sessionKey;

        public string SessionKey
        {
            get { return _sessionKey; }
            set { _sessionKey = value; }
        }


        //public ICommand ShowLicenseManagerCommand { get; private set; }

        public ICommand ShowSettingsCommand { get; private set; }

        public ICommand UpdatePluginsCommand { get; private set; }

        public ICommand UpdateShellCommand { get; private set; }

        public ICommand ShowAboutCommand { get; private set; }

        public string Version
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(App.CurrentVersion))
                    return App.CurrentVersion;
                else
                    return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public Size WindowStartSize
        {
            get { return new Size(SystemParameters.PrimaryScreenWidth * 0.75, SystemParameters.PrimaryScreenHeight * 0.75); }
        }

        #endregion Public Properties

        #region Public Methods

        public void ClosePlugins()
        {
            ListPluginDescriptions.Clear();

            foreach (var plugin in ListPlugins.ToList())
                ClosePlugin(plugin);

            ListPlugins.Clear();
        }

        public void LoadButtons(IEnumerable<int> availablePlugins)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            try
            {
                ListPluginDescriptions.Clear();

                string pluginsDir = _settingsProvider.GetPluginDir();
                var plugins = _settingsProvider.GetExistedPlugins()
                    .Where(item => availablePlugins.Contains(item.PluginId)).ToList();

                foreach (var plugin in plugins)
                {
                    string assemblyPath = Path.Combine(pluginsDir, plugin.Name, plugin.MainFile);
                    if (!File.Exists(assemblyPath))
                        continue;

                    PluginDescription info = new PluginDescription();
                    info.Caption = string.IsNullOrEmpty(plugin.Caption) ? plugin.Name : plugin.Caption;
                    info.Description = plugin.Description;
                    info.Name = plugin.Name;
                    info.AssemblyPath = assemblyPath;
                    info.Version = plugin.Version;
                    info.Autorun = plugin.Autorun;
                    ListPluginDescriptions.Add(info);
                    if (info.Autorun)
                        StartPlugin(info, false);
                }

            }
            finally
            {
                Mouse.OverrideCursor = null;

            }
            //if (ListPlugins.Count == 0)
            //    OpenStartPage(null);
            //else
            //ClosePlugin("StartPage");
        }


        #endregion Public Methods

        #region Private Methods

        private void ShowAbout(object obj)
        {
            new WndAbout().ShowDialog(App.Current.MainWindow);
        }

        private void Authorize(object obj)
        {
            Authorize();
        }

        private void Authorize()
        {
            try
            {
                ClosePlugins();
                SessionKey = null;
                Logger.Instance.SessionKey = SessionKey;
                ListPluginDescriptions.Clear();

                if (!IS_DEBUG && string.IsNullOrWhiteSpace(SessionKey))
                {
                    WndAuthorization autorizeUser = new WndAuthorization();
                    bool? result = autorizeUser.ShowDialog(App.Current.MainWindow);
                    if (result.HasValue && (bool)result)
                    {
                        // Получение ключа сессии 
                        SessionKey = autorizeUser.sessionKey.AccessToken;
                        Logger.Instance.SessionKey = SessionKey;
                        //TODO добавить прокси
                        _updater.AddAuthorization(SessionKey);
                    }
                    else
                    {
                        return;
                    }
                }

                if (!IS_DEBUG)
                {
                    if (_settingsProvider.GetUpdateChecking(CheckUpdateType.Plugins))
                    {
                        _updater.DeleteSubscribers();
                        _updater.CheckUpdatesCompleted += (sender, e) =>
                        {
                            if (e.UpdatesAvailable)
                            {
                                // Обновление плагинов 
                                UpdateMessage updateMessage = new UpdateMessage("Доступны обновления плагинов. Выполнить загрузку обновлений?", UpdateMessageUI.All);
                                updateMessage.ShowDialog(App.Current.MainWindow);

                                // Сохранение флага, отвечающего за проверку обновлений 
                                _settingsProvider.SetUpdateChecking(CheckUpdateType.Plugins, updateMessage.CheckUpdates);
                                if (updateMessage.DialogResult.HasValue && updateMessage.DialogResult.Value)
                                {
                                    UpdatePlugins();
                                }
                                else
                                {
                                    LoadButtons(_updater.GetAvailablePlugins());
                                }

                            }

                            if (!_isUpdateBegin)
                                LoadButtons(_updater.GetAvailablePlugins());
                        };

                        _updater.IsAnyUpdateAvailableAsync();
                    }
                    else if (!string.IsNullOrWhiteSpace(SessionKey))
                        LoadButtons(_updater.GetAvailablePlugins());
                }
                else
                {
                    var avalialablePlugins = _settingsProvider.GetExistedPlugins()
                        .Select(item => item.PluginId);
                    LoadButtons(avalialablePlugins);

                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, ExceptionType.Other);
            }
        }

        private bool CanAuthorize(object obj)
        {
            return string.IsNullOrWhiteSpace(SessionKey) && !IS_DEBUG;
        }

        private bool CanLoadPlugin(object obj)
        {
            PluginDescription pluginDesc = obj as PluginDescription;
            return pluginDesc != null && !String.IsNullOrWhiteSpace(pluginDesc.AssemblyPath);
        }

        private bool CanStartPLugin(PluginDescription pluginDesc)
        {
            //TODO доработать выдов сервиса лицензирования.
            return true;
        }

        private void CloseApplication(object obj)
        {
            ClosePlugins();
            App.Current.Shutdown();
        }

        private void ClosePlugin(string pluginName)
        {
            List<Plugin> tmpList = ListPlugins.Where(plugin => plugin.Name == pluginName).ToList();
            foreach (var plugin in tmpList)
                ClosePlugin(plugin);
        }

        private void ClosePlugin(object obj)
        {
            Plugin plugin = obj as Plugin;
            if (plugin == null)
                return;

            PluginDispatcher.Instance.ExitPluginProcess(plugin.UID);
            ListPlugins.Remove(plugin);
        }

        private void Initialize()
        {
            try
            {
                _settingsProvider = SimpleSettingsProvider.Instance;

                Logger.Instance.Initialize(_settingsProvider);

                _updater = new Updater(_settingsProvider, _settingsProvider.GetAddress(ServiceType.PluginService))
                    .AddPluginsDir(_settingsProvider.GetPluginDir())
                    .AddTempDir(_settingsProvider.GetTempDir());

                if (string.IsNullOrWhiteSpace(App.Launcher) && (_settingsProvider.GetUpdateChecking(CheckUpdateType.Shell)))
                {
                    _updater.DeleteSubscribers();
                    _updater.CheckUpdatesCompleted += (s, args) =>
                    {
                        if (args.UpdatesAvailable)
                        {
                            string message = "Доступно обновление UMS. Желаеле ли Вы обновить приложение прямо сейчас? Потребуется перезапуск.";
                            UpdateMessage updateMessage = new UpdateMessage(message, UpdateMessageUI.All);
                            bool? result = updateMessage.ShowDialog(App.Current.MainWindow);
                            _settingsProvider.SetUpdateChecking(CheckUpdateType.Shell, updateMessage.CheckUpdates);

                            if (result.HasValue && result.Value)
                                UpdateShellFromStorage();
                        }
                    };

                    _updater.IsShellUpdatesAvailableAsync(SHELL_VERSION);
                }

                if (!_isUpdateBegin)
                    Authorize();
            }
            catch (Exception ex)
            {
                throw new ServiceException("Ошибка при получении обновлений", ex);
            }
        }

        private void LoadInNewPage(object obj)
        {
            try
            {
                PluginDescription pluginDesc = obj as PluginDescription;
                if (CanStartPLugin(pluginDesc))
                    StartPlugin(pluginDesc, true);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, ExceptionType.Other);
            }
        }

        private void LoadPlugin(object obj)
        {
            try
            {
                PluginDescription pluginDesc = obj as PluginDescription;
                if (CanStartPLugin(pluginDesc))
                    StartPlugin(pluginDesc, false);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, ExceptionType.Other);
            }
        }

        private void OpenStartPage(object obj)
        {
            if (App.Current == null || App.Current.Dispatcher.HasShutdownStarted)
                return;
            Style startPageStyle = App.Current.FindResource("StartPageStyle") as Style;
            System.Windows.Controls.UserControl content = new System.Windows.Controls.UserControl();
            content.Style = startPageStyle;
            Plugin startPage = new Plugin(content);
            startPage.Header = "Доступные плагины";
            startPage.Name = "StartPage";
            ListPlugins.Add(startPage);
            SelectedPlugin = startPage;
        }

        private void PluginDispatcher_PluginProcessStatusChanged(object sender, PluginDispatcher.PluginRespondingEventArgs e)
        {
            Plugin plugin = ListPlugins.FirstOrDefault(item => item.UID == e.PluginID);

            switch (e.IsResponding)
            {
                case PluginProcessStatuses.Stable:
                    break;
                case PluginProcessStatuses.NotResponding:
                    if (plugin == null)
                        break;
                    MessageBoxResult result = MessageBox.Show(
                      string.Format("Плагин {0} не отвечает, Закрыть плагин?", plugin.Header),
                      plugin.Header,
                      MessageBoxButton.YesNo,
                      MessageBoxImage.Warning
                      );

                    if (result == MessageBoxResult.No)
                        break;
                    ClosePlugin(plugin);
                    break;
                case PluginProcessStatuses.Exited:
                    if (plugin == null)
                        break;
                    ClosePlugin(plugin);
                    break;
            }
        }

        //private void ShowLicenseManager(object obj)
        //{
        //    new WndLicenseManager(SessionKey).ShowDialog(App.Current.MainWindow);
        //}

        private void ShowSettings(object obj)
        {
            ShowSettings();
        }

        private void ShowSettings()
        {
            new WndSettings().ShowDialog(App.Current.MainWindow);
        }

        private void StartPlugin(PluginDescription pluginDesc, bool inNewTab)
        {
            PluginHostProxy pluginHost = new PluginHostProxy();
            Plugin plugin = new Plugin(pluginHost.GetPluginView(pluginDesc.AssemblyPath, SessionKey));
            plugin.Name = pluginDesc.Name;
            plugin.Icon = pluginDesc.Icon;
            plugin.Header = pluginDesc.Caption;
            if (inNewTab)
                ListPlugins.Add(plugin);
            else
            {
                int index = SelectedIndex;
                if (index >= 0)
                    ListPlugins[index] = plugin;
                else if (ListPlugins.Count == 0)
                    ListPlugins.Add(plugin);
            }

            SelectedPlugin = plugin;
            if (PluginHostProxy.IsProcessUsingAvailable())
                PluginDispatcher.Instance.RegisterPlugin(pluginDesc, SelectedPlugin, pluginHost.ProcessID);
        }

        private void UpdatePlugins(object obj)
        {
            _updater.DeleteSubscribers();

            if (SessionKey.Equals(Guid.Empty))
            {
                MessageBox.Show(
                    "Перед проверкой обновлений необходимо авторизоваться в системе.",
                    App.Current.MainWindow.Title,
                    MessageBoxButton.OK,
                    MessageBoxImage.Information
                    );
                return;
            }

            try
            {
                if (_updater.IsAnyUpdateAvailable())
                {
                    UpdateMessage updateMessage = new UpdateMessage("Доступны обновления плагинов. Выполнить загрузку обновлений?", UpdateMessageUI.OKCancel);
                    updateMessage.ShowDialog(App.Current.MainWindow);

                    // Сохранение флага, отвечающего за проверку обновлений 
                    _settingsProvider.SetUpdateChecking(CheckUpdateType.Plugins, updateMessage.CheckUpdates);

                    if (updateMessage.DialogResult.HasValue && updateMessage.DialogResult.Value)
                    {
                        UpdatePlugins();
                    }
                }
                else
                {
                    UpdateMessage updateMessage = new UpdateMessage("Нет доступных обновлений", UpdateMessageUI.OK);
                    updateMessage.ShowDialog(App.Current.MainWindow);
                }
            }
            catch(Exception ex)
            {
                ShowSettings(null);
            }
        }

        private void UpdatePlugins()
        {
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;

                _isUpdateBegin = true;
                WndLoadingProcess wndLoading = new WndLoadingProcess();

                //TODO добавить счетчик обновлений
                wndLoading.UpdateData.OverallMaximum = 2;

                // Инициализация фонового потока 

                // Отслеживание прогрeсса выполнения 
                _updater.UpdateProgressChanged += (sender, e) =>
                {
                    if (e.Progress == 0.0)
                    {
                        wndLoading.UpdateData.OverallCurrent++;
                        wndLoading.UpdateData.OverallStatus = e.Status;
                        wndLoading.UpdateData.ComponentCurrent = 0.0;
                        wndLoading.UpdateData.ComponentStatus = e.Status;
                    }
                    else
                    {
                        wndLoading.UpdateData.ComponentCurrent = e.Progress;
                        wndLoading.UpdateData.ComponentStatus = e.Status;
                    }
                };

                _updater.UpdateCompleted += (sender, e) =>
                {
                    if (e.Error != null)
                        ExceptionHandler.HandleException(e.Error, ExceptionType.Service);
                    wndLoading.Close();
                    _isUpdateBegin = false;
                    LoadButtons(_updater.GetAvailablePlugins());
                };

                ClosePlugins();
                wndLoading.Show(App.Current.MainWindow);
                _updater.UpdatePluginsAsync();
            }
            catch (Exception e)
            {
                _isUpdateBegin = false;
                throw e;
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        private void UpdateShell(object obj)
        {
            if (!string.IsNullOrWhiteSpace(App.Launcher))
                UpdateShell();
            else if (_updater.IsShellUpdatesAvailable(SHELL_VERSION))
            {
                string message = "Доступно обновление UMS. Желаеле ли Вы обновить приложение прямо сейчас? Потребуется перезапуск.";
                UpdateMessage updateMessage = new UpdateMessage(message, UpdateMessageUI.OKCancel);
                bool? result = updateMessage.ShowDialog(App.Current.MainWindow);
                _settingsProvider.SetUpdateChecking(CheckUpdateType.Shell, updateMessage.CheckUpdates);

                if (result.HasValue && result.Value)
                    UpdateShellFromStorage();
            }
            else
            {
                UpdateMessage updateMessage = new UpdateMessage("Нет доступных обновлений", UpdateMessageUI.OK);
                updateMessage.ShowDialog(App.Current.MainWindow);
            }
        }

        private void UpdateShell()
        {
            if (!File.Exists(App.Launcher))
            {
                MessageBox.Show("Не найден ярлык запуска приложения. Переустановите приложение.",
                    App.Current.MainWindow.Title,
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            object[] args = new object[]
            {
                "updateonly="+true,
                "Pid=" + Process.GetCurrentProcess().Id
            };

            Process.Start(App.Launcher, args.ToPlainString(" "));
        }

        private void UpdateShellFromStorage()
        {
            try
            {
                _updater.DeleteSubscribers();

                Mouse.OverrideCursor = Cursors.Wait;
                _isUpdateBegin = true;

                WndLoadingProcess fmLoading = new WndLoadingProcess();
                fmLoading.UpdateData.OverallMaximum = 2;

                // Отслеживание прогрeсса выполнения 
                _updater.UpdateProgressChanged += (sender, e) =>
                {
                    if (e.Progress == 0.0)
                    {
                        fmLoading.UpdateData.OverallCurrent++;
                        fmLoading.UpdateData.OverallStatus = e.Status;
                        fmLoading.UpdateData.ComponentCurrent = 0.0;
                        fmLoading.UpdateData.ComponentStatus = String.Empty;
                    }
                    else
                    {
                        fmLoading.UpdateData.ComponentCurrent = e.Progress;
                        fmLoading.UpdateData.ComponentStatus = e.Status;
                    }
                };

                // Завершение фонового потока 
                _updater.UpdateCompleted += (sender, e) =>
                {
                    _isUpdateBegin = false;

                    if (e.Error != null)
                    {
                        ExceptionHandler.HandleException(e.Error, ExceptionType.Other);
                        fmLoading.Close();
                        LoadButtons(_updater.GetAvailablePlugins());
                    }
                    else
                    {
                        Process.Start(Application.ResourceAssembly.Location);
                        Application.Current.Shutdown();
                    }
                };

                // Запуск фонового потока скачивания обновлений 
                fmLoading.Show(App.Current.MainWindow);
                _updater.UpdateShellAsync(SHELL_VERSION);
            }
            catch (Exception e)
            {
                _isUpdateBegin = false;
                throw e;
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        #endregion Private Methods
    }
}