﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Data;
using PluginPlatform.Core.ViewModels;

namespace PluginPlatform.Shell.ViewModels
{
    public class UpdatesViewModel : ViewModelBase
    {
        private double _overallMaximum;

        public double OverallMaximum
        {
            get { return _overallMaximum; }
            set { _overallMaximum = value; RaisePropertyChanged("OverallMaximum"); }
        }

        private double _overallCurrent;

        public double OverallCurrent
        {
            get { return _overallCurrent; }
            set { _overallCurrent = value; RaisePropertyChanged("OverallCurrent"); }
        }

        private string _overallStatus;

        public string OverallStatus
        {
            get { return _overallStatus; }
            set { _overallStatus = value; RaisePropertyChanged("OverallStatus"); }
        }

        private double _componentMaximum;

        public double ComponentMaximum
        {
            get { return _componentMaximum; }
            set { _componentMaximum = value; RaisePropertyChanged("ComponentMaximum"); }
        }

        private double _componentCurrent;

        public double ComponentCurrent
        {
            get { return _componentCurrent; }
            set { _componentCurrent = value; RaisePropertyChanged("ComponentCurrent"); }
        }

        private string _componentStatus;

        public string ComponentStatus
        {
            get { return _componentStatus; }
            set
            {
                _componentStatus = value;
                ComponentLog.Insert(0, string.Format("{0}: {1}", DateTime.Now, value));
            }
        }

        private ObservableCollection<string> _componentLog = new ObservableCollection<string>();

        public ObservableCollection<string> ComponentLog
        {
            get { return _componentLog; }
            set
            {
                if (_componentLog != value)
                {
                    _componentLog = value;
                    RaisePropertyChanged("ComponentLog");
                }
            }
        }
    }
}
