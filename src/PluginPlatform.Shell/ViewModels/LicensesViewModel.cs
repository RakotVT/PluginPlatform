﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITWebNet.UMS.Core.Util;
using ITWebNet.UMS.Core.ViewModels;
using ITWebNet.UMS.ServicesApi.LicensingService;

namespace ITWebNet.UMS.Shell.ViewModels
{
    public class LicensesViewModel : ViewModelBase
    {
        private ThreadSafeCollection<LicenseInfoWrapped> _licenses;

        public ThreadSafeCollection<LicenseInfoWrapped> Licenses
        {
            get { return _licenses; }
            set { _licenses = value; RaisePropertyChanged("Licenses"); }
        }

        private string _sessionKey;

        public string SessionKey
        {
            get { return _sessionKey; }
            private set { _sessionKey = value; }
        }

        private LicensingClient _licenser;

        public LicensesViewModel(string sessionKey, LicensingClient licenser)
        {
            _sessionKey = sessionKey;
            _licenser = licenser;
            Licenses = new ThreadSafeCollection<LicenseInfoWrapped>(_licenser.GetAllUserLicenses(sessionKey));
        }

        public bool ActivateNewKey(string activationKey)
        {
            bool result = _licenser.ActivateLicense(activationKey) != null;
            if (result)
                Licenses = new ThreadSafeCollection<LicenseInfoWrapped>(_licenser.GetAllUserLicenses(_sessionKey));

            return result;
        }
    }
}
