﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using PluginPlatform.Core.Extentions;
using PluginPlatform.Core.LocalSettings;
using PluginPlatform.Core.LocalSettings.UserStorage;
using PluginPlatform.Core.ViewModels;
using ITWebNet.WPF.Controls;
using PluginPlatform.Common.DataModels;

namespace PluginPlatform.Shell.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {

        private bool _isEdited;

        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                if (_isEdited != value)
                {
                    _isEdited = value;
                    RaisePropertyChanged("IsEdited");
                }
            }
        }

        private string _authServicePort;

        public string AuthServicePort
        {
            get { return _authServicePort; }
            set
            {
                if (_authServicePort != value)
                {
                    _authServicePort = value;
                    RaisePropertyChanged("AuthServicePort");
                    IsEdited = true;
                }
            }
        }

        private string _licensingServicePort;

        public string LicensingServicePort
        {
            get { return _licensingServicePort; }
            set
            {
                if (_licensingServicePort != value)
                {
                    _licensingServicePort = value;
                    RaisePropertyChanged("LicensingServicePort");
                    IsEdited = true;
                }
            }
        }

        private string _logServicePort;

        public string LogServicePort
        {
            get { return _logServicePort; }
            set
            {
                if (_logServicePort != value)
                {
                    _logServicePort = value;
                    RaisePropertyChanged("LogServicePort");
                    IsEdited = true;
                }
            }
        }

        private string _updateSericePort;

        public string UpdateSericePort
        {
            get { return _updateSericePort; }
            set
            {
                if (_updateSericePort != value)
                {
                    _updateSericePort = value;
                    RaisePropertyChanged("UpdateSericePort");
                    IsEdited = true;
                }
            }
        }

        private string _serverIpAddress;

        public string ServerIpAddress
        {
            get { return _serverIpAddress; }
            set
            {
                if (_serverIpAddress != value)
                {
                    _serverIpAddress = value;
                    RaisePropertyChanged("ServerIpAddress");
                    IsEdited = true;
                }
            }
        }

        private string _logsStoragePath;

        public string LogsStoragePath
        {
            get { return _logsStoragePath; }
            set
            {
                if (_logsStoragePath != value)
                {
                    _logsStoragePath = value;
                    RaisePropertyChanged("LogsStoragePath");
                    IsEdited = true;
                }
            }
        }

        private string _pluginsDirectory;

        public string PluginsDirectory
        {
            get { return _pluginsDirectory; }
            set
            {
                if (_pluginsDirectory != value)
                {
                    _pluginsDirectory = value;
                    RaisePropertyChanged("PluginsDirectory");
                    IsEdited = true;
                }
            }
        }

        private string _tempDirectory;

        public string TempDirectory
        {
            get { return _tempDirectory; }
            set
            {
                if (_tempDirectory != value)
                {
                    _tempDirectory = value;
                    RaisePropertyChanged("TempDirectory");
                    IsEdited = true;
                }
            }
        }

        private string _proxyIpAddress;

        public string ProxyIpAddress
        {
            get { return _proxyIpAddress; }
            set
            {
                if (_proxyIpAddress != value)
                {
                    _proxyIpAddress = value;
                    RaisePropertyChanged("ProxyIpAddress");
                    IsEdited = true;
                }
            }
        }

        private string _proxyLogin;

        public string ProxyLogin
        {
            get { return _proxyLogin; }
            set
            {
                if (_proxyLogin != value)
                {
                    _proxyLogin = value;
                    RaisePropertyChanged("ProxyLogin");
                    IsEdited = true;
                }
            }
        }

        private string _proxyPassword;

        public string ProxyPassword
        {
            get { return _proxyPassword; }
            set
            {
                if (_proxyPassword != value)
                {
                    _proxyPassword = value;
                    RaisePropertyChanged("ProxyPassword");
                    IsEdited = true;
                }
            }
        }

        private string _proxyPort;

        public string ProxyPort
        {
            get { return _proxyPort; }
            set
            {
                if (_proxyPort != value)
                {
                    _proxyPort = value;
                    RaisePropertyChanged("ProxyPort");
                    IsEdited = true;
                }
            }
        }

        private bool _isUseIeProxy;

        public bool IsUseIEProxy
        {
            get { return _isUseIeProxy; }
            set
            {
                if (_isUseIeProxy != value)
                {
                    _isUseIeProxy = value;
                    RaisePropertyChanged("IsUseIEProxy");
                    IsEdited = true;
                }
            }
        }

        private bool _isUseProxy;

        public bool IsUseProxy
        {
            get { return _isUseProxy; }
            set
            {
                if (_isUseProxy != value)
                {
                    _isUseProxy = value;
                    RaisePropertyChanged("IsUseProxy");
                    IsEdited = true;
                }
            }
        }

        private bool _isShowExtended;

        public bool IsShowExtended
        {
            get { return _isShowExtended; }
            set
            {
                if (_isShowExtended != value)
                {
                    _isShowExtended = value;
                    RaisePropertyChanged("IsShowExtended");
                }
            }
        }

        private List<Plugin> _existedPlugins;

        public List<Plugin> ExistedPlugins
        {
            get { return _existedPlugins; }
            set
            {
                if (_existedPlugins != value)
                {
                    _existedPlugins = value;
                    RaisePropertyChanged("ExistedPlugins");
                }
            }
        }

        private CheckUpdateType _currentUpdateType;

        public CheckUpdateType CurrentUpdateType
        {
            get { return _currentUpdateType; }
            set
            {
                if (_currentUpdateType != value)
                {
                    _currentUpdateType = value;
                    RaisePropertyChanged("CurrentUpdateType");
                    IsEdited = true;
                }
            }
        }

        public Dictionary<CheckUpdateType, string> UpdateTypes
        {
            get
            {
                return Enum.GetValues(typeof(CheckUpdateType)).
                    Cast<CheckUpdateType>().
                    ToDictionary(item => item, item => item.GetDescription());
            }
        }

        private bool _rememberMe;

        public bool RememberMe
        {
            get { return _rememberMe; }
            set
            {
                if (_rememberMe != value)
                {
                    _rememberMe = value;
                    RaisePropertyChanged("RememberMe");
                    IsEdited = true;
                }
            }
        }

        private bool _autorun;

        public bool Autorun
        {
            get { return _autorun; }
            set
            {
                if (_autorun != value)
                {
                    _autorun = value;
                    RaisePropertyChanged("Autorun");
                    IsEdited = true;
                }
            }
        }

        public ICommand DeletePluginCommand { get; private set; }
        public ICommand SaveSettingsCommand { get; private set; }
        public ICommand SelectDirectoryCommand { get; private set; }
        public ICommand TestConnectionCommand { get; private set; }

        public SettingsViewModel()
        {
            DeletePluginCommand = new DelegateCommand(DeletePlugin);
            SaveSettingsCommand = new DelegateCommand(SaveSettings);
            TestConnectionCommand = new DelegateCommand(TestConnection);
            SelectDirectoryCommand = new DelegateCommand(SelectDirectory);
            LoadSettings();
        }

        private void DeletePlugin(object obj)
        {
            Plugin plugin = obj as Plugin;
            if (plugin == null)
                return;
            ISettingsProvider context = SimpleSettingsProvider.Instance;
            context.DeleteExistingPlugin(plugin.PluginId);
            string pluginDirectory = Path.Combine(PluginsDirectory, plugin.Name);
            if (Directory.Exists(pluginDirectory))
                Directory.Delete(pluginDirectory, true);
            ExistedPlugins = context.GetExistedPlugins().OrderBy(item => item.Name).ToList();
        }

        private void SelectDirectory(object obj)
        {
            string parameter = obj as string;
            if (string.IsNullOrWhiteSpace(parameter))
                return;

            OpenFolderDialog dialog = new OpenFolderDialog();
            bool? result;
            switch (parameter)
            {
                case "plugins":
                    dialog.InitialFolder = PluginsDirectory;
                    result = dialog.ShowDialog();
                    if (result.HasValue && result.Value)
                        PluginsDirectory = dialog.Folder;
                    break;
                case "logs":
                    dialog.InitialFolder = LogsStoragePath;
                    result = dialog.ShowDialog();
                    if (result.HasValue && result.Value)
                        PluginsDirectory = dialog.Folder;
                    break;
                case "temp":
                    dialog.InitialFolder = TempDirectory;
                    result = dialog.ShowDialog();
                    if (result.HasValue && result.Value)
                        PluginsDirectory = dialog.Folder;
                    break;
                default:
                    break;
            }
        }

        private void LoadSettings()
        {
            ISettingsProvider context = SimpleSettingsProvider.Instance;

            AuthServicePort = context.GetServicePort(ServiceType.AuthorizationService);
            ExistedPlugins = context.GetExistedPlugins().OrderBy(item => item.Name).ToList();
            CurrentUpdateType = context.GetCheckUpdateType();
            IsUseIEProxy = context.GetIsUseIEProxy();
            IsUseProxy = context.GetIsUseProxy();
            LicensingServicePort = context.GetServicePort(ServiceType.LicensingService);
            LogServicePort = context.GetServicePort(ServiceType.LogService);
            LogsStoragePath = context.GetLogsStoragePath();
            PluginsDirectory = context.GetPluginDir();
            ProxyIpAddress = context.GetProxyIpAddress();
            ProxyLogin = context.GetProxyLogin();
            ProxyPassword = context.GetProxyPassword();
            ProxyPort = context.GetProxyPassword();
            RememberMe = context.GetRememberMe();
            ServerIpAddress = context.GetServerIpAddress();
            UpdateSericePort = context.GetServicePort(ServiceType.PluginService);
            TempDirectory = context.GetTempDir();
            IsShowExtended = context.GetIsShowExtended();

            IsEdited = false;
            HasErrors = false;
        }

        private void SaveSettings(object obj)
        {
            ISettingsProvider context = SimpleSettingsProvider.Instance;

            context.SetCheckUpdateType(CurrentUpdateType);
            context.SetIsUseIEProxy(IsUseIEProxy);
            context.SetIsUseProxy(IsUseProxy);
            context.SetLogsStoragePath(LogsStoragePath);
            context.SetPluginDir(PluginsDirectory);
            context.SetProxyIpAddress(ProxyIpAddress);
            context.SetProxylogin(ProxyLogin);
            context.SetProxyPassword(ProxyPassword);
            context.SetProxyPort(ProxyPort);
            context.SetRememberMe(RememberMe);
            context.SetServerIpAddress(ServerIpAddress);
            context.SetServicePort(ServiceType.AuthorizationService, AuthServicePort);
            context.SetServicePort(ServiceType.LicensingService, LicensingServicePort);
            context.SetServicePort(ServiceType.LogService, LogServicePort);
            context.SetServicePort(ServiceType.PluginService, UpdateSericePort);
            context.SetTempDir(TempDirectory);
            context.SetIsShowExtended(IsShowExtended);

            IsEdited = false;
            HasErrors = false;
        }

        private void TestConnection(object obj)
        {
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format(
                    "http://{0}:{1}/UserService.svc",
                    ServerIpAddress,
                    AuthServicePort
                    ));
                if (IsUseIEProxy)
                {
                    request.Proxy = WebRequest.GetSystemWebProxy();
                    if ((!String.IsNullOrWhiteSpace(ProxyLogin)) || (!String.IsNullOrWhiteSpace(ProxyPassword)))
                        request.Proxy.Credentials = new NetworkCredential(ProxyLogin, ProxyPassword);
                }
                if (IsUseProxy)
                {
                    WebProxy proxy = new WebProxy();
                    proxy.Address = new Uri(string.Format("http://{0}:{1}", ProxyIpAddress.Trim(), ProxyPort.Trim()));
                    request.Proxy = proxy;
                    if ((!String.IsNullOrWhiteSpace(ProxyLogin)) || (!String.IsNullOrWhiteSpace(ProxyPassword)))
                        request.Proxy.Credentials = new NetworkCredential(ProxyLogin, ProxyPassword);
                }
                if ((!IsUseIEProxy) && (!IsUseProxy))
                    request.Proxy = null;
                request.Timeout = 10000;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                MessageBox.Show(
                    "Соединение установлено.",
                    "Настройки подключения",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information
                    );
                response.GetResponseStream().Close();
                request.GetResponse().Close();
            }
            catch
            {
                MessageBox.Show(
                    "Не удалось подключиться к серверу. Пожалуйста проверьте настройки подключения",
                    "Настройки подключения",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information
                    );
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        public override string this[string columnName]
        {
            get
            {
                HasErrors = true;
                string propValue = TypeDescriptor.GetProperties(this).Find(columnName, true).GetValue(this) as string;
                Regex urlEx = new Regex(@"^([a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9\-]+)*\.[a-zA-Z]{2,6}(?:\/?|(?:\/[\w\-]+)*)(?:\/?|\/\w+\.[a-zA-Z]{2,4}(?:\?[\w]+\=[\w\-]+)?)?(?:\&[\w]+\=[\w\-]+)*)$|localhost|LOCALHOST");

                Regex ipEx = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$");

                switch (columnName)
                {
                    case "PluginsDirectory":
                    case "LogsStoragePath":
                        if (!Directory.Exists(propValue))
                            return "Выбранная папка не существует или отсутствуют права на чтение";
                        break;
                    case "AuthServicePort":
                    case "LicensingServicePort":
                    case "LogServicePort":
                    case "UpdateServicePort":
                    case "ProxyPort":
                        if (string.IsNullOrWhiteSpace(propValue))
                            break;
                        int port;
                        if (Int32.TryParse(propValue, out port) && (port < 1024 || port > 49151))
                            return "Порт должен быть числом в диапазоне 1024—49151";
                        break;
                    case "ProxyIpAddress":
                        if (!IsUseProxy || IsUseIEProxy)
                            break;
                        if (string.IsNullOrWhiteSpace(propValue))
                            return "Адрес прокси-сервера не должен быть пустым";
                        if (!urlEx.IsMatch(propValue) && !ipEx.IsMatch(propValue))
                            return "Проверьте введенный адрес";
                        break;
                    case "ServerIpAddress":
                        if (string.IsNullOrWhiteSpace(propValue))
                            return "Адрес сервера не должен быть пустым";
                        if (!urlEx.IsMatch(propValue) && !ipEx.IsMatch(propValue))
                            return "Проверьте введенный адрес";
                        break;
                    default:
                        break;
                }

                HasErrors = false;

                return string.Empty;
            }
        }
    }
}
