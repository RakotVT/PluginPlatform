﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using ums.Core.Settings;
using ums.PluginCore.Licensing;
using ums.Shell.Forms.Licensing;
using ums.Shell.LicensingService;
using ums.Shell.Objects;

namespace ums.Shell.Services
{
    internal class LicenseChecker
    {
        private string _licExtention = ".license";
        private Binding _binding = new BasicHttpBinding();
        EndpointAddress _address = new EndpointAddress("http://localhost:63273/LicensingService.svc");
        private ISettingsProvider _settings = new SimpleSettingsProvider();


        public void ActivatePlugin(PluginDescription plugin)
        {
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();

            FrmActivate frm = new FrmActivate();
            bool? result = frm.ShowDialog();
            if (result.HasValue && (bool)result)
            {
                using (LicensingServiceClient client = new LicensingServiceClient(_binding, _address))
                {
                    client.Open();

                    LicenseInfo info = client.GenerateLicense(frm.ActivationCode);
                    if (info.IsActivated)
                        using (FileStream file = new FileStream(plugin.AssemblyPath + _licExtention, FileMode.Create, FileAccess.Write))
                        {
                            BinaryFormatter formatter = new BinaryFormatter();
                            byte[] fileData;
                            using (MemoryStream stream = new MemoryStream())
                            {
                                formatter.Serialize(stream, info);
                                fileData = stream.ToArray();
                            }

                            byte[] sign = client.SignLicenseFile(fileData, info);

                            using (BinaryWriter writer = new BinaryWriter(file))
                            {
                                writer.Write(fileData);
                                writer.Write(sign);
                            }
                        }
                }
            }
        }

        public void ViewLicense(PluginDescription plugin)
        {
            LicenseInfo info;
            using (FileStream file = new FileStream(plugin.AssemblyPath + _licExtention, FileMode.Open, FileAccess.Read))
            {
                byte[] licenseData;
                byte[] licenseSign;

                ParseFile(plugin.AssemblyPath + _licExtention, out licenseData, out licenseSign);
                info = ConvertFromBytes(licenseData);
            }

            new FrmViewLicense().ShowDialog();
        }

        public bool CheckLicenseOnline(PluginDescription plugin)
        {
            if (!File.Exists(plugin.AssemblyPath + _licExtention))
            {
                ActivatePlugin(plugin);
                return true;
            }

            bool result = false;
            using (LicensingServiceClient client = new LicensingServiceClient(_binding, _address))
            {
                byte[] licenseData;
                byte[] licenseSign;

                ParseFile(plugin.AssemblyPath + _licExtention, out licenseData, out licenseSign);
                LicenseInfo info = ConvertFromBytes(licenseData);

                result = client.CheckLicence(info.Number, info.ActivationSing);
            }

            return result;
        }

        public bool CheckLicenseOffline(PluginDescription plugin)
        {
            System.Diagnostics.Debugger.Launch();
            System.Diagnostics.Debugger.Break();

            if (!File.Exists(plugin.AssemblyPath + _licExtention))
            {
                ActivatePlugin(plugin);
                return true;
            }

            bool result = false;

            LicenseInfo info;
            byte[] licenseHash;
            byte[] licenseData;
            byte[] licenseSign;

            ParseFile(plugin.AssemblyPath + _licExtention, out licenseData, out licenseSign);
            info = ConvertFromBytes(licenseData);

            using (SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider())
                licenseHash = sha1.ComputeHash(licenseData);


            using (DSACryptoServiceProvider DSA = new DSACryptoServiceProvider())
            {
                DSA.FromXmlString(Encoding.Unicode.GetString(info.SignInfo));
                DSASignatureDeformatter DSADeformatter = new DSASignatureDeformatter(DSA);
                DSADeformatter.SetHashAlgorithm("SHA1");
                result = DSADeformatter.VerifySignature(licenseHash, licenseSign);
            }

            return result;
        }

        public LicenseInfo GetLicenseInfo(PluginDescription plugin)
        {
            LicenseInfo info;
            byte[] licenseData;
            byte[] licenseSign;

            if (!File.Exists(plugin.AssemblyPath + _licExtention))
                return null;
            ParseFile(plugin.AssemblyPath + _licExtention, out licenseData, out licenseSign);
            info = ConvertFromBytes(licenseData);

            return info;
        }

        public void RemoveLicenseFile(PluginDescription plugin)
        {
            if (File.Exists(plugin.AssemblyPath + _licExtention))
                File.Delete(plugin.AssemblyPath + _licExtention);
        }

        private void ParseFile(string filePath, out byte[] licenseData, out byte[] licenseSign)
        {
            using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                licenseData = new byte[file.Length - 40];
                licenseSign = new byte[40];

                file.Read(licenseData, 0, licenseData.Length);
                file.Read(licenseSign, 0, licenseSign.Length);
            }
        }

        private LicenseInfo ConvertFromBytes(byte[] licenseData)
        {
            LicenseInfo info;

            using (MemoryStream licenseStream = new MemoryStream())
            {
                licenseStream.Write(licenseData, 0, licenseData.Length);
                licenseStream.Position = 0;
                BinaryFormatter formatter = new BinaryFormatter();
                info = (LicenseInfo)formatter.Deserialize(licenseStream);
            }

            return info;
        }

    }
}
