﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using ITWebNet.UMS.Core.DllLoading;
using ITWebNet.UMS.Core.LocalSettings;
using ITWebNet.UMS.ServicesApi.LogService;
using ums.PluginCore;
using ITWebNet.UMS.Shell.Objects;

namespace ITWebNet.UMS.Shell.Services
{
    // Не компилируем при сборке проекта, так как пока функционал класса не нужен
    class AssemblySearch
    {

        public AssemblySearch()
        {
            _settingsProvider = SimpleSettingsProvider.Instance;
        }

        ISettingsProvider _settingsProvider;

        public List<PluginDescription> GetPluginsDescription(int[] availablePlugins)
        {

            List<PluginDescription> result = new List<PluginDescription>();

            string direct = _settingsProvider.GetPluginDir();
            var plugins = _settingsProvider.GetExistingPlugins()
                .Where(item => availablePlugins.Contains(item.PluginId)).ToList();

            foreach (var plugin in plugins)
            {
                string pluginDir = Path.Combine(_settingsProvider.GetPluginDir(), plugin.Name);

                if (!Directory.Exists(pluginDir))
                    continue;
                AppDomainSetup set = new AppDomainSetup();
                set.PrivateBinPath = string.Format("{0};", pluginDir.Substring(pluginDir.LastIndexOf("\\") + 1));
                string config = Directory.GetFiles(pluginDir, "*.config", SearchOption.TopDirectoryOnly).FirstOrDefault();
                set.ConfigurationFile = config;
                Evidence evidence = AppDomain.CurrentDomain.Evidence;
                AppDomain domain = AppDomain.CreateDomain("tempDomain", evidence, set);

                try
                {
                    var loader = (Loader)domain.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().GetName().FullName, typeof(Loader).FullName);

                    //string[] pluginFiles = null;
                    //var ext = new string[] { ".dll", ".exe" };
                    //pluginFiles = (from fi in new DirectoryInfo(pluginDir).GetFiles()
                    //               where ext.Contains(fi.Extension.ToLower()) &&
                    //               !fi.Name.StartsWith("ums.PluginHost")
                    //               select fi.FullName)
                    //                         .ToArray();
                    string assemblyName = Path.Combine(pluginDir, plugin.MainFile);
                    //foreach (var assemblyPath in pluginFiles)
                    //{
                    try
                    {
                        PluginDescription plug = new PluginDescription();
                        loader.GetPluginDescription(assemblyName, ref plug);
                        if (plug != null && plug.Name != null)
                            result.Add(plug);
                    }
                    catch (Exception ex)
                    {
                        var logger = new Thread(() =>
                        {
                            Logger.Instance.WriteLog(new LoggerEntry()
                                {
                                    Exception = ex,
                                    MakeScreen = false,
                                    MakeDump = false,
                                    MessageLevel = MessageLevel.Info,
                                    MessageType = MessageType.Action,
                                    ShowMessageform = true
                                });
                        });

                        logger.SetApartmentState(ApartmentState.STA);
                        logger.IsBackground = true;
                        logger.Start();
                    }
                    //}

                    AppDomain.Unload(domain);
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(new LoggerEntry()
                    {
                        Exception = ex,
                        MakeScreen = false,
                        MakeDump = false,
                        MessageLevel = MessageLevel.Error,
                        MessageType = MessageType.Error,
                        ShowMessageform = true
                    });
                }
                finally
                {
                    Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
                }
            }
            return result;
        }

        public class Loader : MarshalByRefObject
        {
            public Loader()
            {
                AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            }

            Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
            {
                Assembly resolvedAssembly = null;
                Assembly objExecutingAssemblies;
                string strTempAssmbPath = "";

                objExecutingAssemblies = Assembly.GetCallingAssembly();//Assembly.GetExecutingAssembly();
                AssemblyName[] arrReferencedAssmbNames = objExecutingAssemblies.GetReferencedAssemblies();

                //Loop through the array of referenced assemblyInfo names.
                foreach (AssemblyName strAssmbName in arrReferencedAssmbNames)
                {
                    //Check for the assemblyInfo names that have raised the "AssemblyResolve" event.
                    if (strAssmbName.FullName.Substring(0, strAssmbName.FullName.IndexOf(",")) == args.Name.Substring(0, args.Name.IndexOf(",")))
                    {
                        //Build the startInfo of the assemblyInfo from where it has to be loaded. 
                        if (!string.IsNullOrWhiteSpace(strAssmbName.CodeBase))
                            resolvedAssembly = Assembly.LoadFrom(new Uri(strAssmbName.CodeBase).LocalPath);
                        break;
                    }
                }

                //LoadPlugin the assemblyInfo from the specified startInfo. 
                if (resolvedAssembly == null)
                {
                    strTempAssmbPath = Path.Combine(
                        AppDomain.CurrentDomain.BaseDirectory,
                        "CommonAssemblies",
                        args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll");
                    if (!File.Exists(strTempAssmbPath))
                        strTempAssmbPath = Path.Combine(
                            AppDomain.CurrentDomain.BaseDirectory,
                            args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll");

                    if (File.Exists(strTempAssmbPath))
                        resolvedAssembly = Assembly.LoadFrom(strTempAssmbPath);
                }

                //Return the loaded assemblyInfo.
                return resolvedAssembly;
            }

            public void GetPluginDescription(string assemblyName, ref PluginDescription info)
            {
                try
                {

                    Environment.CurrentDirectory = Path.GetDirectoryName(assemblyName);
                    FileInfo fi = new FileInfo(assemblyName);
                    var assembly = Assembly.LoadFrom(assemblyName);

                    var types = assembly.GetTypes();
                    foreach (Type type in types)
                    {
                        if (type.IsClass && type.GetInterface("IPlugin") != null)
                        {
                            //if (type.IsSubclassOf(typeof(System.Windows.UIElement)))
                            //{
                            //    foreach (Type plugin in types)
                            //    {
                            //        if (plugin.IsSubclassOf(typeof(System.Windows.Application)))
                            //        {
                            //            var app = Activator.CreateInstance(plugin);
                            //            MethodInfo main = plugin.GetMethod("InitializeComponent");
                            //            main.Invoke(app, null);
                            //            break;
                            //        }
                            //    }
                            //}

                            //info = new PluginDescription();
                            Object obj = Activator.CreateInstance(type);
                            IPlugin plugin = (IPlugin)obj;
                            info.Caption = plugin.PluginCaption;
                            info.Description = plugin.PluginDescription;
                            info.Name = plugin.PluginName;
                            info.AssemblyPath = assembly.Location;
                            info.Version = plugin.PluginVersion;
                            if (plugin.PluginIcon != null)
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    plugin.PluginIcon.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                                    PngBitmapDecoder decoder = new PngBitmapDecoder(ms, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                                    info.Icon = decoder.Frames[0];
                                }
                            }

                            DllHelper.WriteAssembliesToConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CommonAssemblies"), fi.FullName);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
