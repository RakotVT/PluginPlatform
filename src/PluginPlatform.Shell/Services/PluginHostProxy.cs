﻿using System;
using System.AddIn.Pipeline;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Security.Policy;
using System.Threading;
using System.Windows;
using PluginPlatform.Core.Hashing;
using PluginPlatform.Core.PluginLoader;
using PluginPlatform.PluginCore.API;
using PluginPlatform.Shell.Model;

namespace PluginPlatform.Shell.Services
{
    class PluginHostProxy : MarshalByRefObject
    {
        #region Variables

        static bool _isProcessUsing;
        private AppDomain _domain;
        private string _name;
        private IPluginLoader _pluginLoader;
        private Process _process;
        private EventWaitHandle _readyEvent;

        #endregion

        public int ProcessID { get { return _process.Id; } }

        #region Public Methods

        public static bool IsProcessUsingAvailable()
        {
            Assembly assembly = Assembly.Load(new AssemblyName("System.Runtime.Remoting, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"));
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            Version currVersion = new Version(versionInfo.FileMajorPart, versionInfo.FileMinorPart, versionInfo.FileBuildPart, versionInfo.FilePrivatePart);
            return _isProcessUsing = currVersion.CompareTo(new Version(4, 0, 30319, 17929)) >= 0;
            //return false;
        }

        [STAThread]
        public FrameworkElement GetPluginView(string assemblyName, string currentSessionId)
        {
            try
            {
                if (IsProcessUsingAvailable())
                {
                    Start(assemblyName);
                    OpenPluginLoader();
                }
                else
                    CreateDomainPluginHost(assemblyName);

                if (_pluginLoader == null)
                    return null;
                (_pluginLoader as IUserInfo).CurrentSessionID = currentSessionId;
                var contract = _pluginLoader.LoadPlugin(assemblyName);

                return FrameworkElementAdapters.ContractToViewAdapter(contract);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private string CopyPluginHost(string assemblyName)
        {
            try
            {
                HashProvider hashProvider = new HashProvider();
                FileInfo fi = new FileInfo(assemblyName);
                string pluginHostFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ums.PluginHost.exe");
                string fileName = Path.Combine(fi.DirectoryName, String.Format("ums.PluginHost.{0}", fi.Name));
                string configName = String.Format("{0}.config", assemblyName);
                string newConfigName = Path.Combine(fi.DirectoryName, String.Format("ums.PluginHost.{0}.config", fi.Name));
                if (File.Exists(configName))
                    File.Copy(configName, newConfigName, true);

                if (!File.Exists(fileName) || hashProvider.ComputeHash(fileName) != hashProvider.ComputeHash(pluginHostFile))
                    File.Copy(pluginHostFile, fileName, true);
                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CreateDomainPluginHost(string assemblyName)
        {
            string localAssemblyStorage = string.Format("\"{0}\"", Path.Combine(Environment.CurrentDirectory, "CommonAssemblies"));

            AppDomainSetup set = new AppDomainSetup();
            set.ConfigurationFile = String.Format("{0}.config", assemblyName);
            set.PrivateBinPath = "CommonAssemblies";

            Evidence evidence = AppDomain.CurrentDomain.Evidence;
            _domain = AppDomain.CreateDomain("ums.PluginHost." + Path.GetFileName(assemblyName), evidence, set);
            _pluginLoader = _domain.CreateInstanceFromAndUnwrap(
                "ums.PluginHost.exe",
                "ums.PluginHost.PluginLoader") as IPluginLoader;
            _domain.DomainUnload += Plugin_Crashes;
        }

        private void OpenPluginLoader()
        {
            try
            {
                if (_pluginLoader != null)
                    return;

                if (!_readyEvent.WaitOne(5000))
                {
                    throw new InvalidOperationException("Plugin host process not ready");
                }

                IpcChannelRegistration.RegisterChannel();

                var url = "ipc://" + _name + "/PluginLoader";
                _pluginLoader = (IPluginLoader)Activator.GetObject(typeof(IPluginLoader), url);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Start(string assemblyName)
        {
            try
            {
                if (_process != null)
                    return;
                _name = "ums.PluginHost." + Guid.NewGuid().ToString();
                var eventName = _name + ".Ready";
                _readyEvent = new EventWaitHandle(false, EventResetMode.ManualReset, eventName);

                string localAssemblyStorage = string.Format("\"{0}\"", Path.Combine(Environment.CurrentDirectory, "CommonAssemblies"));

                var processName = CopyPluginHost(assemblyName);

                _process = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        Arguments = string.Join(" ", _name, localAssemblyStorage),
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        FileName = processName,
                        WorkingDirectory = Path.GetDirectoryName(processName)
                    }
                };
                _process.EnableRaisingEvents = true;
                _process.Exited += Plugin_Crashes;
                _process.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region EventHandlers

        private void OnPluginDisposed(object sender, EventArgs args)
        {
            try
            {
                var plugin = sender as Plugin;
                if (plugin == null)
                    return;

                plugin.Disposed -= OnPluginDisposed;
                _pluginLoader.Terminate();
                if (_isProcessUsing)
                    _process.Exited -= Plugin_Crashes;
                else
                    _domain.DomainUnload -= Plugin_Crashes;
                _pluginLoader = null;
                _process = null;
                _domain = null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        void Plugin_Crashes(object sender, EventArgs e)
        {
            //if (_plugin != null)
            //    _plugin.Dispose();
        }

        #endregion

        public override object InitializeLifetimeService()
        {
            return null;
        }

        class IpcChannelRegistration
        {
            private static object _lock = new object();
            private static bool _registered;

            public static void RegisterChannel()
            {
                try
                {
                    lock (_lock)
                    {
                        if (_registered)
                            return;
                        var clientProvider = new BinaryClientFormatterSinkProvider();
                        var properties = new Hashtable();
                        properties["tokenImpersonationLevel"] = "impersonation";
                        properties["secure"] = "true";

                        var channel = new IpcChannel(properties, clientProvider, null);
                        ChannelServices.RegisterChannel(channel, true);
                        _registered = true;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}
