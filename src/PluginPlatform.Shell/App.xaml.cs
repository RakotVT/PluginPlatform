﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using PluginPlatform.Shell.Views;
using System.Linq;

namespace PluginPlatform.Shell
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //move $(TargetDir)*.dll $(TargetDir)CommonAssemblies\
        //public static Guid SessionKey { get; set; }
        public static string Launcher { get; set; }
        public static string CurrentVersion { get; set; }

        void AppStart(object sender, StartupEventArgs e)
        {
            bool isUpdated;
            string sessionKey;
            string launcher;
            string version;
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();

            if (e.Args.Length == 0)
            {
                isUpdated = false;
                sessionKey = Guid.Empty.ToString();
                launcher = string.Empty;
                version = string.Empty;
            }
            else
            {
                var startArgs = e.Args.ToDictionary(item => item.Split('=')[0], item => item.Split('=')[1]);
                string updated;
                startArgs.TryGetValue("updated", out updated); // ;
                startArgs.TryGetValue("sessionkey", out sessionKey);
                startArgs.TryGetValue("launcher", out launcher);
                startArgs.TryGetValue("_version", out version);
                isUpdated = Convert.ToBoolean(updated);
            }
            //if (!string.IsNullOrWhiteSpace(sessionKey))
            //    SessionKey = Guid.Parse(sessionKey);
            Launcher = launcher;
            CurrentVersion = version;
            new WndMain().Show();
        }
    }

    public class SingleInstanceApp
    {
        [DllImportAttribute("User32.dll", SetLastError = true)]
        private static extern int FindWindow(String ClassName, String WindowName);

        [DllImportAttribute("User32.dll", SetLastError = true)]
        private static extern IntPtr SetForegroundWindow(int hWnd);

        private static System.Threading.Mutex _mutex;

        [STAThread]
        [LoaderOptimization(LoaderOptimization.MultiDomainHost)]
        public static void Main()
        {
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();

            bool isRun;
            bool requestInitialOwnership = true;
            _mutex = new System.Threading.Mutex(requestInitialOwnership, "PluginSystemMutex", out isRun);
            if (!(requestInitialOwnership && isRun))
            {
                //поиск окна по заголовку
                //MessageBox.Show("Запуск нескольких копий приложения запрещен.", "Приложение уже запущено.", MessageBoxButton.OK, MessageBoxImage.Information);

                //int hWnd = FindWindow(null, "Авторизация");
                //if (hWnd > 0)
                //    SetForegroundWindow(hWnd);
                //else
                //{
                    int hWnd = FindWindow(null, "Universal Management System");
                    if (hWnd > 0)
                        SetForegroundWindow(hWnd);
                //}
                System.Environment.Exit(0);
            }
            else
            {
                App.Main();
            };
            //_mutex.ReleaseMutex();
        }
    }
}
