﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Windows.Media.Imaging;

namespace PluginPlatform.Shell.Model
{
    [Serializable]
    public class PluginDescription
    {
        #region Private Fields

        [NonSerialized]
        private BitmapSource _icon = null;

        private byte[] _imageBytes = null;

        #endregion Private Fields

        #region Public Constructors

        public PluginDescription()
        {
        }

        #endregion Public Constructors

        #region Public Properties

        public string AssemblyPath { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public BitmapSource Icon
        {
            get
            {
                return _icon;
            }

            set
            {
                _icon = value;
            }
        }

        public string Name { get; set; }

        public string Version { get; set; }

        public bool Autorun { get; set; }

        #endregion Public Properties

        #region Private Methods

        [OnDeserialized]
        private void LoadImage(StreamingContext sc)
        {
            if (_imageBytes != null)
            {
                using (MemoryStream stream = new MemoryStream(_imageBytes))
                {
                    PngBitmapDecoder decoder = new PngBitmapDecoder(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                    _icon = decoder.Frames[0];
                }
            }
        }

        [OnSerializing]
        private void StreamImage(StreamingContext sc)
        {
            if (_icon != null)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(_icon));
                    encoder.Save(stream);
                    _imageBytes = stream.ToArray();
                }
            }
        }

        #endregion Private Methods
    }
}