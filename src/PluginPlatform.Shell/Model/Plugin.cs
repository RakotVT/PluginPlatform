﻿using System;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PluginPlatform.Core.ViewModels;

namespace PluginPlatform.Shell.Model
{
    public class Plugin : ViewModelBase, IDisposable
    {
        #region Private Fields

        private string _header;

        private BitmapSource _icon;

        private string _name;

        private Guid _uid;

        private FrameworkElement _view;

        #endregion Private Fields

        #region Public Constructors

        public Plugin(FrameworkElement view)
        {
            View = view;
            UID = Guid.NewGuid();
        }

        #endregion Public Constructors

        #region Public Properties

        public string Header
        {
            get
            {
                return _header;
            }

            set
            {
                if (_header != value)
                {
                    _header = value;
                    RaisePropertyChanged("Header");
                }
            }
        }

        public BitmapSource Icon
        {
            get
            {
                return _icon;
            }

            set
            {
                if (_icon != value)
                {
                    _icon = value;
                    RaisePropertyChanged("Icon");
                }
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        public Guid UID
        {
            get
            {
                return _uid;
            }

            set
            {
                if (_uid != value)
                {
                    _uid = value;
                    RaisePropertyChanged("UID");
                }
            }
        }

        public FrameworkElement View
        {
            get
            {
                return _view;
            }

            set
            {
                if (_view != value)
                {
                    _view = value;
                    RaisePropertyChanged("View");
                }
            }
        }

        #endregion Public Properties

        #region Public Methods

        public void Dispose()
        {
            if (Disposed != null)
                Disposed(this, EventArgs.Empty);
        }

        #endregion Public Methods

        #region Public Events

        public event EventHandler Disposed;

        #endregion Public Events
    }
}