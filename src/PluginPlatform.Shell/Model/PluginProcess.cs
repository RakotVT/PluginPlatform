﻿using System;

namespace PluginPlatform.Shell.Model
{
    internal class PluginProcess
    {
        #region Private Fields

        private bool _isResponding = true;
        private bool _isTracking = true;
        private Plugin _plugin;
        private PluginDescription _pluginDesc;
        private int _processID;

        #endregion Private Fields

        #region Public Constructors

        public PluginProcess(PluginDescription pluginDesc, Plugin plugin, int processID)
        {
            if (pluginDesc == null)
                throw new ArgumentNullException("pluginDesc");
            if (processID == 0)
                throw new ArgumentException("Value must be a positive number", "processID");
            if (plugin == null)
                throw new ArgumentNullException("exisedPlugin");
            _plugin = plugin;
            _pluginDesc = pluginDesc;
            _processID = processID;
        }

        #endregion Public Constructors

        #region Public Properties

        public bool IsResponding
        {
            get { return _isResponding; }
            set { _isResponding = value; }
        }

        public bool IsTracking
        {
            get { return _isTracking; }
            set { _isTracking = value; }
        }

        public PluginDescription PluginDesc
        {
            get { return _pluginDesc; }
            set { _pluginDesc = value; }
        }

        public IntPtr PluginViewHandle
        {
            get { return (_plugin.View as System.Windows.Interop.HwndHost).Handle; }
        }

        public int ProcessID
        {
            get { return _processID; }
            set { _processID = value; }
        }

        public Guid UID
        {
            get { return _plugin.UID; }
        }

        #endregion Public Properties
    }
}