﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using PluginPlatform.ServicesCore.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Data.Entity.Core.Objects;
using RefactorThis.GraphDiff;

namespace PluginPlatform.PluginService.Controllers
{
    public class ManageController : ApiController
    {
        public UmsDbContext Context { get { return Request.GetOwinContext().Get<UmsDbContext>(); } }

        // api/Manage/Delete/id 
        [HttpPost]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var plugin = await Context.Plugins.FindAsync(id);
            if (plugin == null)
                return NotFound();

            Context.Plugins.Remove(plugin);

            await Context.SaveChangesAsync();

            return Ok();
        }

        // api/Manage/GetAll 
        public IHttpActionResult GetAll()
        {
            var result = Context.Plugins
                .Include("Versions.PluginFiles").ToList();

            return Ok(result);
        }

        // api/Manage/UploadFiles?versionId=id
        [HttpPost]
        public async Task<IHttpActionResult> UploadFiles(int versionId, List<PluginFile> files)
        {
            if (files == null)
                return BadRequest();

            var version = await Context.Versions
                .Include("PluginFiles.Content")
                .FirstOrDefaultAsync(v => v.Id == versionId);

            if (version == null)
                return NotFound();

            if (version.PluginFiles.Count == 0)
                version.PluginFiles = files;
            else
            {
                var existedFiles = version.PluginFiles.ToDictionary(item => item.Name);

                foreach (var item in files)
                {
                    PluginFile existed;
                    if (existedFiles.TryGetValue(item.Name, out existed))
                    {
                        if (!existed.Hash.Equals(item.Hash))
                        {
                            existed.Hash = item.Hash;
                            existed.Content.Data = item.Content.Data;
                        }
                    }
                    else
                    {
                        version.PluginFiles.Add(item);
                    }
                }
            }
            await Context.SaveChangesAsync();

            var result = await Context.Versions
                .Include("PluginFiles")
                .FirstOrDefaultAsync(v => v.Id == versionId);

            return Ok(result);
        }

        // api/Manage/DeleteFiles?versionId=id
        [HttpPost]
        public async Task<IHttpActionResult> DeleteFiles(int versionId, List<int> filesId)
        {
            var version = await Context.Versions
                .Include("PluginFiles.Content")
                .FirstOrDefaultAsync(v => v.Id == versionId);

            if (version == null)
                return NotFound();

            if (version.PluginFiles.Count > 0)
            {
                foreach (var item in filesId)
                {
                    var file = await Context.PluginFiles.FindAsync(item);
                    Context.PluginFiles.Remove(file);
                    //var file = version.PluginFiles.FirstOrDefault(f => f.Id == item);
                    //version.PluginFiles.Remove(file);
                }
                await Context.SaveChangesAsync();

            }

            var result = await Context.Versions
                .Include("PluginFiles")
                .FirstOrDefaultAsync(v => v.Id == versionId);

            return Ok(result);
        }

        // api/Manage/SetMainFile?pluginFileId=id
        [HttpPost]
        public async Task<IHttpActionResult> SetMainFile(int pluginFileId)
        {
            var file = await Context.PluginFiles.FindAsync(pluginFileId);
            if (file == null)
                return NotFound();

            var version = await Context.Versions
                .Include("PluginFiles")
                .FirstOrDefaultAsync(v => v.Id == file.VersionId);
            foreach (var item in version.PluginFiles)
                item.IsMainFile = false;
            file.IsMainFile = true;
            await Context.SaveChangesAsync();

            return Ok(version);
        }

        // api/Manage/PublishVersion?versionId=id
        [HttpPost]
        public async Task<IHttpActionResult> PublishVersion(int versionId)
        {
            var version = await Context.Versions.FindAsync(versionId);
            if (version == null)
                return NotFound();

            version.IsPublished = true;
            await Context.SaveChangesAsync();

            var result = await Context.Versions
                .Include("PluginFiles")
                .FirstOrDefaultAsync(v => v.Id == versionId);

            return Ok(result);
        }

        // api/Manage/Create 
        public async Task<IHttpActionResult> Create(Plugin model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Context.Plugins.Add(model);
            try
            {
                await Context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (Context.Plugins.Any(item => item.Id == model.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            return Ok(model);
        }

        // api/Manage/Update/id 
        public async Task<IHttpActionResult> Update(int id, Plugin model)
        {
            if (!Context.Plugins.Any(item => item.Id == id))
                return NotFound();

            Context.UpdateGraph(model, map => map
                .OwnedCollection(p => p.Versions));

            //if (id != model.Id)
            //    return BadRequest();

            //Plugin origin = await Context.Plugins.FindAsync(id);
            //Context.Entry(origin).CurrentValues.SetValues(model);
            //Context.Entry(origin).Collection(r => r.Versions).Load();

            //var originPlugins = origin.Versions.Select(p => p.Id);
            //var modelPlugins = model.Versions.Select(p => p.Id);

            //if (model.Versions != null)
            //{
            //    foreach (var item in model.Versions)
            //        if (!originPlugins.Contains(item.Id))
            //            origin.Versions.Add(item);

            //    foreach (var item in origin.Versions)
            //        if (!modelPlugins.Contains(item.Id))
            //            origin.Versions.Remove(item);
            //}

            try
            {
                await Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (Context.Plugins.Any(item => item.Id == model.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            var result = await Context.Plugins
                .Include("Versions.PluginFiles")
                .FirstOrDefaultAsync(p => p.Id == id);

            return Ok(result);
        }

        private void UpdateRelated<T>(IEnumerable<T> collection)
            where T : class
        {
            foreach (var item in collection)
            {


                var entry = Context.Entry(item);
                if (entry.State == EntityState.Detached)
                    Context.Set<T>().Attach(item);
                else
                    entry.State = EntityState.Modified;
            }
        }


        private string ComputeHash(byte[] fileData)
        {
            try
            {
                SHA512 shaM = new SHA512Managed();
                byte[] result = shaM.ComputeHash(fileData);
                string hex = BitConverter.ToString(result).Replace("-", string.Empty);
                return hex;
            }
            catch (Exception e) { throw e; }
        }
    }
}
