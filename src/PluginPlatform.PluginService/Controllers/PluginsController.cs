﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using PluginPlatform.Common.DataModels.Exceptions;
using PluginPlatform.Common.DataModels.PluginService;
using PluginPlatform.PluginService.Storage;

namespace PluginPlatform.PluginService.Controllers
{
    //[Authorize]
    public class PluginsController : ApiController, IPluginsController
    {
        public DataBaseStorage Storage { get { return Request.GetOwinContext().Get<DataBaseStorage>(); } }

        [HttpGet]
        public PluginInfo GetPluginInfo(int pluginId = 0, string pluginName = null)
        {
            return Storage.GetPluginInfo(pluginId, pluginName);
        }

        [HttpGet]
        public List<UpdateEntry> GetPluginUpdates(int pluginId, string currentVersion)
        {
            try
            {
                List<UpdateEntry> entries = null;
                System.Version version;
                if (System.Version.TryParse(currentVersion, out version))
                    entries = Storage.GetPluginUpdates(pluginId, version);
                if (entries == null || entries.Count == 0)
                    throw new ServiceException("Сбой при определении доступных обновлений");
                return entries;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        public List<UpdateEntry> GetPluginLatestUpdates(int pluginId)
        {
            try
            {
                List<UpdateEntry> entries = Storage.GetPluginLatestUpdates(pluginId);
                if (entries.Count == 0)
                    throw new ServiceException("Сбой при определении доступных обновлений");
                return entries;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        public bool IsUpdatesAvailable(int pluginId, string currentVersion)
        {
            System.Version version;
            if (System.Version.TryParse(currentVersion, out version))
                return Storage.GetPluginLatestVersion(pluginId) > version;
            else
                return false;
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Storage.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
