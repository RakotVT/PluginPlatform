﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using PluginPlatform.Common.DataModels.Exceptions;
using PluginPlatform.Common.DataModels.PluginService;
using PluginPlatform.PluginService.Storage;

namespace PluginPlatform.PluginService.Controllers
{
    public class ShellController : ApiController, IShellController
    {
        public DataBaseStorage Storage { get { return Request.GetOwinContext().Get<DataBaseStorage>(); } }

        [HttpGet]
        public PluginInfo GetShellInfo()
        {
            return Storage.GetPluginInfo(-1, "UMS");
        }

        [HttpGet]
        public UpdateEntry GetInstaller(string currentVersion = null)
        {
            try
            {
                Version version;
                int shellId = Storage.GetPLuginIdByName("UMS");
                List<UpdateEntry> entries = null;

                if (Version.TryParse(currentVersion, out version))
                    entries = Storage.GetPluginSpecificUpdates(shellId, version);
                else
                    entries = Storage.GetPluginLatestUpdates(shellId);

                if (entries == null || entries.Count != 1)
                    throw new ServiceException("Сбой при определении доступных обновлений");
                return entries.First();
            }
            catch (Exception)
            {
                throw;
            }
        }

        //[HttpGet]
        //public UpdateEntry GetShellLatestUpdates()
        //{
        //    try
        //    {
        //        int shellId = storage.GetPluginInfo(-1, "UMS").Id;

        //        List<UpdateEntry> entries = storage.GetPluginLatestUpdates(shellId);
        //        if (entries.Count != 1)
        //            throw new ServiceException("Сбой при определении доступных обновлений");
        //        return entries.First();
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}

        //[HttpGet]
        //public UpdateEntry GetShellUpdates(string currentVersion)
        //{
        //    try
        //    {
        //        int shellId = storage.GetPluginInfo(-1, "UMS").Id;

        //        List<UpdateEntry> entries = null;
        //        Version version;
        //        if (Version.TryParse(currentVersion, out version))
        //            entries = storage.GetPluginUpdates(shellId, version);
        //        if (entries == null || entries.Count != 1)
        //            throw new ServiceException("Сбой при определении доступных обновлений");
        //        return entries.First();
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}

        [HttpGet]
        public bool IsShellUpdatesAvaliable(string currentVersion)
        {
            Version version;
            if (Version.TryParse(currentVersion, out version))
            {
                int shellId = Storage.GetPLuginIdByName("UMS");
                return Storage.GetPluginLatestVersion(shellId) > version;
            }
            else
                return false;

        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Storage.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
