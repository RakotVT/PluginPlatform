﻿using System;
using System.Collections.Generic;
using System.Linq;
using PluginPlatform.Common.DataModels.PluginService;
using PluginPlatform.ServicesCore.Models;

namespace PluginPlatform.PluginService.Storage
{
    public class DataBaseStorage : IDisposable
    {
        private UmsDbContext context = new UmsDbContext();

        public DataBaseStorage()
        {
            context.Configuration.LazyLoadingEnabled = true;
            context.Configuration.ProxyCreationEnabled = true;
        }

        private int GetPluginLatestVersionId(int pluginId)
        {
            try
            {
                var version = context.Plugins.Find(pluginId).Versions
                    .OrderByDescending(v => v.Major)
                    .ThenByDescending(v => v.Minor)
                    .ThenByDescending(v => v.Build)
                    .FirstOrDefault();

                //var major = context.Plugins.FirstOrDefault(item => item.Id == pluginId)
                //    .MajorVersions.OrderByDescending(item => item.Number).FirstOrDefault();
                //var minor = major.MinorVersions.OrderByDescending(item => item.Number).FirstOrDefault();
                //var build = minor.BuildVersions.Where(item => item.IsPublished).OrderByDescending(item => item.Number).FirstOrDefault();

                return version.Id;

            }
            catch (Exception)
            {
                throw;
            }
        }

        internal static DataBaseStorage Create()
        {
            return new DataBaseStorage();
        }

        public int GetPLuginIdByName(string pluginName)
        {
            int pluginId = context.Plugins.Where(x => x.Name == pluginName).Select(y => y.Id).FirstOrDefault();
            return pluginId;
        }

        public List<UpdateEntry> GetPluginLatestUpdates(int pluginId)
        {
            try
            {
                string pluginName = context.Plugins.Where(item => item.Id == pluginId).Select(item => item.Name).FirstOrDefault();

                string latestVersion = GetPluginLatestVersion(pluginId).ToString();

                int latestVersionId = GetPluginLatestVersionId(pluginId);

                var updates = context.PluginFiles.Where(item => item.VersionId == latestVersionId);

                List<UpdateEntry> listUpdateFiles = new List<UpdateEntry>();
                foreach (var item in updates)
                {
                    listUpdateFiles.Add(new UpdateEntry()
                    {
                        EntryAction = UpdateAction.Add,
                        EntryBinaryData = item.Content.Data,
                        EntryHash = item.Hash,
                        EntryName = item.Name,
                        EntryPath = item.RelativePath,
                        EntryPluginName = pluginName,
                        EntryVersion = latestVersion,
                    });
                }

                return listUpdateFiles;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<UpdateEntry> GetPluginSpecificUpdates(int pluginId, System.Version currentVersion)
        {
            try
            {
                string pluginName = context.Plugins.Where(item => item.Id == pluginId).Select(item => item.Name).FirstOrDefault();

                var currentBuild = context.Versions.FirstOrDefault(
                    item =>
                        item.Major == currentVersion.Major &&
                        item.Minor == currentVersion.Minor &&
                        item.Build == currentVersion.Build);

                var updates = context.PluginFiles.Where(item => item.VersionId == currentBuild.Id);

                List<UpdateEntry> listUpdateFiles = new List<UpdateEntry>();
                foreach (var item in updates)
                {
                    listUpdateFiles.Add(new UpdateEntry()
                    {
                        EntryAction = UpdateAction.Add,
                        EntryBinaryData = item.Content.Data,
                        EntryHash = item.Hash,
                        EntryName = item.Name,
                        EntryPath = item.RelativePath,
                        EntryPluginName = pluginName,
                        EntryVersion = currentVersion.ToString(),
                    });
                }

                return listUpdateFiles;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public System.Version GetPluginLatestVersion(int pluginId)
        {
            try
            {
                int latestId = GetPluginLatestVersionId(pluginId);
                var build = context.Versions.FirstOrDefault(item => item.Id == latestId);

                System.Version latest = new System.Version(build.Major, build.Minor, build.Build);
                return latest;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string GetPluginMainFile(int pluginId)
        {
            int latestId = GetPluginLatestVersionId(pluginId);
            var build = context.Versions.FirstOrDefault(item => item.Id == latestId);

            PluginFile file = build.PluginFiles.FirstOrDefault(item => item.IsMainFile);
            if (file != null)
                return file.Name;
            return null;
        }

        public string GetPluginName(int pluginId)
        {
            var pluginName = context.Plugins.FirstOrDefault(item => item.Id == pluginId).Name;
            return pluginName;
        }

        public string[] GetPluginsNames(int[] pluginsId)
        {
            var pluginsName = context.Plugins.Where(item => pluginsId.Contains(item.Id)).Select(item => item.Name).ToArray();
            return pluginsName;
        }

        public List<UpdateEntry> GetPluginUpdates(int pluginId, System.Version currentVersion)
        {
            string pluginName = context.Plugins.Where(item => item.Id == pluginId).Select(item => item.Name).FirstOrDefault();

            if (currentVersion.Build < 0)
                currentVersion = new System.Version(currentVersion.Major, currentVersion.Minor, 0);

            var currentBuild = context.Versions.FirstOrDefault(
                item =>
                    item.Major == currentVersion.Major &&
                    item.Minor == currentVersion.Minor &&
                    item.Build == currentVersion.Build);

            System.Version latestVersion = GetPluginLatestVersion(pluginId);

            int latestId = GetPluginLatestVersionId(pluginId);
            var latestBuild = context.Versions.FirstOrDefault(item => item.Id == latestId);

            var currentVersionFiles = currentBuild.PluginFiles.ToArray();

            var latestVersionFiles = latestBuild.PluginFiles.ToArray();

            List<UpdateEntry> updates = new List<UpdateEntry>();

            foreach (var item in latestVersionFiles)
            {
                HashSet<string> toAdd = new HashSet<string>(currentVersionFiles.Select(i => i.Name.ToLowerInvariant()));

                if (toAdd.Add(item.Name.ToLowerInvariant()))
                {
                    updates.Add(new UpdateEntry()
                    {
                        EntryAction = UpdateAction.Add,
                        EntryBinaryData = item.Content.Data,
                        EntryHash = item.Hash,
                        EntryName = item.Name,
                        EntryPath = item.RelativePath,
                        EntryPluginName = pluginName,
                        EntryVersion = latestVersion.ToString()
                    });
                }
                else if (!item.Hash.Equals(
                    currentVersionFiles.FirstOrDefault(i => i.Name.ToLowerInvariant() == item.Name.ToLowerInvariant()).Hash,
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    updates.Add(new UpdateEntry()
                    {
                        EntryAction = UpdateAction.Overwrite,
                        EntryBinaryData = item.Content.Data,
                        EntryHash = item.Hash,
                        EntryName = item.Name,
                        EntryPath = item.RelativePath,
                        EntryPluginName = pluginName,
                        EntryVersion = latestVersion.ToString()
                    });
                }
            }

            foreach (var item in currentVersionFiles)
            {
                HashSet<string> toDelete = new HashSet<string>(latestVersionFiles.Select(i => i.Name.ToLowerInvariant()));
                if (toDelete.Add(item.Name.ToLowerInvariant()))
                    updates.Add(new UpdateEntry()
                    {
                        EntryAction = UpdateAction.Remove,

                        //EntryBinaryData = item.content,
                        EntryHash = item.Hash,
                        EntryName = item.Name,
                        EntryPath = item.RelativePath,
                        EntryPluginName = pluginName,
                        EntryVersion = latestVersion.ToString()
                    });
            }

            return updates;
        }

        public PluginInfo GetPluginInfo(int pluginId, string pluginName)
        {
            try
            {
                if (pluginId < 0)
                    pluginId = GetPLuginIdByName(pluginName);
                else if (string.IsNullOrWhiteSpace(pluginName))
                    pluginName = GetPluginName(pluginId);


                Plugin plugin = context.Plugins.Where(item => item.Id == pluginId).FirstOrDefault();
                var allVersions = plugin.Versions.Select(v => $"{v.Major}.{v.Minor}.{v.Build}").ToList();


                PluginInfo info = new PluginInfo();
                info.Id = pluginId;
                info.Name = pluginName;
                info.LatestVersion = GetPluginLatestVersion(pluginId).ToString();
                info.AllVersions = allVersions;
                info.MainFileName = GetPluginMainFile(pluginId);
                if (plugin == null)
                    return info;
                info.Caption = plugin.Caption;
                info.Description = plugin.Description;

                return info;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
        }

        ~DataBaseStorage()
        {
            Dispose(false);
        }
    }
}