﻿namespace PluginPlatform.PluginService.Storage
{
    interface IUpdateServiceStorage
    {
        /// <summary>
        /// Получает файл обновления плагина. 
        /// </summary>
        /// <param name="pluginId"> Идентификатор плагина. </param>
        /// <param name="pluginVersion"> номер версии плагина. </param>
        /// <param name="fileName"> Имя файла. </param>
        /// <returns> Файл обновления плагина в формате Stream. </returns>
        byte[] GetPluginEntry(int pluginId, System.Version pluginVersion, string fileName);

        /// <summary>
        /// Возвращает идентификатор плагина. 
        /// </summary>
        /// <param name="pluginId"> Имя плагина для поиска. </param>
        /// <returns> Идентификатор плагина. </returns>
        int GetPLuginIdByName(string pluginName);

        /// <summary>
        /// Возвращает список файлов плагина. 
        /// </summary>
        /// <param name="pluginId"> Идентификатор плагина. </param>
        /// <returns> Список файлов обновления плагина. </returns>
        System.Collections.Generic.List<Common.DataModels.PluginService.UpdateEntry> GetPluginLatestUpdates(int pluginId);

        /// <summary>
        /// Возвращает список файлов плагина. 
        /// </summary>
        /// <param name="pluginId"> Идентификатор плагина. </param>
        /// <param name="currentVersion"> Номер версии плагина. </param>
        /// <returns> Список файлов плагина для заданной версии. </returns>
        System.Collections.Generic.List<Common.DataModels.PluginService.UpdateEntry> GetPluginSpecificUpdates(int pluginId, System.Version currentVersion);

        /// <summary>
        /// Возвращает последнюю версию плагина. 
        /// </summary>
        /// <param name="pluginId"> Идентификатор плагина. </param>
        /// <returns> Номера версии плагина. </returns>
        System.Version GetPluginLatestVersion(int pluginId);

        /// <summary>
        /// Возвращает имя главного файла последней версии плагина. 
        /// </summary>
        /// <param name="pluginId"> Идентификатор плагина. </param>
        /// <returns> Имя главного файла. </returns>
        string GetPluginMainFile(int pluginId);

        /// <summary>
        /// Возвращает имя плагина по идетификатору. 
        /// </summary>
        /// <param name="pluginId"> Идентификатор плагина. </param>
        /// <returns> Имя плагина. </returns>
        string GetPluginName(int pluginId);

        /// <summary>
        /// Возвращает список имен плагинов по их идентификаторам. 
        /// </summary>
        /// <param name="pluginsId"> Массив идентификаторов плагинов. </param>
        /// <returns> Список имен плагинов. </returns>
        string[] GetPluginsNames(int[] pluginsId);

        /// <summary>
        /// Возвращает список обновлений обновления плагина. 
        /// </summary>
        /// <param name="pluginId"> Идентификатор плагина. </param>
        /// <param name="currentVersion"> Номер версии плагина. </param>
        /// <returns> Список обновлений плагина для заданной версии. </returns>
        System.Collections.Generic.List<Common.DataModels.PluginService.UpdateEntry> GetPluginUpdates(int pluginId, System.Version currentVersion);
    }
}
