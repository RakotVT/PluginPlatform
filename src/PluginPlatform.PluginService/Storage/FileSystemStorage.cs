﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ITWebNet.UMS.PluginService.Helpers;

namespace ITWebNet.UMS.PluginService.Storage
{
    public class FileSystemStorage
    {
        #region Private Fields

        private string DBPath = AppDomain.CurrentDomain.BaseDirectory + "bin\\Test\\UpdateDB.xml";
        private string PluginsPath = AppDomain.CurrentDomain.BaseDirectory + "bin\\Test\\pluginsId\\";
        private string ShellPath = AppDomain.CurrentDomain.BaseDirectory + "bin\\Test\\shell\\";

        #endregion Private Fields

        #region IUpdateServiceStorage Members

        public List<UpdateFile> GetPluginEntries(string pluginName, int pluginVersion)
        {
            List<string> names;

            XDocument storage = XDocument.Load(DBPath);
            var pluginPacket = storage
                .Descendants("pluginsId")
                .Descendants("plugin")
                .Where(p => p.Attribute("name").Value == pluginName)
                .Descendants("packet")
                .Where(p => p.Attribute("version").Value == pluginVersion.ToString())
                .FirstOrDefault();

            names = (from p in pluginPacket.Descendants("entry")
                     select p.Attribute("name").Value).ToList();

            return new List<UpdateFile>();
        }

        public Stream GetPluginEntry(string pluginName, int pluginVersion, string fileName)
        {
            StringBuilder entryPath = new StringBuilder();
            string[] str = { "df", "sdf" };
            String.Join("\\", str);
            //EntryPath.Append(PluginsPath).Append(fileName).AppendFormat("_{0}\\", currentVersion).Append(fileName);

            return GetEntry(entryPath.ToString());
        }

        public int GetPLuginIdByName(string pluginName)
        {
            throw new NotImplementedException();
        }

        public int GetPluginLatestVersion(string pluginName)
        {
            XDocument storage = XDocument.Load(DBPath);
            var packets = (from p in storage.Descendants("pluginsId").Descendants("plugin")
                           where p.Attribute("name").Value == pluginName
                           select p.Descendants("packet")).FirstOrDefault();

            List<int> versions = new List<int>();

            foreach (var packet in packets)
            {
                versions.Add(int.Parse(packet.Attribute("version").Value));
            }

            return versions.Max();
        }

        public string GetPluginMainFile(string pluginName)
        {
            throw new NotImplementedException();
        }

        public string GetPluginName(int pluginId)
        {
            throw new NotImplementedException();
        }

        public string[] GetPluginsNames(int[] pluginsId)
        {
            List<string> pluginNames = new List<string>();

            XDocument storage = XDocument.Load(DBPath);
            var plugins = storage.Descendants("plugin");
            foreach (var plugin in plugins)
            {
                pluginNames.Add(plugin.Attribute("name").Value);
            }

            return pluginNames.ToArray();
        }

        public List<string> GetShellEntriesNames(int shellVersion)
        {
            List<string> names;

            XDocument storage = XDocument.Load(DBPath);
            var shellPacket = (from p in storage.Descendants("shell").Descendants("packet")
                               where p.Attribute("version").Value == shellVersion.ToString()
                               select p).FirstOrDefault();

            names = (from e in shellPacket.Descendants("entry") select e.Attribute("name").Value).ToList();

            return names;
        }

        public Stream GetShellEntry(string entryName, int shellVersion)
        {
            StringBuilder entryPath = new StringBuilder();

            entryPath.Append(ShellPath).AppendFormat("{0}\\", shellVersion).Append(entryName);

            return GetEntry(entryPath.ToString());
        }

        public int GetShellLatestVersion()
        {
            List<int> versions = new List<int>();

            XDocument storage = XDocument.Load(DBPath);
            var shell = storage.Descendants("shell");
            var shellPackets = shell.Descendants("packet");
            foreach (var packet in shellPackets)
            {
                versions.Add(int.Parse(packet.Attribute("version").Value));
            }

            return versions.Max();
        }

        #endregion IUpdateServiceStorage Members

        #region Private Methods

        private Stream GetEntry(string path)
        {
            try
            {
                Stream stream = File.OpenRead(path.ToString());
                return stream;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion Private Methods

        //public Dictionary<string, string> GetAvailablePlugins(string clientLogin)
        //{
        //    XDocument storage = XDocument.Load(DBPath);
        //    Dictionary<string, string> entries = new Dictionary<string, string>();
        //    foreach (var p in storage.Descendants("pluginsId").Descendants("plugin"))
        //    {
        //        entries.Add(p.Attribute("name").Value, GetPluginLatestVersion(p.Attribute("name").Value).ToString());
        //    }
        //    return entries;
        //}
    }
}