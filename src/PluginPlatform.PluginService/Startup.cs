﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PluginPlatform.PluginService.Storage;
using PluginPlatform.ServicesCore.Models;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Extensions;
using Owin;

[assembly: OwinStartup(typeof(PluginPlatform.PluginService.Startup))]
namespace PluginPlatform.PluginService
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.CreatePerOwinContext(UmsDbContext.Create);
            app.CreatePerOwinContext(DataBaseStorage.Create);

            app.UseStageMarker(PipelineStage.Authenticate);
            app.UseOAuthBearerAuthentication(new Microsoft.Owin.Security.OAuth.OAuthBearerAuthenticationOptions());

            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(config);
        }
    }
}
